\documentclass[a4paper, 12pt]{article}
\usepackage[slovene]{babel}
\usepackage{lmodern}
\usepackage[T1]{fontenc}
\usepackage[utf8]{inputenc}
\usepackage{url}

\topmargin=0cm
\topskip=0cm
\textheight=25cm
\headheight=0cm
\headsep=0cm
\oddsidemargin=0cm
\evensidemargin=0cm
\textwidth=16cm
\parindent=0cm
\parskip=12pt

\renewcommand{\baselinestretch}{1.2}

\begin{document}

%%%%%%%%%%%%%%%%%%%%%%%%%% Filled out by the candidate! %%%%%%%%%%%%%%%%%%%%%%%%%%
\newcommand{\ImeKandidata}{Toni} % Name
\newcommand{\PriimekKandidata}{K. Turk} % Surname
\newcommand{\VpisnaStevilka}{63140110 } % Enrollment number
\newcommand{\StudyProgramme}{Computer and information science, MAG} % Study programme
\newcommand{\NaslovBivalisca}{ Potok 12, 8351 Straža, Slovenia } % the candidate address
\newcommand{\SLONaslov}{Len funkcijski programski jezik brez čistilca pomnilnika} % Slovenian title
\newcommand{\ENGNaslov}{Lazy functional programming language without garbage collector} % English title
%%%%%%%%%%%%%%%%%%%%%%%%%% End of filling in  %%%%%%%%%%%%%%%%%%%%%%%%%%


\newcommand{\Kandidat}{\ImeKandidata~\PriimekKandidata}
\noindent
\Kandidat\\
\NaslovBivalisca \\
Study programme: \StudyProgramme \\
Enrollment number: \VpisnaStevilka
\bigskip

{\bf Committee for Student Affairs}\\
Univerza v Ljubljani, Fakulteta za računalništvo in informatiko\\
Večna pot 113, 1000 Ljubljana\\

{\Large\bf
{\centering
    The master’s thesis topic proposal \\%[2mm]
\large Kandidat: \Kandidat \\[10mm]}}


I, \Kandidat, a student of the 2nd cycle study programme at the University of Ljubljana, Faculty of Computer and Information Science, am submitting a thesis topic proposal to be considered by the Committee for Student Affairs with the following title:

%\hfill\begin{minipage}{\dimexpr\textwidth-2cm}
Slovene: {\bf \SLONaslov}\\
English: {\bf \ENGNaslov}
%\end{minipage}

This topic was already approved last year: { \textit{NO} }


I declare that the mentors listed below have approved the submission of the thesis topic proposal described in the remainder of this document.

I would like to write the thesis in English to reach a further audience and more eloquently formulate ideas and results.

I propose the following mentor:

%%%%%%%%%%%%%%%%%%%%%%%%%% Filled in by the candidate! %%%%%%%%%%%%%%%%%%%%%%%%%%
\hfill\begin{minipage}{\dimexpr\textwidth-2cm}
Name and surname, title: Boštjan Slivnik, doc. dr.\\
Institution: Faculty of Computer Science, Ljubljana\\
E-mail: bostjan.slivnik@fri.uni-lj.si
\end{minipage}

%%%%%%%%%%%%%%%%%%%%%%%%%% End of filling in %%%%%%%%%%%%%%%%%%%%%%%%%%

\bigskip

\hfill Ljubljana, \today.


\clearpage
\section*{Proposal of the masters thesis topic}

\section{The narrow field of the thesis topic}

English: e.g. compilers, functional programming languages, non-strict evaluation


\section{Key-words}

English: compilers, lazy functional programming languages, garbage collection, interpreter


\section{Detailed thesis proposal}

% Navodilo (pobrišite v končnem izdelku):
% V nadaljevanju opredelite izhodišča magistrskega dela in utemeljite znanstveno ali strokovno relevantnost predlagane teme.

\textbf{Past approvements of the proposed thesis topic:}\\
The proposed thesis has not been submitted nor approved in previous years.

% In case you are submitting a topic that has already been approved in the previous years, please state this here and explain the potential differences to that topic.




\subsection{Introduction and problem formulation}

Programming language design remains an active research field in computer science, as new languages are born, age, and eventually die.

There are several ways to distinguish programming languages.
\textit{Programming paradigms} are a way to classify programming languages based on their features. There are four basic paradigms (or models) that describe most programming today: \textit{imperative}, \textit{applicative} (or \textit{functional}), \textit{rule-based}, and \textit{object-oriented} \cite{Pratt}.

We also classify programming languages based on their \textit{strictness} \cite{todo}. A programming language is said to be \textit{strict} when it only supports strict functions - functions that eagerly evaluate their arguments \cite{todo}. A consequence of strictness is that by applying a non-terminating expression to a strict function, the function will never terminate as well (which might not be the case in a non-strict language) \cite{todo}. Most well-known programming languages are strict \cite{todo}.

All programs have to manage the way they use a computer’s memory while running \cite{rust}. Computer memory is allocated during execution to store data (user-defined data structures, functions, etc.) \cite{todo}. Another way to categorize programming languages is by their mechanism of memory management. Some languages require manual memory allocation and deallocation, and some do it automatically \cite{todo}. Nowadays, most programming language designers opt for an automatic memory management system \cite{todo}. For instance, Rust uses a mechanism called \textit{ownership} \cite{todo}, Swift uses \textit{automatic reference counting} \cite{todo}, and Java, Python, C\# and alike use \textit{garbage collectors} \cite{todo}. To our knowledge, all well-known functional programming languages (Haskell, OCaml, F\#, Standard ML) use garbage collectors as well.

The goal of this thesis is to design a \textit{non-strict} (\textit{lazy}) \textit{functional} programming language whose memory management will be handled \textit{automatically} without a \textit{garbage collector}, and implement accompanying compiler and run-time environment (an interpreter \cite{todo} and/or Read Eval Run-time Loop \cite{todo}).

\subsection{Related work}

The usual way of presenting an evaluation model for a functional programming language is to define an \textit{abstract machine} \cite{STG}. We introduce three works that are relevant to our thesis in greater detail and mention some other pieces as well.

The \textit{Spineless Tagless G-Machine} is an abstract machine designed to support non-strict higher-order functional languages. In comparison with earlier abstract machines (G-machine, Spineless G-machine, and the Three Instruction Machine to name a few), STG exhibits a couple of unusual features: 1. The abstract machine language is also a functional language, albeit very small, and is assigned the usual \textit{denotational semantics}. 2. Objects on the heap, both \textit{thunks} (unevaluated suspensions) \cite{todo} and \textit{head normal forms} \cite{todo}, have uniform representation. 3. \textit{Lambda lifting} \cite{todo} is not part of the compilation process \cite{STG}.

Another (more recent) abstract machine is \textbf{LVM} (Lazy Virtual Machine). It defines a portable instruction set and file format. Just like the STG, it is designed to execute languages with non-strict semantics. LVM is implemented on OCaml's runtime system. LVM is a set of tools that first translate an untyped, rich intermediate language (enriched $\lambda$-calculus) proclaimed $\lambda_{core}$ (corresponds to GHC's Core language) into $\lambda_{lvm}$ (a language with a stricter form), and finally into the actual LVM instruction set. LVM is garbage collected \cite{LVM}.

We will base the design of the programming language for this thesis on Haskell. It is the obvious choice as it is held as the \textit{de facto} non-strict functional programming language. The most well known Haskell compiler is \textbf{GHC} (Glasgow Haskell Compiler). GHC first parses and converts rich Haskell syntax into a much simpler language called \textit{Core language} (second-order $\lambda$-calculus). Several transformations are applied to the core language to improve the code. Another pass then converts Core to an even less-complex (purely functional) language, called \textit{Shared Term Graph} (STG). The target language of the Glasgow compiler is C. Haskell is a garbage collected language - it utilizes a technique known as \textit{generational garbage collection}, which exploits an observation that \textit{most objects die young} \cite{GHC}.

\subsection{Expected contributions}

Manual memory management is notoriously hard - programmers often forget to clean-up resources manually, leading to memory-leaks and, in the worse-case, to a crash \cite{todo}. Garbage collectors (GC) help alleviate this problem quite well \cite{todo}. Nevertheless, there are some negative consequences of using them. Performance issues can arise in real-time systems because of the non-deterministic nature of garbage collectors. In the worst case, the Garbage collector can entirely pause the execution of a program \cite{todo}. Some GC algorithms also lead to \textit{memory fragmentation}, which can cause significant performance problems as well \cite{todo}.

By avoiding the Garbage collectors altogether, we will potentially improve the run-time execution of non-strict programs at least in one realm: the overhead of the garbage disposal will be predictable and deterministic.

\subsection{Methodology}

We will implement the front-end of the compiler based on modern theoretical frameworks - we will split it into several independent phases, each operating on a different \textit{abstract language} \cite{Appel}. We will use \textit{monads} \cite{todo} to encapsulate non-pure code (code with side-effects).

To support run-time execution, we will have to answer a couple of questions: 1. How will we represent functions, data, and thunks during execution? 2. How will function application be performed (partial-application)? 3. How are data structures assembled and dis-assembled (pattern-matching)? \cite{STG} To answer these questions we will analyze solutions of a couple of different works \cite{several works}, and integrate those that seem most suitable at the time.

The most important question, however, is 4. How will the memory be disposed of automatically without a garbage collector? We may construct our solution based on \textit{Rust}, where we would design our language so that the compiler will be able to handle memory management in \textit{compile-time}. We could also use automatic reference counting (ARC), as done by LLVM (Swift, Objective-C). Another (similar to ARC) solution might be to have a data-structure living in a reserved portion of the program memory, which would keep track of references to objects. When designing any of the stated, or some other, solution, we must keep in mind that our language is going to be \textit{non-strict}, which adds additional complexity.

\subsection{References}
\label{literatura}


\renewcommand\refname{}
\vspace{-50px}
\bibliographystyle{elsarticle-num}
\bibliography{./bibliografija/bibliography}




%\bigskip
%
%Ljubljana, \today .

\end{document}
