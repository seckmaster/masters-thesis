# Magistrsko delo 

## 1. Uvod (iz dispozicije)

### 1.1. Pregled sorodnih del in predvideni prispevki

* https://github.com/Kindelia/HVM
* https://ziglang.org/documentation/0.9.1/#Memory - PL z ročnim upravljanjem s pomnilnikom, ki omogoča fin nadzor nad alokatorji.
  https://kristoff.it/blog/what-is-zig-comptime/#compile-time-reflection
* https://github.com/ValeLang/Vale
  https://verdagon.dev/blog/hybrid-generational-memory
* Linear Haskell

https://www.amazon.com/Implementation-Functional-Programming-Languages-Theoretical/dp/0521621127

## 2. Programski jezik Bitis

### 2.1. Sintaksa

_Ali je potrebno definirati in navesti celotno kontekstno-neodvisno gramatiko?_

* Temelji na Haskellu (mini Haskell) z dodanimi referencami in identifikatorji življenskih dob.

### 2.2. Semantika

#### 2.2.1. Sistem podatkovnih tipov 

* Temelji na Haskellu
* Parametričen polimorfizem
* Samodejno prepoznavanje tipov (type-inference)

#### 2.2.2. Upravljanje s pomnilnikom

* Lastništvo (ownership)
* Izposoja (borrowing)
* Samodejno prepoznavnaje življenskih dob (lifetime-inference)
* Cikli

## 2. Implementacija prevajalnika

### 2.1. Monad-i

_Glede na to, da je celoten prevajalnik implementiran s pomočjo monadov, se mi zdi smiselno narediti kratek uvod v njih._

### 3.1. Parsing

[1] [Monadic Parsing](https://www.cs.nott.ac.uk/~pszgmh/monparsing.pdf)

### 3.2. Type-checking

[2] [Monadic Typechecking](https://eliasc.github.io/papers/sle19.pdf)

### 3.3. Upravljanje s pomnilnikom

#### 3.3.1. Samodejno prepoznavanje življenjskih dob

V tej fazi prevajalnik prepreči to, da bi referenca na objekt živela dlje od objekta samega.

```Haskell
f :: () -> &'A Int
f () = let a = 10 in &a -- error
```

#### 3.3.2. Zagotavljanje pravil lastništva

1. Objekt ima lahko največ enega lastnika
2. Dokler je objekt izposojen (t.j. obstaja referenca nanj), je prepovadano prenesti njegovo lastništvo.
3. Če je objekt lastništvo prenesel, ga ni možno nikoli več, hkrati pa ga ni možno referencirati.
4. Cikli niso dovoljeni

### 3.4. Navidezni stroj z lenim izvajanjem

Dva načina lenega izvajanja:

1. Appel
2. GHC (evalvacija do weak-head normal form)

Implementacija ujemanja vzorcev:
https://alan-j-hu.github.io/writing/pattern-matching.html

* https://link.springer.com/chapter/10.1007/3-540-15975-4_48
* https://www.researchgate.net/publication/221057054_Compiling_pattern_matching_to_good_decision_trees


Equation lifting

## 4. Vrednotenje

## 5. Zaključek
