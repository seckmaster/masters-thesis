\babel@toc {english}{}\relax 
\babel@toc {slovene}{}\relax 
\contentsline {chapter}{Povzetek}{}{chapter*.1}%
\babel@toc {english}{}\relax 
\contentsline {chapter}{Abstract}{}{section*.3}%
\babel@toc {slovene}{}\relax 
\contentsline {chapter}{\numberline {1}Uvod}{1}{chapter.1}%
\contentsline {subsection}{\numberline {1.0.1}Pregled sorodnih del}{3}{subsection.1.0.1}%
\contentsline {chapter}{\numberline {2}Programski jezik Blang}{9}{chapter.2}%
\contentsline {section}{\numberline {2.1}Sintaksa}{9}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Prototipi}{10}{subsection.2.1.1}%
\contentsline {subsection}{\numberline {2.1.2}Spremenljivke}{10}{subsection.2.1.2}%
\contentsline {subsubsection}{Funkcije}{11}{section*.6}%
\contentsline {subsubsection}{Currying}{11}{section*.7}%
\contentsline {subsubsection}{Aplikacija}{12}{section*.8}%
\contentsline {subsubsection}{Ujemanje vzorcev}{13}{section*.9}%
\contentsline {subsection}{\numberline {2.1.3}Algebrai\IeC {\v c}ni tipi}{16}{subsection.2.1.3}%
\contentsline {subsection}{\numberline {2.1.4}Izrazi}{16}{subsection.2.1.4}%
\contentsline {section}{\numberline {2.2}Semanti\IeC {\v c}na pravila in sistem tipov}{19}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Notacija}{19}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Podatkovni tipi}{19}{subsection.2.2.2}%
\contentsline {subsubsection}{Osnovni podatkovni tipi}{19}{section*.10}%
\contentsline {subsubsection}{Algebrai\IeC {\v c}ni tipi}{20}{section*.11}%
\contentsline {subsubsection}{Funkcijski tipi}{21}{section*.12}%
\contentsline {subsection}{\numberline {2.2.3}Parametri\IeC {\v c}ni polimorfizem}{21}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Obmo\IeC {\v c}je vidnosti}{23}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Tipiziranost}{23}{subsection.2.2.5}%
\contentsline {subsubsection}{Konstante}{24}{section*.13}%
\contentsline {subsubsection}{Dvo\IeC {\v c}leni izrazi}{24}{section*.14}%
\contentsline {subsubsection}{Pogojni izraz}{24}{section*.15}%
\contentsline {subsubsection}{Terka}{24}{section*.16}%
\contentsline {subsubsection}{Izraz lambda}{24}{section*.17}%
\contentsline {subsubsection}{Ime}{25}{section*.18}%
\contentsline {subsubsection}{Izraz let}{25}{section*.19}%
\contentsline {subsubsection}{Izraz case}{25}{section*.20}%
\contentsline {subsubsection}{Izraz referenca}{25}{section*.21}%
\contentsline {subsubsection}{Izraz dereferenca}{25}{section*.22}%
\contentsline {subsection}{\numberline {2.2.6}Strategije ra\IeC {\v c}unanja izrazov}{26}{subsection.2.2.6}%
\contentsline {subsubsection}{Neskon\IeC {\v c}ne podatkovne strukture}{26}{section*.23}%
\contentsline {section}{\numberline {2.3}Upravljanje s pomnilnikom}{27}{section.2.3}%
\contentsline {subsection}{\numberline {2.3.1}Zgodovina lastni\IeC {\v s}tva}{27}{subsection.2.3.1}%
\contentsline {subsection}{\numberline {2.3.2}Lastni\IeC {\v s}tvo}{28}{subsection.2.3.2}%
\contentsline {subsubsection}{Drevo objektov}{29}{section*.25}%
\contentsline {subsection}{\numberline {2.3.3}Premik}{31}{subsection.2.3.3}%
\contentsline {subsection}{\numberline {2.3.4}Izposoja}{34}{subsection.2.3.4}%
\contentsline {subsection}{\numberline {2.3.5}\IeC {\v Z}ivljenjske dobe}{37}{subsection.2.3.5}%
\contentsline {subsubsection}{Podatkovni tipi in reference}{38}{section*.29}%
\contentsline {subsection}{\numberline {2.3.6}Samoizposoja}{41}{subsection.2.3.6}%
\contentsline {subsection}{\numberline {2.3.7}Dereferenciranje}{41}{subsection.2.3.7}%
\contentsline {subsection}{\numberline {2.3.8}Pomnilni\IeC {\v s}ki cikli}{42}{subsection.2.3.8}%
\contentsline {subsubsection}{Graf odvisnosti}{43}{section*.30}%
\contentsline {subsubsection}{Mo\IeC {\v c}ni in \IeC {\v s}ibki cikli}{45}{section*.32}%
\contentsline {chapter}{\numberline {3}Sprednji del prevajalnika}{47}{chapter.3}%
\contentsline {section}{\numberline {3.1}Leksikalna in sintaksna analiza}{48}{section.3.1}%
\contentsline {section}{\numberline {3.2}Preverjanje in izpeljava tipov}{48}{section.3.2}%
\contentsline {section}{\numberline {3.3}Upravljanje s pomnilnikom}{49}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Analiza \IeC {\v z}ivljenjskih dob}{49}{subsection.3.3.1}%
\contentsline {subsubsection}{Analiza}{50}{section*.34}%
\contentsline {subsubsection}{Samoizposoja}{51}{section*.36}%
\contentsline {subsection}{\numberline {3.3.2}Analiza lastni\IeC {\v s}tva in izposoje}{52}{subsection.3.3.2}%
\contentsline {subsubsection}{Premik in izposoja neveljavne spremenljivke}{52}{section*.38}%
\contentsline {subsubsection}{Premik izposojene spremenljivke}{52}{section*.39}%
\contentsline {subsubsection}{Premik iz izposoje}{53}{section*.40}%
\contentsline {subsubsection}{Pomnilni\IeC {\v s}ki cikli}{53}{section*.41}%
\contentsline {chapter}{\numberline {4}Zadnji del prevajalnika}{55}{chapter.4}%
\contentsline {section}{\numberline {4.1}Raz\IeC {\v s}irjena Applova vmesna koda}{55}{section.4.1}%
\contentsline {section}{\numberline {4.2}Generiranje vmesne kode}{58}{section.4.2}%
\contentsline {subsection}{\numberline {4.2.1}Ovojnice}{58}{subsection.4.2.1}%
\contentsline {subsection}{\numberline {4.2.2}Predstavitev objektov v pomnilniku}{59}{subsection.4.2.2}%
\contentsline {subsection}{\numberline {4.2.3}Samoposodabljajo\IeC {\v c} model}{60}{subsection.4.2.3}%
\contentsline {subsection}{\numberline {4.2.4}Upravljanje s pomnilnikom}{61}{subsection.4.2.4}%
\contentsline {subsubsection}{Destruktorji}{61}{section*.43}%
\contentsline {subsubsection}{Izposoja in \IeC {\v s}ibke reference}{64}{section*.44}%
\contentsline {subsubsection}{Vejitveni izrazi}{68}{section*.49}%
\contentsline {subsubsection}{Ujemanje vzorcev}{70}{section*.50}%
\contentsline {section}{\numberline {4.3}Navidezni stroj}{71}{section.4.3}%
\contentsline {chapter}{\numberline {5}Prakti\IeC {\v c}na evalvacija in meritve}{73}{chapter.5}%
\contentsline {section}{\numberline {5.1}Analiza}{73}{section.5.1}%
\contentsline {section}{\numberline {5.2}Pomanjkljivosti jezika}{76}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Semantika funkcijskih tipov}{76}{subsection.5.2.1}%
\contentsline {subsubsection}{1. Funkcije vi\IeC {\v s}jega reda in delna aplikacija}{77}{section*.54}%
\contentsline {subsubsection}{2. Izposoja funkcij}{79}{section*.55}%
\contentsline {subsection}{\numberline {5.2.2}\IeC {\v Z}ivljenjske dobe izrazov}{79}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Nadaljnje delo}{81}{section.5.3}%
\contentsline {subsection}{\numberline {5.3.1}Natan\IeC {\v c}no brisanje pomnilnika}{81}{subsection.5.3.1}%
\contentsline {subsubsection}{Vejitveni izrazi}{82}{section*.56}%
\contentsline {subsection}{\numberline {5.3.2}Analiza ponovne uporabe}{82}{subsection.5.3.2}%
\contentsline {subsection}{\numberline {5.3.3}Analiza strogosti}{82}{subsection.5.3.3}%
\contentsline {subsection}{\numberline {5.3.4}Ostale optimizacije}{83}{subsection.5.3.4}%
\contentsline {chapter}{\numberline {6}Zaklju\IeC {\v c}ek in sklepne ugotovitve}{85}{chapter.6}%
\contentsline {chapter}{\numberline {A}Nabor programov za analizo}{87}{appendix.A}%
