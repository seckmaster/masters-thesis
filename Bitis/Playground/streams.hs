import CL

add_one :: &'A Int -> 'A Int
add_one n = n + 1

id_ref :: &'A Int -> 'A Int
id_ref x = x + 0

-- stream of natural numbers
-- 1,2,3,4,5,...

nats :: (CL Int)
nats = Cons 1 (map add_one (&nats))
print print_list (take 10 nats)

-- stream of 1
-- 1,1,1,1,...

ones :: (CL Int)
ones = Cons 1 (map id_ref (&ones))
print print_list (take 10 ones)

ones1 :: (CL Int)
ones1 = Cons 1 (Ref (&ones1))
print print_list (take 10 ones1)
