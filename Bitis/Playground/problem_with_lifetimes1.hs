f :: &'A Bool -> Int
f &true = 1
f &false = 0

a = let
  b = true
in f (&b)
print a
