import CL

add_ref :: &'A Int -> &'A Int -> 'A Int
add_ref x y = x + y + 0

fibs0 :: (CL Int)
tail_fibs = tail (&fibs0)
fibs0 = Cons 1 (Cons 1 (zip_with add_ref (&fibs0) (&tail_fibs)))
-- 99% memory cleared

fibs :: (CL Int)
fibs = Cons 1 (Cons 1 (zip_with1 add_ref (&fibs) (tail (&fibs))))
-- 48% memory cleared

print print_list (take_ref 10 (&fibs))
print print_list (take_ref 10 (&fibs0))

