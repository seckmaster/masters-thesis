data Graph = Node Int [Graph]
clique :: [Int] -> [Graph]
clique xs = let c = fmap (\i -> Node i c) xs in c
