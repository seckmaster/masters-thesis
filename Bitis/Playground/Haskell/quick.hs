data List a = Cons a (List a) | Nil ()

instance Show a => Show (List a) where
   show (Nil ()) = ""
   show (Cons x xs) = show x ++ " " ++ show xs

mmerge (Nil ()) ys = ys
mmerge (Cons x xs) ys = Cons x (mmerge xs ys)

mfilter (Nil ()) f = Nil ()
mfilter (Cons x xs) f = if f x then Cons x (mfilter xs f)
                               else mfilter xs f

quick (Nil ())    f = Nil ()
quick (Cons x xs) f = let
                        as = quick (mfilter xs (\y -> y <= x)) f
                        bs = quick (mfilter xs (\y -> y > x)) f
                      in mmerge as (mmerge (Cons x (Nil ())) bs)
