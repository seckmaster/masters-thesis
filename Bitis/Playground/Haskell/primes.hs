data Lst = Cons Int Lst | Nil

instance Show Lst where
   show (Nil) = ""
   show (Cons x xs) = show x ++ " " ++ show xs

mmap f Nil = Nil
mmap f (Cons x xs) = Cons (f x) (mmap f xs)

mfilter f Nil         = Nil
mfilter f (Cons x xs) = if f x then Cons x (mfilter f xs) else mfilter f xs

mtake 0 xs = Nil
mtake n Nil = Nil
mtake n (Cons x xs) = Cons x (mtake (n - 1) xs)

nats = Cons 2 (mmap (\x -> x + 1) nats)
primes = sieve nats

sieve (Cons x xs) = Cons x (sieve (mfilter (\p -> mod p x /= 0) xs))


