data Lst a = Cons a (Lst a) | Nil

instance Show a => Show (Lst a) where
   show xs = "[" ++ show1 xs ++ "]"
             where show1 Nil = ""
                   show1 (Cons x Nil) = show x
                   show1 (Cons x xs) = show x ++ "," ++ show1 xs

mmap f Nil = Nil
mmap f (Cons x xs) = Cons (f x) (mmap f xs)

mfilter f Nil         = Nil
mfilter f (Cons x xs) = if f x then Cons x (mfilter f xs) else mfilter f xs

mtake 0 xs  = Nil
mtake n Nil = Nil
mtake n (Cons x xs) = Cons x (mtake (n - 1) xs)

mmerge Nil ys = ys
mmerge (Cons x xs) ys = Cons x (mmerge xs ys)

mmconcat Nil         = Nil
mmconcat (Cons x xs) = mmerge x (mmconcat xs)

subs Nil = Cons Nil Nil
subs (Cons x xs) = let
                     yss = subs xs
                     zss = (mmap (\el -> Cons x el) yss)
                   in mmerge zss yss

interleave :: Int -> Lst Int -> Lst (Lst Int)
interleave x Nil = Cons (Cons x Nil) Nil
interleave x (Cons y ys) = let
                             as = Cons x (Cons y ys)
                           in Cons as (mmap (Cons y) (interleave x ys))

perms Nil         = Cons Nil Nil
perms (Cons x xs) = mmconcat (mmap (interleave x) (perms xs))
