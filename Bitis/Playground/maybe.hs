import maybe

compose f g = \a -> g (f a)

neg true  = false
neg false = true

x = Just 10
y = Just true
z = fmap (compose neg neg) y
w = fmap (\a -> a * 2) (Nothing ())
q = Just 1
r = flat_map (\a -> case a of 0 -> Just false | 1 -> Just true | n -> Nothing ()) q

print (is_just (&x))
print (is_nothing (&x))
print (extract (&x))
print (extract (&z))
print (is_nothing (&w))
print (extract (&r))
