import graph
import list

convert :: &'A (List a) -> (ListRef ['A, a])
convert $(Cons x xs) = ConsRef x (convert xs)
convert $(Nil ())    = NilRef ()

print_graph_list :: &'A (List (Graph ['A])) -> 'A ()
print_graph_list $(Cons g gs) = let
  print print_graph g
in print_graph_list gs
print_graph_list $(NilRef ()) = ()

xs :: (List Int)
xs = Cons 10 (Cons 20 (Cons 30 (Cons 40 (Cons 50 (Nil ())))))

clique :: (List (Graph []))
clique = fmap (λi -> Node i (convert (&clique))) xs

print print_graph_list (&clique)
