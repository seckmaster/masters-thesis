data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef ()

sum_ref         :: &'A (ListRef ['A, Int]) -> &'A Int
drop            :: Int -> &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
take            :: Int -> &'A (ListRef ['A, a]) -> (ListRef ['A, a])
create_list_ref :: &'A Int -> (ListRef ['A, a])
hd_ref          :: &'A (ListRef ['A, a]) -> &'A a
len_ref         :: (ListRef ['A, a]) -> &'A Int
tail_ref        :: &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
last_ref        :: &'A (ListRef ['A, a]) -> &'A a
fmap_ref        :: (&'A a -> &'A b) -> &'A (ListRef ['A, a]) -> (ListRef ['A, b])
merge_ref       :: &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a]) -> (ListRef ['A, a])

zero  = 0
one   = 1
two   = 2
three = 3
nil_ref_empty = NilRef ()

sum_ref $(NilRef ())    = &zero
sum_ref $(ConsRef x xs) = *x + sum_ref xs

drop 0 xs              = xs
drop n $(ConsRef _ xs) = drop (n - 1) xs
drop _ $(NilRef _)     = &nil_ref_empty

take n xs = case xs of -- todo: also add test for this function implemented as two equations
  $(NilRef ()) -> NilRef ()
  | $(ConsRef x xss) -> case n of
      0 -> NilRef ()
      | m -> ConsRef (*x) (take (m - 1) xss)

len_ref  $(NilRef ())     = &zero
len_ref  $(ConsRef _ xss) = &one + len_ref xss

create_list_ref &0 = NilRef ()
create_list_ref p  = ConsRef p (create_list_ref (p - (&one)))

hd_ref $(ConsRef x _) = *x
tail_ref $(ConsRef _ xs) = xs

last_ref $(ConsRef x1 xss) = case xss of
  $(NilRef ()) -> *x1
  | _ -> last_ref xss
last_ref $(ConsRef _ xss)  = last_ref xss

fmap_ref f xs = case xs of
  $(NilRef ()) -> let -- todo: workaround because of typechecker
      a :: (ListRef [b])
      a = NilRef ()
    in a
  | $(ConsRef x xss) -> let
      b :: (ListRef [b])
      b = ConsRef (f (*x)) (fmap_ref f xss)
    in b

id_ref :: &'A a -> &'A a
id_ref a = a

convert_list xs = fmap_ref id_ref xs

merge_ref xs ys = case xs of
  $(NilRef ()) -> convert_list ys
  | $(ConsRef x xss) -> ConsRef (*x) (merge_ref (xss) ys)

print_list_ref :: &'A (ListRef ['A, c]) -> 'A ()
print_list_ref $(ConsRef x xs) = let print x in print_list_ref (xs)
print_list_ref $(NilRef ())    = ()
