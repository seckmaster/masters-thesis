data Maybe [Wrapped] = Just Wrapped | Nothing ()

is_just :: &'A (Maybe a) -> 'A Bool
is_just $(Nothing ()) = false
is_just $(Just _)     = true

is_nothing :: &'A (Maybe a) -> 'A Bool
is_nothing $(Nothing ()) = true
is_nothing $(Just _)     = false

fmap :: (j -> k) -> (Maybe j) -> (Maybe k)
fmap f m = case m of
             $(Nothing ()) -> Nothing ()
             | $(Just x) -> Just (f x)

flat_map :: (a -> (Maybe b)) -> (Maybe a) -> (Maybe b)
flat_map f m = case m of
                 $(Nothing ()) -> Nothing ()
                 | $(Just x) -> f x

extract :: &'A (Maybe a) -> &'A a
extract $(Just a) = a
