data MaybeRef ['T, T] = JustRef (&'T T) | NothingRef ()

fmap_ref :: (&'A a -> &'A b) -> &'A (MaybeRef ['A, a]) -> (MaybeRef ['A, b])
fmap_ref f m = case m of
  $(NothingRef ()) -> NothingRef ()
  | $(JustRef x) -> JustRef (f (*x))

flat_map_ref :: (&'A a -> (MaybeRef ['A, a])) -> &'A (MaybeRef ['A, a]) -> (MaybeRef ['A, b])
flat_map_ref f m = case m of
                     $(NothingRef ()) -> NothingRef ()
                     | $(JustRef x) -> f (*x)

extract_ref :: &'A (MaybeRef ['A, a]) -> &'A a
extract_ref $(JustRef x) = *x

is_nothing_ref :: &'A (MaybeRef ['A, a]) -> Bool
is_nothing_ref $(NothingRef ()) = true
is_nothing_ref _                = false

