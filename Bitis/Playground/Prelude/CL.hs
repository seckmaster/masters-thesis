data CL ['A, u] = Cons ('A u) (CL ['A, u]) | Ref (&'A (CL ['A, u])) | Nil ()

-- take

take :: Int -> (CL ['A, a]) -> 'A (CL ['A, a])
take a as = case a of
              0 -> Nil ()
              | n -> case as of
                $(Nil ()) -> Nil ()
                | $(Cons x xs) -> Cons x (take (n - 1) xs)
                | $(Ref xss) -> take n (*xss)

take_ref :: Int -> &'A (CL ['A, a]) -> 'A (CL ['A, a])
take_ref a as = case a of
              0 -> Nil ()
              | n -> case as of
                $(Nil ()) -> Nil ()
                | $(Cons x xs) -> Cons x (take_ref (n - 1) xs)
                | $(Ref xss) -> take_ref n (*xss)
  
-- drop_ref

drop_ref :: Int -> &'A (CL ['A, a]) -> 'A (CL ['A, a])
drop_ref n xs = case n of
             0 -> Ref xs
             | m -> case xs of
                      $(Cons _ as) -> drop_ref (m - 1) as
                      | $(Nil ()) -> Nil ()
                      | $(Ref as) -> drop_ref m (*as)

-- drop

drop :: Int -> (CL ['A, a]) -> 'A (CL ['A, a])
drop n xs = case n of
             0 -> xs
             | m -> case xs of
                      $(Cons _ as) -> drop (m - 1) as
                      | $(Nil ()) -> Nil ()
                      | $(Ref as) -> drop m (*as)
-- map

map :: (&'A a -> 'A b) -> &'A (CL ['A, a]) -> (CL ['A, 'A b])
map f xs = case xs of
             --$(Ref xs) -> map f (*xs)
             $(Cons a as) -> Cons (f a) (map f as)
             | $(Nil ()) -> Nil ()
             
-- sum_ref

sum_ref :: &'A (CL ['A, Int]) -> 'A Int
sum_ref $(Nil ())    = 0
sum_ref $(Cons x xs) = x + sum_ref xs
sum_ref $(Ref xs)    = sum_ref (*xs)

sum :: (CL ['A, Int]) -> 'A Int
sum $(Nil ())    = 0
sum $(Cons x xs) = x + sum xs
-- sum $(Ref xs)    = sum xs

-- zip_with

zip_with :: (&'A a -> &'A b -> 'A c) -> &'A (CL ['A, a]) -> &'A (CL ['A, b]) -> (CL ['A, c])
zip_with f xs ys = case xs of
                     $(Nil ()) -> Nil ()
                     | $(Ref xss) -> zip_with f (*xss) ys
                     | $(Cons a as) -> case ys of
                       $(Nil ()) -> Nil ()
                         | $(Ref yss) -> zip_with f xs (*yss)
                         | $(Cons b bs) -> Cons (f a b) (zip_with f as bs)

zip_with1 :: (&'A a -> 'A b -> 'A c) -> &'A (CL ['A, a]) -> 'A (CL ['A, b]) -> (CL ['A, c])
zip_with1 f xs ys = case xs of
                      $(Nil ()) -> Nil ()
                      | $(Ref xss) -> zip_with1 f (*xss) ys
                      | $(Cons a as) -> case ys of
                        $(Nil ()) -> Nil ()
                        | $(Ref yss) -> zip_with1 f xs (*yss)
                        | $(Cons b bs) -> Cons (f a b) (zip_with1 f as bs)
                      
-- tail

tail :: &'A (CL ['A, a]) -> (CL ['A, a])
tail $(Nil ())    = Nil ()
tail $(Cons x xs) = Ref xs
tail $(Ref xs)    = tail (*xs)

-- get

get :: Int -> &'A (CL ['A, a]) -> &'A Int
get n xs = case n of
  0 -> (case xs of
    $(Ref xss) -> get 0 (*xss)
    | $(Cons a as) -> a)
  | m -> (case xs of
    $(Ref xss) -> get m (*xss)
    | $(Cons a as) -> get (m - 1) as)

-- takeWhile

takeWhile :: (&'A a -> 'A Bool) -> (&'A a -> 'A a) -> &'A (CL ['A, a]) -> (CL ['A, a])
takeWhile f g xs = case xs of
  $(Ref xss) -> takeWhile f g (*xss)
  | $(Nil ()) -> Nil ()
  | $(Cons x ys) -> if f x then Cons (g x) (takeWhile f g ys)
                           else Nil ()

-- print_list

print_list_ref :: &'A (CL ['A, a]) -> 'A ()
print_list_ref $(Cons x xs) = let print x in print_list_ref xs
print_list_ref $(Ref xs)    = print_list_ref (*xs)
print_list_ref $(Nil ())    = ()

print_list :: (CL ['A, a]) -> 'A ()
print_list $(Cons x xs) = let print x in print_list xs
print_list $(Ref xs)    = print_list_ref xs
print_list $(Nil ())    = ()
