import listref

data Graph ['A] = Node Int (ListRef ['A, (Graph ['A])])

print_list_ref_g :: &'A (ListRef ['A, c]) -> 'A ()

print_graph :: &'A (Graph ['A]) -> ()
print_graph $(Node n edges) = let
  print n
  print print_list_ref_g edges
in ()

print_list_ref_g $(ConsRef x xs) = let print {--print_graph (*x)--} x in print_list_ref_g (xs)
print_list_ref_g $(NilRef ())    = ()
