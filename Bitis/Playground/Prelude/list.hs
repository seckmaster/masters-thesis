data List [e] = Cons e (List e) | Nil ()

hd :: (List a) -> a
hd $(Cons a _) = a

tl :: (List a) -> (List a)
tl $(Cons _ xs) = xs

print_list :: &'A (List a) -> 'A ()
print_list $(Nil ()) = ()
print_list $(Cons x xs) = let print x in print_list xs

fmap :: (a -> b) -> (List a) -> (List b)
fmap f xs = case xs of
              $(Cons a as) -> Cons (f a) (fmap f as)
              | $(Nil ()) -> Nil ()

__zero__ = 0
__one__  = 1
__enumerate__ :: &'STATIC Int -> ((&'STATIC Int, a) -> b) -> (List a) -> (List b)

enumerated_fmap :: ((&'STATIC Int, a) -> b) -> (List a) -> (List b)
enumerated_fmap f xs = __enumerate__ (&__zero__) f xs

__enumerate__ idx f xs = case xs of
                           $(Cons a as) -> Cons (f (idx, a)) (__enumerate__ (idx + &__one__) f as)
                           | $(Nil ()) -> Nil ()


__enumerated__ :: &'STATIC Int -> (List Int) -> (List (&'STATIC Int, Int))
__enumerated__ idx xs = case xs of
                          $(Cons a as) -> let
                            ys :: (List (&Int, Int))
                            zs :: (List (&Int, Int))
                            zs = (__enumerated__ (idx + &__one__) as)
                            ys = Cons (idx, a) zs
                          in ys
                          | $(Nil ()) -> Nil ()

enumerated :: (List Int) -> (List (&'STATIC Int, Int))
enumerated xs = __enumerated__ (&__zero__) xs

foldl :: (b -> a -> b) -> b -> (List a) -> b
foldl f acc xs = case xs of $(Nil ()) -> acc
                   | $(Cons a as) -> foldl f (f acc a) as
                   
