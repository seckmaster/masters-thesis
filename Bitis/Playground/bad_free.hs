{--
data Maybe = Just Bool | Nothing ()

compose f g = \a -> g (f a)
fmap f m = case m of
 $(Nothing ()) -> Nothing ()
 | $(Just x) -> Just (f x)
neg true  = false
neg false = true

print fmap (compose neg neg) (Just true)
--}

{--
x = 10
b = let
  y = &x
  z = *y
  print case z of q -> q * 2
in 1
print b
print case x of w -> w + 18
--}


{--
a = (10,20)
b = let 
  b = &a
  c = *b
  print case c of (e,f) -> e + f
in 1
print b
print case a of (o,p) -> o + p
--}



{--
a = 10
b = let
  b = &a
  c = *b
  print c
in 1
print b
print a
--}

{--
data CL ['A, u] = Cons ('A u) (CL ['A, u]) | Ref (&'A (CL ['A, u])) | Nil ()

take :: Int -> &'A (CL ['A, a]) -> 'A (CL ['A, a])
take a as = case a of 0 -> Nil ()
              | n -> case as of
                       $(Nil ()) -> Nil ()
                       | $(Cons x xs) -> Cons (*x) (take (n - 1) xs)
                       | $(Ref xss) -> take n (*xss)

map :: (&'A a -> 'A b) -> &'A (CL ['A, a]) -> (CL ['A, 'A b])
map f xs = case xs of
             $(Ref xs) -> map f (*xs)
             | $(Cons a as) -> Cons (f a) (map f as)
             | $(Nil ()) -> Nil ()

-- infinite stream of natural numbers

print_list_ref :: &'A (CL ['A, a]) -> 'A ()
print_list_ref $(Cons x xs) = let print x in print_list_ref xs
print_list_ref $(Ref xs)    = print_list_ref (*xs)
print_list_ref $(Nil ())    = ()

print_list :: (CL ['A, a]) -> 'A ()
print_list $(Cons x xs) = let print x in print_list xs
print_list $(Ref xs)    = print_list_ref xs
print_list $(Nil ())    = ()

add_one :: &'A Int -> 'A Int
add_one n = n + 0

nats :: (CL Int)
nats = Cons 1 (map add_one (&nats))

print print_list (take 10 (&nats))

--}

data List = Cons ('A Int) (List []) | Nil ()
print case Cons 1 (Nil ()) of $(Cons a b) -> a
