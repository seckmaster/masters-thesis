data Tree ['A, a] = Leaf ('A a) | Node (Tree ['A, a]) (Tree ['A, a])

one = 1

gen :: &'A Int -> (Tree ['A, Int])
gen &0 = Leaf 1
gen n  = Node (gen (n - (&one))) (gen (n - (&one)))

sum :: (Tree ['A, Int]) -> 'A Int
sum $(Leaf x) = x
sum $(Node l r) = sum l + sum r

n = 15
print sum (gen (&n))
-- 32768
