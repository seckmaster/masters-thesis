data List ['A, e] = Cons (&'A e) (List ['A, e]) | Nil ()

foldl :: ('A b -> &'A a -> 'A b) -> 'A b -> (List ['A, a]) -> 'A b
foldl f acc xs = case xs of $(Nil ()) -> acc
                   | $(Cons a as) -> foldl f (f acc a) as

gen :: &'A Int -> Int -> (List ['A Int])
gen v n = case n of
            0 -> Nil ()
            | m -> Cons v (gen v (m - 1))

add :: 'A Int -> &'A Int -> 'A Int
add e f = e + f

three = 3
print foldl add 0 (gen (&three) 500)
-- 1500
