data List [u] = Cons u (List u) | Nil ()
data ListR ['EL, k] = ConsR (&'EL k) (ListR ['EL, k]) | NilR ()

mod :: &'A Int -> &'A Int -> 'A Int
mod a b = a - ((a / b) * b) + 0

mod_not_zero :: &'A Int -> &'A Int -> 'A Bool
mod_not_zero b a = mod a b /= 0

add_one :: &'A Int -> 'A Int
add_one n = n + 1

take :: Int -> (ListR ['A, a]) -> (ListR ['A, a])
take a as = case a of
    0 -> NilR ()
    | n -> case as of
      $(NilR ()) -> NilR ()
      | $(ConsR x xs) -> ConsR x (take (n - 1) xs)

print_list_r :: (ListR ['A, a]) -> 'A ()
print_list_r $(ConsR x xs) = let print x in print_list_r xs
print_list_r $(NilR ())    = ()

map :: (&'A a -> 'A b) -> &'A (List a) -> (List ('A b))
map f xs = case xs of
             $(Cons a as) -> Cons (f a) (map f as)
             | $(Nil ()) -> Nil ()
             
filter :: (&'A a -> 'A Bool) -> (ListR ['A, a]) -> (ListR ['A, a])
filter g ys = case ys of
   $(ConsR r rs) -> if g r then ConsR r (filter g rs)
                           else filter g rs

   
sieve :: (ListR ['A, Int]) -> (ListR ['A, Int])
sieve $(ConsR p ps) = ConsR p (sieve (filter (mod_not_zero p) ps))

convert :: &'A (List a) -> (ListR ['A, Int])
convert $(Nil())     = NilR ()
convert $(Cons x xs) = ConsR x (convert xs)

nats :: (List Int)
nats = Cons 2 (map add_one (&nats))

primes :: (ListR Int)
primes = sieve (convert (&nats))

print print_list_r (take 20 primes)
-- 2,3,5,7,11,13,...
