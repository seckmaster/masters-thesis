data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef ()
data Graph ['A] = Node Int (ListRef ['A, (Graph ['A])])
data List [e] = Cons e (List e) | Nil ()

print_list_ref_g :: &'A (ListRef ['A, c]) -> 'A ()

print_graph :: &'A (Graph ['A]) -> ()
print_graph $(Node n edges) = let
  print n
  print print_list_ref_g edges
in ()

print_list_ref_g $(ConsRef x xs) = let print x in print_list_ref_g (xs)
print_list_ref_g $(NilRef ())    = ()

convert :: &'A (List a) -> (ListRef ['A, a])
convert $(Cons x xs) = ConsRef x (convert xs)
convert $(Nil ())    = NilRef ()

fmap :: (a -> b) -> (List a) -> (List b)
fmap f xs = case xs of
      $(Cons a as) -> Cons (f a) (fmap f as)
      | $(Nil ()) -> Nil ()

print_graph_list :: &'A (List (Graph ['A])) -> 'A ()
print_graph_list $(Cons g gs) = let
  print print_graph g
in print_graph_list gs
print_graph_list $(NilRef ()) = ()

xs :: (List Int)
xs = Cons 10 (Cons 20 (Cons 30 (Cons 40 (Cons 50 (Nil ())))))

clique :: (List (Graph []))
clique = fmap (λi -> Node i (convert (&clique))) xs

print print_graph_list (&clique)
-- 10 [*10,*20,*30,*40,*50]
-- 20 [*10,*20,*30,*40,*50]
-- 30 [*10,*20,*30,*40,*50]
-- 40 [*10,*20,*30,*40,*50]
-- 50 [*10,*20,*30,*40,*50]
