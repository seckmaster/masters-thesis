data List [u] = Cons u (List u) | Nil ()
data ListR ['EL, k] = ConsR (&'EL k) 'EL (ListR ['EL, k]) | NilR ()

mod :: &'A Int -> &'A Int -> 'A Int
mod a b = a - ((a / b) * b) + 0

mod_not_zero :: &'A Int -> &'A Int -> 'A Bool
mod_not_zero b a = mod a b /= 0

add_one :: &'A Int -> 'A Int
add_one n = n + 1

take :: Int -> (ListR ['A, a]) -> (ListR ['A, a])
take a as = case a of
    0 -> NilR ()
    | n -> case as of
      -- $(NilR ()) -> NilR ()
      $(ConsR x xs) -> ConsR x (take (n - 1) xs)

print_list_r :: (ListR ['A, a]) -> 'A ()
print_list_r $(ConsR x xs) = let print x in print_list_r xs
print_list_r $(NilR ())    = ()

map :: (&'A a -> 'A b) -> &'A (List a) -> (List ('A b))
map f xs = case xs of
             $(Cons a as) -> Cons (f a) (map f as)
             | $(Nil ()) -> Nil ()
             
filter :: (&'A a -> 'A Bool) -> (ListR ['A, a]) -> (ListR ['A, a])
filter = \g -> \ys -> case ys of
   -- uncommenting the line bellow makes the code 
   -- much much slower as it allocates 5x more memory,
   -- which does not get cleared at all ?!?!?!?
   -- $(NilR ()) -> NilR ()

   $(ConsR r rs) -> if g r then ConsR r (filter g rs)
                           else filter g rs
   | $(NilR ()) -> NilR ()

id :: (ListR ['A, Int]) -> (ListR ['A, Int])   
id $(ConsR x xs) = ConsR x xs

map2 :: (ListR ['A, a]) -> (ListR ['A, a])
map2 xs = case xs of
             $(ConsR a as) -> ConsR a (map2 as)
             -- | $(NilR ()) -> NilR ()
  
one = 1
filter2 :: (ListR ['A, Int]) -> (ListR ['A, Int])
filter2 ys = case ys of
   -- this does not make program memory consumption diverge.
   -- see 'filter' ...
   -- $(NilR ()) -> NilR ()

   -- $(ConsR r rs) -> ConsR r (filter2 rs)
   -- $(ConsR r rs) -> ConsR (&one) (filter2 (rs))
   $(ConsR r rs) -> ConsR r (map2 (rs))
   
   -- okay:
   -- $(ConsR r rs) -> ConsR r rs
   -- $(ConsR r rs) -> ConsR r (id rs)
   -- $(ConsR r rs) -> ConsR r (filter2 (*&rs)) -- this works okay, except borrow checker rejects

   -- | $(NilR ()) -> NilR ()
   
sieve :: (ListR ['A, Int]) -> (ListR ['A, Int])
sieve $(ConsR p ps) = ConsR p (sieve (filter2 ps))
-- sieve $(NilR ())    = NilR ()

convert :: &'A (List a) -> (ListR ['A, Int])
convert $(Cons x xs) = ConsR x (convert xs)
-- convert $(Nil())     = NilR ()

nats :: (List Int)
nats = Cons 2 (map add_one (&nats))

primes :: (ListR Int)
primes = sieve (convert (&nats))

print print_list_r (take 10 primes)
