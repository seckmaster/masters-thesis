data List ['A, u] = Cons ('A u) (List ['A, u]) | Nil ()

map :: (&'A a -> 'A b) -> &'A (List ['A, a]) -> (List ['A, 'A b])
map f xs = case xs of
             $(Cons a as) -> Cons (f a) (map f as)
             | $(Nil ()) -> Nil ()
             
add_one :: &'A Int -> 'A Int
add_one n = n + 1

take :: Int -> (List ['A, a]) -> 'A (List ['A, a])
take a as = case a of
              0 -> Nil ()
              | n -> case as of
                $(Nil ()) -> Nil ()
                | $(Cons x xs) -> Cons x (take (n - 1) xs)

print_list :: (List ['A, a]) -> 'A ()
print_list $(Cons x xs) = let print x in print_list xs
print_list $(Nil ())    = ()

nats :: (List Int)
nats = Cons 1 (map add_one (&nats))
-- neskončen seznam [1,2,3,...

print print_list (take 200 nats)
-- 1,2,3,...,200
