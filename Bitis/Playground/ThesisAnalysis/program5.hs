data CL ['A, u] = Cons ('A u) (CL ['A, u]) | Ref (&'A (CL ['A, u])) | Nil ()

add_ref :: &'A Int -> &'A Int -> 'A Int
add_ref x y = x + y + 0

zip_with :: (&'A a -> &'A b -> 'A c) -> &'A (CL ['A, a]) -> &'A (CL ['A, b]) -> (CL ['A, c])
zip_with f xs ys = case xs of
                     $(Nil ()) -> Nil ()
                     | $(Ref xss) -> zip_with f (*xss) ys
                     | $(Cons a as) -> case ys of
                       $(Nil ()) -> Nil ()
                         | $(Ref yss) -> zip_with f xs (*yss)
                         | $(Cons b bs) -> Cons (f a b) (zip_with f as bs)
                         
take_ref :: Int -> &'A (CL ['A, a]) -> 'A (CL ['A, a])
take_ref a as = case a of
              0 -> Nil ()
              | n -> case as of
                $(Nil ()) -> Nil ()
                | $(Cons x xs) -> Cons (*x) (take_ref (n - 1) xs)
                | $(Ref xss) -> take_ref n (*xss)
                
tail :: &'A (CL ['A, a]) -> (CL ['A, a])
tail $(Nil ())    = Nil ()
tail $(Cons x xs) = Ref xs
tail $(Ref xs)    = tail (*xs)

print_list_ref :: &'A (CL ['A, a]) -> 'A ()
print_list_ref $(Cons x xs) = let print x in print_list_ref xs
print_list_ref $(Ref xs)    = print_list_ref (*xs)
print_list_ref $(Nil ())    = ()

print_list :: (CL ['A, a]) -> 'A ()
print_list $(Cons x xs) = let print x in print_list xs
print_list $(Ref xs)    = print_list_ref xs
print_list $(Nil ())    = ()

fibs :: (CL Int)
tl = tail (&fibs)
fibs = Cons 1 (Cons 1 (zip_with add_ref (&fibs) (&tl)))

print print_list (take_ref 100 (&fibs))
-- 1,1,2,3,5,8,13,...
