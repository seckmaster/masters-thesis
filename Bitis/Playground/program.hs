import list

{--
lambda :: (&'STATIC Int, Int) -> Int
lambda = \arg -> case arg of (idx, el) -> el + idx

xs = Cons 1 (Cons 2 (Cons 3 (Nil ())))
ys = enumerated_fmap lambda xs

print print_list (&ys)

zs = Cons 1 (Cons 2 (Cons 3 (Nil ())))
zzs = fmap lambda (enumerated zs)

print print_list (&zzs)
--}

f :: &'A Int -> Int -> Int
f x = \y -> x + y

one = 1
a = f (&one)

print a 2
print a 3
