data Tree ['A, a] = Leaf ('A a) | Node (Tree ['A, a]) (Tree ['A, a])

one = 1

gen :: &'A Int -> (Tree ['A, Int])
gen &0 = Leaf 1
gen n  = Node (gen (n - (&one))) (gen (n - (&one)))

sum :: (Tree ['A, Int]) -> 'A Int
sum $(Leaf x) = x
sum $(Node l r) = sum l + sum r

ten = 16
t1 = gen (&ten)
t2 = Node (Node (Node (Node (Leaf 1) (Leaf 1)) (Node (Leaf 1) (Leaf 1))) (Node (Node (Leaf 1) (Leaf 1)) (Node (Leaf 1) (Leaf 1)))) (Node (Node (Node (Leaf 1) (Leaf 1)) (Node (Leaf 1) (Leaf 1))) (Node (Node (Leaf 1) (Leaf 1)) (Node (Leaf 1) (Leaf 1))))
print sum (t1)
print sum (t2)
