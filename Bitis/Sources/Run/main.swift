import Bitis
import Foundation
import SwiftParsec

/// -----------------------------------------------------------------------
/// Execute the CLI.

Main.main()

/// -----------------------------------------------------------------------

extension FileHandle: TextOutputStream {
  public func write(_ string: String) {
    write(string.data(using: .utf8)!)
  }
}

class TextStream: TextOutputStream {
  var flush = false
  var buffer = ""
  
  func write(_ string: String) {
    if !buffer.isEmpty { buffer += "\n" }
    buffer += string
    if flush { print(string) }
  }
}

let todo_data_bindigs_typechecker = """
data Maybe [a, b, c] = Just a (a, b) [a] | Nothing c
data EitherRef ['A, 'B, a, b] = L (&'A a) | R (&'B b)

-- data Bool [a] = Just c Int | Nothing a

-- x = Just 10 (10, 10) [1,2,3] -- <-- TODO: dodaj teste and fix this !!!!
"""


let todo_typechecker = """
{-- todo: type checker not working correctly
f :: (C []) -> Int
f a = case x of
  $(A q) -> q
  -- | $(B true) -> 18
  -- | $(B false) -> 19
--}
"""

// -------

let todo_lifetime_analysis_program = """
x = let
  a = 10
  b = &a
in (a, b) -- both borrow and the referent are moved at the same time (similar to self-borrow)
"""

let todo_design_proposal = """
x = let
  b = true
  a = if &b then 1 else 2
  --  ~~~~~~~~~~~~~~~~~~~ what is the type of this expression?
  --  i)   &'aa Int <- this does not seem correct
  --  ii)  'aa Int  <- (&Bool, Int) <- this one seems to be the correct one
  --                    ~~~~~  ~~~ runtime represenation of the thunk-closure
  --  iii) Int      <- this one is wrong for sure - we loose the information about
  --                   the lifetime and could move the value out of the scope.
in ...

-- (i) proposed design:

f :: 'A Int -> 'A ('A Int -> 'A Int)  |
-- or                                 |  ↣ these two "should" be considered equivalent
f :: 'A Int -> 'A Int -> 'A Int       |
--   ~~        ~~         ~~
--   require non-reference types to have associated lifetimes as well!
f x y = x + y

-- 1.
x = let
  a = true  -- a :: '{} Bool
  --                  ↖ no lifetime
  b = 10    -- b :: '{} Int
  --                  ↖ no lifetime

  c = if &a then b else 12 -- c :: 'aa Int
  --                                ↖ "c" has limited lifetime
  --
  --    ~ 'aa  ~~ '{}  ~~>   <'aa, {}> => 'aa
  d = f c      10         -- d :: 'aa Int
in d -- error: "d" outlives "a"

-- 2.
y = let
  a = 10
  b = 20
  c = f a b -- c :: '{} Int
in c -- okay

-- 3.
z = let
  x = true
  a = if &x then 1 else 2 -- a :: 'aa Int
  b = let
    y = false
    c = if &y then 9 else 8 -- c :: 'acb Int
    d = f a c -- not okay ~> "f" expects single lifetime 'A, got two lifetimes ('aa and 'acb)
  in ...
in ...

-- (ii) proposed design:

--                       ~~~~~~~ <...lifetime...> means: pick the shortest liftime
--                       ~~~~~~~ <'A,'B> from the two lifetimes 'A and 'B, pick the shortest one
f :: 'A Int -> 'B Int -> <'A,'B> Int
f x y = x + y

-- 3.
z = let
  x = true
  a = if &x then 1 else 2 -- a :: 'aa Int
  b = let
    y = false
    c = if &y then 9 else 8 -- c :: 'acb Int
    --  ~~~~~ this is now okay; <'aa, 'acb> = 'acb
    d = f a c -- d :: 'acb Int
    -- "x" je označen kot izposojen
  in d -- error: "d" outlives "y" (though not "x")
in ...

{--
properties of <...>:
  0. all lifetimes inside <> must have a common parent other than 'STATIC
  1. <'A, {}> = 'A
  2. <{}, {}> = {}
  3. <'STATIC, 'A> = 'A
  4. <'A, 'B> = 'A => <'B, 'A> = 'A
--}
"""

let computation_monad_example = """
f :: &'A a -> (x1, &'A b)
g :: &'A b -> (x2, &'A c)
h :: &'A c -> (x3, &'A d)

o :: (&'A a -> (x1, &'A b)) -> (&'A b -> (x2, &'A c)) -> (&'A a -> &'A b)
f (o) g = ...

i = f o g o h

result = do
  data = 10
  x <- f &data
  y <- g x
  z <- h y
in z
"""

let parallelism = """
str_to_int      :: String -> Int
int_to_string   :: Int -> String
read_file_lines :: String -> (IO [String])
putStr          :: String -> (IO ())
forkIO          :: IO () -> IO ThreadId
fmap_IO         :: (a -> (IO b)) -> [a] -> IO [b]
fmap_ref_IO     :: (&'A a -> 'A (IO b)) -> &'A [a] -> 'A IO [b]
fac             :: &'A Int -> 'A Int
fac 0 = 0
fac n = n * fac (n - 1)

_ = let
  _ = read_file_lines "integers.txt" >>= (λlines ->
        return $ map str_to_int lines >>= (λintegers ->
           fmap_ref_IO return . fac &integers >>= (λfacs ->
             fmap_IO putStr . int_to_string facs >>= (λ_ ->
               -- at this point the memo
             )
           )
        )
      )
in ()
"""

// @NOTE: - In rust it is actually completely okay
// to have one reference only ...
let data_structure_different_lifetimes = """
      data L ['A, a] = C (&'A a) (L ['A, a]) | N ()
      
      x = let
        a = 1
        y = let
          b = 2
          z = let
            c = 3
            xs = C (&a) (C (&b) (C (&c) (N ())))
            --   ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            --   the compiler should reject this program becase of different lifetimes
          in ...
        in ...
      in ...
      
      -- solution ...
      
      data L ['A, 'B, a] = C (&'A a) (L ['B, a]) | N ()
      
      x = let
        a = 1
        y = let
          b = 2
          z = let
            c = 3
            xs = C (&a) (C (&b) (C (&c) (N ())))
          in ...
        in ...
      in ...
      """


/// **To discuss:**
///
/// 1. Koka & perceus: ARC, reuse analysis
///     - v našem jeziku bolj učinkovita implementacija
///    Alokator razvit za Koka run-time sistem:
///     - https://github.com/microsoft/mimalloc
/// 2. Nova implementacija šibkih referenc
/// 3. Problem s funkcijami in double-free
/// **4. Analiza: programi(1..7.hs):**
///   - čudno obnašanje primes
///   - `quicksort`, `subsequences` ne delajo  
/// 5. Destruktorji:
///   5.i) nedeterminizem:
///      - rezširimo ovojnico z dodatnimi elementi
///      - optimizacija nedeterminizma
///        - i) pointer na seznam T\F
///        - ii) neučakano brisanje
///   5.2) pattern matching (tuple/deconstruct)
///      - ujemanje vzorcev lahko preda lastništvo nad določenimi
///        elementi terke izhodnemu izrazu, zato je te elemente treba
///        pobrisati ven iz destruktorja
///      - dejanske terke razširimo z dodatnim poljem, ki vsebuje
///        kazalec na starševsko ovojnice
///      - funkcijske ovojnice razširimo z glavo
///      - kodo/e vseh vej case izraza razširimo s kodo, ki v starševski
///        ovojnici elemente, ki so premaknjeni, odstrani iz destruktorja
/// 6. Optimizacije: 20% pohitritev
/// 7. Implementacija tail-call
/// 8. GHC - one-line interpreter (kontinuacije?)
/// 9. Preimenovanje jezika: 'boomslang','b.lang','λb.lang','Blang'
/// 10. Odstranitev življenjskih dob / referenc. 
/// 12. Alokatorji.
/// 13. **Trenutna implementacija destruktorjev**: seznam močnih referenc, podvajanje kazalcev (free variables, strong pointers), ... 
///     - Kako deluje GC (če ovojnice ne vsebujejo kazalcev na pod-izraze, kako lahko GC algoritem
///     zgradi pomnilniški graf?)
///       - odgovor: če na objekt ni kazalca je seveda lahko izbrisan ... :)
///       - z neučakanim brisanjem bi lahko tudi v našem jeziku precej zmanjšali
///         footprint, saj pod-izrazov ne bi bilo potrebno hraniti
/// 14. Kako komisija oceni pregled literature?
/// 15. Polimorfizem. Kako bi ga definiral v enem stavkui. Razlika med overloadingom in ad-hoc polimorfizom.
///    C++ templates, generics, metaprogramming
/// 16. Zagovor: koga vse ponavadi študentje povabijo (družino?), darilo, ...
/// 17. Kakšna so bila vaša pričakovanja, ko ste mi zadali to temo ...


/// -----------------------------------------------------------------------

///
/// TODO LIST:
///
/// Legend:
/// ✅ - done
/// ❌ - todo
/// ⭕️ - work in progress / needs testing
///
///
/// ----
///
///
/// ✅ 1. Parse lifetime specifier for type constructor, i.e., x :: (MaybeRef ['A, a])
/// ✅    1.1. Type check
/// ✅      1.1.1. Make sure type constructors contain correct generic params for a given data type
/// ✅    1.2  Analyse lifetimes
/// ✅    1.3  Borrow check
///
///
/// ✅ 2. Data binding constructors should only contain lifetimes defined in the generic params clause
///
///
/// ❌ 3. List construction operator support (i.e. 1:2:[]])
///
///
/// ❌ 4. Bindings should pattern match (i.e. x:xs = [1,2,3], (a,b) = (10, true), ...)
///
///
/// ✅ 5. Handle self-borrows.
/// ✅    5.1 The implementation might not be robust (name shadowing , ...)
///
///
/// ✅ 6. All values, not just references, can have an associated lifetime.
///      Consider the programs:
///        - `if &condition then 10 else 20 [&'A Bool -> 'A Int]`
///        - `&10 + 20 [&'A Int -> Int -> 'A Int]`
///        - `&10 + &20 [&'A Int -> &'B Int -> ('A, 'B) Int]`
///        - `true || &false [Bool -> &'A Bool -> 'A Bool]`
///        - ...
/// ✅     6.1. If-then-else
/// ✅       6.1.1. Type check
/// ✅       6.1.2. Lifetime analysis
/// ✅       6.1.3. Thorough QA
/// ✅     6.2. Arithmetic
/// ✅       6.2.1. Type check
/// ✅       6.2.2. Lifetime analysis
/// ✅       6.2.3. Thorough QA
/// ✅     6.3. Logical
/// ✅       6.3.1. Type check
/// ✅       6.3.2. Lifetime analysis
/// ✅       6.3.3. Thorough QA
///
///
/// ✅ 7. Binary expressions: allow moving and borrowing at the same time
///      Example: `y = &x + x`
///  ✅ 7.1 Arithmetic
///  ✅ 7.2 Logical
///
///
/// ❌ 8. Functions capturing values strongly
///      Either prohibit, or implement FnOnce
///    [Linear Haskell article - orthogonal work to mine]
///
/// ❌ 9. Middle-end of the compiler should be completely decoupled from the back-end.
///       More specifically, frame evaluator and code generator must not have access to
///       memory. One important benefit of this, alongside better code segregation, is that
///       we can dump the IR into a file and interpret it separately from the compilation.
///
/// ❌ 10. Implement different kind of memory allocator
///        Currently, we have a simple bump alocator
///        - Arena allocator (bump allocator with expandable buffers)
///        - Slab allocator
