import Foundation

public func benchmark(_ run: () throws -> Void) rethrows -> Double {
  let current = currentTime()
  try run()
  return currentTime() - current
}

public func benchmark<T>(_ run: () throws -> T) rethrows -> (time: Double, value: T) {
  let current = currentTime()
  let value = try run()
  return (currentTime() - current, value)
}

public func currentTime() -> Double {
  #if os(macOS) || os(iOS)
  return CFAbsoluteTimeGetCurrent()
  #else
  return Date().timeIntervalSinceNow
  #endif
}
