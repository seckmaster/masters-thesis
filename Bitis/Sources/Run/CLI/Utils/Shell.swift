//
//  Shell.swift
//  
//
//  Created by Toni K. Turk on 17/08/2022.
//

import Foundation

@discardableResult
func safeShell(_ command: String, shell: String = "/opt/homebrew/bin/fish") throws -> String {
  let task = Process()
  let pipe = Pipe()
  
  task.standardOutput = pipe
  task.standardError = pipe
  task.arguments = ["-c", command]
  task.executableURL = URL(fileURLWithPath: shell)
  task.standardInput = nil
  
  try task.run()
  
  let data = pipe.fileHandleForReading.readDataToEndOfFile()
  let output = String(data: data, encoding: .utf8)!
  
  return output
}
