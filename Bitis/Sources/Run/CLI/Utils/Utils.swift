//
//  Utils.swift
//  
//
//  Created by Toni K. Turk on 17/08/2022.
//

import Foundation

struct FileNotFoundError: Error, CustomStringConvertible {
  let file: String
  
  var description: String {
    "File '\(file)' does not exist!"
  }
}

extension String {
  func loadFile() throws -> String {
    do {
      return String(data: try Data(contentsOf: URL(fileURLWithPath: self)), encoding: .utf8) ?? ""
    } catch {
      throw FileNotFoundError(file: self)
    }
  }
}

func resolveFilePath(_ file: String, includeFilePaths: [String]) throws -> (code: String, fullPath: String) {
  do {
    return (try file.loadFile(), file)
  } catch {
    for includePath in includeFilePaths {
      let filePath = "\(includePath)/\(file)"
      if let code = try? filePath.loadFile() {
        return (code, filePath)
      }
    }
    throw FileNotFoundError(file: file)
  }
}

let DefaultIncludePaths = try! safeShell("echo $LBC_INCLUDE_PATHS")
  .split(separator: " ", omittingEmptySubsequences: true)
  .map { $0.trimmingCharacters(in: .whitespacesAndNewlines) }

func loadContentsOfADirectory(
  path: String,
  recursive: Bool = true,
  fileExtensions: Set<String>,
  fileManager: FileManager
) throws -> Set<String> {
  var files = Set<String>()
  var visited = Set<String>()
  
  func work(path: String) throws {
    let contents = try fileManager.contentsOfDirectory(atPath: path)
    for name in contents {
      let wholePath = "\(path)/\(name)"
      
      guard !visited.contains(wholePath) else { continue }
      visited.insert(wholePath)
      
      var isDirectory: ObjCBool = false
      fileManager.fileExists(
        atPath: wholePath,
        isDirectory: &isDirectory)
      
      if isDirectory.boolValue {
        guard recursive else { continue }
        try work(path: wholePath)
      } else if fileExtensions.contains(URL(string: name)?.pathExtension ?? "") {
        files.insert(wholePath)
      }
    }
  }
  
  try work(path: path)
  return files
}
