//
//  Error.swift
//  
//
//  Created by Toni K. Turk on 12/01/2022.
//

import Foundation
import Bitis

func prettyError(
  code: String,
  position: Lexer.Position,
  error: String
) -> String {
  if position.start.line == position.end.line {
    let (spaces, line) = (0, line(position.start.line, code: code))
    return """
      \(position.start.line) | \(line)
      \(indent(position.start.line.description.count + 2 - spaces, string: singleLineCarrets(for: position, indent: min(0, -spaces))))
    
    \(error)
    """
  } else {
    let lines = lines(for: position, code: code)
    let (carrets1, carrets2) = multiLineCarrets(for: position)
    return """
    \(indent(position.start.line.description.count + 5, string: carrets1))
    \(lines)
    \(indent(position.start.line.description.count + 5, string: carrets2))
    
    \(error)
    """
  }
}

fileprivate func line(
  _ line: Int,
  code: String
) -> String {
  guard line > 0 else { return "" }
  var i = 0
  var buffer: String?
  for char in code {
    if buffer == nil {
      if char == "\n" { i += 1; continue }
      if i + 1 == line { buffer = String(char) }
    } else {
      guard char != "\n" else { break }
      buffer!.append(char)
    }
  }
  guard let buffer = buffer else {
    print("⚠️ Warning: an error occured while analysing the case-lifted AST; this should not happen!")
    return ""
  }

  return buffer
}

fileprivate func lines(
  for position: Lexer.Position,
  code: String
) -> String {
  (position.start.line...position.end.line)
    .map { "  \($0) | " + line($0, code: code) }
    .joined(separator: "\n")
}

fileprivate func singleLineCarrets(
  for position: Lexer.Position,
  carret: String = "~",
  text: String = "-- Here.",
  indent: Int
) -> String {
  guard position != .zero else { return "" }
  guard position.start != position.end else { return "" }
  let spacing1 = (0..<position.start.column+indent).reduce(into: "") { acc, _ in acc.append(" ") }
  let spacing2 = (position.start.column ..< position.end.column - 1)
    .reduce(into: "") { acc, _ in acc.append("~") }
    .dropLast()
  if spacing2.isEmpty {
    return "\(spacing1)\(carret) \(text)"
  }
  return "\(spacing1)\(carret)\(spacing2)\(carret) \(text)"
}

fileprivate func multiLineCarrets(for position: Lexer.Position) -> (String, String) {
  let spacing1 = (0...max(0, position.start.column - 2)).reduce(into: "") { acc, _ in acc.append(" ") }
  let spacing2 = (0...max(0, position.end.column - 3)).reduce(into: "") { acc, _ in acc.append("~") }
  let carret1 = "\(spacing1)↙ -- Starts here."
  let carret2 = "\(spacing2)↗ -- Ends here."
  return (carret1, carret2)
}

fileprivate func trim(spaces string: String) -> (Int, String) {
  var spaces = 0
  for char in string {
    if char == " " { spaces += 1 }
    else { break }
  }
  return (spaces, string.substring(from: string.index(string.startIndex, offsetBy: spaces)))
}

fileprivate func indent(_ indent: Int, string: String) -> String {
  guard indent > 0 else { return string }
  let indent = [String](repeating: " ", count: indent).joined()
  return string
    .split(separator: "\n")
    .map { indent + $0 }
    .joined(separator: "\n")
}
