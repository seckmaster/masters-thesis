//
//  REPL.swift
//  
//
//  Created by Toni K. Turk on 15/08/2022.
//

import Foundation
import ArgumentParser
import Bitis
import Bow
import SwiftParsec
import Rainbow

struct REPL: ParsableCommand {
  @Argument(help: "Source code file.")
  var file: String?
  
  @ArgumentParser.Option(name: .shortAndLong, help: "If set to `true`, thunk for constants will be pre-computed.")
  var evaluateConstants: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Toggle lazy evaluation.")
  var lazyEvaluation: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Toggle destructors.")
  var destructors: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Size of the heap.")
  var memorySize: Byte = 1 << 20
  
  @ArgumentParser.Option(name: .long, help: "Trace program execution.")
  var trace: Bool = false
  
  @ArgumentParser.Option(name: .long, help: "Execution mode [debug/release].")
  var executionMode: Run.ExecutionMode = .release
  
  @ArgumentParser.Option(name: .customShort("I"), help: "Library include path(s).")
  var includePaths: [String] = []
  
  func run() throws {
    print(header)

    signal(SIGINT, SIG_IGN) // Make sure the signal does not terminate the application.
    
    let allSearchPaths = DefaultIncludePaths + includePaths
    
    func cursor() {
      count += 1
      print(" \(count)> ".cyan, terminator: "")
    }
    
    func bitisParser(filename: String) -> (String) -> Bow.Either<Error, AST> {
      { code in
        switch Parser().parse(sourceName: filename, input: code) {
        case .left(let error):
          return .left(error)
        case .right(let ast):
          return .right(ast)
        }
      }
    }
    
    let memory = Bitis.Interpreter.Memory(size: memorySize)
    var count = 0
    var ast: AST
    if let file = file {
      let result = execute(
        filename: file,
        code: String(data: try Data(contentsOf: URL(fileURLWithPath: file)), encoding: .utf8) ?? "",
        fileParser: bitisParser(filename: file),
        importParser: bitisParser(filename: file),
        includeSearchPaths: allSearchPaths,
        codegenConfiguration: .init(
          isLazyEvaluationEnabled: lazyEvaluation,
          shouldAutomaticallyEvaluateConstants: evaluateConstants,
          envokeDestructors: destructors,
          isRunningInREPL: true),
        ast: .init(bindings: []),
        memory: memory,
        traceOutputHandle: trace ? .standardOutput : nil,
        executionMode: executionMode.interpreter
      )
      switch result {
      case let .success((output, ast_)):
        ast = ast_
        if !output.isEmpty { print(output) }
      case .failure(let error):
        print(String(describing: error).red)
        ast = .init(bindings: [])
      }
    } else {
      ast = .init(bindings: [])
    }
    
    while true {
      cursor()
      guard let line = readLine(strippingNewline: true) else { break }
      
      if line.isEmpty { continue }
      if line.starts(with: ":") {
        if line == ":q" { break }
        if line == ":Q" { print("Enter :q to quit!".magenta); count -= 1; continue }
        if line.starts(with: ":t") {
          guard let arg = line.split(separator: " ").dropFirst().first else {
            print("Enter a valid identifier (:t <identifier>)!".magenta)
            continue
          }
          var found = false
        loop: for binding in ast.bindings {
            switch binding {
            case .binding(_, let name, _, let type?, _, _) where name == arg,
                 .prototype(_, let name, _, let type?) where name == arg:
              print("\(arg) :: \(type.description)".lightGreen)
              found = true
              break loop
            case _:
              continue
            }
          }
          if !found {
            print("Unknown variable '\(arg)'!".magenta)
          }
          continue
        }
        print("Unknown command '\(line.trimmingCharacters(in: .whitespaces))'!".magenta)
        count -= 1
        continue
      }
      
      let result = execute(
        filename: "REPL.bt",
        code: line,
        fileParser: bindingOrExpressionParser(fileName: "REPL.bt", using: .init()),
        importParser: bitisParser(filename: "REPL.bt"),
        includeSearchPaths: allSearchPaths,
        codegenConfiguration: .init(
          isLazyEvaluationEnabled: lazyEvaluation,
          shouldAutomaticallyEvaluateConstants: evaluateConstants,
          envokeDestructors: destructors,
          isRunningInREPL: true
        ),
        ast: ast,
        memory: memory,
        traceOutputHandle: trace ? .standardOutput : nil,
        executionMode: executionMode.interpreter)
      switch result {
      case let .success((result, newAst)):
        if !result.isEmpty {
          print(result)
        }
        ast = newAst
      case .failure(let error):
        print(String(describing: error).red)
      }
    }
  }
}

func execute(
  filename: String,
  code: String,
  fileParser: @escaping (String) -> Bow.Either<Error, AST>,
  importParser: @escaping (String) -> Bow.Either<Error, AST>,
  includeSearchPaths: [String],
  codegenConfiguration: IRState.Configuration,
  ast: AST,
  memory: Bitis.Interpreter.Memory,
  traceOutputHandle: FileHandle?,
  executionMode: Bitis.Interpreter.ExecutionMode,
  runtimeInfo: Box<Interpreter.RuntimeInfo>? = nil
) -> Result<(String, AST), Error> {
  let output = TextStream()
  let astBox = Box<AST>(value: .init(bindings: []))
  
  // TODO: - Make it configurable by CLI
  let optimizer = Optimizer(optimizations: [
    .constantPropagation,
    .constantFolding,
    .algebraicSimplifications
  ])
  
  let compilerPipeline = frontEndPipeline(
    fileParser: fileParser,
    importParser: importParser,
    filename: filename,
    includeSearchPaths: includeSearchPaths,
    mapAstBeforeTypechecking: { .init(bindings: ast.bindings + $0.bindings) },
    ast: astBox
  )
  >>- middleEndPipeline(
    memory: memory,
    config: codegenConfiguration,
    optimizer: optimizer,
    mapAstBeforeCodeGen: {
      .init(bindings: $0.bindings.suffix(astBox.value.bindings.count - ast.bindings.count))
    }
  )
  >>- backEndPipeline(
    output: output,
    traceOutputHandle: traceOutputHandle,
    executionMode: executionMode,
    isRunningREPL: true,
    runtimeInfo: runtimeInfo
  )
  
  let (_, result) = benchmark {
    compilerPipeline(code)
  }
  
  switch result.isLeft {
  case true:
    return .failure(result.leftValue)
  case false:
    return .success((output.buffer, astBox.value))
  }
}

fileprivate var anonymousVariableCounter = 1

fileprivate func bindingOrExpressionParser(
  fileName: String,
  using parser: Parser
) -> (String) -> Bow.Either<Error, AST> {
  {
    do {
      let parser = parser.statement.attempt.map { AST(bindings: [$0]) } <|> parser.expression.map {
        defer { anonymousVariableCounter += 1 }
        return .init(bindings: [
          .binding(
            position: .zero,
            name: "$\(anonymousVariableCounter)",
            cases: [.init(name: "$\(anonymousVariableCounter)", position: .zero, parameters: [], body: $0)]),
          .print(position: .zero, expression: .name(position: .zero, identifier: "$\(anonymousVariableCounter)")),
        ])
      }
      let ast = try parser.run(sourceName: fileName, input: $0)
      return .right(ast)
    } catch {
      return .left(error)
    }
  }
}
