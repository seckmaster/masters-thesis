//
//  Main.swift
//  
//
//  Created by Toni K. Turk on 15/08/2022.
//

import Foundation
import ArgumentParser

struct Main: ParsableCommand {
  static var configuration: CommandConfiguration {
    .init(
      commandName: "Blang",
      abstract: "Blang compiler and tools.",
      version: version,
      subcommands: [Run.self, REPL.self, Analyse.self]
    )
  }
}

let version = "v0.2"
let header = """
\(join(strings: [lambda, b, dot, lang], colors: [ { $0.lightMagenta }, { $0.lightBlue }, { $0.lightMagenta }, { $0.lightBlue }]))
Version: \(version)



"""

// https://textkool.com/en/test-ascii-art-generator

fileprivate let lambda = """
⠀⠀⢀⣤⣤⣤⣤⣀⠀⠀⠀⠀⠀⠀⠀⠀⠀
⠀⢸⣿⣿⣿⣿⣿⣿⣷⡀⠀⠀⠀⠀⠀⠀⠀
⠀⢸⣿⠟⠉⠙⣿⣿⣿⣷⠀⠀⠀⠀   
⠀ ⠙ ⠀⠀⢸⣿⣿⣿⣧⠀⠀⠀⠀⠀ 
⠀⠀ ⠀⠀⠀⣼⣿⣿⣿⣿⣆⠀⠀⠀⠀⠀
⠀⠀⠀⠀⠀⣼⣿⣿⣿⣿⣿⣿⡀⠀⠀⠀⠀
⠀⠀⠀⠀⣴⣿⣿⣿⠟⣿⣿⣿⣷⠀⠀⠀⠀
⠀⠀⠀⣰⣿⣿⣿⡏⠀⠸⣿⣿⣿⣇⠀⠀⠀
⠀⠀⢠⣿⣿⣿⡟⠀⠀⠀⢻⣿⣿⣿⡆⠀⠀
⠀⢠⣿⣿⣿⡿⠀⠀⠀⠀⠀⢿⣿⣿⣷⣤⡄
⢀⣾⣿⣿⣿⠁⠀⠀⠀⠀⠀⠈⠿⣿⣿⣿⡇
                   
                   
                   
"""

fileprivate let dot = """
    
    
    
    
    
    
    
    
    
 ██ 
░░  
  
  
  
  
"""

fileprivate let b = """
                                         
                                         
                                         
 █████    
░░███     
 ░███████ 
 ░███░░███
 ░███ ░███
 ░███ ░███
 ████████ 
░░░░░░░░  
          
          
          
"""

fileprivate let lang = """
                                         
                                         
                                         
 ████                               
░░███                               
 ░███   ██████   ████████    ███████
 ░███  ░░░░░███ ░░███░░███  ███░░███
 ░███   ███████  ░███ ░███ ░███ ░███
 ░███  ███░░███  ░███ ░███ ░███ ░███
 █████░░████████ ████ █████░░███████
░░░░░  ░░░░░░░░ ░░░░ ░░░░░  ░░░░░███
                            ███ ░███
                           ░░██████ 
                            ░░░░░░  
"""

fileprivate func join(
  strings: [String], 
  separator: String = " ",
  colors: [(String) -> String]
) -> String {
  precondition(strings.count == colors.count)
  guard !strings.isEmpty else { return "" }
  
  let lines = strings.enumerated().map { idx, el in el.split(separator: "\n").map { colors[idx](String($0)) } }
  var buffer = ""
  for i in 0..<lines.max(by: { $0.count < $1.count })!.count {
    buffer += (0..<strings.count).map { String(lines[$0][safe: i] ?? "") }.joined(separator: separator)
    buffer += "\n"
  }
  return buffer
}

extension Collection {
  subscript (safe index: Index) -> Element? {
    guard indices.contains(index) else { return nil }
    return self[index]
  }
}
