//
//  Run.swift
//  
//
//  Created by Toni K. Turk on 06/07/2022.
//

import Foundation
import ArgumentParser
import Bitis
import Bow
import SwiftParsec
import Rainbow

private var spinners: [Spinner] = [.default]

struct Run: ParsableCommand {
  static var configuration: CommandConfiguration {
    .init(commandName: "run")
  }
  
  @Argument(help: "Source code file.")
  var file: String
  
  @Argument(help: "Arguments.")
  var args: [String] = []
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Silent any warnings generated by the compiler.")
  var silentCompilerWarnings: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "If set to `true`, thunk for constants will be pre-computed.")
  var evaluateConstants: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Toggle lazy evaluation.")
  var lazyEvaluation: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Toggle destructors.")
  var destructors: Bool = true
  
  @ArgumentParser.Option(name: .shortAndLong, help: "Size of the heap.")
  var memorySize: Byte = 1 << 21
  
  @ArgumentParser.Option(name: .long, help: "Trace program execution.")
  var trace: Bool = false
  
  @ArgumentParser.Option(name: .long, help: "Output file for program trace. Only applicable if 'trace=true'.")
  var traceOutputFile: String?
  
  @ArgumentParser.Option(name: .long, help: "Execution mode [debug/release].")
  var executionMode: ExecutionMode = .release
  
  @ArgumentParser.Option(name: .customShort("I"), help: "Library include path(s).")
  var includePaths: [String] = []
  
  mutating func run() throws {
    print(header)
    
    ARE_WARNINGS_ENABLED = !silentCompilerWarnings
    
    try interpret(
      filename: file,
      includePaths: includePaths + DefaultIncludePaths,
      codegenConfiguration: .init(
        isLazyEvaluationEnabled: lazyEvaluation,
        shouldAutomaticallyEvaluateConstants: evaluateConstants,
        envokeDestructors: destructors,
        isRunningInREPL: false
      ),
      memorySize: memorySize,
      trace: trace,
      traceOutputFile: traceOutputFile,
      executionMode: executionMode.interpreter
    )
  }
}

extension Run {
  enum ExecutionMode: String, ExpressibleByArgument {
    case debug, release
    
    var interpreter: Bitis.Interpreter.ExecutionMode {
      switch self {
      case .debug:
        return .debug
      case .release:
        return .release
      }
    }
  }
}

func interpret(
  filename: String,
  includePaths: [String],
  codegenConfiguration: IRState.Configuration,
  memorySize: Byte,
  trace: Bool,
  traceOutputFile: String?,
  executionMode: Bitis.Interpreter.ExecutionMode
) throws {
  let code = try resolveFilePath(filename, includeFilePaths: []).code
  
  let output = TextStream()
  output.flush = true
  
  let bitisParser = { (code: String) -> Bow.Either<Error, AST> in
    switch Parser().parse(sourceName: filename, input: code) {
    case .left(let error):
      return .left(error)
    case .right(let ast):
      return .right(ast)
    }
  }
  
  // TODO: - Make it configurable by CLI
  let optimizer = Optimizer(optimizations: [
    .constantPropagation,
    .constantFolding,
    .algebraicSimplifications
  ])
  
  let (time, result) = benchmark { () -> Bow.Either<Error, ()> in
    _ = traceOutputFile.map { FileManager.default.createFile(atPath: $0, contents: nil) }
    let traceOutputHandle = trace ? traceOutputFile.flatMap { FileHandle(forWritingAtPath: $0) } ?? .standardOutput : nil
    let pipeline = interpretCompilerPipeline(
      parser: bitisParser,
      filename: filename,
      includeSearchPaths: includePaths,
      memorySize: memorySize,
      config: codegenConfiguration,
      output: output,
      traceOutputHandle: traceOutputHandle,
      executionMode: executionMode,
      isRunningREPL: false,
      optimizer: optimizer
    )
    return pipeline(code)
  }
  
  let reportError: (String, Lexer.Position, String) -> Void = { msg, pos, path in
    spinners[0].fail(text: "Failed               ")
    spinners[0].stop()
    print()
    print(
      "In file '\(URL(string: path)!.lastPathComponent)':".red, "\n\n",
      prettyError(
        code: try! resolveFilePath(path, includeFilePaths: []).code,
        position: pos,
        error: msg
      ).red
    )
  }
  
  switch result.isLeft {
  case true:
    switch result.leftValue {
    case is Bitis.Interpreter.RuntimeError,
         is Bitis.Interpreter.Memory.Error:
      print("Runtime error:".red, String(describing: result.leftValue).red)
    case let error as FileNotFoundError:
      spinners[0].fail(text: "Failed               ")
      spinners[0].stop()
      print()
      print("File '\(URL(string: error.file)!.lastPathComponent)' not found!".red)
    case let error as BacktraceError:
      reportError(
        String(describing: result.leftValue).red,
        error.position,
        error.fullFilePath
      )
    case let error as Bitis.ParseError:
      reportError(
        error.description.red,
        error.position,
        error.fullFilePath
      )
    case _:
      fatalError()
    }
    print()
  case false:
    if output.flush {
      print()
    } else {
      print(output.buffer, "\n")
    }
  }
  print("Executed in:", String(format: "%.2fs".bold, time))
}

func interpretCompilerPipeline(
  parser: @escaping (String) -> Bow.Either<Error, AST>,
  filename: String,
  includeSearchPaths: [String],
  memorySize: Byte,
  config: IRState.Configuration,
  output: TextOutputStream,
  traceOutputHandle: FileHandle?,
  executionMode: Interpreter.ExecutionMode,
  isRunningREPL: Bool,
  optimizer: Optimizer,
  ast: Box<AST>? = nil
) -> (String) -> Bow.Either<Error, ()> {
  let script = URL(string: filename)!.lastPathComponent.cyan
  spinners[0].text = "Compiling \(script)"
  spinners[0].start()
  
  let start = currentTime()
  return frontEndPipeline(
    fileParser: parser,
    importParser: parser,
    filename: filename,
    includeSearchPaths: includeSearchPaths
  )
  >>- middleEndPipeline(
    memorySize: memorySize,
    config: config,
    optimizer: optimizer
  ) >>- {
    let delta = currentTime() - start
    spinners[0].succeed()
    print(" ✶ Compiled in: \(String(format: "%.2fs", delta).cyan).")
    print("Running script: \(script):\n")
    spinners[0].stopAndClear()
    return .pure($0)^
  }
  >>- backEndPipeline(
    output: output,
    traceOutputHandle: traceOutputHandle,
    executionMode: executionMode,
    isRunningREPL: isRunningREPL
  )
}

func frontEndPipeline(
  fileParser: @escaping (String) -> Bow.Either<Error, AST>,
  importParser: @escaping (String) -> Bow.Either<Error, AST>,
  filename: String,
  includeSearchPaths: [String],
  mapAstBeforeTypechecking mapAST: ((AST) -> AST)? = nil,
  ast: Box<AST>? = nil
) -> (String) -> Bow.Either<Error, AST> {
  var resolvedImports = Set<String>()
  func resolveImportPipeline() -> (String) throws -> Bow.Either<Error, AST> {
    { module in
      guard !resolvedImports.contains(module) else {
        return .pure(.init(bindings: []))^
      }
      let (code, fullFilePath) = try resolveFilePath(
        module+".hs",
        includeFilePaths: includeSearchPaths
      )
      resolvedImports.insert(module)
      return parse(fileName: fullFilePath, using: importParser)(code)
        .flatMap(resolveImports(compilerPipeline: resolveImportPipeline()))^
    }
  }
  
  // @MARK: - Lexer & Parser
  return parse(fileName: filename, using: fileParser)
  
  // @MARK: - Resolve imports
  >>- resolveImports(compilerPipeline: resolveImportPipeline())
  
  // @MARK: - Transform AST if necessary
  >>- { .pure(mapAST?($0) ?? $0)^ }
  
  // @MARK: - Semantic analysis - type checking
  >>- typeChecker
  // @MARK: - Semantic analysis - memory management
  >>- lifetimeAnalyser
  >>- borrowChecker(discard: true)
  
  >>- liftCases
  
  // @TODO: - Doing this part twice!
  
  // @MARK: - Semantic analysis - type checking
  >>- typeChecker
  // @MARK: - Semantic analysis - memory management
  >>- lifetimeAnalyser
  >>- borrowChecker()
  
  >>- {
    ast?.value = $0
    return .pure($0)^
  }
}

// @MARK: - Middle-end
func middleEndPipeline(
  memorySize: Byte,
  config: IRState.Configuration,
  optimizer: Optimizer,
  mapAstBeforeCodeGen mapAST: ((AST) -> AST)? = nil
) -> (AST) -> Bow.Either<Error, (CodeGenerator.CodeChunk, Interpreter.Memory)> {
  stackFrames
  >>- { ast, state in .pure((mapAST?(ast) ?? ast, state))^ }
  >>- generateIR(
    memorySize: memorySize,
    config: config,
    optimizer: optimizer
  )
  >>- linearizeCode
}

// @MARK: - Middle-end
func middleEndPipeline(
  memory: Interpreter.Memory,
  config: IRState.Configuration,
  optimizer: Optimizer,
  mapAstBeforeCodeGen mapAST: ((AST) -> AST)? = nil
) -> (AST) -> Bow.Either<Error, (CodeGenerator.CodeChunk, Interpreter.Memory)> {
  stackFrames
  >>- { ast, state in .pure((mapAST?(ast) ?? ast, state))^ }
  >>- generateIR(
    memory: memory,
    config: config,
    optimizer: optimizer
  )
  >>- linearizeCode
}

// @MARK: - Back-end / interpreter
func backEndPipeline(
  output: TextOutputStream,
  traceOutputHandle: FileHandle?,
  executionMode: Interpreter.ExecutionMode,
  isRunningREPL: Bool,
  runtimeInfo: Box<Interpreter.RuntimeInfo>? = nil
) -> (CodeGenerator.CodeChunk, Interpreter.Memory) -> Bow.Either<Error, ()> {
  interpret(
    outputStream: output,
    traceOutputHandle: traceOutputHandle,
    executionMode: executionMode,
    isRunningREPL: isRunningREPL,
    runtimeInfo: runtimeInfo
  )
}

extension Spinner {
  static let `default` = Spinner(pattern: .dots)
}
