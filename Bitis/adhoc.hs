class Square a where
    square :: a -> a
    
instance Square Int where
    square x = x * x

instance Square Float where
    square x = x * x

f :: (Square a, Show a) => a -> a
f x = square x
