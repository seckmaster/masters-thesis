# Ownership manifesto

At the heart of memory management in Bitis, there is a pair of promises:
  - The lifetime of values is explicit and in control of the programmer.
  - Bitis guarantees that no pointer will be dereferenced after it has been freed (a common issue in C/C++ known as the *dangling pointer*).

Several languages, including Haskell, fullfil the second promise using garbage collection (GC). But in exchange of using GC, the first promise is broken. Bitis is able to fullfil both promises by restricting the way values are constructed, moved from one place to another and shared between several pieces of code.

## Ownership

In Bitis, every value has exactly one owner which determines its lifetime. When the owner is freed (dropped), the owned value is dropped as well. The rule of a single ownership makes it easy for the programmer to infer the lifetime of a value by simply looking at the code.

A variable ownes its value; when the variable leaves the scope in which it's declared, the value it owns is automatically dropped.

```Haskell
_ = let 
  x = [1,2,3]
in true -- `[1,2,3]` is dropped here
```

<img src="ownership1.png" width=75% border=1px>

Variable `x` is the owner of the list `[1,2,3]`. Note that `x` only owns `Cons 1 *` directly; the other two elements are owned by the previous elements in the list (this is a common way lists are organized in functional programming languages), forming a **hiearchy** (tree) of objects. When `x` goes out of scope, the whole tree is dropped automatically.

In general, every Algebraic Data Type (ADT) owns all the values it contains:

```Haskell
tup = (true, [1,2])
```

<img src="ownership2.png" width=75% border=1px>

The consequences of the ownership relations presented thus far are the following:
  - every value has a single owner, making it easy to decide when to drop it,
  - a single value may own other values and those values may own values of their own.

Thus, values form *trees*. At the root of each tree is a variable, and when the variable goes out of scope, the whole tree is dropped. In Bitis, it is not possible to construct arbitrary graphs of objects, which is possible in other languages (even Haskell). This limitation, however, is precisely the mechanism which enables the compiler to perform powerful analysis.

Having said that, the language would be far too rigid to be useful with just the rules given thus far. Bitis extends the ownership rules with two additional concepts:
  - Values can be **moved** from owner to owner.
  - Values can be **borrowed** using references.

## Moves

Standard operations, like assigning a value to a variable, applying function to an argument, returning a value from a function, etc., *move* the value - the original owner relinquishes ownership of the value and transfers it to the destination. The original owner becomes *suspended* and can no longer be used in the program:

```Haskell
x = 10
y = x -- `x` transfers ownership of its value to `y` and becomes suspended
z = x -- error: `x` has been moved
```

```Haskell
f :: a -> ()
f _ = ()

x = 10
y = f x -- `x` transfers ownership to the function
z = x -- error: `x` has been moved
```

```Haskell
f :: () -> Int
f _ = 10 -- the function transfers the ownership of `10` to the caller

x = f () -- `x` owns the returned value
```

```Haskell
x = 10
y = 20
z = false
w = if z then x else y -- `z`, `x`, and `y` are moved
a = x -- error: `x` has been moved
b = y -- error: `y` has been moved
c = z -- error: `z` has been moved
```

Pattern matching is another way of taking ownership of values:

```Haskell
f :: [Int] -> ()
f [] = ()
f (x:xs) = f xs -- `x` takes ownership of the head, `xs` of the tail; 
                -- the ownership of the tail is further moved to a recursive call of `f`

x = [1,2,3]
_ = f x -- `x` is moved to the function; the function 
        -- consumes and drops all elements of the list
```

## References

Instead of moving a value, Bitis allows it to be borrowed using a reference. References do not change the lifetime of a value. It's actually the opposite: a reference to a value must never outlive its referent.

Let's take a look at why borrowing is useful and how it works with a simple example; imagine we would like to sum the values of a list:

```Haskell
f :: [Int] -> Int
f [] = 0
f (x:xs) = x + (f xs)

x = [1,2,3]
y = f x -- `y` equals to 6
...
```

The above program works, but as we've just learned, calling a function moves the argument, making `x` suspended:

```Haskell
...
z = f x -- error: `x` has been moved
```

This is, in most cases, rather limiting. The problem can be solved using references:

```Haskell
f :: &'A [Int] -> 'A Int
f &[] = 0 -- in future, '&' should be redundant
f (x:xs) = x + (f xs)
...
```

The type of the argument becomes `&'A [Int]` and the type of return value `'A Int`. The `&` signs means "reference", and `'A` represents a generic lifetime. The type `&'A [Int]` can be read as: "a reference to a list of integers with any lifetime 'A".

```Haskell
...
x = [1,2,3]
y = f (&x) -- `&x` creates a reference to `x`
z = f (&x) -- ok
```

There is no limit to how many references can exist simultaneously to any value, as long as they do not outlive the referent.

Because Bitis is lazy, there is an important caveat; the value returned by function `f`, even though it is not a reference, also contains a lifetime. This is because the computation of the value is delayed (laziness) and performed when strictly needed (or never). For this reason the returned value (the **thunk**) can live only as long as the reference using which the value is computed. Note that this is not necessary in Rust because it is a strict language.

The compiler must make sure that a reference never outlives its referent. Let's take a look at a couple of examples:

```Haskell
x = let
  a = 10
in &a -- error: the reference outlives `a`
```

```Haskell
data Ref ['A, a] = R (&'A a) | N

f :: () -> (Ref ['A, Int])
f () = let
  x = 10
  y = if true then N () else R (&x)
in y -- error: `y` contains a reference to the local `x`
```

```Haskell
data Tree ['A] = Node (&'A (Tree ['A])) (&'A (Tree ['A])) | Leaf
a = let
  x = Leaf ()
  y = Node (&x) (&x)
  z = Node (&y) (&y)
in z -- error
```

There is an exception to this rule - a value is allowed to contain a reference to itself (a concept called **self-borrow**). Again, self-borrow is only possible because the language is lazy (in Rust, a self-borrow cannot be created). The following program demonstrates why self-borrows are useful:

```Haskell
data ListRef ['A, a] = Cons (&'A a) (ListRef ['A, a]) | Nil
data Graph2 ['A] = Node Int (ListRef ['A, (Graph2 ['A])])

fmap :: (a -> (Graph2 ['A])) -> [a] -> [(Graph2 ['A])]
-- implementation omitted

-- convert a reference to a list to a list of references
convert :: &'A [a] -> (ListRef ['A, a])
-- implementation omitted

-- generate a clique using the given list as
-- the list of nodes
clique :: [Int] -> [(Graph2 ['A])]
clique xs = let
  c :: [(Graph2 [])]
  c = fmap (\\i -> Node i (convert (&c))) xs -- self-borrow: `c` contains a borrow of `c`
in c
```

Even though `clique` returns a value containing a reference to a local value (itself), the referenced value is moved at the same time, which is why self-borrowing is a valid construct and does not create memory errors.


While there exists at least one reference to a value, the value cannot be moved. 

```Haskell
x = 10
y = &x
z = x -- error: cannot move `x` while it's borrowed
```

## Cycles

We said that values in Bitis form trees. Technically, this is not completely correct because of two reasons:
  1. Bitis is lazy
  2. References

The simplest possible **strong** cycle is that of a single value pointing to itself:

```Haskell
x :: Int
x = x - error: a strong cycle
```

<img src="cycle1.png" width=75% border=1px>

There is a foundamental problem to the above program; when `x` goes out of scope, it tries to drop what it owns - which is `x`, so `x` drops what it owns, which is `x` ... entering a never ending cycle of memory cleanup, haulting the program! This is why (strong) cycles are not allowed.

A **strong** cycle is a cycle in which all edges are owning pointers. On the other hand, a **weak** cycle is a cycle in which at least one edge is a borrowing reference.

We can solve the above problem by creating a weak cycle:

```Haskell
_ = let 
  x :: &Int
  x = x -- ok, a weak cycle
in 0
```

<img src="cycle2.png" width=75% border=1px>

*(Note that a dotted arrow represents a borrow)*

Now `x` contains a borrow of itself, so no memory issue is present. 

Let's take a look at a couple more examples of cycles:

```Haskell
x :: Int
y :: Int
x = y
y = x -- error: a strong cycle
```

```Haskell
x :: Int
y :: Int
x = (\\y -> y) y
y = x -- error: a strong cycle
```

```Haskell
a :: Int
b :: Int
a = b
b = (\\i -> a) () -- error: a strong cycle
```

```Haskell
x :: Int
y :: Int
z = 10
x = y
y = if true then z else x -- cycle is created only if the false branch executes (which it does not in this case);
                          -- however, the compiler is conservative and considers the program invalid regardless
                          -- of the value of the condition expression
```

```Haskell
data List [a] = Cons a (List a) | Nil

x :: (List Int)
y :: (List Int)
x = Cons 10 y
y = Cons 20 x -- error: a strong cycle
```

```Haskell
data List ['A] = Cons (&'A Int) (List ['A]) | Nil

gen :: &'A Int -> (List ['A])
gen a = let
  x :: (List [])
  y :: (List [])
  x = Cons a y
  y = Cons a x -- error: a strong cycle
in `
```

And an example, which we already saw, that generates a clique using weak cycles:

```Haskell
data ListRef ['A, a] = Cons (&'A a) (ListRef ['A, a]) | Nil
data Graph2 ['A] = Node Int (ListRef ['A, (Graph2 ['A])])

fmap :: (a -> (Graph2 ['A])) -> [a] -> [(Graph2 ['A])]
-- implementation omitted

-- convert a reference to a list to a list of references
convert :: &'A [a] -> (ListRef ['A, a])
-- implementation omitted

clique :: [Int] -> [(Graph2 ['A])]
clique xs = let
  c :: [(Graph2 [])]
  c = fmap (\\i -> Node i (convert (&c))) xs -- ok, a weak cycle
in c
```


The manifesto is written with the help of [1].

[1] J. Blandy, J. Ordendorff : Programming Rust

---

# Implementation

The compiler, as it currently stands, is split into two separate memory management related phases, each with its own responsibilities. These are:
  1. Lifetime analyser
  2. Borrow checker

## Lifetime analyser

The main responsibility of the lifetime analyser is to compute and store the lifetimes of variables and values. Using the computed lifetimes it also makes sure that programs are, from its standpoint correct. It rejects programs with the following types of errors:
  - moving a reference to a local value out of scope (prevents dangling pointers)
  - referencing constants (the future versions of the compiler will, at some point, support this)
  - a reference in a function prototype does not contain a lifetime specifier (i.e. `f :: &'Int -> ()`)
  - variable prototype does contain a lifetime specifier (i.e. `x :: &'A Int`)
  - function is called with an argument that has an inappropriate lifetime

We now present in detail how lifetimes are modeled, calculated, and analysed by the compiler.

### Lifetimes

Every lifetime contains a set of child lifetimes and a parent lifetime (except the `'STATIC` lifetime, which is the root lifetime), forming a tree of lifetimes:

<img src="lifetimes1.png" width=75% border=1px>

*Above diagram shows how the compiler represents lifetimes.*

When the program enters a new scope, it creates a new nested scope whose parent is the current lifetime with an empty set of children lifetimes.

There are three core operations on lifetimes that have to be supported efficiently:
  1. is lifetime A parent of lifetime B
  2. is lifetime A child of litetime B
  3. common parent of lifetimes A and B (remember that static lifetime is a parent of every other lifetime)

These operations are easily implemented using a prefix tree, as shown in the diagram.

To check whether a lifetime A is parent of lifetime B, we have to check whether A is the prefix of B (the exception, of course, is the `'STATIC` lifetime, which is parent of every lifetime, even itself). If the lifetimes live on the same scope (for example lifetimes `'aaa` and `'aab` live on the scope `'aa`), the lifetime is parent of the other if it's smaller alphabetically (i.e. the binding defined before another binding lives longer).

For example, the lifetime `'a` is parent of lifetimes `'aa`, `'aba`, and `'agbaaca`, but not of `'b`, `'STATIC`, and `'caaa`.

Whether a lifetime is a child of another is merely an opposite relation of the parent relation, and a common parent of two lifetimes is a lifetime with their common prefix (or `'STATIC` if their prefix is different).

For example, a common parent of `'baab` and `'bbc` is lifetime `'b`, and a common parent of lifetimes `'a` and `'b` is `'STATIC`.

Lifetimes can also be generic. To be able to differentiate between local and generic lifetimes, local lifetimes are lowercased (i.e. `'aa` or `'abba`) and generic lifetimes uppercased (i.e. `'A` or `'LIST`). A generic lifetime is used in function signatures to indicate that the function is polymorphic in the lifetime - it should work for any lifetime, either local or static. A lifetime in the function signature can also be `'STATIC`, indicating that this function only accepts values that live throughout the entire lifetime of the program.

### Lifetime analysis

Every binding is associated with two lifetimes:
  1. the **binding lifetime**, which represents the scope in which the variable is defined, and
  2. the **borrow lifetime**, which is a lifetime (or multiple lifetimes) of value(s) that the binding borrows

If a binding does not borrow anything, its borrow lifetime is empty.

Let's take a look at an example:

```Haskell
              --  [binding l.] --|-- [borrow l.]
x = let       -- `x`: ['STATIC] []
  a = true    -- `a`: ['aa] []
  b = 10      -- `b`: ['ab] []
  c = &(a, b) -- `c`: ['ac] ['aa, 'ab]
in c
```

The variable `x` is defined on the static scope, and the variables `a`, `b`, and `c` on the local scope `'a`. The borrow lifetime is empty for all variables except `c`, which borrows values from `a` and `b`. Obviously, the above program contains an error, as returning `c` out of the scope would create two dangling pointers. The compiler is able to prevent this by comparing the binding lifetime of `x` with the two borrow lifetimes of `c`. Because the lifetime `'a` is not a parent of the static lifetime, the compiler rejects the program with an error.

Let's take a look at another similar, but valid, program:

```Haskell
x = let            -- ['STATIC] []
  a = true         -- ['aa] []
  b = 10           -- ['ab] []
  c = let          -- ['ac] ['aa, 'ab]
    pair = &(a, b) -- ['acaa] ['aa, 'ab]
  in pair -- ok
in 10
```

Returning `pair` from the inner scope is valid, because both `a` and `b` outlive their borrows.

In general, when during the traversal of the AST the compiler visits a *name expression*, it does the following:
  - If the name expression is nested inside a reference, the lifetime of the name expression is the **binding** lifetime of the associated binding.
  - Otherwise, the lifetime of the name expression is the **borrow** lifetime.
  
The reasoning behind this logic is the following: when borrowing from a name, the lifetime of the reference is, obviously, the lifetime of the binding as the reference cannot outlive the binding; on the other hand, when the name is not behind a borrow, we are interested in what the associated binding itself is borrowing, not the scope it is defined in.

Function application has additional rules; the result of calling a function depends on the argument only if their lifetimes match. For example,

```Haskell
f :: &'A a -> &'A a
f a = a

x = let 
  a = 10     -- ['aa] []
  b = f (&a) -- ['ab] ['aa]
in b -- error: returing a borrow to a local value
```

Here `b` borrows from `a` because the result of `f` depends on its argument. The next example demonstrates the scenario where the result does not depend on the argument:

```Haskell
f :: &'A a -> ()
f a = a

x = let 
  a = 10     -- ['aa] []
  b = f (&a) -- ['ab] []
in b -- ok, the value of `b` does not depend on `a`
```

Or take a look at the next example:

```Haskell
f :: &'A a -> &'B a -> &'A a
f a b = b

a = 10            -- ['STATIC] []
x = let 
  b = 20          -- ['aa] []
  c = f (&a) (&b) -- ['ab] ['STATIC]
in c -- ok
```

Now let's modify the above function to return a tuple of two references instead:

```Haskell
f :: &'A a -> &'B a -> (&'A a, &'B a)
f a b = b

a = 10            -- ['STATIC] []
x = let 
  b = 20          -- ['aa] []
  c = f (&a) (&b) -- ['ab] ['STATIC, 'aa]
in c -- error: returning a borrow to a local value
```

Constructors can contain references as well. As making instances of data types in nothing else then invoking their constructors (i.e. function application), there is not much new to add. Nevertheless, we give a couple examples containing data types for completeness sake.

```Haskell
data Ref ['A] = R (&'A Int) | N

a = let
  x = 10
  y = N () -- ['aa] []
in y -- this is okay, no borrows inside `y`

b = let
  x = 10     -- ['aa] []
  y = R (&x) -- ['ab] ['aa]
in y -- error, `y` contains a borrow of `x`

c = let
  x = 10                                                      -- ['aa] []
  y = if (some very complex expression) then N () else R (&x) -- ['ab] ['aa]
in y -- error

d = let
  x = 10             -- ['aa] []
  y = [N (), R (&x)] -- ['aa] ['ab]
in y -- error
```

#### Self-borrows

Handling self-borrows is pretty straightforward; consider the folowing program:

```Haskell
data Ref ['A] = R &'A (Ref ['A])

x = let         -- ['STATIC] []
  a :: (Ref []) -- ['aa]
  a = R (&a)    -- ['aa] ['aa]
in a -- ok
```

As we can see, variable `a`'s borrow lifetime is the same as its binding lifetime. This means that the variable contains a borrow of itself and the program is valid.

## Borrow checker

Borrow checker prevents the following:
  - strong cycles
  - moving a suspended variable
  - moving a variable while it's borroed
  - moving out of borrow
  - borrowing from a moved value

### Cycles

To detect (strong) cycles, the compiler computes and stores the *dependencies* for each expression. A dependency is a name which is not behind a borrow. A set of dependecies of an expression is a union of dependencies of all its sub-expressions.

Examples:

```Python
dependencies(x) = { "x" }
dependencies(if true then x else 1) = { "x" }
dependencies(if c then x else y) = { "c", "x", "y" }
dependencies([x, y, true]) = { "x", "y" }
dependencies(f a b c]) = { "f", "a", "b", "c" }
dependencies((a, [b, c], f a)) = { "a", "b", "c", "f" }
dependencies(&a) = {}
dependencies(&(a, b)) = {}
```

Dependencies are stored in a graph, where the nodes represent bindings, and a directed edge between two nodes represents a dependency.

For the program

```Haskell
x :: [Int]
y :: [Int]
x = 1:2:y
y = 3:4:x
```

we obtain the following graph:

<img src="cycle3.png" width=25% border=1px>

Clearly there exists a strong cycle, and the compiler rejects the program.

Let's look at some more examples:

```Haskell
x :: Int
y :: Int
z :: Int
x = y
y = if true then x else z
z = 10
```

<img src="cycle4.png" width=40% border=1px>

```Haskell
x :: Int
y :: Int
x = (\\y -> y) y
y = x
```

<img src="cycle3.png" width=25% border=1px>

```Haskell
fst :: (a, b) -> a
fst (a, b) = a
a :: Int
b :: [Int]
x :: (Int, (Int, [Int]))
y :: Int
z :: (Int, [Int])
a = 10
b = [1,2,3,y]
x = (10, z)
z = (a, b)
y = fst x
```

<img src="cycle5.png" width=25% border=1px>

However, if we change at least one owning edge to a borrow edge, the program becomes valid:

```Haskell
fst :: (a, &'A b) -> a
fst (a, b) = a
a :: Int
b :: [Int]
x :: (Int, (Int, &[Int]))
y :: Int
z :: (Int, &[Int])
a = 10
b = [1,2,3,y]
x = (10, z)
z = (a, &b)
y = fst x
```

<img src="cycle6.png" width=25% border=1px>

### Moving a suspended variable

A variable is considered to be moved if any other binding uses it as a (strong) dependency. Let's look at a couple of examples:

```Haskell
x = 10
y = x
z = x -- error: `x` already moved
```

```Haskell
x = 10
y = let 
  a = x
in 10
z = x -- error: `x` already moved
```

```Haskell
x = 10
y = f x
z = x -- error: `x` already moved
```

```Haskell
x = "abc"
y = [x, x] -- error: `x` moved twice
```

```Haskell
x = "abc"
y = (x, x) -- error: `x` moved twice
```

```Haskell
x = "abc"
y = if true then x else "efg"
z = x -- error: `x` already moved
```

If expression has to be handled a bit differently as either its true branch or its false branch is executed, but never both at the same time. Hence, the two branches can move the same object:

```Haskell
x = 10
y = if true then x else x -- this is okay
z = x -- error: `x` already moved
```

Note that if the condition expression moves an object, the two branches cannot do it as well:

```Haskell
x = true
y = if x then x else 10 -- error: `x` already moved
```

The workaround is to change at least one move to borrow:

```Haskell
x = true
y1 = if x then &x else 10
-- or
y2 = if &x then x else 10 
```

Implementation is very straightforward; the compiler contains an environment mapping variables (names) into boolean flags, indicating whether a variable is already moved or not. When a variable `x` depends on variable `y`, the compiler ensures that the variable `y` is not already moved and then marks it as such. Keep in mind that this environment is not scoped - even if a move occurs in a nested sub-scope of the scope in which the variable is defined, the move persists to the outer scope.

### Moving a variable while it's borrowed

To know which variables are borrowed, we re-use the information from the Lifetime Analyser - the *borrow lifetime*. Borrow lifetime contains a lifetime (or several lifetimes) of a variable the binding borrows from. Because the lifetimes are unique, we can map a lifetime to a binding and mark it as borrowed in the environment.

Lets look at a couple of examples:

```Haskell
x = 10
y = &x
z = x -- error: cannot move `x` while it's borrowed
```

```Haskell
x = 10
y = (true, "abc", &x)
z = f x -- error: cannot move `x` while it's borrowed
```

```Haskell
x = 10
y = [&x, &x, &x]
z = f x -- error: cannot move `x` while it's borrowed
```

Compared to the enviroment which keeps track of moved variables, this environment is scoped. This means that if a borrow occurs inside a nested scope, it is removed once the scope is removed:

```Haskell
x = 10
y = let
  a = &x   -- a borrow occurs inside the scope
in [1,2,3] -- after the scope is popped, the borrow is removed
z = x      -- ok
```

However, a borrow can escape its scope if it is returned by the `let` expression:

```Haskell
x = 10
y = let
  a = &x
in a     -- borrow escapes its scope
z = x    -- error: cannot move `x` while it's borrowed
```

An exception to this rule are binary expressions (arithmetic, logical, ...); these expressions can contain a move and a borrow of the same variable:

```Haskell
x = 10
y = &x + x
```

Nevertheless, a move can occur only once:

```Haskell
x = 10
y = &x + x + x -- error: `x` moved twice
```
