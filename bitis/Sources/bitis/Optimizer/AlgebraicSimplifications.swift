//
//  AlgebraicSimplifications.swift
//  
//
//  Created by Toni K. Turk on 15/09/2022.
//

import Foundation

extension Optimizer {
  func algebraicSimplifications(_ stmt: ExpressionTree.Statement) -> ExpressionTree.Statement {
    switch stmt {
    case .jump, .label, .trap, .marker:
      return stmt
    case .exp(let exp):
      return .exp(algebraicSimplifications(exp))
    case let .move(dst, src):
      return .move(
        dst: algebraicSimplifications(dst),
        src: algebraicSimplifications(src))
    case let .cjump(cond, t, f):
      return .cjump(
        cond: algebraicSimplifications(cond),
        true: t,
        false: f)
    case let .seq(stmts):
      return .seq(stmts.map(algebraicSimplifications))
    }
  }
  
  private func algebraicSimplifications(_ expr: ExpressionTree.Expression) -> ExpressionTree.Expression {
    switch expr {
    case .name, .temp, .constant, .print, .malloc:
      return expr
    case let .mem(expr):
      return .mem(algebraicSimplifications(expr))
    case let .call(fn, args):
      return .call(
        fn: algebraicSimplifications(fn),
        args: args.map(algebraicSimplifications))
    case let .dealloc(expr, bytes):
      return .dealloc(algebraicSimplifications(expr), bytes: bytes)
    case let .eseq(stmt, expr):
      return .eseq(
        algebraicSimplifications(stmt),
        algebraicSimplifications(expr))
      
    // a * 1 = a
    case let .binop(.arithmetic(.mul), .constant(1), rhs):
      return rhs
    // 1 * a = a
    case let .binop(.arithmetic(.mul), lhs, .constant(1)):
      return lhs
    // a * 0 = 0
    case .binop(.arithmetic(.mul), .constant(0), _):
      return .constant(0)
    // 0 * a = 0
    case .binop(.arithmetic(.mul), _, .constant(0)):
      return .constant(0)
     
    // 0 + a = a
    case let .binop(.arithmetic(.plus), .constant(0), rhs):
      return rhs
    // a + 0 = a
    case let .binop(.arithmetic(.plus), lhs, .constant(0)):
      return lhs
      
    // a - 0 = a
    case let .binop(.arithmetic(.minus), lhs, .constant(0)):
      return lhs
      
    // a / 1 = a
    case let .binop(.arithmetic(.div), lhs, .constant(1)):
      return lhs
      
    // TODO: - Boolean algebra
    // TODO: - Other simplifications
      
    case let .binop(op, lhs, rhs):
      return .binop(
        op,
        lhs: algebraicSimplifications(lhs),
        rhs: algebraicSimplifications(rhs))
    }
  }
}
