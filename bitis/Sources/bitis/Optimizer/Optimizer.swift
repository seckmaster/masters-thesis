//
//  Optimizer.swift
//  
//
//  Created by Toni K. Turk on 15/09/2022.
//

import Foundation

public class Optimizer {
  let optimizations: Optimizations
  var constantPropagationMapping: Box<[FrameTemp: WordSizeType]>?
  
  public init(optimizations: Optimizations) {
    self.optimizations = optimizations
  }
}

public extension Optimizer {
  func optimize(code: ExpressionTree.Statement) -> ExpressionTree.Statement {
    var code = code
    
    /// 1. Constant propagation
    if optimizations.contains(Optimizations.constantPropagation) {
      constantPropagationMapping = .init(value: .empty())
      code = constantPropagation(code)
    }
    
    /// 2. Constant folding
    if optimizations.contains(Optimizations.constantFolding) {
      code = constantFolding(code)
    }
    
    /// 3. Algebraic simplifications
    if optimizations.contains(Optimizations.algebraicSimplifications) {
      code = algebraicSimplifications(code)
    }
    
    return code
  }
  
  func optimize(chunk: IRState.CodeChunk) -> IRState.CodeChunk {
    return .init(
      frame: chunk.frame,
      stmt: optimize(code: chunk.stmt)
    )
  }
}

public extension Optimizer {
  struct Optimizations: OptionSet {
    public var rawValue: Int
    
    public init(rawValue: Int) {
      self.rawValue = rawValue
    }
    
    public static let constantPropagation = Optimizations(rawValue: 1 << 0)
    public static let constantFolding = Optimizations(rawValue: 1 << 1)
    public static let algebraicSimplifications = Optimizations(rawValue: 1 << 2)
  }
}

extension WordSizeType {
  var bool: Bool {
    isZero ? false : true
  }
}
