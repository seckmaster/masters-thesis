//
//  ConstantPropagation.swift
//  
//
//  Created by Toni K. Turk on 15/09/2022.
//

import Foundation

extension Optimizer {
  func constantPropagation(_ stmt: ExpressionTree.Statement) -> ExpressionTree.Statement {
    switch stmt {
    case .jump, .label, .trap, .marker:
      return stmt
    case .exp(let exp):
      return .exp(constantPropagation(exp))
    case let .move(.temp(temp), .constant(constant)):
      constantPropagationMapping!.value[temp] = constant
      return stmt
    case let .move(dst, src):
      return .move(
        dst: constantPropagation(dst),
        src: constantPropagation(src))
    case let .cjump(cond, t, f):
      return .cjump(
        cond: constantPropagation(cond),
        true: t,
        false: f)
    case let .seq(stmts):
      return .seq(stmts.map(constantPropagation))
    }
  }
  
  private func constantPropagation(_ expr: ExpressionTree.Expression) -> ExpressionTree.Expression {
    switch expr {
    case .name, .constant, .print, .malloc:
      return expr
    case .temp(let temp):
      if let value = constantPropagationMapping!.value[temp] {
        return .constant(value)
      }
      return .temp(temp)
    case let .binop(op, lhs, rhs):
      return .binop(
        op,
        lhs: constantPropagation(lhs),
        rhs: constantPropagation(rhs))
    case let .mem(expr):
      return .mem(constantPropagation(expr))
    case let .call(fn, args):
      return .call(
        fn: constantPropagation(fn),
        args: args.map(constantPropagation))
    case let .dealloc(expr, bytes):
      return .dealloc(constantPropagation(expr), bytes: bytes)
    case let .eseq(stmt, expr):
      return .eseq(
        constantPropagation(stmt),
        constantPropagation(expr))
    }
  }
}
