//
//  ConstantFolding.swift
//  
//
//  Created by Toni K. Turk on 15/09/2022.
//

import Foundation

extension Optimizer {
  func constantFolding(_ stmt: ExpressionTree.Statement) -> ExpressionTree.Statement {
    switch stmt {
    case .jump, .label, .trap, .marker:
      return stmt
    case .exp(let exp):
      return .exp(constantFolding(exp))
    case let .move(dst, src):
      return .move(
        dst: constantFolding(dst),
        src: constantFolding(src))
    case let .cjump(cond, t, f):
      return .cjump(
        cond: constantFolding(cond),
        true: t,
        false: f)
    case let .seq(stmts):
      return .seq(stmts.map(constantFolding))
    }
  }
  
  private func constantFolding(_ expr: ExpressionTree.Expression) -> ExpressionTree.Expression {
    switch expr {
    case .name, .temp, .constant, .print, .malloc:
      return expr
    case let .binop(op, .constant(lhs), rhs: .constant(rhs)):
      switch op {
      case .arithmetic(let op):
        switch op {
        case .plus:
          return .constant(lhs + rhs)
        case .minus:
          return .constant(lhs - rhs)
        case .mul:
          return .constant(lhs * rhs)
        case .div:
          return .constant(lhs / rhs)
        }
      case .logical(let op):
        switch op {
        case .equal:
          return .constant(lhs == rhs)
        case .notEqual:
          return .constant(lhs != rhs)
        case .lowerThan:
          return .constant(lhs < rhs)
        case .greaterThan:
          return .constant(lhs > rhs)
        case .lowerOrEqualThan:
          return .constant(lhs <= rhs)
        case .greaterOrEqualThan:
          return .constant(lhs >= rhs)
        case .and:
          return .constant(lhs.bool && rhs.bool)
        case .or:
          return .constant(lhs.bool || rhs.bool)
        }
      case .bitwise(let op):
        switch op {
        case .shiftLeft:
          return .constant(UInt64(lhs) << UInt64(rhs))
        case .shiftRight:
          return .constant(UInt64(lhs) >> UInt64(rhs))
        case .and:
          return .constant(UInt64(lhs) & UInt64(rhs))
        case .or:
          return .constant(UInt64(lhs) | UInt64(rhs))
        }
      }
    case let .binop(op, lhs, rhs):
      return .binop(
        op,
        lhs: constantFolding(lhs),
        rhs: constantFolding(rhs))
    case let .mem(expr):
      return .mem(constantFolding(expr))
    case let .call(fn, args):
      return .call(
        fn: constantFolding(fn),
        args: args.map(constantFolding))
    case let .dealloc(expr, bytes):
      return .dealloc(constantFolding(expr), bytes: bytes)
    case let .eseq(stmt, expr):
      return .eseq(
        constantFolding(stmt),
        constantFolding(expr))
    }
  }
}
