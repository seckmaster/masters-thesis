//
//  LiftCases.swift
//  
//
//  Created by Toni K. Turk on 20/01/2022.
//

import Foundation

/// TODO: - A good name for this?
///  i) unfold cases
///  ii) unfold equations
///
///  ----------------------------------------------------------
///
/// The following homomorphism transforms bindings by lifting parameters
/// into explicit lambdas, where the pattern in each lambda expression is always
/// an `identifer` pattern.
/// Addtionally, cases are reduced into a single case expression.
///
/// This transformation transforms the AST so that each `AST.Binding.case` (an equation)
/// has zero parameters (lambdas are explicit) and only one equation per binding.
///
/// More specifically, given a function of the form:
///
///        1.  2.  3.    e
///     f p11 p12 p13 = e1  i)
///     f p21 p22 p23 = e2  ii)
///     f p31 p32 p33 = e2  iii)
///
/// it transforms it into the following single-equation represention:
///
///     f = λq1 -> λq2 -> λq3 -> case q1 of
///                           p11 -> case q2 of
///                             p12 -> case q3 of
///                               p13 -> e1
///                           | p21 -> case q2 of
///                               p22 -> case q3 of
///                                 p23 -> e2
///                           | p31 -> case q2 of
///                               p32 -> case q3 of
///                                 p33 -> e3
/// (_note that qi is an artificial identifier created by the compiler)
///
/// Function `f`, as described above, contains three equations with four variables
/// (three variables on left side of the equation and one on the right).
/// Hence, we can represent each binding in a matrix form `A = e`.
///
/// Examples:
///
///     f x = x + 2  ~~>  f = \\q1 -> case q1 of
///                                     x -> x + 2
///
///
///     f x y z = x + y + z  ~~>  f = \\q1 -> \\q2 -> \\q3 -> case q1 of
///                                                             x -> case q2 of
///                                                               y -> case q3 of
///                                                                 z -> x + y + z
///
///
///     f $(Cons (_, true) xs) = x + len xs  ~~>  \\q1 -> case q1 of
///     f $(Nil ())            = 0                          $(Cons (_, true) xs) = x + len xs
///                                                           | $(Nil ())        = 0
///
///
///     f x (1, 2) = true              ~~>  \\q1 -> \\q2 -> case q1 of
///     f _        = λ(a, b) -> false                         x -> case q2 of
///                                                             (1, 2) -> true
///                                                           | _ -> case q2 of
///                                                             (a, b) -> false
///
public class LiftCases {
  public init() {}
  
  private var counter = 0
  
  public func transformAST(_ ast: AST) -> AST {
    let bindings = ast.bindings.map(transformBinding)
    return .init(bindings: bindings)
  }
}

extension LiftCases {
  func transformBinding(_ binding: AST.Binding) -> AST.Binding {
    counter += 1
    
    switch binding {
    case .data:
      return binding
    case .prototype:
      return binding
    case let .binding(_, name, cases, _, _, _):
//      if cases.count == 1 && cases[0].parameters.count == 1 && cases[0].parameters[0].isIdentifier {
        // @NOTE: - Ad-hoc fix.
//        return .binding(
//          position: .next,
//          name: name,
//          cases: [
//            .init(
//              name: name,
//              position: .next,
//              parameters: [],
//              body: .lambda(
//                position: .next,
//                parameter: cases[0].parameters[0],
//                body: cases[0].body))
//          ])
//      }
      let reducedCase = reduceCases(cases)
      return .binding(
        position: .next,
        name: name,
        cases: [reducedCase])
    case let .print(position, expression, type):
      return .print(
        position: position,
        expression: transformExpr(expression),
        tcType: type)
    case let .include(module):
      return .include(module: module)
    }
  }
  
  func transformExpr(_ expr: AST.Expression) -> AST.Expression {
    switch expr {
    case .literal:
      return expr
    case .name:
      return expr
    case let .let(position, bindings, expression, type, lifetime):
      return .let(
        position: position,
        bindings: bindings.map(transformBinding),
        expression: transformExpr(expression),
        tcType: type,
        lifetime: lifetime)
    case let .case(position, expression, matches, type, lifetime):
      return .case(
        position: position,
        expression: transformExpr(expression),
        matches: matches.map(transformMatch),
        tcType: type,
        lifetime: lifetime)
    case let .ref(position, expression, type, lifetime):
      return .ref(
        position: position,
        expression: transformExpr(expression),
        tcType: type,
        lifetime: lifetime)
    case let .deref(position, expression, type, lifetime):
      return .deref(
        position: position,
        expression: transformExpr(expression),
        tcType: type,
        lifetime: lifetime)
    case let .tuple(position, expressions, type, lifetime):
      return .tuple(
        position: position,
        expressions: expressions.map(transformExpr),
        tcType: type,
        lifetime: lifetime)
    case let .list(position, expressions, type, lifetime):
      return .list(
        position: position,
        expressions: expressions.map(transformExpr),
        tcType: type,
        lifetime: lifetime)
    case let .binary(.application(position, fn, arg, type, lifetime)):
      return .binary(.application(
        position: position,
        fn: transformExpr(fn),
        arg: transformExpr(arg),
        tcType: type,
        lifetime: lifetime))
    case let .binary(.arithmetic(position, op, left, right, type, lifetime)):
      return .binary(.arithmetic(
        position: position,
        operator: op,
        left: transformExpr(left),
        right: transformExpr(right),
        tcType: type,
        lifetime: lifetime))
    case let .binary(.concat(position, left, right, type, lifetime)):
      return .binary(.concat(
        position: position,
        left: transformExpr(left),
        right: transformExpr(right),
        tcType: type,
        lifetime: lifetime))
    case let .binary(.ifThenElse(position, condition, `true`, `false`, type, lifetime)):
      return .binary(.ifThenElse(
        position: position,
        condition: transformExpr(condition),
        true: transformExpr(`true`),
        false: transformExpr(`false`),
        tcType: type,
        lifetime: lifetime))
    case let .binary(.logical(position, op, left, right, type, lifetime)):
      return .binary(.logical(
        position: position,
        operator: op,
        left: transformExpr(left),
        right: transformExpr(right),
        tcType: type,
        lifetime: lifetime))
    case let .lambda(position, parameter, body, type, lifetime):
      return .lambda(
        position: position,
        parameter: parameter,
        body: transformExpr(body),
        tcType: type,
        lifetime: lifetime)
    }
  }
  
  func transformMatch(_ match: AST.Expression.Match) -> AST.Expression.Match {
    .init(
      position: match.position,
      pattern: match.pattern,
      expression: transformExpr(match.expression)
    )
  }
}

extension LiftCases {
  func reduceCases(_ cases: [AST.Binding.Case]) -> AST.Binding.Case {
    precondition(!cases.isEmpty)
    
    /// TODO: -
    /// - optimise
    ///   - generated code
    ///   - the code itself
    /// - clean
    
    let shiftedCases = cases.map { $0.shiftLambdasLeft() }
    if shiftedCases.count == 1, shiftedCases[0].parameters.isEmpty {
      return .init(
        name: shiftedCases[0].name,
        position: shiftedCases[0].position,
        parameters: [],
        body: transformExpr(shiftedCases[0].body))
    }
    
    let n = shiftedCases.count
    let m = shiftedCases[0].parameters.count
    
    var caseExpressions = [AST.Expression]()
    caseExpressions.reserveCapacity(n)
    
    for i in (0..<n) {
      var outerCaseExpression: AST.Expression?
      for j in (0..<m).reversed() {
        outerCaseExpression = AST.Expression.case(
          position: .next,
          expression: .name(
            position: .next,
            identifier: identifier(index: j + 1)),
          matches: [
            .init(
              position: .next,
              pattern: shiftedCases[i].parameters[j],
              expression: outerCaseExpression ?? shiftedCases[i].body)
          ])
      }
      caseExpressions.append(outerCaseExpression!)
    }
    
    var matches = [AST.Expression.Match]()
    matches.reserveCapacity(n)
    
    for expr in caseExpressions {
      switch expr {
      case let .case(_, _, matches_, _, _):
        // @TODO: -
//        precondition(matches_.count == 1)
//        matches.append(matches_[0])
        matches.append(contentsOf: matches_)
      case _:
        fatalError()
        break
      }
    }
    
    let caseExpr: AST.Expression
    switch caseExpressions.first {
    case let .case(_, expression, _, _, _):
      caseExpr = .case(
        position: .next,
        expression: expression,
        matches: matches)
    case _:
      fatalError()
//      caseExpr = caseExpressions.first!
    }
    
    var lambdaExpr: AST.Expression!
    for i in (0..<m).reversed() {
      let identifier = /*m == 1 // @FIXME: - Ad-hoc
        ? shiftedCases[0].parameters[0].identifiers.first?.description ?? "__q\(i+1)__"
                        :  */ identifier(index: i + 1)
      
      if lambdaExpr == nil {
        lambdaExpr = .lambda(
          position: .next,
          parameter: .identifier(
            position: .next,
            identifier: identifier),
          body: caseExpr)
      } else {
        lambdaExpr = .lambda(
          position: .next,
          parameter: .identifier(
            position: .next,
            identifier: identifier),
          body: lambdaExpr)
      }
    }
    
    return .init(
      name: cases[0].name,
      position: .next,
      parameters: [],
      body: lambdaExpr)
  }
}

extension LiftCases {
  func identifier(index: Int) -> String {
    var id = ""
    var counter = counter
    if counter > 25 {
      print(terminator: "")
    }
    while counter > 0 {
      id += counter > 25 ? "z" : String((counter % 25).ascii)
      counter = counter - 25
    }
    return "__\(id)\(index)__"
  }
}

extension AST.Binding.Case {
  /// Do the following transformation:
  ///
  ///     f = \\x -> \\y -> e ~~>
  ///     f x y = e
  ///
  /// i.e., move outer-most lambda expressions into the `parameters` list.
  func shiftLambdasLeft() -> AST.Binding.Case {
    var params = parameters
    var expr_ = body
    loop: while true {
      switch expr_ {
      case let .lambda(_, parameter, body, _, _):
        params.append(parameter)
        expr_ = body
      case _:
        break loop
      }
    }
    return .init(
      name: name,
      position: position,
      parameters: params,
      body: expr_,
      type: type,
      lifetime: lifetime)
  }
  
  /// Do the reverse transformation:
  ///
  func shiftLambdasRight() -> AST.Binding.Case {
    var count = parameters.count - 1
    var lambda: AST.Expression?
    var type = type!
    for param in parameters.reversed() {
      let expr: AST.Expression
      if lambda == nil {
        expr = body
      } else {
        expr = lambda!
      }
      lambda = .lambda(
        position: Parser.position(param, body),
        parameter: param,
        body: expr,
        tcType: type)
      type = type.asArrow!.ret
      count -= 1
    }
    guard let lambda = lambda else {
      return self
    }

    return .init(
      name: name,
      position: position,
      parameters: [],
      body: lambda,
      type: type)
  }
}

// @FIXME: - Ugly non-pure code :)
fileprivate var current: Int = 0
extension Lexer.Position {
  static var next: Self {
    let position = Self(
      start: .init(
        line: current,
        column: current),
      end: .init(
        line: current,
        column: current))
    current += 1
    return position
  }
}
