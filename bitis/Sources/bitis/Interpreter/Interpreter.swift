//
//  Interpreter.swift
//  
//
//  Created by Toni K. Turk on 14/01/2022.
//

import Foundation
import Rainbow

public class Interpreter {
  public enum ExecutionMode {
    case debug
    case release
  }
  
  public enum RuntimeError: Error, CustomStringConvertible {
    case trap(reason: String)
    case divisionByZero
    case stackOverflow
    case cannotCast(from: Any, to: Any)
    case registerNotInitialised(FrameTemp)
    case onlyCodeChunksCanBeCalled
    
    public var description: String {
      switch self {
      case let .cannotCast(from, to):
        return "Cannot cast value of type '\(type(of: from))' to value of type \(type(of: to))'!"
      case .divisionByZero:
        return "Division by zero is undefined!"
      case .onlyCodeChunksCanBeCalled:
        return "Cannot invoke a non-callable object!"
      case let .registerNotInitialised(temp):
        return "Register '\(temp)' not initialized!"
      case .stackOverflow:
        return "Stack overflow!"
      case let .trap(reason):
        return "Trap: \(reason)!"
      }
    }
  }
  
  // Pointer to the bottom of a stack frame.
  var stackPointer: Byte
  
  // Pointer to the top of a stack frame.
  var framePointer: Byte
  
  // Memory (heap & stack).
  let memory: Memory
  
  // Stdout
  public private(set) var stdout: TextOutputStream
  
  // Dump program execution trace handle.
  public var trace: FileHandle?
 
  // Execution mode.
  public var executionMode: ExecutionMode = .release {
    didSet {
      sharedExecutionMode.value = executionMode
    }
  }
  let sharedExecutionMode: Box<ExecutionMode> = .init(value: .release)
  
  // Indentation (for debug printing).
  private var indent = 0
  
  // Information collected during the runtime of a program.
  public internal(set) var runtimeInfo: RuntimeInfo = .empty
  
  //
  public init(
    memory: Memory,
    stdout: TextOutputStream
  ) {
    self.stackPointer = memory.size
    self.framePointer = memory.size
    self.memory = memory
    self.stdout = stdout
    self.memory.interpreterExecutionMode = sharedExecutionMode
  }
}

extension Interpreter {
  public func interpret(chunk: CodeGenerator.CodeChunk) throws {
    try __interpret__(chunk: chunk)
    return ()
  }
  
  func __interpret__(chunk: CodeGenerator.CodeChunk) throws {
    let frame = chunk.frame
    
    let newStackPointer = stackPointer - frame.stackLocalsSize - wordSize
    guard newStackPointer >= memory.heapPointer else {
      throw RuntimeError.stackOverflow
    }
    
    try memory.stM(
      address: newStackPointer,
      value: framePointer)
    framePointer = stackPointer
//    memory.stT(
//      temp: frame.framePointer,
//      value: framePointer)
    stackPointer = stackPointer - frame.size
    
    if executionMode == .debug {
      debugPrint(
        "Begin".with(indent: indent),
        "SL "+String(frame.staticLevel),
        chunk.description
      )
      if chunk.frame.entry != AST.mainLabel {
        for i in 0..<chunk.frame.parameterCount {
          guard let arg = memory.ldMdebug(address: framePointer + wordSize * i) else { continue }
          debugPrint("Arg [\(i)]:".with(indent: indent + 2), arg)
        }
      }
      indent += 2
    }
    
    var pc = 0
    while pc < chunk.code.count {
      let instruction = chunk.code[pc]
      let result = try execute(instruction)
      switch result {
      case let label as FrameLabel:
        // @TODO: - optimise jumping logic (cache positions of labels ..?)
        for (idx, instr) in chunk.code.enumerated() {
          if case let .label(instrLabel) = instr, instrLabel.name == label.name {
            pc = idx
            break
          }
        }
      case _:
        break
      }
      pc += 1
    }
    
    if executionMode == .debug {
      indent -= 2
      debugPrint("End:".with(indent: indent), chunk.description)
    }
    
    guard let oldFP = try memory.ldM(address: framePointer - frame.stackLocalsSize - wordSize) as? Byte else {
      throw RuntimeError.stackOverflow
    }
    let oldSP = stackPointer + frame.stackLocalsSize
    
    if executionMode == .debug {
      for ptr in stride(from: stackPointer, through: newStackPointer, by: .Stride(wordSize)) {
        memory.heap.removeValue(forKey: ptr)
      }
    }
    
    framePointer = oldFP
    stackPointer = oldSP
    try memory.stM(
      address: stackPointer,
      value: memory.ldT(temp: frame.returnValue))
  }
  
  func execute(
    _ stmt: IR.Statement
  ) throws -> Memory.Value? {
    switch stmt {
    case let .seq(stmts):
      try stmts.enumerated().forEach {
        _ = try execute($0.element)
      }
      return 0.0
    // @MARK: - Move
    case let .move(dst, src):
      let srce = try execute(src)!
      switch dst {
      case .temp(let temp):
        memory.stT(temp: temp, value: srce)
      case .mem(let expr):
        let address = Memory.Address(try numeric(try execute(expr)!))
        try memory.stM(address: address, value: srce)
      case .constant(let address):
        try memory.stM(address: UInt(address), value: srce)
      case _:
        fatalError("Bitis: `dst` in `move` can either be `temp` or `mem`. got:\n`\(dst.prettyDescription)`!")
      }
      return nil
    case let .jump(label):
      return label
    case .label:
      return nil
    case let .exp(expr):
      _ = try execute(expr)
      return nil
    case let .cjump(cond, `true`, `false`):
      let cond = try numeric(try execute(cond)!)
      if cond.isZero {
        return `false`
      }
      return `true`
    case .trap(let reason):
      throw RuntimeError.trap(reason: reason)
    case .marker:
      fatalError()
    }
  }

  func execute(
    _ expr: IR.Expression
  ) throws -> Memory.Value? {
    switch expr {
    case let .eseq(stmt, expr):
      let _ = try execute(stmt)
      return try execute(expr)
//      fatalError("Bitis: cannot interpret an `eseq`!")
    case let .constant(constant):
      return constant
    case let .binop(op, lhs, rhs):
      let lhse = try execute(lhs)!
      let rhse = try execute(rhs)!
      let lhsn = try numeric(lhse)
      let rhsn = try numeric(rhse)
      switch op {
      case .arithmetic(let op):
        switch op {
        case .plus:
          return lhsn + rhsn
        case .minus:
          return lhsn - rhsn
        case .div:
          guard !rhsn.isZero else {
            throw RuntimeError.divisionByZero
          }
          return UInt(lhsn) / UInt(rhsn)
        case .mul:
          return lhsn * rhsn
        }
      case .logical(let op):
        switch op {
        case .equal:
          return lhsn == rhsn ? 1.0 : 0.0
        case .notEqual:
          return lhsn != rhsn ? 1.0 : 0.0
        case .and:
          return bool(lhsn) && bool(rhsn) ? 1.0 : 0.0 // FIXME: - casting twice
        case .or:
          return bool(lhsn) || bool(rhsn) ? 1.0 : 0.0 // FIXME: - casting twice
        case .greaterThan:
          return lhsn > rhsn ? 1.0 : 0.0
        case .lowerThan:
          return lhsn < rhsn ? 1.0 : 0.0
        case .greaterOrEqualThan:
          return lhsn >= rhsn ? 1.0 : 0.0
        case .lowerOrEqualThan:
          return lhsn <= rhsn ? 1.0 : 0.0
        }
      case .bitwise(let op):
        switch op {
        case .shiftLeft:
          return UInt64(lhsn) << UInt64(rhsn)
        case .shiftRight:
          return UInt64(lhsn) >> UInt64(rhsn)
        case .and:
          return UInt64(lhsn) & UInt64(rhsn)
        case .or:
          return UInt64(lhsn) | UInt64(rhsn)
        }
      }
    // MARK: - Mem
    case .mem(let expr):
      let address = Memory.Address(try numeric(try execute(expr)!))
      return try memory.ldM(address: address)
    // MARK: - Call
    case let .call(fn, args):
      let address = Memory.Address(try numeric(try execute(fn)!))
      
      guard let chunk = try memory.ldM(address: address) as? CodeGenerator.CodeChunk else {
        throw RuntimeError.onlyCodeChunksCanBeCalled
      }
      
      if executionMode == .debug {
        if chunk.frame.entry != AST.extractLabel {
          precondition(
            args.count == chunk.frame.parameterCount,
            "Expected \(chunk.frame.parameterCount) parameters, got \(args.count) (\(chunk.frame.entry) - \(chunk.frame.__description__))"
          )
        }
      }
      
      func addressForOffset(_ offset: Int) -> Byte {
        stackPointer + (wordSize * Byte(offset))
      }
    
      for (idx, arg) in args.enumerated() {
        let arge = try execute(arg)!
        try memory.stM(
          address: addressForOffset(idx),
          value: arge
        )
      }
      
      try __interpret__(chunk: chunk)
      if executionMode == .debug {
        debugPrint("Result [\(chunk.frame.entry)]:".with(indent: indent), try memory.ldM(address: stackPointer))
      }
      let value = try memory.ldM(address: stackPointer)
      return value
    // MARK: - Malloc
    case let .malloc(bytes, runtimeInfo):
      guard bytes > 0 else {
        fatalError("Bitis: allocating 0 bytes!")
      }
      let address = memory.heapPointer
      memory.heapPointer += bytes
      memory.free -= bytes
      
      if executionMode == .debug {
        debugPrint("♠︎ Did allocate \(bytes)B at: \(address) - \(String(describing: runtimeInfo))".with(indent: indent))
        
        self.runtimeInfo.allocatedMemory += bytes
        self.runtimeInfo.peakAllocatedMemory = max(
          self.runtimeInfo.peakAllocatedMemory,
          self.memory.allocated
        )
        self.runtimeInfo.avg.add(bytes: memory.allocated)
        self.memory.info[address] = runtimeInfo
        
        for addr in stride(from: address, to: memory.heapPointer, by: UInt.Stride(wordSize)) {
          try memory.stM(address: addr, value: 0) // mark as uninitialized
        }
      }
      return address
    // MARK: - Dealloc
    case let .dealloc(expr, bytes):
      let address_ = try numeric(try execute(expr)!)
      guard address_ > 0 else {
        throw Memory.Error.cannotDereferenceNullPointer
      }
      
      memory.free += bytes
      
      let address = Byte(address_)
      var deletedBytes = Byte.zero
      for addr in stride(from: address, to: address + bytes, by: .Stride(wordSize)) {
        if executionMode == .debug {
          if memory.heap[addr] == nil {
            debugPrint("Double free!".with(indent: indent))
            continue
          }
          deletedBytes += wordSize
        }
        memory.heap[addr] = nil
      }
      if executionMode == .debug {
        debugPrint("♤ Did deallocate \(deletedBytes)B [\(bytes)B] at: \(address)".with(indent: indent))
        
        self.runtimeInfo.deallocatedMemory += deletedBytes
        self.runtimeInfo.peakAllocatedMemory = max(
          self.runtimeInfo.peakAllocatedMemory,
          self.memory.allocated
        )
        self.runtimeInfo.avg.add(bytes: memory.allocated)
        
        if deletedBytes < bytes {
          fatalError()
        }
      }
      return nil
    case let .name(label):
      switch label.name {
      case "FP":
        return framePointer
      case "SP":
        return stackPointer
      case _:
        return memory.address(for: label)
      }
    case let .temp(temp):
      return try memory.ldT(temp: temp)
    // MARK: - Print
    case let .print(expr):
      let val = try execute(expr)!
      if let chunk = val as? CodeGenerator.CodeChunk {
        stdout.write(chunk.description)
      } else {
        var str = String(describing: try numeric(val))
        if str.suffix(2) == ".0" {
          str = String(str[..<str.index(str.endIndex, offsetBy: -2)])
        }
        stdout.write(str)
      }
      return nil
    }
  }
  
  func debugPrint(_ items: Any...) {
    guard let handle = trace else { return }
    var data = Data()
    let isStdout = handle == FileHandle.standardOutput
    
    for (idx, item) in items.enumerated() {
      let isLast = idx == items.count - 1
      let itemStr = String(describing: item)
      let itemWColor = isStdout ? itemStr.lightCyan : itemStr
      data.append(itemWColor.data(using: .utf8)!)
      data.append((isLast ? "\n" : " ").data(using: .utf8)!)
    }
    try! handle.write(contentsOf: data)
  }
}

public extension Interpreter {
  class Memory: CustomStringConvertible {
    public enum Error: Swift.Error, CustomStringConvertible {
      case addressOutOfBounds(address: Byte, heapSize: Byte)
      case cannotDereferenceNullPointer
      case alignment(address: Byte)
      case emptyAddress(address: Byte)
      
      public var description: String {
        switch self {
        case let .addressOutOfBounds(address, heapSize):
          return "Invalid address '\(address)' (heap size: '\(heapSize)')!"
        case .alignment(let address):
          return "Address '\(address)' is not aligned!"
        case .cannotDereferenceNullPointer:
          return "Dereferenced a null pointer!"
        case .emptyAddress(let address):
          return "Address '\(address)' is empty!"
        }
      }
    }
    
    typealias Address = Byte
    typealias Value = Any
    
    let size: Byte // The overall amount of memory.
    var free: Byte // The amount of free memory at the point of execution.
    var allocated: Byte { size - free }
    
    fileprivate var interpreterExecutionMode: Box<ExecutionMode>?
    
    public init(
      size: Byte
    ) {
      self.size = size
      self.free = size
    }
    
    var heap: [Address: Value] = [:]
    var heapPointer: Byte = wordSize
    var labelToAddressMapping: [FrameLabel: Address] = [:]
    var registers: [FrameTemp: Value] = [:]
    var info: [Address: Any] = [:]
    
    public var description: String {
      customizedDescription(
        heap: true, 
        reverseHeap: false, 
        registers: true, 
        labels: true
      )
    }
    
    public func customizedDescription(
      heap: Bool = true,
      reverseHeap: Bool = false,
      registers: Bool = true,
      labels: Bool = true
    ) -> String {
      var str = ""
      if registers {
        str += "Registers:\n"
        let regs = self.registers
          .sorted(by: { $0.key.identifier < $1.key.identifier })
          .map { "\($0.key): \($0.value)" }
          .joined(separator: "\n")
        str += regs + "\n"
      }
      if labels {
        str += labelToAddressMapping.description + "\n"
      }
      str += "Memory:\n"
      if heap {
        let heap = self.heap
          .sorted(by: { reverseHeap ? $0.key < $1.key : $0.key > $1.key })
          .map { "\($0.key): \($0.value) [\(info[$0.key].map(String.init(describing:)) ?? "")]" }
          .joined(separator: "\n")
        str += heap + "\n"
      }
      return str
    }
  }
}

extension Interpreter.Memory {
  private var isDebugMode: Bool {
    switch interpreterExecutionMode?.value {
    case .debug?:
      return true
    case _:
      return false
    }
  }
  
  func stM(address: Address, value: Value) throws {
    if isDebugMode {
      if address >= size {
        throw Error.addressOutOfBounds(address: address, heapSize: size)
      }
      if address == 0 {
        throw Error.cannotDereferenceNullPointer
      }
      if address % wordSize != 0 {
        throw Error.alignment(address: address)
      }
    }
    heap[address] = value
  }
  
  func ldM(address: Address) throws -> Value {
    if isDebugMode {
      if address >= size {
        throw Error.addressOutOfBounds(address: address, heapSize: size)
      }
      if address == 0 {
        throw Error.cannotDereferenceNullPointer
      }
      if address % wordSize != 0 {
        throw Error.alignment(address: address)
      }
      guard let value = heap[address] else {
        throw Error.emptyAddress(address: address)
      }
      return value
    }
    return heap[address] ?? 0
  }
  
  func ldMdebug(address: Address) -> Value? {
    heap[address]
  }
  
  func ldM(address: Address, count: Int) -> [Value?] {
    (0..<count)
      .map { address + Address($0) * wordSize }
      .map { heap[$0] }
  }
  
  func stT(temp: FrameTemp, value: Value) {
    if isDebugMode {
      guard !(value is CodeGenerator.CodeChunk) else {
        fatalError("Bitis: a code chunk cannot be stored in a register!")
      }
    }
    registers[temp] = value
  }
  
  func ldT(temp: FrameTemp) throws -> Value {
    guard let value = registers[temp] else {
      throw Interpreter.RuntimeError.registerNotInitialised(temp)
    }
    return value
  }
  
  func address(for label: FrameLabel) -> Address {
    labelToAddressMapping[label]!
  }
  
  func stM(label: FrameLabel, value: Value) throws {
    try stM(address: address(for: label), value: value)
  }
  
  func ldM(label: FrameLabel) -> Value {
    heap[address(for: label)]!
  }
}

extension Interpreter.Memory {
  func dump() {
    print(description)
  }
  
  func dumpStack() {
    var str = ""
    for (key, value) in heap.sorted(by: { $0.key > $1.key }) {
      guard key > heapPointer else { break }
      str += "\(key): \(value)\n"
    }
    print(str)
  }
}

extension Interpreter {
  func numeric(_ any: Any) throws -> Double {
    if let double = any as? Double {
      return double
    }
    if let int = any as? Int {
      return Double(int)
    }
    if let uint = any as? UInt {
      return Double(uint)
    }
    if let uint = any as? UInt64 {
      return Double(uint)
    }
    throw RuntimeError.cannotCast(from: any, to: Double.self)
  }
  
  func bool(_ numeric: Double) -> Bool {
    if executionMode == .debug {
      if numeric.isZero { return false }
      if numeric == 1   { return true }
      fatalError("Bitis: expected a boolean value (0|1), got \(numeric)!")
    }
    return numeric.isZero ? false : true
  }
}

public class Box<T> {
  public var value: T
  
  public init(value: T) { self.value = value }
}

public extension Interpreter {
  struct RuntimeInfo: CustomStringConvertible {
    public var allocatedMemory       : Byte = 0
    public var deallocatedMemory     : Byte = 0
    public var peakAllocatedMemory   : Byte = 0
    public var averageAllocatedMemory: Byte { avg.avg }
    fileprivate var avg: AverageBox = .init()
  
    public init(
      allocatedMemory: Byte, 
      deallocatedMemory: Byte,
      peakAllocatedMemory: Byte
    ) {
      self.allocatedMemory = allocatedMemory
      self.deallocatedMemory = deallocatedMemory
      self.peakAllocatedMemory = peakAllocatedMemory
    }
    
    public var description: String {
      "Allocated: \(allocatedMemory), Freed: \(deallocatedMemory), Delta: \(allocatedMemory - deallocatedMemory)"
    }
    
    public static var empty: RuntimeInfo {
      .init(
        allocatedMemory: 0, 
        deallocatedMemory: 0,
        peakAllocatedMemory: 0
      )
    }
  }
  
  struct AverageBox {
    var count = 0
    var sum: Byte = 0
    
    var avg: Byte {
      guard count > 0 else { return 0 } 
      return Byte(Double(sum) / Double(count))
    }
    
    mutating func add(bytes: Byte) {
      count += 1
      sum += bytes
    }
  }
}
