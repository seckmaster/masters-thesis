//
// Created by Toni K. Turk on 02/11/2020.
//

import Foundation
import SwiftParsec
import Bow

public typealias TCState = (Env, UniqStream, Subs, Backtrace)
public typealias TypecheckM<A> = StateT<EitherPartial<TCError>, TCState, A>

public protocol TypeCheckable {
  func typeChecker() -> TypecheckM<Self>
  var type: TCType? { get }
}

public extension StateT where F == EitherPartial<TCError>, S == TCState, A == TCAst {
  func run() -> EitherOf<TCError, TCAst> {
    runA((.empty, .init(), .empty, .init()))
  }
}

public extension StateT where F == EitherPartial<TCError>, S == TCState {
  func failWith<X: TypeCheckable, Y: TypeCheckable>(
    _ x: X,
    _ y: Y,
    _ f: @escaping (TCType, TCType) -> TypeError
  ) -> TypecheckM<A> {
    self <|> {
      Subs.Action.mapType(x.type!) >>- { a in
        Subs.Action.mapType(y.type!) >>- { b in
          .error(f(a ?? x.type!, b ?? y.type!))
        }
      }
    }()
  }
  
  func failWith<X: TypeCheckable>(
    _ x: X,
    _ f: @escaping (TCType) -> TypeError
  ) -> TypecheckM<A> {
    self <|> {
      Subs.Action.mapType(x.type!) >>- { a in
        .error(f(a ?? x.type!))
      }
    }()
  }
  
  static func error(_ error: TypeError) -> StateT<EitherPartial<TCError>, TCState, A> {
    .init { _, _, _, backtrace in
      .raiseError(.init(backtrace: backtrace, error: error))^
    }
  }
  
  func failWith(_ error: TypeError) -> StateT<EitherPartial<TCError>, TCState, A> {
    self <|> .init { _, _, _, backtrace in
      .raiseError(.init(backtrace: backtrace, error: error))^
    }
  }
  
  static func backtrace<A: HasPosition>(_ hasPosition: A) -> TypecheckM<A> {
    .init { env, stream, subs, backtrace in
      var backtrace = Backtrace(backtrace: backtrace)
      backtrace.trace.append(hasPosition)
      return .pure(((env, stream, subs, backtrace), hasPosition))
    }
  }
}

public extension StateT where F: Monad {
  func withState() -> StateT<F, S, (A, S)> {
    self >>- { a in
      .init { s in
        .pure((s, (a, s)))
      }
    }
  }
}

public extension StateT where F: MonadError {
  func failWith(_ error: F.E) -> StateT<F, S, A> {
    self <|> .raiseError(error)^
  }
}

public func >>-<A: HasPosition, B>(
  _ m: StateT<EitherPartial<TCError>, TCState, A>,
  _ f: @escaping (A) -> StateT<EitherPartial<TCError>, TCState, B>
) -> StateT<EitherPartial<TCError>, TCState, B> {
  m
    .flatMap(TypecheckM<A>.backtrace)
    .flatMap(f)^
}

public func >>-<F, S, A, B>(
  _ m: StateT<F, S, A>,
  _ f: @escaping (A) -> StateT<F, S, B>
) -> StateT<F, S, B> where F: Monad {
  m.flatMap(f)^
}

public func <|><F, S, A>(
  _ m: StateT<F, S, A>,
  _ recovery: StateT<F, S, A>
) -> StateT<F, S, A> where F: MonadError {
  m.handleErrorWith { _ in recovery }^
}

public func *><F, S, A: HasPosition, B>(
  _ ml: StateT<F, S, A>,
  _ mr: StateT<F, S, B>
) -> StateT<F, S, B> where F: Monad {
  ml >>- { _ in mr }
}

public func *><F, S, A, B>(
  _ ml: StateT<F, S, A>,
  _ mr: StateT<F, S, B>
) -> StateT<F, S, B> where F: Monad {
  ml >>- { _ in mr }
}

public func <*<F, S, A, B>(
  _ ml: StateT<F, S, B>,
  _ mr: StateT<F, S, A>
) -> StateT<F, S, B> where F: Monad {
  ml >>- { b in
    mr >>- { _ in
      .pure(b)^
    }
  }
}
