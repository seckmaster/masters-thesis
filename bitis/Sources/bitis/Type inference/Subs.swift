//
// Created by Toni K. Turk on 20/11/2020.
//

import Foundation
import Bow
import SwiftParsec

fileprivate let __MAX_DEPTH__ = 1000

// substitution table
public struct Subs {
  public typealias Table = (from: TCType, to: TCType)
  public typealias Scope = [Table] // TODO: - it would probably be a good idea to re-implement as a Graph
  private(set) var scopes: [Scope]
}

public extension Subs {
  static var empty: Subs { .init(scopes: [[]]) }
  
  func addSubstitution(from: TCType, to: TCType) -> Result<Subs, TypeError> {
    guard !scopes.isEmpty else {
      fatalError("Subs.addSubstitution: empty scopes")
    }
    
    let from = from.asPolymorphic
    guard from.isPolymorphic  || (from.lifetime != to.lifetime) else { return .success(self) }
    
    let to = to.asPolymorphic
    guard from != to else { return .success(self) }
    
    if let existingSubs = mapType(from, false, depth: 0), case .failure = existingSubs.matchWith(type: to) {
      return .failure(.typeMismatch(.match(lhs: existingSubs, rhs: to)))
    }
    
    var subs = self
    subs.scopes[subs.scopes.count - 1].append((from, to))
    return .success(subs)
  }

  func mapType(_ type: TCType) -> TCType? {
    mapType(type, true, depth: 0)?.asPolymorphic
  }

  func pushScope() -> Subs {
    var subs = self
    subs.scopes.append(.empty())
    return subs
  }

  func popScope() -> Subs {
    var subs = self
    _ = subs.scopes.popLast()
    return subs
  }
  
  private func mapType(_ type: TCType, _ recursive: Bool, depth: Int) -> TCType? {
    guard depth <= __MAX_DEPTH__ else {
      return type
    }

    func map(type: TCType) -> TCType? {
      for scope in scopes.reversed() {
        let polyType = type.asPolymorphic
        for mapping in scope.reversed() { // FIXME: - reversed() needed as a hack!
          switch (mapping.from == polyType || mapping.from == type, recursive) {
          case (true, true):
            switch mapping.from {
            case .polymorphic(_, _, .typeVariable(let name, _)) where mapping.to.containsTypeVariable(name: name):
              // NOTE: - this prevents infinite recursion
              return mapping.to
            case _:
              if mapping.to.isSimple {
                return mapping.to
              }
              return mapType(mapping.to, recursive, depth: depth + 1) ?? mapping.to
            }
          case (true, false):
            return mapping.to
          case _:
            continue
          }
        }
      }
      return nil
    }
    
    let polyType = type.asPolymorphic
    switch polyType {
    case let .polymorphic(_, _, .arrow(arg, ret, lifetimeSpecifier)):
      switch (mapType(.simple(arg), recursive, depth: depth + 1), mapType(.simple(ret), recursive, depth: depth + 1)) {
      case let (.polymorphic(typeVariables1, context1, arg)?,
                .polymorphic(typeVariables2, _, ret)?):
        return .polymorphic(
          typeVariables: typeVariables1.union(typeVariables2),
          context: context1,
          type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
      case let (.polymorphic(typeVariables, context, arg), .simple(ret)?),
           let (.simple(arg), .polymorphic(typeVariables, context, ret)?):
        return .polymorphic(
          typeVariables: typeVariables,
          context: context,
          type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
      case let (.simple(arg)?, .simple(ret)?):
        return .arrow(arg: arg, ret: ret)
      case let (.polymorphic(typeVariables1, context1, arg)?, nil):
        return .polymorphic(
          typeVariables: typeVariables1,
          context: context1,
          type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
      case let (nil, .polymorphic(typeVariables1, context1, ret)?):
        return .polymorphic(
          typeVariables: typeVariables1,
          context: context1,
          type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
      case let (.simple(arg)?, nil):
        return .arrow(arg: arg, ret: ret)
      case let (nil, .simple(ret)?):
        return .arrow(arg: arg, ret: ret)
      case _:
        return map(type: polyType)
      }
    case let .polymorphic(_, _, .tuple(elements, lifetimeSpecifier)):
      let elems = elements
        .map { mapType(.simple($0), recursive, depth: depth + 1) ?? .simple($0) }
        .reduce(into: [TCType.SimpleType]()) { acc, next in
          switch next {
          case .polymorphic(_, _, let simple),
               .simple(let simple):
            acc.append(simple)
          case _:
            break
          }
        }
      return .tuple(elements: elems, lifetimeSpecifier: lifetimeSpecifier).asPolymorphic
    case let .polymorphic(_, _, .list(element, lifetimeSpecifier)):
      switch mapType(.simple(element), recursive, depth: depth + 1) {
      case .polymorphic(_, _, let simple)?,
           .simple(let simple)?:
        return .list(element: simple, lifetimeSpecifier: lifetimeSpecifier).asPolymorphic
      case _:
        return nil
      }
    case let .polymorphic(_, _, .typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier)):
      let types = types
        .map { map(type: .simple($0)) ?? .simple($0) }
        .reduce(into: [TCType.SimpleType]()) { acc, next in
          switch next {
          case .polymorphic(_, _, let simple),
               .simple(let simple):
            acc.append(simple)
          case _:
            break
          }
        }
      return .simple(.typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: lifetimeSpecifier))
        .asPolymorphic
    case let .polymorphic(_, _, .reference(type, lifetimeSpecifier)):
      switch mapType(.simple(type), recursive, depth: depth + 1) {
      case .polymorphic(_, _, let simple)?,
           .simple(let simple)?:
        return .reference(simple, lifetimeSpecifier: lifetimeSpecifier).asPolymorphic
      case _:
        return nil
      }
    case _:
      return map(type: polyType)
    }
  }
}

// MARK: - Monadic actions
public extension Subs {
  enum Action {
    static func addSubstitution(from: TCType, to: TCType) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        switch subs.addSubstitution(from: from, to: to) {
        case .success(let subs):
          return .pure(((env, stream, subs, backtrace), ()))
        case .failure(let error):
          return .raiseError(.init(backtrace: backtrace, error: error))^
        }
      }
    }

    static func mapType(_ type: TCType) -> TypecheckM<TCType?> {
      .init { env, stream, subs, backtrace in
        let mapped = subs.mapType(type)
        return .pure(((env, stream, subs, backtrace), mapped))
      }
    }

    static func createSubstitutionWithMoreSpecificType(a: TCType, b: TCType) -> TypecheckM<TCType> {
      guard a != b else {
        return .pure(a)^
      }
      
      return .init { env, stream, subs, backtrace in
        switch a.chooseMoreSpecificType(type: b) {
        case .success(let specificType):
          let mapping = a.createSubstitutionMapping(type: b)
          var subs = subs
          for (key, value) in mapping {
            switch subs.addSubstitution(from: key, to: value) {
            case .success(let subs_):
              subs = subs_
            case .failure(let error):
              return .raiseError(.init(backtrace: backtrace, error: error))^
            }
          }
          return .pure(((env, stream, subs, backtrace), specificType))
        case .failure(let error):
          return .raiseError(.init(backtrace: backtrace, error: error))^
        }
      }
    }
    
    static func createSubstitutionWithMoreSpecificType(
      a: TCType,
      b: TCType,
      allowRefDifference: Bool,
      mapIfOneIsReference: ((TCType) -> TCType)? = nil
    ) -> TypecheckM<TCType> {
      func wrap(type: TCType, with reference: TCAst.Lifetime?) -> TCType {
        precondition(!type.isReference)
        switch type {
        case .simple(let simple),
             .polymorphic(_, _, let simple):
          let lifetimeSpecifier = reference?.identifier.identifier ?? "'X"
          return .reference(simple, lifetimeSpecifier: lifetimeSpecifier).asPolymorphic
        case _:
          fatalError()
        }
      }
      switch allowRefDifference {
      case false:
        return createSubstitutionWithMoreSpecificType(a: a, b: b)
      case true:
        switch (a.isReference, b.isReference) {
        case (false, false), (true, true):
          return createSubstitutionWithMoreSpecificType(a: a, b: b)
        case (true, false):
          return createSubstitutionWithMoreSpecificType(
            a: a.withLifetime(a.lifetime?.identifier.identifier ?? "'X"),
            b: wrap(type: b, with: a.lifetime)
          )
          .map { mapIfOneIsReference?($0) ?? $0 }^
        case (false, true):
          return createSubstitutionWithMoreSpecificType(
            a: wrap(type: a, with: b.lifetime),
            b: b.withLifetime(b.lifetime?.identifier.identifier ?? "'X")
          )
          .map { mapIfOneIsReference?($0) ?? $0 }^
        }
      }
    }

    static func pushScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let subs = subs.pushScope()
        return .pure(((env, stream, subs, backtrace), ()))
      }
    }

    static func popScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let subs = subs.popScope()
        return .pure(((env, stream, subs, backtrace), ()))
      }
    }

    static func inNewScope<A>(_ f: () -> TypecheckM<A>) -> TypecheckM<A> {
      pushScope() *> f() <* popScope()
    }
    
    static func nonDeterminism<A>(first: TypecheckM<A>, fallback: @autoclosure @escaping () -> TypecheckM<A>) -> TypecheckM<A> {
      func persistChanges() -> TypecheckM<()> {
        .init { env in
          var subs = env.2
          for mapping in subs.scopes.last! {
            subs.scopes[subs.scopes.count - 2].append(mapping)
          }
          return .pure(((env.0, env.1, subs, env.3), ()))
        }
      }
      return inNewScope {
        (first <* persistChanges()).handleErrorWith { error in
          fallback().failWith(error)
        }^
      }
    }
  }
}
