//
// Created by Toni K. Turk on 14/11/2020.
//

import Foundation
import SwiftParsec
import Bow

public typealias TVE = TypeVariableEnvironment

public struct TypeVariableEnvironment {
  typealias Scope = Set<String>
  internal private(set) var scopes: [Scope]
}

public extension TypeVariableEnvironment {
  static var empty: TVE {
    .init(scopes: [.empty()])
  }

  internal var currentScope: Scope {
    guard let scope = scopes.last else {
      fatalError("TVE: no scopes!")
    }
    return scope
  }
}

extension TypeVariableEnvironment: HasScope {
  public func addTypeVariable(name: String) -> TypeVariableEnvironment {
    guard !scopes.isEmpty else {
      fatalError("TVE: no scopes!")
    }
    var env = self
    env.scopes[scopes.count - 1].insert(name)
    return env
  }

  public func containsType(name: String) -> Bool {
    currentScope.contains(name)
  }

  public func pushScope() -> TypeVariableEnvironment {
    var env = self
    env.scopes.append(.empty())
    return env
  }

  public func popScope() -> TypeVariableEnvironment {
    guard scopes.count > 1 else {
      fatalError("TVE.popScope: not allowed to pop when count < 2!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}
