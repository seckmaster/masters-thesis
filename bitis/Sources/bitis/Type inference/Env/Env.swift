//
// Created by Toni K. Turk on 14/11/2020.
//

import Foundation
import SwiftParsec
import Bow

public protocol HasScope {
  func pushScope() -> Self
  func popScope() -> Self
}

public struct Env {
  var lve: LVE // Local Variable Environment
  var lte: LVE // Local Type Environment
  var tve: TVE // Type Variable Environment
  var validateTypeConstructorLifetimeParams: Bool // `true` if typechecking variable binding
}

public extension Env {
  static var empty: Env {
    .init(
      lve: .empty,
      lte: .empty,
      tve: .empty,
      validateTypeConstructorLifetimeParams: false
    )
  }

  static func withBinding(name: String, binding: LVE.Kind) -> Env {
    .init(
      lve: .init(scopes: [
        .init(elements: [(name, binding)])
      ]),
      lte: .empty,
      tve: .empty,
      validateTypeConstructorLifetimeParams: false
    )
  }
  
  func withPrototype(name: String, type: TCType) -> Env {
    let lve = lve.bind(prototype: name, with: type)
    return .init(lve: lve, lte: lte, tve: tve, validateTypeConstructorLifetimeParams: validateTypeConstructorLifetimeParams)
  }
}

extension Env: HasScope {
  public func pushScope() -> Env {
    .init(
      lve: lve.pushScope(),
      lte: lte.pushScope(),
      tve: tve.pushScope(),
      validateTypeConstructorLifetimeParams: validateTypeConstructorLifetimeParams
    )
  }

  public func popScope() -> Env {
    .init(
      lve: lve.popScope(),
      lte: lte.popScope(),
      tve: tve.popScope(),
      validateTypeConstructorLifetimeParams: validateTypeConstructorLifetimeParams
    )
  }
}

// MARK: - Monadic actions on LVE
public extension Env {
  enum ActionLVE {
    static func bind(name: String, with type: TCType) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let lve = env.lve.bind(name: name, with: type)
        let env = Env(lve: lve, lte: env.lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env, stream, subs, backtrace), ()))
      }
    }

    static func bind(prototype: String, with type: TCType) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let lve = env.lve.bind(prototype: prototype, with: type)
        let env = Env(lve: lve, lte: env.lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env, stream, subs, backtrace), ()))
      }
    }

    static func queryType(for name: String, currentScopeOnly: Bool = false) -> TypecheckM<TCType> {
      .init { env, stream, subs, backtrace in
        guard let type = env.lve.queryType(for: name, currentScopeOnly: currentScopeOnly) else {
          return .raiseError(.init(backtrace: backtrace, error: .variable(.undefined(name: name))))
        }
        return .pure(((env, stream, subs, backtrace), type))
      }
    }
    
    static func queryTypeEither(for name: String, currentScopeOnly: Bool = false) -> TypecheckM<Bow.Either<TCType, TypeError>> {
      .init { env, stream, subs, backtrace in
        guard let type = env.lve.queryType(for: name, currentScopeOnly: currentScopeOnly) else {
          return .pure(((env, stream, subs, backtrace), .right(.variable(.undefined(name: name)))))
        }
        return .pure(((env, stream, subs, backtrace), .left(type)))
      }
    }

    static func containsOnCurrentScope(binding: String) -> TypecheckM<Bool> {
      .init { env, stream, subs, backtrace in
        .pure(((env, stream, subs, backtrace), env.lve.containsOnCurrentScope(binding: binding)))
      }
    }

    static func containsOnCurrentScope(prototype: String) -> TypecheckM<Bool> {
      .init { env, stream, subs, backtrace in
        .pure(((env, stream, subs, backtrace), env.lve.containsOnCurrentScope(prototype: prototype)))
      }
    }

    static func doesNotContain(prototype: String) -> TypecheckM<()> {
      containsOnCurrentScope(prototype: prototype) >>- {
        switch $0 {
        case true:
          return .error(.variable(.alreadyDefined(name: prototype)))
        case false:
          return .pure(())^
        }
      }
    }

    static func doesNotContain(binding: String) -> TypecheckM<()> {
      containsOnCurrentScope(binding: binding) >>- {
        switch $0 {
        case true:
          return .error(.variable(.alreadyDefined(name: binding)))
        case false:
          return .pure(())^
        }
      }
    }

    static func addPatternVariableNamesToEnv(_ pattern: TCAst.Pattern, afterBeingTypeChecked type: TCType) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        switch type.matchWithPattern(pattern) {
        case .success(let mapping):
          var lve = env.lve
          for (name, type) in mapping {
            lve = lve.bind(name: name, with: type.asPolymorphic)
          }
          let env = Env(lve: lve, lte: env.lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
          return .pure(((env, stream, subs, backtrace), ()))^
        case .failure(let error):
          return .raiseError(.init(backtrace: backtrace, error: error))^
        }
      }
    }
    
    static func formUnion(lve1: LVE, lve2: LVE) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        switch lve1.formUnion(lve2) {
        case .success(let lve):
          let env = Env(lve: lve, lte: env.lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
          return .pure(((env, stream, subs, backtrace), ()))^
        case .failure(let error):
          return .raiseError(.init(backtrace: backtrace, error: error))^
        }
      }
    }
    
    static func formUnion(lve: LVE) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        switch env.lve.formUnion(lve) {
        case .success(let lve):
          let env = Env(lve: lve, lte: env.lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
          return .pure(((env, stream, subs, backtrace), ()))^
        case .failure(let error):
          return .raiseError(.init(backtrace: backtrace, error: error))^
        }
      }
    }
  }
}

// MARK: - Monadic actions on LTE
public extension Env {
  enum ActionLTE {
    static func bind(name: String, with type: TCType) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let lte = env.lte.bind(name: name, with: type)
        let newEnv = Env(lve: env.lve, lte: lte, tve: env.tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((newEnv, stream, subs, backtrace), ()))
      }
    }
    
    static func queryType(for name: String, currentScopeOnly: Bool = false) -> TypecheckM<TCType> {
      .init { env, stream, subs, backtrace in
        guard let type = env.lte.queryType(for: name, currentScopeOnly: currentScopeOnly) else {
          return .raiseError(.init(backtrace: backtrace, error: .unknownType(name: name)))
        }
        return .pure(((env, stream, subs, backtrace), type))
      }
    }
    
    static func containsOnCurrentScope(name: String) -> TypecheckM<Bool> {
      .init { env, stream, subs, backtrace in
        .pure(((env, stream, subs, backtrace), env.lte.containsOnCurrentScope(binding: name)))
      }
    }
    
    static func doesNotContain(name: String) -> TypecheckM<()> {
      containsOnCurrentScope(name: name) >>- {
        switch $0 {
        case true:
          return .error(.variable(.alreadyDefined(name: name)))
        case false:
          return .pure(())^
        }
      }
    }
  }
}

// MARK: - Monadic actions on TVE
public extension Env {
  enum ActionTVE {
    static func addTypeVariable(name: String) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let tve = env.tve.addTypeVariable(name: name)
        let env = Env(
          lve: env.lve, 
          lte: env.lte, 
          tve: tve, 
          validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env, stream, subs, backtrace), ()))^
      }
    }

    static func addTypeVariables<C: Collection>(names: C) -> TypecheckM<()> where C.Element == String {
      .init { env, stream, subs, backtrace in
        var tve = env.tve
        for name in names {
          tve = tve.addTypeVariable(name: name)
        }
        let env = Env(
          lve: env.lve, 
          lte: env.lte, 
          tve: tve, 
          validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env, stream, subs, backtrace), ()))^
      }
    }

    static func typeVariablesOnCurrentScope() -> TypecheckM<TVE.Scope> {
      .init { env, stream, subs, backtrace in
        .pure(((env, stream, subs, backtrace), env.tve.currentScope))
      }
    }

    static func containsVariable(name: String) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        guard env.tve.containsType(name: name) else {
          return .raiseError(.init(backtrace: backtrace, error: .unknownType(name: name)))
        }
        return .pure(((env, stream, subs, backtrace), ()))^
      }
    }

    static func pushScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let tve = env.tve.pushScope()
        let env_ = Env(lve: env.lve, lte: env.lte, tve: tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env_, stream, subs, backtrace), ()))
      }
    }

    static func popScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let tve = env.tve.popScope()
        let env_ = Env(lve: env.lve, lte: env.lte, tve: tve, validateTypeConstructorLifetimeParams: env.validateTypeConstructorLifetimeParams)
        return .pure(((env_, stream, subs, backtrace), ()))
      }
    }

    static func inNewScope<A>(_ f: () -> TypecheckM<A>) -> TypecheckM<A> {
      pushScope() *> f() <* popScope()
    }
  }
}

// MARK: - Monadic actions on all Environments
public extension Env {
  enum Action {
    static func pushScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let env_ = env.pushScope()
        return .pure(((env_, stream, subs, backtrace), ()))
      }
    }

    static func popScope() -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        var boundNames = Set<String>()
        for (name, kind) in env.lve.currentScope.elements.reversed() {
          switch kind {
          case .prototype where !boundNames.contains(name):
            // @FIXME: - 
//            return .raiseError(.init(backtrace: backtrace, error: .missingBinding(name: name)))
            continue
          case .prototype:
            continue
          case .bind:
            boundNames.insert(name)
          }
        }
        let env_ = env.popScope()
        return .pure(((env_, stream, subs, backtrace), ()))
      }
    }

    static func inNewScope<A>(_ f: () -> TypecheckM<A>) -> TypecheckM<A> {
      pushScope() *> f() <* popScope()
    }

    static func validateTypeConstructorLifetimeParams<A>(_ isBinding: Bool, f: () ->  TypecheckM<A>) -> TypecheckM<A> {
      let a: TypecheckM<()> = .init { env, stream, subs, backtrace in
        var env = env
        env.validateTypeConstructorLifetimeParams = isBinding
        return .pure(((env, stream, subs, backtrace), ()))
      }
      let b: TypecheckM<()> = .init { env, stream, subs, backtrace in
        var env = env
        env.validateTypeConstructorLifetimeParams = false
        return .pure(((env, stream, subs, backtrace), ()))
      }
      return a *> f() <* b
    }

    static var isBinding: TypecheckM<Bool> {
      .init { env, stream, subs, backtrace in 
        .pure(((env, stream, subs, backtrace), env.validateTypeConstructorLifetimeParams))
      }
    }
  }
}
