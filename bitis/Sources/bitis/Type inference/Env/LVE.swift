//
// Created by Toni K. Turk on 05/11/2020.
//

import Foundation

public typealias LVE = LocalVariableEnv

public struct LocalVariableEnv {
  internal private(set) var scopes: [SymbolTable]
}

public extension LocalVariableEnv {
  struct SymbolTable {
    var elements: [(String, Kind)]

    mutating func append(name: String, kind: Kind) {
      elements.append((name, kind))
    }

    subscript(name: String) -> Kind? {
      get {
        elements
          .reversed()
          .first { $0.0 == name }?.1
      }
    }
    
    static var empty: SymbolTable {
      .init(elements: .empty())
    }
  }

  enum Kind {
    case prototype(TCType) // type signature
    case bind(TCType)

    var type: TCType {
      switch self {
      case .prototype(let type):
        return type
      case .bind(let type):
        return type
      }
    }
  }
}

// MARK: - API
public extension LocalVariableEnv {
  static var empty: LVE = .init(
    scopes: [.init(elements: .empty())]
  )

  // query

  func queryType(for name: String, currentScopeOnly: Bool) -> TCType? {
    guard !currentScopeOnly else {
      return currentScope[name]?.type
    }
    for table in scopes.reversed() {
      guard let kind = table[name] else { continue }
      return kind.type
    }
    return nil
  }

  func containsVariable(with name: String) -> Bool {
    queryType(for: name, currentScopeOnly: false) != nil
  }

  func containsOnCurrentScope(binding: String) -> Bool {
    switch currentScope[binding] {
    case .bind?:
      return true
    case _:
      return false
    }
  }

  func containsOnCurrentScope(prototype: String) -> Bool {
    switch currentScope[prototype] {
    case .prototype?:
      return true
    case _:
      return false
    }
  }

  // bind

  func bind(name: String, with type: TCType) -> LVE {
    guard !scopes.isEmpty else {
      fatalError("LVE.addVariable: no scopes!")
    }
    var env = self
    env.scopes[env.scopes.count - 1].append(name: name, kind: .bind(type))
    return env
  }

  func bind(prototype: String, with type: TCType) -> LVE {
    guard !scopes.isEmpty else {
      fatalError("LVE.addVariable: no scopes!")
    }
    var env = self
    env.scopes[env.scopes.count - 1].append(name: prototype, kind: .prototype(type))
    return env
  }
}

extension LocalVariableEnv: HasScope {
  public func pushScope() -> LVE {
    var env = self
    env.scopes.append(.init(elements: .empty()))
    return env
  }

  public func popScope() -> LVE {
    guard scopes.count > 1 else {
      fatalError("LVE.popScope: not allowed to pop when count < 2!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}

extension LocalVariableEnv: Mergable {
  public func formUnion(_ other: LocalVariableEnv) -> Result<LocalVariableEnv, TypeError> {
    // TODO: - Refactor to a more pure code.
    
    assert(self.scopes.count == other.scopes.count, "LocalVariableEnv.formUnion: Environments must have the same number of scopes!") // NOTE: - this my assumption ATM, which might be wrong ...
    
    var env = LVE(scopes: .init(repeating: .empty, count: scopes.count))
    
    for i in 0..<scopes.count {
      let s1 = self.scopes[i]
      let s2 = other.scopes[i]
      var alreadyResolved = Set<String>()
      
      for (name, kind) in s1.elements {
        switch (kind, s2[name]) {
        case (_, nil):
          env.scopes[i].append(name: name, kind: kind)
          alreadyResolved.insert(name)
        case let (.prototype(t1), .prototype(t2)?):
          switch t1.chooseMoreSpecificType(type: t2) {
          case .success(let type):
            env.scopes[i].append(name: name, kind: .prototype(type))
            alreadyResolved.insert(name)
          case .failure(let error):
            return .failure(error)
          }
        case let (.prototype(t1), .bind(t2)?),
             let (.bind(t1), .prototype(t2)?),
             let (.bind(t1), .bind(t2)?):
          switch t1.chooseMoreSpecificType(type: t2) {
          case .success(let type):
            env.scopes[i].append(name: name, kind: .bind(type))
            alreadyResolved.insert(name)
          case .failure(let error):
            return .failure(error)
          }
        }
        
        for (name, kind) in s2.elements where !alreadyResolved.contains(name) {
          env.scopes[i].append(name: name, kind: kind)
        }
      }
    }
    return .success(self)
  }
}

// MARK: - Internal
extension LocalVariableEnv {
  var currentScope: SymbolTable {
    guard let table = scopes.last else {
      fatalError("LVE.currentScope: empty list!")
    }
    return table
  }
}
