//
// Created by Toni K. Turk on 13/11/2020.
//

import Foundation
import Bow

// TODO: - Can we make this a Comonad instance?
public struct UniqStream {
  private let current: UInt32
  private let ignore: Set<String>

  public init(current: UInt32 = 0, ignore: Set<String> = .empty()) {
    self.current = current
    self.ignore = ignore
  }

  func next() -> (String, UniqStream) {
    var current = self.current
    var scalar: String!
    while true {
      let ascii = current % 25 + UnicodeScalar("a").value
      scalar = String(UnicodeScalar(ascii)!)
      if ignore.contains(scalar) {
        current += 1
      } else {
        break
      }
    }
    let stream = UniqStream(current: current + 1, ignore: ignore)
    return (scalar, stream)
  }
}

// MARK: - Monadic actions
public extension UniqStream {
  enum Action {
    public static func nextUniq() -> TypecheckM<String> {
      .init { env, stream, subs, backtrace in
        let (next, stream_) = stream.next()
        return .pure(((env, stream_, subs, backtrace), next))
      }
    }

    public static func ignore(names: Set<String>) -> TypecheckM<()> {
      .init { env, stream, subs, backtrace in
        let stream_ = UniqStream(
          current: stream.current,
          ignore: names.union(stream.ignore))
        return .pure(((env, stream_, subs, backtrace), ()))
      }
    }
  }
}
