//
// Created by Toni K. Turk on 05/11/2020.
//

import Foundation
import Bow
import SwiftParsec

public typealias TCAst = AST

extension TCAst: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst> {
    bindings
      .typeChecker()
      .map { .init(bindings: $0) }^
  }

  public var type: TCType? {
    nil
  }
}

// MARK: - Binding
extension TCAst.Binding: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst.Binding> {
    switch self {
    case let .prototype(position, name, type, nil):
      return Env.ActionTVE.inNewScope {
        Env.ActionLVE.doesNotContain(prototype: name) *> type.typeChecker() >>- { type in
          Env.ActionTVE.typeVariablesOnCurrentScope() >>- { polyTypeVars in
            Env.ActionLVE.bind(prototype: name, with: type.type!) >>- {
              UniqStream.Action.ignore(names: polyTypeVars) >>- {
                Env.Action.validateTypeConstructorLifetimeParams(!type.type!.isArrow) {
                  type.typeChecker() >>- { type in
                    let prototype = TCAst.Binding.prototype(
                      position: position,
                      name: name,
                      type: type,
                      tcType: type.type!)
                    return .pure(prototype)^
                  }
                } 
              }
            }
          }
        }
      }
    case let .prototype(_, name, type: _, type?):
      return Env.ActionLVE.queryTypeEither(for: name) >>- { either in
        switch either.isLeft {
        case true:
          return .pure(self)^
        case false:
          // @NOTE: - This handles REPL mode where an already typechecked variable
          // has to be manually added to the environment.
          // @TODO: - Check if we are really in REPL mode, otherwise
          // there is something wrong.
          return Env.ActionLVE.bind(prototype: name, with: type) *> .pure(self)^
        }
      }
    case let .binding(position, name, cases, nil, _, _):
      return Subs.Action.inNewScope {
        Env.ActionLVE.doesNotContain(binding: name) >>- {
          func casesM(type: TCType?) -> TypecheckM<[TCAst.Binding.Case]> {
            cases.foldM([TCAst.Binding.Case]()) { acc, case_ in
              case_.typeChecker(functionName: name) >>- { case_ in
                let `case` = case_
                if acc.isEmpty {
                  return .pure([`case`])^
                }
                return Subs.Action.createSubstitutionWithMoreSpecificType(
                  a: acc.last!.type!,
                  b: case_.type!
                ) >>- { _ in
                  (acc + [`case`]).typeChecker(functionName: name) >>- { cases in
                    .pure(cases)^
                  }
                }
              }
            }^
          }
          
          func bindM(_ cases: [TCAst.Binding.Case], prototypeType: TCType?) -> TypecheckM<TCAst.Binding> {
            Env.ActionLVE.bind(
              name: name,
              with: prototypeType ?? cases.first!.type!
            ) >>- {
              let binding = TCAst.Binding.binding(
                position: position,
                name: name,
                cases: cases.map { $0.withType(prototypeType ?? $0.type!) }, // FIXME: - This is an adhoc fix
                tcType: prototypeType ?? cases.first!.type)
              return .pure(binding)^
            }
          }
        
          return Env.ActionLVE.queryTypeEither(for: name, currentScopeOnly: true) >>- { either in
            switch either.isLeft {
            case true:
              return UniqStream.Action.ignore(names: either.leftValue.typeVariables) >>- {
                casesM(type: either.leftValue) >>- { cases in
                  TCType.Action.matchTypes(
                    lhs: cases.first!.type!,
                    rhs: either.leftValue,
                    strict: true
                  ) >>- { _ in
                    Subs.Action.inNewScope {
                      Subs.Action.createSubstitutionWithMoreSpecificType(
                        a: either.leftValue,
                        b: cases.first!.type!
                      ) >>- { _ in
                        cases.typeChecker(functionName: name) >>- {
                          bindM($0, prototypeType: either.leftValue)
                        }
                      }
                    }
                  }
                }
              }
            case false:
              return casesM(type: nil) >>- { bindM($0, prototypeType: nil) }
            }
          }
        }
      }
    case let .binding(_, name, _, type?, _, _):
      return Env.ActionLVE.queryTypeEither(for: name) >>- { either in
        switch either.isLeft {
        case true:
          return .pure(self)^
        case false:
          // @NOTE: - This handles REPL mode where an already typechecked variable
          // has to be manually added to the environment.
          // @TODO: - Check if we are really in REPL mode, otherwise
          // there is something wrong.
          return Env.ActionLVE.bind(name: name, with: type) *> .pure(self)^
        }
      }
    // @NOTE: - Handle both cases (if already typechecked or not).
    // (REPL)
    case let .data(position, name, params, constructors, _):
      return Env.ActionLTE.doesNotContain(name: name) >>- {
      Env.ActionLTE.bind(
        name: name,
        with: .data(
          name: name,
          constructors: [],
          genericParams: params)
      ) >>- { // NOTE: - add into env for recursive definitions
        Env.ActionTVE.inNewScope {
          Env.ActionTVE.addTypeVariables(names: params.typeParameters) >>- {
            constructors.map { $0.typeChecker() }.sequence()^ >>- { constructors in
              let typeConstructor = TCType.polymorphic(
                typeVariables: Set(params.typeParameters),
                context: .empty,
                type: .typeConstructor(
                  name: name,
                  types: params.typeParameters.map { .typeVariable(name: $0, lifetimeSpecifier: nil) },
                  lifetimeSpecifiers: params.lifetimeParameters,
                  lifetimeSpecifier: params.lifetimeParameters.first)) // @FIXME: - Use all lifetime params, not just the first one!
              let constructorTypes = constructors
                .map { $0.type!.addReturnType(typeConstructor.asSimple!).asPolymorphic }
              return constructors.typeChecker() >>- { constructors in // to make sure type constr. have correct number of generic params
                let data = TCAst.Binding.data(
                  position: position,
                  name: name,
                  params: params,
                  constructors: constructors,
                  tcType: .data(
                    name: name,
                    constructors: zip(constructors, constructorTypes).map { .init(name: $0.0.name, type: $0.1) },
                    genericParams: params))
                return zip(constructorTypes, constructors)
                  .map { Env.ActionLVE.bind(name: $1.name, with: $0) }
                  .sequence()^ >>- { _ in
                    Env.ActionLTE
                      .bind(name: name, with: data.type!)
                      .map { data }^
                  }
              }
            }
          }
        }
      }
      }
    case let .print(position, expression, nil):
      return expression.typeChecker() >>- { expr in
        .pure(.print(
          position: position,
          expression: expr,
          tcType: expr.type!
        ))^
      }
    case _:
      return .pure(self)^
    }
  }

  public var type: TCType? {
    switch self {
    case .prototype(_, _, _, let type),
         .binding(_, _, _, let type, _, _),
         .data(_, _, _, _, let type),
         .print(_, _, let type):
      return type
    case .include:
      fatalError()
    }
  }
}

extension TCAst.Binding.Case {
  /// TODO: - Refactor case typechecking as `name` of the binding
  /// is now part of the AST.
  
  public func typeChecker(functionName: String) -> TypecheckM<TCAst.Binding.Case> {
    Env.ActionLVE.queryTypeEither(for: functionName, currentScopeOnly: true) >>- { prototypeType in
      switch (prototypeType.isLeft, type) {
      case (true, nil):
        return typeChecker(prototypeType: prototypeType.leftValue)
      case (true, let type?):
        return typeChecker(with: type)
      case (false, nil):
        return typeChecker(prototypeType: nil)
      case (false, let type?):
        return typeChecker(with: type)
      }
    }
  }
  
  private func typeChecker(withoutType: () = (), prototypeType: TCType?) -> TypecheckM<TCAst.Binding.Case> {
    func addVariablesToEnvM(pattern: TCAst.Pattern, type: TCType?) -> TypecheckM<()> {
      switch pattern {
      case .deconstruct(_, let constructor, _, _, _):
        return Env.ActionLVE.queryType(for: constructor) >>- { constructorType in
          let lifetime = type.flatMap { $0.isReference ? type?.lifetime : nil }
          return Env.ActionLVE.addPatternVariableNamesToEnv(
            pattern,
            afterBeingTypeChecked: lifetime.map { constructorType.wrapWithReference(lifetimeSpecifier: $0.identifier.identifier) } ?? constructorType)
        }
      case _:
        return Env.ActionLVE.addPatternVariableNamesToEnv(
          pattern,
          afterBeingTypeChecked: pattern.type!)
      }
    }
    func addPatternVariableNamesToEnvM(parameters: [TCAst.Pattern]) -> TypecheckM<[TCAst.Pattern]> {
      func arrowArguments() -> [TCType?] {
        guard let prototypeType = prototypeType else {
          return .init(repeating: nil, count: parameters.count)
        }
        var curriedArgs: [TCType?] = prototypeType.curriedArrowArguments
        for _ in curriedArgs.count..<max(parameters.count, curriedArgs.count) {
          curriedArgs.append(nil)
        }
        return curriedArgs
      }
      
      let curriedArrowArguments = arrowArguments()
      return zip(parameters, curriedArrowArguments)
        .map { addVariablesToEnvM(pattern: $0.0, type: $0.1) }
        .sequence()
        .map { _ in parameters }^
    }
      
    return Subs.Action.inNewScope {
      Env.Action.inNewScope {
        let typecheckParamsM: TypecheckM<[TCAst.Pattern]>
        let typecheckBodyM: TypecheckM<TCAst.Expression>
        switch prototypeType {
        case let existingType?:
          let curriedArrowArguments = existingType.curriedArrowArguments
          do {
            // TODO: - This check might actually be uneccessary; check!
            if curriedArrowArguments.count >= parameters.count {
              let parameters = zip(curriedArrowArguments, parameters)
                .map { $0.1.withType($0.0) }
              typecheckParamsM = parameters.typeChecker() >>- addPatternVariableNamesToEnvM >>- {
                $0.typeChecker()
              }
            } else {
              typecheckParamsM = parameters.typeChecker() >>- addPatternVariableNamesToEnvM
            }
          }
          do {
            typecheckBodyM = {
              func updateExpression(expr: TCAst.Expression, types: [TCType]) -> TypecheckM<TCAst.Expression> {
                switch expr {
                case let .lambda(position, parameter, body, type, lifetime):
                  guard !types.first!.isTypeVariable else {
                    return .pure(expr)^
                  }
                  let parameter = parameter.withType(types.first!)
                  return addVariablesToEnvM(
                    pattern: parameter,
                    type: types.first!
                  ) >>- {
                    updateExpression(
                      expr: body,
                      types: Array(types.dropFirst())
                    ) >>- { body in
                      let lambda = TCAst.Expression.lambda(
                        position: position,
                        parameter: parameter,
                        body: body,
                        tcType: type,
                        lifetime: lifetime)
                      return .pure(lambda)^
                    }
                  }
                case _:
                  return .pure(expr)^
                }
              }
              let diff = curriedArrowArguments.count - parameters.count
              guard diff > 0 else {
                return body.typeChecker()
              }
              return updateExpression(
                expr: body,
                types: Array(curriedArrowArguments[parameters.count...])
              ) >>- {
                $0.typeChecker()
              }
            }()
          }
        case nil:
          typecheckParamsM = parameters.typeChecker() >>- addPatternVariableNamesToEnvM >>- { $0.typeChecker() }
          typecheckBodyM = body.typeChecker()
        }
        
        return typecheckParamsM >>- { parameters in
          typecheckBodyM >>- { body in
            parameters.typeChecker() >>- { parameters in
              if let prototypeType = prototypeType {
                // TODO: - Code dupl
                return Subs.Action.createSubstitutionWithMoreSpecificType(
                  a: prototypeType.curriedArrow.dropFirst(parameters.count).formArrow,
                  b: body.type!
                ) >>- { _ in
                  body.typeChecker() >>- { body in
                    let bindingType = parameters
                      .reversed()
                      .reduce(body.type!) { acc, patt in .createArrow(arg: patt.type!, ret: acc) }
                    let case_ = TCAst.Binding.Case(
                      name: name,
                      position: position,
                      parameters: parameters,
                      body: body,
                      type: bindingType)
                    return .pure(case_)^
                  }
                }
              } else {
                // TODO: - Code dupl
                let bindingType = parameters
                  .reversed()
                  .reduce(body.type!) { acc, patt in .createArrow(arg: patt.type!, ret: acc) }
                let case_ = TCAst.Binding.Case(
                  name: name,
                  position: position,
                  parameters: parameters,
                  body: body,
                  type: bindingType)
                return .pure(case_)^
              }
            }
          }
        }
      }
    }
  }
  
  private func typeChecker(with type: TCType) -> TypecheckM<TCAst.Binding.Case> {
    Subs.Action.mapType(type) >>- { subsType in
      parameters.typeChecker() >>- { parameters in
        body.typeChecker() >>- { body in
          let case_ = TCAst.Binding.Case(
            name: name,
            position: position,
            parameters: parameters,
            body: body,
            type: subsType ?? type)
          return .pure(case_)^
        }
      }
    }
  }
}

extension TCAst.Binding.Constructor: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst.Binding.Constructor> {
    let typecheckTypesM = types.map {
      $0.typeChecker() >>- { type -> TypecheckM<TCAst.AType> in
        Env.ActionTVE.typeVariablesOnCurrentScope() >>- { currentTypeVars in
          /// Make sure all type variables are actually defined by the data binding.
          let typeVars = type.type!.typeVariables
          return typeVars.isSubset(of: currentTypeVars)
            ? .pure(type)^
            : .error(.unknownTypeVariable(typeVars.subtracting(currentTypeVars).first!))
        }
      }
    }
      
    return typecheckTypesM.sequence()^ >>- { types in
      let constr = TCAst.Binding.Constructor(
        position: position,
        name: name,
        types: types,
        type: formConstructorType(from: types.map { $0.type! }),
        lifetime: nil)
      return .pure(constr)^
    }
  }
  
  func formConstructorType<C: RandomAccessCollection>(
    from types: C
  ) -> TCType where C.Element == TCType, C.Index == Int {
    switch types.count {
    case 0:
      return .unit
    case 1:
      return types[0]
    case 2:
      return .createArrow(arg: types[0], ret: types[1])
    case _:
      return .createArrow(arg: types[0], ret: formConstructorType(from: Array(types.dropFirst())))
    }
  }
}

// MARK: - Pattern
extension TCAst.Pattern: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst.Pattern> {
    switch self {
    case .list(let list):
      return list.typeChecker().map(TCAst.Pattern.list)^
    case let .identifier(position, identifier, nil, _):
      return UniqStream.Action
        .nextUniq()
        .map { name in
          TCAst.Pattern.identifier(
            position: position,
            identifier: identifier,
            tcType: .polymorphic(typeVariables: [name], context: .empty, type: .typeVariable(name: name)))
        }^
    case let .identifier(position, identifier, type?, _):
      return Env.ActionLVE.queryTypeEither(for: identifier) >>- { either in
        if type.isTypeVariable {
          let queriedType = either.isLeft ? either.leftValue : type
          return Subs.Action
            .mapType(queriedType)
            .map {
              .identifier(
                position: position,
                identifier: identifier,
                tcType: $0 ?? queriedType)
            }^
        } else {
          return Subs.Action
            .mapType(type)
            .map {
              .identifier(
                position: position,
                identifier: identifier,
                tcType: $0 ?? type)
            }^
        }
      }
    case let .wildcard(position, nil):
      return UniqStream.Action.nextUniq() >>- { name in
        let pattern = TCAst.Pattern.wildcard(
          position: position,
          tcType: .polymorphic(typeVariables: [name], context: .empty, type: .typeVariable(name: name)))
        return .pure(pattern)^
      }
    case let .wildcard(position, type?):
      return Subs.Action.mapType(type) >>- { subsType in
        let pattern = TCAst.Pattern.wildcard(
          position: position,
          tcType: subsType ?? type)
        return .pure(pattern)^
      }
    case let .tuple(position, patterns, nil, _):
      return patterns.typeChecker() >>- { patterns in
        var elements = [TCType.SimpleType]()
        var typeVariables = TCType.TypeVariables()
        
        for el in patterns {
          switch el.type! {
          case let .polymorphic(typeVariables_, _, type):
            typeVariables.formUnion(typeVariables_)
            elements.append(type)
          case let .simple(simple):
            elements.append(simple)
          case _:
            fatalError("TCAst.Type: Invalid type")
          }
        }
        
        let finalType: TCType
        if typeVariables.isEmpty {
          finalType = .simple(.tuple(elements: elements))
        } else {
          finalType = .polymorphic(
            typeVariables: typeVariables,
            context: .empty,
            type: .tuple(elements: elements))
        }
        
        let pattern = TCAst.Pattern.tuple(
          position: position,
          patterns: patterns,
          tcType: finalType)
        return .pure(pattern)^
      }
    case let .tuple(position, patterns, type?, _):
      guard patterns.count > 1 else {
        return .pure(self)^
      }
      guard let tupleType = type.asTuple, patterns.count == tupleType.count else {
        return .error(.patternMatching(.cannotMatch(pattern: self, type: type)))
      }
      return Subs.Action.mapType(type) >>- { subsType in
        zip(patterns, tupleType)
          .map { $0.0.withType($0.1) }
          .typeChecker() >>- { patterns in
            let pattern = TCAst.Pattern.tuple(
              position: position,
              patterns: patterns,
              tcType: subsType ?? type)
            return .pure(pattern)^
          }
      }
    case let .literal(position, literal, nil, _):
      let pattern = TCAst.Pattern.literal(
        position: position,
        literal: literal,
        tcType: literal.type)
      return .pure(pattern)^
    case let .ref(position, pattern, nil, _):
      return pattern.typeChecker() >>- { pattern in
//        guard let dereferencedType = pattern.type!.dereference else {
//          return .error(.cannotDereference(pattern.type!))
//        }
        let pattern = TCAst.Pattern.ref(
          position: position,
          pattern: pattern,
          tcType: .reference(pattern.type!.asSimple!).asPolymorphic)
        return .pure(pattern)^
      }
    case let .ref(position, pattern, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        pattern/*.withType(type)*/.typeChecker() >>- { pattern in
          let pattern = TCAst.Pattern.ref(
            position: position,
            pattern: pattern,
            tcType: subsType ?? type)
          return .pure(pattern)^
        }
      }
    // MARK: - Deconstruct
    case let .deconstruct(position, constructor, patterns, nil, _):
      return Env.ActionLVE.queryType(for: constructor) >>- { constructorType in
        let typeConstructor = constructorType
          .fullyAppliedReturnType!
          .asTypeConstructor!
        let constructorArgs = constructorType.curriedArrowArguments
        guard constructorArgs.count == patterns.count else {
          return .error(.patternMatching(.cannotMatch(pattern: self, type: constructorType))) // TODO: - probably not a correct error ??
        }
        return Env.ActionLTE.queryType(for: typeConstructor.name) >>- { dataType in
          patterns.typeChecker() >>- { patterns in
            Subs.Action.inNewScope {
              zip(patterns.map { $0.type! }, constructorArgs)
                .map { Subs.Action.createSubstitutionWithMoreSpecificType(a: $0.0, b: $0.1) }
                .sequence()^ >>- { _ in
                  Subs.Action.mapType(constructorType) >>- { constructorType_ in
                    let typeConstructor = (constructorType_ ?? constructorType)
                      .fullyAppliedReturnType!
                      .asTypeConstructor!
                    let patt = TCAst.Pattern.deconstruct(
                      position: position,
                      constructor: constructor,
                      patterns: patterns,
                      tcType: .simple(.typeConstructor(
                        name: typeConstructor.name,
                        types: typeConstructor.types,
                        lifetimeSpecifiers: typeConstructor.lifetimeSpecifiers,
                        lifetimeSpecifier: nil)).asPolymorphic)
                    return .pure(patt)^
                  }
                }
            }
          }
        }
      }
    case let .deconstruct(position, constructor, patterns, type?, _):
      // TODO: - Code duplication
      return Subs.Action.mapType(type) >>- { mappedType in
        let existingType = mappedType ?? type
        return Env.ActionLVE.queryType(for: constructor) >>- { constructorType in
          let typeConstructor = constructorType
            .fullyAppliedReturnType!
            .asTypeConstructor!
          let lifetime = type.isReference ? (type.lifetime ?? .local("'X")): nil
          let constructorArgs = (lifetime
            .map {
              constructorType
                .wrapWithReference(lifetimeSpecifier: $0.identifier.identifier)
                .replaceLifetimeSpecifier(
                  typeConstructor.lifetimeSpecifier.map(Lifetime.init(stringLiteral:)),
                  with: existingType.lifetime)
            } ?? constructorType)
            .curriedArrowArguments
          
          guard constructorArgs.count == patterns.count else {
            return .error(.patternMatching(.cannotMatch(pattern: self, type: constructorType))) // TODO: - probably not a correct error ??
          }
          return Env.ActionLTE.queryType(for: typeConstructor.name) >>- { dataType in
            zip(patterns, constructorArgs).map({ $0.0.withType($0.1) }).typeChecker() >>- { patterns in
              Subs.Action.inNewScope {
                zip(patterns.map { $0.type! }, constructorArgs)
                  .map { Subs.Action.createSubstitutionWithMoreSpecificType(a: $0.0, b: $0.1) }
                  .sequence()^ >>- { _ in
                    patterns.typeChecker() >>- { patterns in
                      let patt = TCAst.Pattern.deconstruct(
                        position: position,
                        constructor: constructor,
                        patterns: patterns,
                        tcType: existingType)
                      return .pure(patt)^
                    }
                  }
              }
            }
          }
        }
      }
    case _:
      return .pure(self)^
    }
  }

  public var type: TCType? {
    switch self {
    case .identifier(_, _, let type, _),
         .wildcard(_, let type),
         .literal(_, _, let type, _),
         .tuple(_, _, let type, _),
         .ref(_, _, let type, _),
         .deconstruct(_, _, _, let type, _):
      return type
    case .list(let list):
      return list.type
    }
  }
}

// MARK: - Pattern.List
extension TCAst.Pattern.List: TypeCheckable {
  public func typeChecker() -> TypecheckM<AST.Pattern.List> {
    switch self {
    case .empty(let position, nil, _):
      return UniqStream.Action.nextUniq() >>- { name in
        let list = TCAst.Pattern.List.empty(
          position: position,
          tcType: TCType.list(element: .typeVariable(name: name)).asPolymorphic)
        return .pure(list)^
      }
    case .empty(let position, let type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        let list = TCAst.Pattern.List.empty(
          position: position,
          tcType: subsType ?? type)
        return .pure(list)^
      }
    case let .cons(position, head, tail, nil, _):
      return tail.typeChecker() >>- { tail in
        head.typeChecker() >>- { head in
          let headAsListType: TCType
          let tailType: TCType
          
          switch head.type! {
          case let .polymorphic(typeVariables, context, type):
            headAsListType = .polymorphic(typeVariables: typeVariables, context: context, type: .list(element: type))
          case .simple(let type):
            headAsListType = .list(element: type)
          case _:
//            return .error(.unexpectedType(head.type!))
            fatalError()
          }
          
          switch tail.type! {
          case .polymorphic(_, _, .list),
               .simple(.list):
            tailType = tail.type!
          case let .polymorphic(typeVariables, context, .typeVariable(name, lifetimeSpecifier)):
            tailType = .polymorphic(
              typeVariables: typeVariables,
              context: context,
              type: .list(element: .typeVariable(name: name, lifetimeSpecifier: lifetimeSpecifier)))
          case _:
            return .error(.typeMismatch(.tailNotAList(tail: tail.type!)))
          }
          
          return Subs.Action.inNewScope {
            Subs.Action.createSubstitutionWithMoreSpecificType(a: headAsListType, b: tailType) >>- { type in
              let listElement: TCType.SimpleType
              let lifetimeSpecifier: String?
              switch type as TCType {
              case let .polymorphic(_, _, .list(element, ls)):
                listElement = element
                lifetimeSpecifier = ls
              case let .simple(.list(element, ls)):
                listElement = element
                lifetimeSpecifier = ls
              case _:
                fatalError()
              }
              let head = head.withType(.simple(listElement).withLifetime(lifetimeSpecifier))
              let tail = tail.withType(type)
              return head.typeChecker() >>- { head in
                tail.typeChecker() >>- { tail in
                  let list = TCAst.Pattern.List.cons(
                    position: position,
                    head: head,
                    tail: tail,
                    tcType: tail.type!)
                  return .pure(list)^
                }
              }
            }
          } <|> .error(.typeMismatch(.headAndTail(head: head.type!, tail: tail.type!)))
        }
      }
    case let .cons(position, head, tail, type?, _):
//      let element = type.asList!
//      let list = TCType.list(element: element).asPolymorphic
      return Subs.Action.mapType(type) >>- { subsType in
        head/*.withType(element.asPolymorphic)*/.typeChecker() >>- { head in
          tail/*.withType(list)*/.typeChecker() >>- { tail in
            let list = TCAst.Pattern.List.cons(
              position: position,
              head: head,
              tail: tail,
              tcType: subsType ?? type)
            return .pure(list)^
          }
        }
      }
    }
  }

  public var type: TCType? {
    switch self {
    case .empty(_, let type, _),
         .cons(_, _, _, let type, _):
      return type
    }
  }
}

// MARK: - Type
extension TCAst.AType: TypeCheckable {
  public func typeChecker() -> TypecheckM<AST.AType> {
    switch self {
    case let .name(position, "Int", lifetimeSpecifier, nil):
      return .pure(.name(
        position: position,
        name: "Int",
        lifetimeParameter: lifetimeSpecifier,
        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: lifetimeSpecifier))))^
    case let .name(position, "Float", lifetimeSpecifier, nil):
      return .pure(.name(
        position: position,
        name: "Float",
        lifetimeParameter: lifetimeSpecifier,
        tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: lifetimeSpecifier))))^
    case let .name(position, "Bool", lifetimeSpecifier, nil):
      return .pure(.name(
        position: position,
        name: "Bool",
        lifetimeParameter: lifetimeSpecifier,
        tcType: .simple(.atomic(.bool, lifetimeSpecifier: lifetimeSpecifier))))^
    case let .name(position, "Char", lifetimeSpecifier, nil):
      return .pure(.name(
        position: position,
        name: "Char",
        lifetimeParameter: lifetimeSpecifier,
        tcType: .simple(.atomic(.char, lifetimeSpecifier: lifetimeSpecifier))))^
    case let .name(position, "String", lifetimeSpecifier, nil):
      return .pure(.name(
        position: position,
        name: "String",
        lifetimeParameter: lifetimeSpecifier,
        tcType: .simple(.atomic(.string, lifetimeSpecifier: lifetimeSpecifier))))^
    case let .name(position, name, lifetimeSpecifier, nil):
      return Env.ActionLTE.queryType(for: name).handleError { _ in TCType.simple(.typeVariable(name: name)).asPolymorphic }^ >>- { type in
        let astType = TCAst.AType.name(
          position: position,
          name: name,
          lifetimeParameter: lifetimeSpecifier,
          tcType: type)
        return .pure(astType)^
      }
    case let .list(position, element, lifetimeSpecifier, _):
      return element.typeChecker() >>- { element in
        let finalType: TCType
        switch element.type! {
        case let .polymorphic(typeVariables, context, type):
          finalType = .polymorphic(
            typeVariables: typeVariables,
            context: context,
            type: .list(element: type, lifetimeSpecifier: lifetimeSpecifier))
        case .simple(let simple):
          finalType = .list(element: simple, lifetimeSpecifier: lifetimeSpecifier)
        case _:
          fatalError("TCAst.Type: Invalid type")
        }

        let list = TCAst.AType.list(
          position: position,
          element: element,
          lifetimeParameter: lifetimeSpecifier,
          tcType: finalType)
        return .pure(list)^
      }
    case let .tuple(position, types, lifetimeSpecifier, _):
      guard !types.isEmpty else {
        let tuple = TCAst.AType.tuple(
          position: position,
          types: types,
          lifetimeParameter: lifetimeSpecifier,
          tcType: .unit)
        return .pure(tuple)^
      }
      return types.map { $0.typeChecker() }.sequence()^ >>- { types in
        var elements = [TCType.SimpleType]()
        var typeVariables = TCType.TypeVariables()
        
        for el in types {
          switch el.type! {
          case let .polymorphic(typeVariables_, _, type):
            typeVariables.formUnion(typeVariables_)
            elements.append(type)
          case let .simple(simple):
            elements.append(simple)
          case _:
            fatalError("TCAst.Type: Invalid type")
          }
        }

        let finalType: TCType
        if typeVariables.isEmpty {
          finalType = .simple(.tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier))
        } else {
          finalType = .polymorphic(
            typeVariables: typeVariables,
            context: .empty,
            type: .tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier))
        }

        let tuple = TCAst.AType.tuple(
          position: position,
          types: types,
          lifetimeParameter: lifetimeSpecifier,
          tcType: finalType)
        return .pure(tuple)^
      }
    case let .typeConstructor(position, name, types, lifetimeSpecifiers, lifetimeSpecifier, nil):
      return Env.ActionLTE.queryType(for: name) >>- { _ in
        types.typeChecker()^ >>- { tcTypes in
          let typeConstructor = TCAst.AType.typeConstructor(
            position: position,
            name: name,
            types: tcTypes,
            lifetimeSpecifiers: lifetimeSpecifiers,
            lifetimeParameter: lifetimeSpecifier,
            tcType: .simple(.typeConstructor(
              name: name,
              types: tcTypes.map { $0.type!.asSimple! },
              lifetimeSpecifiers: lifetimeSpecifiers,
              lifetimeSpecifier: lifetimeSpecifier)).asPolymorphic)
          return .pure(typeConstructor)^
        }
      }
    case let .typeConstructor(_, name, types, lifetimeSpecifiers, _, _):
      return Env.ActionLTE.queryType(for: name) >>- { dataType in
        Env.Action.isBinding >>- { isVariableBinding in
          let dataType = dataType.asData!
          var expected = dataType.genericParams.typeParameters.count
          guard expected == types.count else {
            return .error(.typeConstructor(.genericTypes(expected: expected, got: types.count)))
          }
          if isVariableBinding {
            guard lifetimeSpecifiers.isEmpty else {
              return .error(.typeConstructor(.genericLifetimes(expected: 0, got: lifetimeSpecifiers.count)))
            }
          } else {
            // NOTE: - Only functions are required to specifiy lifetime params
            expected = dataType.genericParams.lifetimeParameters.count
            guard expected == Set(lifetimeSpecifiers).count else {
              return .error(.typeConstructor(.genericLifetimes(expected: expected, got: lifetimeSpecifiers.count)))
            }
          }
          return .pure(self)^
        }
      }
    case let .function(position, arg, ret, lifetimeSpecifier, _):
      return arg.typeChecker() >>- { arg in
        ret.typeChecker() >>- { ret in
          let fn = TCAst.AType.function(
            position: position,
            arg: arg,
            ret: ret,
            lifetimeParameter: lifetimeSpecifier,
            tcType: .createArrow(arg: arg.type!, ret: ret.type!, lifetimeSpecifier: lifetimeSpecifier))
          return .pure(fn)^
        }
      }
    case let .ref(position, type, lifetime, _):
      return type.typeChecker() >>- { type in
        let refType = TCAst.AType.ref(
          position: position,
          type: type,
          lifetimeParameter: lifetime,
          tcType: type.type!.injectLifetimeToReference(lifetimeSpecifier: lifetime))
        return .pure(refType)^
      }
    case _:
      return .pure(self)^
    }
  }

  public var type: TCType? {
    switch self {
    case .name(_, _, let lifetimeParameter, let type),
         .list(_, _, let lifetimeParameter, let type),
         .tuple(_, _, let lifetimeParameter, let type),
         .typeConstructor(_, _, _, _, let lifetimeParameter, let type),
         .function(_, _, _, let lifetimeParameter, let type),
         .ref(_, _, let lifetimeParameter, let type):
      return type?.withLifetime(lifetimeParameter)
    }
  }
}

// MARK: - Expression
extension TCAst.Expression: TypeCheckable {
  public func typeChecker() -> TypecheckM<AST.Expression> {
    switch self {
    case .binary(let binary):
      return binary.typeChecker().map { .binary($0) }^
    case let .literal(position, literal, nil, _):
      let expr = TCAst.Expression.literal(
        position: position,
        literal: literal,
        tcType: literal.type)
      return .pure(expr)^
    case let .literal(position, literal, type?, _):
      return Subs.Action.mapType(type) >>- { mappedType in
        let expr = TCAst.Expression.literal(
          position: position,
          literal: literal,
          tcType: mappedType ?? type)
        return .pure(expr)^
      }
    // MARK: - Lambda
    case let .lambda(position, parameter, body, nil, _):
      return parameter.typeChecker() >>- { parameter in
        Env.ActionLVE.addPatternVariableNamesToEnv(
          parameter,
          afterBeingTypeChecked: parameter.type!
        ).withState() >>- { _, s1 in
          body.typeChecker().withState() >>- { body, s2 in
            Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
              parameter.typeChecker() >>- { parameter in
                body.typeChecker() >>- { body in
                  let expr = TCAst.Expression.lambda(
                    position: position,
                    parameter: parameter,
                    body: body,
                    tcType: .createArrow(arg: parameter.type!, ret: body.type!))
                  return .pure(expr)^
                }
              }
            }
          }
        }
      }
    case let .lambda(position, parameter, body, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        parameter.typeChecker() >>- { parameter in
          body.typeChecker() >>- { body in
            let expr = TCAst.Expression.lambda(
              position: position,
              parameter: parameter,
              body: body,
              tcType: subsType ?? type)
            return .pure(expr)^
          }
        }
      }
    case let .tuple(position, expressions, nil, _):
      switch expressions.count {
      case 0:
        let expr = TCAst.Expression.tuple(
          position: position,
          expressions: expressions,
          tcType: .unit)
        return .pure(expr)^
      case 1:
        return expressions[0].typeChecker() >>- { expression in
          let expr = TCAst.Expression.tuple(
            position: position,
            expressions: [expression],
            tcType: expression.type!)
          return .pure(expr)^
        }
      case _:
        return expressions.map { $0.typeChecker().withState() }.sequence()^ >>- { exprEnvPairs in
          let environmentM = exprEnvPairs.foldM(()) { _, pair in
            Env.ActionLVE.formUnion(lve: pair.1.0.lve)
          }^
          return environmentM >>- {
            exprEnvPairs.map { $0.0 }.typeChecker() >>- { expressions in
              let expr = TCAst.Expression.tuple(
                position: position,
                expressions: expressions,
                tcType: .createTuple(elements: expressions.map { $0.type! }))
              return .pure(expr)^
            }
          }
        }
      }
    case let .tuple(position, expressions, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        expressions.typeChecker() >>- { expressions in
          let expr = TCAst.Expression.tuple(
            position: position,
            expressions: expressions,
            tcType: subsType ?? type)
          return .pure(expr)^
        }
      }
    case let .name(position, identifier, nil, _, _):
      return Env.ActionLVE.queryType(for: identifier) >>- { type in
        let expr = TCAst.Expression.name(
          position: position,
          identifier: identifier,
          tcType: type)
        return .pure(expr)^
      }
    case let .name(position, identifier, type?, _, _):
      return Subs.Action.mapType(type) >>- { subsType in
        (Env.ActionLVE.queryType(for: identifier) >>- { queriedType in
          let currType = subsType ?? type
          switch currType.chooseMoreSpecificType(type: queriedType) {
          case .success(let actualType):
            let expr = TCAst.Expression.name(
              position: position,
              identifier: identifier,
              tcType: actualType)
            return .pure(expr)^
          case .failure(let error):
            return .error(error)
          }
        }) <|> {
          let expr = TCAst.Expression.name(
            position: position,
            identifier: identifier,
            tcType: subsType ?? type)
          return .pure(expr)^
        }()
      }
    case let .list(position, expressions, nil, _):
      switch expressions.isEmpty {
      case true:
        return UniqStream.Action.nextUniq() >>- { uniq in
          let expr = TCAst.Expression.list(
            position: position,
            expressions: [],
            tcType: .polymorphic(typeVariables: [uniq], context: .empty, type: .list(element: .typeVariable(name: uniq))))
          return .pure(expr)^
        }
      case false:
        // TODO: - can we optimize?
        let subsM: TypecheckM<[TCAst.Expression]> = expressions.foldM([TCAst.Expression]()) { acc, expr in
          if acc.isEmpty {
            return expr.typeChecker() >>- { expr in
              .pure([expr])^
            }
          } else {
            return expr.typeChecker() >>- { expr in
              Subs.Action.createSubstitutionWithMoreSpecificType(a: acc.last!.type!, b: expr.type!)
                .failWith(.typeMismatch(.listElementsDontMatch(expected: acc.last!.type!, got: expr.type!))) >>- { _ in
                  (acc + [expr]).typeChecker() >>- { expressions in
                    .pure(expressions)^
                  }
                }
            }
          }
        }^
        return subsM >>- { expressions in
          expressions.map { $0.typeChecker().withState() }.sequence()^ >>- { exprEnvPairs in
            let environmentM = exprEnvPairs.foldM(()) { _, pair in
              Env.ActionLVE.formUnion(lve: pair.1.0.lve)
            }^
            return environmentM >>- {
              exprEnvPairs.map { $0.0 }.typeChecker() >>- { expressions in
                let expr = TCAst.Expression.list(
                  position: position,
                  expressions: expressions,
                  tcType: .createList(element: expressions.first!.type!))
                return .pure(expr)^
              }
            }
          }
        }
      }
    case let .list(position, expressions, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        expressions.typeChecker() >>- { expressions in
          let expr = TCAst.Expression.list(
            position: position,
            expressions: expressions,
            tcType: subsType ?? type)
          return .pure(expr)^
        }
      }
    case let .let(position, bindings, expression, nil, _):
      return Env.Action.inNewScope {
        bindings.typeChecker() >>- { bindings in
          expression.typeChecker() >>- { expr in
            let expr = TCAst.Expression.let(
              position: position,
              bindings: bindings,
              expression: expr,
              tcType: expr.type!)
            return .pure(expr)^
          }
        }
      }
    case let .let(position, bindings, expression, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        bindings.typeChecker() >>- { bindings in
          expression.typeChecker() >>- { expression in
            let expr = TCAst.Expression.let(
              position: position,
              bindings: bindings,
              expression: expression,
              tcType: subsType ?? type)
            return .pure(expr)^
          }
        }
      }
    case let .case(position, expression, matches, nil, _):
      return expression.typeChecker() >>- { expression in
        switch expression.type!.isTypeVariable {
        case true:
          return typecheckMatches(matches) >>- { matches in
            matches.map { $0.typeChecker().withState() }.sequence()^ >>- { matchEnvPairs in
              let environmentM = matchEnvPairs.foldM(()) { _, pair in
                Env.ActionLVE.formUnion(lve: pair.1.0.lve)
              }^
              let matches = matchEnvPairs.map { $0.0 }
              return environmentM >>- {
                expression.typeChecker().withState() >>- { expression, s in
                  Env.ActionLVE.formUnion(lve: s.0.lve) >>- {
                    Subs.Action.createSubstitutionWithMoreSpecificType(
                      a: matches.first!.pattern.type!,
                      b: expression.type!
                    ) >>- { _ in
                      typecheckMatches(matches) >>- { matches in
                        expression.typeChecker() >>- { expression in
                          let expr = TCAst.Expression.case(
                            position: position,
                            expression: expression,
                            matches: matches,
                            tcType: matches.first!.expression.type!)
                          return .pure(expr)^
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        case false:
          let matches = matches.map { match in
            TCAst.Expression.Match(
              position: match.position,
              pattern: match.pattern.withType(expression.type!),
              expression: match.expression)
          }
          return typecheckMatches(matches) >>- { matches in
            let expr = TCAst.Expression.case(
              position: position,
              expression: expression,
              matches: matches,
              tcType: matches.first!.expression.type!)
            return .pure(expr)^
          }
        }
      }
    case let .ref(position, expression, nil, _):
      return expression.typeChecker() >>- { expression in
        let finalType: TCType
        switch expression.type! {
        case let .polymorphic(typeVariables, context, type):
          finalType = .polymorphic(
            typeVariables: typeVariables,
            context: context,
            type: .reference(type: type, lifetimeSpecifier: nil))
        case .simple(let simple):
          finalType = .reference(simple, lifetimeSpecifier: nil)
        case _:
          fatalError("TCAst.Expression.ref: Invalid type")
        }
        let ref = TCAst.Expression.ref(
          position: position,
          expression: expression,
          tcType: finalType)
        return .pure(ref)^
      }
    case let .ref(position, expression, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        expression.typeChecker() >>- { expression in
          let ref = TCAst.Expression.ref(
            position: position,
            expression: expression,
            tcType: subsType ?? type)
          return .pure(ref)^
        }
      }
    case let .deref(position, expression, nil, _):
      return expression.typeChecker() >>- { expression in
        guard expression.type!.isReference else {
          return .error(.cannotDereference(expression.type!))
        }
        
        let deref = TCAst.Expression.deref(
          position: position,
          expression: expression,
          tcType: expression.type!.dereference!)
        return .pure(deref)^
      }
    case let .deref(position, expression, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        expression.typeChecker() >>- { expression in
          let deref = TCAst.Expression.deref(
            position: position,
            expression: expression,
            tcType: subsType ?? type)
          return .pure(deref)^
        }
      }
    case let .case(position, expression, matches, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        expression.typeChecker() >>- { expression in
          matches.typeChecker() >>- { matches in
            let `case` = TCAst.Expression.case(
              position: position,
              expression: expression,
              matches: matches,
              tcType: subsType ?? type)
            return .pure(`case`)^
          }
        }
      }
    case _:
      fatalError("TCAst.Expression: tree already type-checked!")
    }
  }

  public var type: TCType? {
    switch self {
    case .let(_, _, _, let type, _),
         .tuple(_, _, let type, _),
         .list(_, _, let type, _),
         .name(_, _, let type, _, _),
         .literal(_, _, let type, _),
         .case(_, _, _, let type, _),
         .lambda(_, _, _, let type, _),
         .ref(_, _, let type, _),
         .deref(_, _, let type, _):
      return type
    case .binary(let binary):
      return binary.type
    }
  }
  
  func typecheckMatches(_ matches: [TCAst.Expression.Match]) -> TypecheckM<[TCAst.Expression.Match]> {
    // TODO: - can we optimize?
    matches.foldM([TCAst.Expression.Match]()) { acc, match in
      if acc.isEmpty {
        return match.typeChecker() >>- { match in
          .pure([match])^
        }
      } else {
        return match.typeChecker() >>- { match in
          Subs.Action.createSubstitutionWithMoreSpecificType(
            a: acc.last!.pattern.type!,
            b: match.pattern.type!
          ) >>- { _ in
            Subs.Action.createSubstitutionWithMoreSpecificType(
              a: acc.last!.expression.type!,
              b: match.expression.type!
            ) >>- { _ in
              (acc + [match]).typeChecker() >>- { matches in
                .pure(matches)^
              }
            }
          }
        }
      }
    }^
  }
}

extension TCAst.Expression.Match: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst.Expression.Match> {
    Env.Action.inNewScope {
      pattern.typeChecker() >>- { pattern in
        let addPatternVariableNamesToEnvM: TypecheckM<()>
        switch pattern {
        case .deconstruct(_, let constructor, _, _, _):
          addPatternVariableNamesToEnvM = Env.ActionLVE.queryType(for: constructor) >>- { constructorType in
            let lifetime = pattern.type.flatMap { $0.isReference ? (pattern.type?.lifetime ?? "'X") : nil }
            return Env.ActionLVE.addPatternVariableNamesToEnv(
              pattern,
              afterBeingTypeChecked: lifetime.map {
                constructorType.wrapWithReference(lifetimeSpecifier: $0.identifier.identifier)
              } ?? constructorType
            )
          }
        case _:
          addPatternVariableNamesToEnvM = Env.ActionLVE.addPatternVariableNamesToEnv(
            pattern,
            afterBeingTypeChecked: pattern.type!)
        }
        return addPatternVariableNamesToEnvM >>- {
          Subs.Action.inNewScope {
            expression.typeChecker() >>- { expression in
              pattern.typeChecker() >>- { pattern in
                let match = TCAst.Expression.Match(
                  position: position,
                  pattern: pattern,
                  expression: expression)
                return .pure(match)^
              }
            }
          }
        }
      }
    }
  }
  
  public var type: TCType? {
    nil
  }
}

extension TCAst.Expression.Binary: TypeCheckable {
  public func typeChecker() -> TypecheckM<TCAst.Expression.Binary> {
    switch self {
    case let .ifThenElse(position, condition, t, f, nil, _):
      func typeCheckerWithConditionType(_ conditionType: TCType) -> TypecheckM<TCAst.Expression.Binary> {
        condition.typeChecker() >>- { condition in
          let lifetime = condition.type!.lifetime ?? conditionType.lifetime
          let lifetimeIdentifier = lifetime?.identifier.identifier
          let firstType = condition.type!.replaceLifetimeSpecifier(
            condition.type?.lifetime?.identifier.identifier,
            with: lifetimeIdentifier)
          let secondType = conditionType.replaceLifetimeSpecifier(
            conditionType.lifetime?.identifier.identifier,
            with: lifetimeIdentifier)
          return Subs.Action
            .createSubstitutionWithMoreSpecificType(
              a: firstType,
              b: secondType
            )
            .failWith(condition, { .typeMismatch(.condition($0)) }) >>- { _ in
              t.typeChecker() >>- { t in
                f.typeChecker() >>- { f in
                  Subs.Action
                    .createSubstitutionWithMoreSpecificType(
                      a: t.type!,
                      b: f.type!,
                      allowRefDifference: true
                    )
                    .failWith(t, f, { .typeMismatch(.ifBranches(left: $0, right: $1)) }) >>- { _ in
                      Subs.Action.createSubstitutionWithMoreSpecificType(
                        a: t.type!,
                        b: t.type!.withLifetime(lifetimeIdentifier)
                      ) >>- { _ in
                        Subs.Action.createSubstitutionWithMoreSpecificType(
                          a: f.type!,
                          b: f.type!.withLifetime(lifetimeIdentifier)
                        ) >>- { _ in
                          t.typeChecker().withState() >>- { t, s1 in
                            f.typeChecker().withState() >>- { f, s2 in
                              condition.typeChecker().withState() >>- { condition, s3 in
                                Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
                                  Env.ActionLVE.formUnion(lve: s3.0.lve) >>- {
                                    condition.typeChecker() >>- { condition in
                                      t.typeChecker() >>- { t in
                                        f.typeChecker() >>- { f in
                                          Subs.Action
                                            .createSubstitutionWithMoreSpecificType(
                                              a: t.type!,
                                              b: f.type!,
                                              allowRefDifference: true
                                            ) >>- { branchesType in
                                              let expr = TCAst.Expression.Binary.ifThenElse(
                                                position: position,
                                                condition: condition,
                                                true: t,
                                                false: f,
                                                tcType: branchesType)
                                              return .pure(expr)^
                                            }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                }
              }
            }
        }
      }
      return Subs.Action.nonDeterminism(
        first: typeCheckerWithConditionType(.bool),
        fallback: Subs.Action.nonDeterminism(
          first: typeCheckerWithConditionType(.atomic(.bool, lifetimeSpecifier: "'X")),
          fallback: typeCheckerWithConditionType(.reference(.bool, lifetimeSpecifier: "'X")))
      )
    case let .ifThenElse(position, condition, t, f, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        condition.typeChecker() >>- { condition in
          t.typeChecker() >>- { t in
            f.typeChecker() >>- { f in
              let expr = TCAst.Expression.Binary.ifThenElse(
                position: position,
                condition: condition,
                true: t,
                false: f,
                tcType: subsType ?? type)
              return .pure(expr)^
            }
          }
        }
      }
    case let .arithmetic(position, op, left, right, nil, _):
      return left.typeChecker() >>- { left in
        right.typeChecker() >>- { right in
          Subs.Action.createSubstitutionWithMoreSpecificType(
            a: left.type!,
            b: right.type!,
            allowRefDifference: true
          ).failWith(left, right, { .typeMismatch(.binaryArithmetic(op: op, left: $0, right: $1)) }) >>- { type in
            (Subs.Action.createSubstitutionWithMoreSpecificType(
              a: type, b: .int, allowRefDifference: true
            ) <|> .error(.typeMismatch(.binaryArithmetic(op: op, left: left.type!, right: .int)))) >>- { _ in
              left.typeChecker().withState() >>- { left, s1 in
                right.typeChecker().withState() >>- { right, s2 in
                  Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
                    left.typeChecker() >>- { left in
                      right.typeChecker() >>- { right in
                        Subs.Action.createSubstitutionWithMoreSpecificType(
                          a: left.type!,
                          b: right.type!,
                          allowRefDifference: true,
                          mapIfOneIsReference: { $0.dereference! }
                        ) >>- { arithmeticType in
                          let expr = TCAst.Expression.Binary.arithmetic(
                            position: position,
                            operator: op,
                            left: left,
                            right: right,
                            tcType: arithmeticType)
                          return .pure(expr)^
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    case let .arithmetic(position, op, left, right, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        left.typeChecker() >>- { left in
          right.typeChecker() >>- { right in
            let expr = TCAst.Expression.Binary.arithmetic(
              position: position,
              operator: op,
              left: left,
              right: right,
              tcType: subsType ?? type)
            return .pure(expr)^
          }
        }
      }
    case let .logical(position, op, left, right, nil, _):
      func allowedTypesForOperation() -> [TCType.SimpleType] {
        switch op {
        case .and, .or:
          return [.bool]
        case .lowerThan, .greaterThan, .lowerOrEqualThan, .greaterOrEqualThan:
          return [.int, .float]
        case .equal, .notEqual:
          return [.char, .int, .bool, .float, .unit, .string]
        }
      }
      
      func resolveType(
        allowedTypes: [TCType.SimpleType],
        branchesType: TCType,
        typeChecker: @escaping () -> TypecheckM<TCAst.Expression.Binary>
      ) -> TypecheckM<TCAst.Expression.Binary> {
        let typeCheckers = allowedTypes.map {
          (Subs.Action.createSubstitutionWithMoreSpecificType(
            a: branchesType,
            b: .simple($0),
            allowRefDifference: true
          ) <|> .error(.typeMismatch(.binaryLogical(op: op, left: branchesType, right: .simple($0))))) *> typeChecker()
        }
        return typeCheckers.foldM(TCAst.Expression.Binary?.none) { acc, next in
          guard acc == nil else {
            return .pure(acc)^
          }
          return (next >>- { .pure($0)^ }) <|> .pure(nil)^
        }
        .flatMap { expr -> TypecheckM<TCAst.Expression.Binary> in
          guard let expr = expr else {
            return .error(.typeMismatch(.logicalOperation(op: op, expected: allowedTypes)))
          }
          return .pure(expr)^
        }^
      }
      
      return left.typeChecker() >>- { left in
        right.typeChecker() >>- { right in
          Subs.Action.createSubstitutionWithMoreSpecificType(
            a: left.type!,
            b: right.type!,
            allowRefDifference: true
          ).failWith(left, right, { .typeMismatch(.binaryLogical(op: op, left: $0, right: $1)) }) >>- { type in
            resolveType(
              allowedTypes: allowedTypesForOperation(),
              branchesType: type,
              typeChecker: {
                left.typeChecker().withState() >>- { left, s1 in
                  right.typeChecker().withState() >>- { right, s2 in
                    Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
                      left.typeChecker() >>- { left in
                        right.typeChecker() >>- { right in
                          Subs.Action.createSubstitutionWithMoreSpecificType(
                            a: left.type!,
                            b: right.type!,
                            allowRefDifference: true,
                            mapIfOneIsReference: { $0.dereference! }
                          ) >>- { substitutedType in
                            let finalType: TCType
                            if substitutedType.isReference {
                              finalType = .reference(.bool, lifetimeSpecifier: substitutedType.lifetime?.identifier.identifier)
                            } else if let lifetime = substitutedType.lifetime {
                              finalType = .atomic(.bool, lifetimeSpecifier: lifetime.identifier.identifier)
                            } else {
                              finalType = .bool
                            }
                            let expr = TCAst.Expression.Binary.logical(
                              position: position,
                              operator: op,
                              left: left,
                              right: right,
                              tcType: finalType)
                            return .pure(expr)^
                          }
                        }
                      }
                    }
                  }
                }
              })
          }
        }
      }
    case let .logical(position, op, left, right, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        left.typeChecker() >>- { left in
          right.typeChecker() >>- { right in
            let expr = TCAst.Expression.Binary.logical(
              position: position,
              operator: op,
              left: left,
              right: right,
              tcType: subsType ?? type)
            return .pure(expr)^
          }
        }
      }
    case .concat(_, _, _, nil, _):
      fatalError()
    case let .application(position, fn, arg, nil, _):
      return fn.typeChecker() >>- { fn in
        switch fn.type!.asArrow {
        case .some:
          return Subs.Action.inNewScope(arg.typeChecker) >>- { arg in
            let (par, _) = fn.type!.asArrow!
            return Subs.Action.createSubstitutionWithMoreSpecificType(
              a: par,
              b: arg.type!
            ).failWith(.typeMismatch(.application(expected: par, got: arg.type!))) >>- { _ in
              fn.typeChecker().withState() >>- { fn, s1 in
                arg.typeChecker().withState() >>- { arg, s2 in
                  Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
                    fn.typeChecker() >>- { fn in
                      arg.typeChecker() >>- { arg in
                        guard let (_, ret) = fn.type!.asArrow else {
                          return .error(.typeMismatch(.notAFunction(fn: fn.type!)))
                        }
                        let expr = TCAst.Expression.Binary.application(
                          position: position,
                          fn: fn,
                          arg: arg,
                          tcType: ret)
                        return .pure(expr)^
                      }
                    }
                  }
                }
              }
            }
          }
        case nil:
          return UniqStream.Action.nextUniq() >>- { fst in
            UniqStream.Action.nextUniq() >>- { snd in
              let arrow = TCType.arrow(arg: .typeVariable(name: String(fst)), ret: .typeVariable(name: String(snd))).asPolymorphic
              return Subs.Action.createSubstitutionWithMoreSpecificType(a: fn.type!, b: arrow) >>- { _ in
                fn.typeChecker() >>- { fn in
                  arg.typeChecker() >>- { arg in
                    guard let (par, _) = fn.type!.asArrow else {
                      return .error(.typeMismatch(.notAFunction(fn: fn.type!)))
                    }
                    return Subs.Action.createSubstitutionWithMoreSpecificType(a: par, b: arg.type!).failWith(.typeMismatch(.application(expected: par, got: arg.type!))) >>- { _ in
                      fn.typeChecker().withState() >>- { fn, s1 in
                        arg.typeChecker().withState() >>- { arg, s2 in
                          Env.ActionLVE.formUnion(lve1: s1.0.lve, lve2: s2.0.lve) >>- {
                            fn.typeChecker() >>- { fn in
                              arg.typeChecker() >>- { arg in
                                guard let (_, ret) = fn.type!.asArrow else {
                                  return .error(.typeMismatch(.notAFunction(fn: fn.type!)))
                                }
                                let expr = TCAst.Expression.Binary.application(
                                  position: position,
                                  fn: fn,
                                  arg: arg,
                                  tcType: ret)
                                return .pure(expr)^
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    case let .application(position, fn, arg, type?, _):
      return Subs.Action.mapType(type) >>- { subsType in
        fn.typeChecker() >>- { fn in
          arg.typeChecker() >>- { arg in
            let expr = TCAst.Expression.Binary.application(
              position: position,
              fn: fn,
              arg: arg,
              tcType: subsType ?? type)
            return .pure(expr)^
          }
        }
      }
    case _:
      fatalError("TCAst.Expression: tree already type-checked!")
    }
  }

  public var type: TCType? {
    switch self {
    case .logical(_, _, _, _, let type, _),
         .arithmetic(_, _, _, _, let type, _),
         .application(_, _, _, let type, _),
         .concat(_, _, _, let type, _),
         .ifThenElse(_, _, _, _, let type, _):
      return type
    }
  }
}

extension Lexer.Token.Literal {
  var type: TCType {
    switch self {
    case .int:
      return .int
    case .logical:
      return .bool
    case .string:
      return .string
    case .char:
      return .char
    case .float:
      return .float
    }
  }
}

extension Array: TypeCheckable where Element: TypeCheckable & CustomStringConvertible {
  public func typeChecker() -> TypecheckM<[Element]> {
    map {
      $0.typeChecker()
    }.sequence()^
  }

  public var type: TCType? {
    .tuple(elements: map { $0.type!.asSimple! })
  }
}

extension Array where Element == TCAst.Binding.Case {
  func typeChecker(functionName: String) -> TypecheckM<[Element]> {
    map { $0.typeChecker(functionName: functionName) }.sequence()^
  }
}
