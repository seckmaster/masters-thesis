//
// Created by Toni K. Turk on 05/11/2020.
//

import Foundation

public typealias TCError = BacktraceErrorImpl<TypeError>

public enum TypeError: Error, Equatable, CustomStringConvertible {
  case variable(Variable)
  case typeMismatch(Mismatch)
  case missingBinding(name: String)
  case patternMatching(PatternMatching)
  case unknownType(name: String)
  case cannotDereference(TCType)
  case unknownTypeVariable(String)
  case typeConstructor(TypeConstructor)
  
  public var description: String {
    switch self {
    case .variable(let variable):
      return variable.description
    case .cannotDereference(let type):
      return "Cannot dereference value of type '\(type.description)'!"
    case .missingBinding(let name):
      return "Binding '\(name)' not found in scope!"
    case .patternMatching(let p):
      return p.description
    case .typeConstructor(let t):
      return t.description
    case .typeMismatch(let m):
      return m.description
    case .unknownType(let name):
      return "Type '\(name)' not defined in scope!"
    case .unknownTypeVariable(let name):
      return "Type variable '\(name)' not found in scope!"
    }
  }
}

public extension TypeError {
  enum Variable: Equatable, CustomStringConvertible {
    case alreadyDefined(name: String)
    case undefined(name: String)
    
    public var description: String {
      switch self {
      case .alreadyDefined(let name):
        return "Variable '\(name)' already defined!"
      case .undefined(let name):
        return "Variable '\(name)' not in scope!"
      }
    }
  }
}

public extension TypeError {
  enum Mismatch: Equatable, CustomStringConvertible {
    case ifBranches(left: TCType, right: TCType)
    case condition(TCType)
    case binaryArithmetic(op: Lexer.Token.Operator.Arithmetic, left: TCType, right: TCType)
    case binaryLogical(op: Lexer.Token.Operator.Logical, left: TCType, right: TCType)
    case notAFunction(fn: TCType)
    case application(expected: TCType, got: TCType)
    case listElementsDontMatch(expected: TCType, got: TCType)
    case tailNotAList(tail: TCType)
    case headAndTail(head: TCType, tail: TCType)
    case match(lhs: TCType, rhs: TCType)
    case logicalOperation(op: Lexer.Token.Operator.Logical, expected: [TCType.SimpleType])

    public static func match(lhs: TCType.SimpleType, rhs: TCType.SimpleType) -> Mismatch {
      .match(lhs: .simple(lhs), rhs: .simple(rhs))
    }
    
    public var description: String {
      switch self {
      case let .ifBranches(left, right):
        return "Incompatible types '\(left.description)' and '\(right.description)'.\nIf branches must have the same type!"
      case let .condition(type):
        return "Condition requires a boolean-like type, got '\(type.description)'!"
      case let .binaryArithmetic(op, left, right):
        return "Types '\(left.description)' and '\(right.description)' are not compatible with operator '\(op.description)'!"
      case let .binaryLogical(op, left, right):
        return "Types '\(left.description)' and '\(right.description)' are not compatible with operator '\(op.description)'!"
      case let .notAFunction(fn):
        return "Cannot call value of type '\(fn.description)'!"
      case let .application(expected, got):
        return "Cannot apply the function to an argument of type '\(got.description)'. Expected '\(expected.description)'!"
      case let .match(lhs, rhs):
        return "Types \(lhs.description) and \(rhs.description) are incompatible!"
      case let .logicalOperation(op, expected):
        return "Operator '\(op.description)' is expects one of: '\(expected.map{$0.description}.joined(separator: ","))'!"
      case .listElementsDontMatch:
        fatalError()
      case .tailNotAList:
        fatalError()
      case .headAndTail:
        fatalError()
      }
    }
  }
}

public extension TypeError {
  enum PatternMatching: Equatable, CustomStringConvertible {
    case cannotMatch(pattern: TCAst.Pattern, type: TCType)
    
    public var description: String {
      switch self {
      case let .cannotMatch(pattern, type):
        return "Cannot match '\(pattern.description)' with '\(type.description)'!"
      }
    }
  }
}

public extension TypeError {
  enum TypeConstructor: Equatable, CustomStringConvertible {
    case genericTypes(expected: Int, got: Int)
    case genericLifetimes(expected: Int, got: Int)
    
    public var description: String {
      switch self {
      case let .genericLifetimes(expected, got):
        return "Type signature expects '\(expected)', got '\(got)' lifetimes!"
      case let .genericTypes(expected, got):
        return "Type signature expects '\(expected)', got '\(got)' types!"
      }
    }
  }
}

public func ==(_ lhs: TypeError, _ rhs: TCError) -> Bool {
  lhs == rhs.error
}

public func ==(_ lhs: TCError, _ rhs: TypeError) -> Bool {
  lhs.error == rhs
}

public func !=(_ lhs: TypeError, _ rhs: TCError) -> Bool {
  lhs != rhs.error
}

public func !=(_ lhs: TCError, _ rhs: TypeError) -> Bool {
  lhs.error != rhs
}
