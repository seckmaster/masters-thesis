//
//  SimpleType+Matching.swift
//  bitis
//
//  Created by Toni K. Turk on 22/11/2020.
//

import Foundation

public extension TCType.SimpleType {
  typealias MatchResult = Result<[(name: String, type: TCType.SimpleType)], TypeError>
  
  func matchWithPattern(_ pattern: TCAst.Pattern) -> MatchResult {
    switch (pattern, self) {
    case (.literal, .arrow),
         (.literal, .list),
         (.literal, .tuple),
         (.literal, .unit),
         (.literal, .reference):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case (.literal, .typeVariable(let name, _)):
      return .success([(name, self)])
    case let (.literal(_, literal, _, _), .atomic(atomic, _)):
      switch (literal, atomic) {
      case (.int, .numeric(.int)),
           (.float, .numeric(.float)),
           (.string, .string),
           (.logical, .bool),
           (.char, .char):
        return .success([])
      case _:
        return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
      }
    case (.wildcard, _):
      return .success([])
    case let (.identifier(_, identifier, _, _), type):
      return .success([(identifier, type)])
    case (.list(.empty), .list):
      return .success([])
    case (.list(.empty), .typeVariable):
      return .success([])
    case (.list(.empty), _):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case let (.list(.cons(_, head, tail, _, _)), .list(element, _)):
      switch (element.matchWithPattern(head), matchWithPattern(tail)) {
      case let (.success(left), .success(right)):
        return .success(left + right)
      case let (.failure(error), _),
           let (_, .failure(error)):
        return .failure(error)
      }
    case let (.list(.cons), .reference(type, lifetimeSpecifier)):
      return type
        .matchWithPattern(pattern)
        .map { $0.map { ($0, .reference(type: $1, lifetimeSpecifier: lifetimeSpecifier)) } }
    case (.list(.cons), _):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case let (.tuple(_, patterns, _, _), .tuple(elements, _)) where patterns.count == elements.count:
      return zip(patterns, elements)
        .map { $0.1.matchWithPattern($0.0) }
        .reduce(.success([])) { acc, result in
          switch (result, acc) {
          case let (.success(left), .success(right)):
            return .success(left + right)
          case let (.failure(error), _),
               let (_, .failure(error)):
            return .failure(error)
          }
        }
    case let (.tuple, .reference(type: type, lifetimeSpecifier)):
      return type
        .matchWithPattern(pattern)
        .map { $0.map { ($0, .reference(type: $1, lifetimeSpecifier: lifetimeSpecifier)) } }
    case (.tuple, .unit):
      return .success([])
    case (.tuple, _):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case (.ref(_, let pattern, _, _),
          .reference(let type, _)):
      return type.matchWithPattern(pattern)
    case (_, .reference(let type, _)):
      return type.matchWithPattern(pattern)
    case (.ref(_, let pattern, _, _), _):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case (_, .typeConstructor):
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: .simple(self))))
    case (.deconstruct(_, _, let patterns, _, _), let .arrow(arg, ret, _)):
      return deconstructPattern(
        arg: arg,
        ret: ret,
        subpatterns: patterns,
        error: .patternMatching(.cannotMatch(pattern: pattern, type: .arrow(arg: arg, ret: ret))))
    case _:
      fatalError("Implement")
    }
  }
  
  private func deconstructPattern(
    arg: TCType.SimpleType,
    ret: TCType.SimpleType,
    subpatterns patterns: [AST.Pattern],
    error: @autoclosure () -> TypeError
  ) -> TCType.SimpleType.MatchResult {
    guard let pattern = patterns.first else {
      return .failure(error())
    }
    switch arg.matchWithPattern(pattern) {
    case .success(let mapping):
      guard let (arg, ret) = ret.asArrow else {
        return patterns.count == 1
          ? .success(mapping)
          : .failure(error())
      }
      return deconstructPattern(
        arg: arg,
        ret: ret,
        subpatterns: Array(patterns.dropFirst()),
        error: error()
      ).map { mapping + $0 }
    case .failure(let error):
      return .failure(error)
    }
  }
}
