//
// Created by Toni K. Turk on 22/11/2020.
//

import Foundation
import Bow
import SwiftParsec

public extension TCType {
  enum Action {
    static func matchTypes(lhs: TCType, rhs: TCType, strict: Bool = false) -> TypecheckM<TCType> {
      .init { state in
        switch lhs.matchWith(type: rhs, strict: strict) {
        case .success(let type):
          return .pure((state, type))^
        case .failure(.typeMismatch(.match)):
          return .raiseError(.init(backtrace: state.3, error: .typeMismatch(.match(lhs: lhs, rhs: rhs))))^
        case .failure(let error):
          return .raiseError(.init(backtrace: state.3, error: error))
        }
      }
    }

    static func chooseMoreSpecificType(lhs: TCType, rhs: TCType, strict: Bool = false) -> TypecheckM<TCType> {
      .init { state in
        switch lhs.chooseMoreSpecificType(type: rhs) {
        case .success(let type):
          return .pure((state, type))^
        case .failure(let error):
          return .raiseError(.init(backtrace: state.3, error: error))^
        }
      }
    }
  }
}
