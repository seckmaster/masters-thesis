//
// Created by Toni K. Turk on 15/11/2020.
//

import Foundation

// @FIXME: - Ugly non-pure code!
public var ARE_WARNINGS_ENABLED = false
func warning(_ items: Any..., separator: String = " ", terminator: String = "\n") {
  #if DEBUG
  guard ARE_WARNINGS_ENABLED else { return }
  print(["⚠️"] + items, separator: separator, terminator: terminator)
  #endif
}

public extension TCType {
  /// Matches `self` with `rhs`.
  ///
  /// This method takes into accounts for polymorphism, contrary to `==`.
  ///
  /// This method is mostly meant to be used to check whether
  /// `rhs` can be applied (or assigned) to `self`- for instance
  /// as a function argument.
  ///
  /// Examples:
  ///
  /// (∀a,b.a -> b -> Int) and (∀e,f.e -> f -> Int) ☑️
  /// (∀a,b.a -> b -> Int) and (Int -> Bool -> Int) ☑️
  /// (∀a.a -> a -> Int) and (Int -> Bool -> Int) ❌
  ///
  /// Check tests for more examples.
  ///
  func matchWith(
    type rhs: TCType,
    strict: Bool = true /// `strict` means "can `rhs` be used as an argument expecting `rhs`"
  ) -> Result<TCType, TypeError> {
    switch (self.asPolymorphic, rhs.asPolymorphic) {
    case let (.simple(lhs), .simple(rhs)) where lhs == rhs:
      return .success(.simple(lhs))
    case let (.simple(.list(lhs, ls1)), .simple(.list(rhs, ls2))):
      return matchPolymorphicTypes(
        strict: strict,
        typeVariables1: .empty(),
        context1: .empty,
        type1: lhs,
        typeVariables2: .empty(),
        context2: .empty,
        type2: rhs
      )
      .map { .list(element: $0.asSimple!) }
    case let (.simple(.arrow(arg1, ret1, ls1)),
              .simple(.arrow(arg2, ret2, ls2))):
      if arg1 == arg2 && ret1 == ret2 {
        return .success(.arrow(arg: arg1, ret: ret1))
      }
      let res1 = matchPolymorphicTypes(
        strict: strict,
        typeVariables1: .empty(),
        context1: .empty,
        type1: arg1,
        typeVariables2: .empty(),
        context2: .empty,
        type2: arg2)
      let res2 = matchPolymorphicTypes(
        strict: strict,
        typeVariables1: .empty(),
        context1: .empty,
        type1: ret1,
        typeVariables2: .empty(),
        context2: .empty,
        type2: ret2)
      switch (res1, res2) {
      case (.success(let arg), .success(let ret)):
        return .success(.arrow(arg: arg.asSimple!, ret: ret.asSimple!, lifetimeSpecifier: ls1).asPolymorphic) // TODO: - Lifetime
      case (.failure(let error), _), (_, .failure(let error)):
        return .failure(error)
      }
    case let (.simple(.typeConstructor(name1, types1, ls1, l1)), (.simple(.typeConstructor(name2, types2, ls2, l2)))):
      return matchPolymorphicTypes(
        strict: strict,
        typeVariables1: .empty(),
        context1: .empty,
        type1: .typeConstructor(name: name1, types: types1, lifetimeSpecifiers: ls1, lifetimeSpecifier: l1),
        typeVariables2: .empty(),
        context2: .empty,
        type2: .typeConstructor(name: name2, types: types2, lifetimeSpecifiers: ls2, lifetimeSpecifier: l2))
    case let (.simple(.reference(type1, lifetimeSpecifier1)),
              .simple(.reference(type2, lifetimeSpecifier2))):
      return matchPolymorphicTypes(
        strict: strict,
        typeVariables1: .empty(),
        context1: .empty,
        type1: type1,
        typeVariables2: .empty(),
        context2: .empty,
        type2: type2
      )
      .flatMap { _ in
        if let ls1 = lifetimeSpecifier1, let ls2 = lifetimeSpecifier2, ls1 == ls2 {
          return .success(self)
        }
        if lifetimeSpecifier2 == nil {
          return .success(self)
        }
        return .success(rhs)
      }
    case let (.polymorphic(typeVariables1, context1, type1),
              .polymorphic(typeVariables2, context2, type2)):
      return matchPolymorphicTypes(
        strict: strict,
        typeVariables1: typeVariables1, context1: context1, type1: type1,
        typeVariables2: typeVariables2, context2: context2, type2: type2)
    case let (.polymorphic(typeVariables, context, type),
              .simple(rhs)):
      return matchPolymorphicTypes(
        strict: strict,
        typeVariables1: typeVariables, context1: context, type1: type,
        typeVariables2: [], context2: .empty, type2: rhs)
    case let (.simple(.reference(type1, lifetime1)),
              .simple(.reference(type2, lifetime2))) where type1 == type2:
      warning("Unused lifetime \(lifetime2 ?? "-")", #line, #file)
      return .success(.reference(type1, lifetimeSpecifier: lifetime1))
    case let (.simple(.atomic(lhs, ls1)), .simple(.atomic(rhs, ls2))) where lhs == rhs:
      return .success(.simple(.atomic(lhs, lifetimeSpecifier: ls1 ?? ls2)))
    case _:
//      warning("TCType.matchWith: Potentially not covered case!")
      return .failure(.typeMismatch(.match(lhs: self, rhs: rhs)))
    }
  }
  
  // Returns a type which is more specific between `self` and `type`
  func chooseMoreSpecificType(
    type: TCType
//    strict: Bool
  ) -> Result<TCType, TypeError> {
    switch self.matchWith(type: type, strict: false) {
    case .success(let type2):
      return .success(type2)
    case .failure:
      switch type.matchWith(type: self, strict: false) {
      case .success(let type):
        return .success(type)
      case .failure(.typeMismatch(.match)):
        return .failure(.typeMismatch(.match(lhs: self, rhs: type)))
      case .failure(let error):
        return .failure(error)
      }
    }
  }
  
  func isLessSpecific(than type: TCType) -> Bool {
    (try? chooseMoreSpecificType(type: type).map { $0 == type }.get()) ?? false
  }

  // Creates a substitution mapping from `self` to `type`.
  // Examples:
  // Int <=> a: a -> Int
  // [Int] <=> [a]: [a] -> [Int], a -> Int
  // [(Int, Bool)] <=> [(a, b)]: [(a, b)] -> [(Int, Bool)], (a, b) -> (Int, Bool), a -> Int, b -> Bool
  func createSubstitutionMapping(type: TCType) -> [TCType: TCType] {
    switch (self.asPolymorphic, type.asPolymorphic) {
    
    case let (.simple(lhs), .simple(rhs)) where lhs == rhs:
      return [:]
    case (.simple, .simple):
      return [self: type]
      
    case (_, .polymorphic(_, _, type: .typeVariable)):
      return [type: self]
    case (.polymorphic(_, _, type: .typeVariable), _):
      return [self: type]
      
    // * -> * => * -> *
    case let (.simple(.arrow(arg2, ret2, _)),
              .polymorphic(_, _, .arrow(arg1, ret1, _))),
         let (.polymorphic(_, _, .arrow(arg1, ret1, _)),
              .simple(.arrow(arg2, ret2, _))),
         let (.polymorphic(_, _, .arrow(arg1, ret1, _)),
              .polymorphic(_, _, .arrow(arg2, ret2, _))):
      return [:]
        .customMerge(TCType.simple(arg1).createSubstitutionMapping(type: .simple(arg2)))
        .customMerge(TCType.simple(ret1).createSubstitutionMapping(type: .simple(ret2)))
      
    // (*, *, *) => (*, *, *)
    case let (.simple(.tuple(elements2, _)),
              .polymorphic(_, _, .tuple(elements1, _))),
         let (.polymorphic(_, _, .tuple(elements1, _)),
              .polymorphic(_, _, .tuple(elements2, _))),
         let (.polymorphic(_, _, .tuple(elements1, _)),
              .simple(.tuple(elements2, _))):
      return zip(elements1, elements2)
        .map { TCType.simple($0).createSubstitutionMapping(type: .simple($1)) }
        .reduce([:]) { acc, next in acc.customMerge(next) }
    
    // [*] => [*]
    case let (.polymorphic(_, _, .list(element1, _)),
              .polymorphic(_, _, .list(element2, _))),
         let (.polymorphic(_, _, .list(element1, _)),
              .simple(.list(element2, _))),
         let (.simple(.list(element2, _)),
              .polymorphic(_, _, .list(element1, _))):
      return TCType.simple(element1).createSubstitutionMapping(type: .simple(element2))
      
    case let (.polymorphic(_, _, .reference(type1, _)),
              .polymorphic(_, _, .reference(type2, _))),
         let (.polymorphic(_, _, .reference(type1, _)),
              .simple(.reference(type2, _))),
         let (.simple(.reference(type1, _)),
              .polymorphic(_, _, .reference(type2, _))):
      return TCType.simple(type1).createSubstitutionMapping(type: .simple(type2))
      
    case _:
      let lessSpecific = type.isLessSpecific(than: self) ? type : self
      let specific = lessSpecific == type ? self : type
      return [lessSpecific: specific]
    }
  }
  
  func matchWithPattern(_ pattern: TCAst.Pattern) -> TCType.SimpleType.MatchResult {
    switch self {
    case let .polymorphic(_, _, type):
      return type.matchWithPattern(pattern)
    case let .simple(simple):
      return simple.matchWithPattern(pattern)
    case _:
      return .failure(.patternMatching(.cannotMatch(pattern: pattern, type: self)))
    }
  }
}

extension TCType {
  typealias Mapping = [String: SimpleType]

  func matchPolymorphicTypes(
    strict: Bool,
    typeVariables1: TypeVariables, context1: Context, type1: SimpleType,
    typeVariables2: TypeVariables, context2: Context, type2: SimpleType
  ) -> Result<TCType, TypeError> {
    if type1 == type2 { return .success(.simple(type1).asPolymorphic) }
    switch (type1, type2) {
    case let (.atomic(lhs, ls1),
              .atomic(rhs, ls2)) where lhs == rhs:
      return .success(.atomic(lhs, lifetimeSpecifier: ls1 ?? ls2))
    case let (.arrow(arg1, ret1, ls1), .arrow(arg2, ret2, ls2)):
      // TODO: - code dupl
      let (result, mapping) = matchArrows(
        strict: strict,
        typeVariables1: typeVariables1, arg1: arg1, ret1: ret1,
        typeVariables2: typeVariables2, arg2: arg2, ret2: ret2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: .empty())
      let remainingTypeVariables = TypeVariables(mapping.keys
        .filter { mapping[$0]!.isTypeVariable })

      switch (result, remainingTypeVariables.isEmpty) {
      case (.success(let type), true):
        return .success(.simple(type))
      case (.success(let type), false):
        return .success(.polymorphic(
          typeVariables: remainingTypeVariables,
          context: context1,
          type: type))
      case (.failure(let error), _):
        return .failure(error)
      }
    case let (.list(l, ls1), .list(r, ls2)):
      // TODO: - code dupl
      let (result, mapping) = matchListsOrReferences(
        strict: strict,
        typeVariables1: typeVariables1, element1: l,
        typeVariables2: typeVariables2, element2: r,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: .empty(),
        constructor: { .list(element: $0, lifetimeSpecifier: ls1 ?? ls2) })
      let remainingTypeVariables = TypeVariables(
        mapping.keys.filter { mapping[$0]!.isTypeVariable }
      )

      switch (result, remainingTypeVariables.isEmpty) {
      case (.success(let type), true):
        return .success(.simple(type))
      case (.success(let type), false):
        return .success(.polymorphic(
          typeVariables: remainingTypeVariables,
          context: context1,
          type: type))
      case (.failure(let error), _):
        return .failure(error)
      }
    case let (.reference(l, ls1), .reference(r, ls2)):
      // TODO: - code dupl
      let (result, mapping) = matchListsOrReferences(
        strict: strict,
        typeVariables1: typeVariables1, element1: l,
        typeVariables2: typeVariables2, element2: r,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: .empty(),
        constructor: { .reference(type: $0, lifetimeSpecifier: ls1 ?? ls2) })
      let remainingTypeVariables = TypeVariables(
        mapping.keys.filter { mapping[$0]!.isTypeVariable }
      )

      switch (result, remainingTypeVariables.isEmpty) {
      case (.success(let type), true):
        return .success(.simple(type))
      case (.success(let type), false):
        return .success(.polymorphic(
          typeVariables: remainingTypeVariables,
          context: context1,
          type: type))
      case (.failure(let error), _):
        return .failure(error)
      }
    case let (.tuple(el1, ls1), .tuple(el2, ls2)):
      // TODO: - code dupl
      let (result, mapping) = matchTuples(
        strict: strict,
        typeVariables1: typeVariables1, elements1: el1,
        typeVariables2: typeVariables2, elements2: el2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: .empty())
      let remainingTypeVariables = TypeVariables(mapping.keys
        .filter { mapping[$0]!.isTypeVariable }
      )

      switch (result, remainingTypeVariables.isEmpty) {
      case (.success(let type), true):
        return .success(.simple(type))
      case (.success(let type), false):
        return .success(.polymorphic(
          typeVariables: remainingTypeVariables,
          context: context1,
          type: type))
      case (.failure(let error), _):
        return .failure(error)
      }
    case let (.typeVariable(name1, lifetimeSpecifier), .typeVariable):
      return .success(.polymorphic(typeVariables: [name1], context: .empty, type: .typeVariable(name: name1, lifetimeSpecifier: lifetimeSpecifier)))
    case (.typeVariable, .reference):
      return .failure(.typeMismatch(.match(
        lhs: .polymorphic(typeVariables: typeVariables1, context: context1, type: type1),
        rhs: .polymorphic(typeVariables: typeVariables2, context: context2, type: type2))))
    case (.typeVariable, _) where typeVariables2.isEmpty:
      return .success(.simple(type2))
    case (.typeVariable, _):
      return .success(.polymorphic(typeVariables: typeVariables2, context: context2, type: type2))
    case let (.typeConstructor(name1, types1, lifetimeSpecifiers1, ls1),
              .typeConstructor(name2, types2, lifetimeSpecifiers2, ls2)) where name1 == name2:
      guard types1.count == types2.count else {
        return .failure(.typeMismatch(.match(lhs: type1, rhs: type2)))
      }
      // TODO: - code dupl
      let (result, mapping) = matchTuples(
        strict: strict,
        typeVariables1: typeVariables1, elements1: types1,
        typeVariables2: typeVariables2, elements2: types2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: .empty())
      let remainingTypeVariables = TypeVariables(mapping.keys
        .filter { mapping[$0]!.isTypeVariable }
      )
      
      let lifetimeSpecifiers = lifetimeSpecifiers1.count < lifetimeSpecifiers2.count ? lifetimeSpecifiers2 : lifetimeSpecifiers1
      
      switch (result, remainingTypeVariables.isEmpty) {
      case let (.success(.tuple(types, lifetime)), true):
        return .success(.simple(.typeConstructor(
          name: name1,
          types: types,
          lifetimeSpecifiers: lifetimeSpecifiers,
          lifetimeSpecifier: ls1 ?? ls2)))
      case let (.success(.tuple(types, lifetime)), false):
        return .success(.polymorphic(
          typeVariables: remainingTypeVariables,
          context: context1,
          type: .typeConstructor(
            name: name1,
            types: types,
            lifetimeSpecifiers: lifetimeSpecifiers,
            lifetimeSpecifier: ls1 ?? ls2)))
      case (.failure(let error), _):
        return .failure(error)
      case _:
        fatalError("TCType.matchPolymorphicTypes: this should not happen")
      }
    case (.reference, .typeVariable):
      return .success(.polymorphic(typeVariables: typeVariables1, context: context1, type: type1))
    case _:
      return .failure(.typeMismatch(.match(
        lhs: .polymorphic(typeVariables: typeVariables1, context: context1, type: type1),
        rhs: .polymorphic(typeVariables: typeVariables2, context: context2, type: type2))))
    }
  }
}

// NOTE: - Mutual recursions
private extension TCType {
  typealias ResultS = Result<SimpleType, TypeError>
  typealias ResultMappingPair = (ResultS, Mapping)

  func matchArrows(
    strict: Bool,
    typeVariables1: TypeVariables, arg1: SimpleType, ret1: SimpleType,
    typeVariables2: TypeVariables, arg2: SimpleType, ret2: SimpleType,
    lifetimeSpecifier1: String?,
    lifetimeSpecifier2: String?,
    mapping: Mapping
  ) -> ResultMappingPair {
    func fixTypes(type: SimpleType, mapping: Mapping) -> SimpleType {
      switch type {
      case .typeVariable(let name, let lifetimeSpecifier):
        return mapping[name] ?? .typeVariable(name: name, lifetimeSpecifier: lifetimeSpecifier)
      case let .arrow(arg, ret, ls):
        return .arrow(
          arg: fixTypes(type: arg, mapping: mapping),
          ret: fixTypes(type: ret, mapping: mapping),
          lifetimeSpecifier: ls)
      case _:
        return type
      }
    }
    
    let (res1, mapping1) = match(
      strict: strict,
      typeVariables1: typeVariables1, lhs: arg1,
      typeVariables2: typeVariables2, rhs: arg2,
      lifetimeSpecifier1: lifetimeSpecifier1,
      lifetimeSpecifier2: lifetimeSpecifier2,
      mapping: mapping)
    switch res1 {
    case .success(let arg):
      let (res2, mapping2) = match(
        strict: strict,
        typeVariables1: typeVariables1, lhs: ret1,
        typeVariables2: typeVariables2, rhs: ret2,
        lifetimeSpecifier1: lifetimeSpecifier1,
        lifetimeSpecifier2: lifetimeSpecifier2,
        mapping: mapping1)
      switch res2 {
      case .success(let ret):
        guard !isCorrectionNeeded(mapping1, mapping2) else {
          return (.success(.arrow(
            arg: fixTypes(type: arg, mapping: mapping2),
            ret: ret,
            lifetimeSpecifier: nil)), mapping2)
        }
        return (.success(.arrow(arg: arg, ret: ret, lifetimeSpecifier: nil)), mapping2)
      case .failure(let error):
        return (.failure(error), mapping)
      }
    case .failure(let error):
      return (.failure(error), mapping)
    }
  }

  func matchListsOrReferences(
    strict: Bool,
    typeVariables1: TypeVariables, element1: SimpleType,
    typeVariables2: TypeVariables, element2: SimpleType,
    lifetimeSpecifier1: String?,
    lifetimeSpecifier2: String?,
    mapping: Mapping,
    constructor: (SimpleType) -> SimpleType // Should be either SimpleType.list or SimpleType.reference
  ) -> ResultMappingPair {
    let (result, mapping) = match(
      strict: strict,
      typeVariables1: typeVariables1, lhs: element1,
      typeVariables2: typeVariables2, rhs: element2,
      lifetimeSpecifier1: lifetimeSpecifier1,
      lifetimeSpecifier2: lifetimeSpecifier2,
      mapping: .empty())
    switch result {
    case .success(let type):
      return (.success(constructor(type)), mapping)
    case .failure(let error):
      return (.failure(error), mapping)
    }
  }

  func matchTuples(
    strict: Bool,
    typeVariables1: TypeVariables, elements1: [SimpleType],
    typeVariables2: TypeVariables, elements2: [SimpleType],
    lifetimeSpecifier1: String?,
    lifetimeSpecifier2: String?,
    mapping: Mapping
  ) -> ResultMappingPair {
    guard elements1.count == elements2.count else {
      return (
        .failure(.typeMismatch(.match(
          lhs: SimpleType.tuple(elements: elements1, lifetimeSpecifier: lifetimeSpecifier1),
          rhs: .tuple(elements: elements2, lifetimeSpecifier: lifetimeSpecifier2)))),
        mapping
      )
    }
    let result = zip(elements1, elements2)
      .reduce(Result.success(([SimpleType](), mapping))) { acc, pair -> Result<([SimpleType], Mapping), TypeError> in
        switch acc {
        case let .success((acc, mapping)):
          let (l, r) = pair
          let (result, mapping_) = match(
            strict: strict,
            typeVariables1: typeVariables1, lhs: l,
            typeVariables2: typeVariables2, rhs: r,
            lifetimeSpecifier1: lifetimeSpecifier1,
            lifetimeSpecifier2: lifetimeSpecifier2,
            mapping: mapping)
          switch result {
          case .success(let type):
            return .success((acc + [type], mapping_))
          case .failure(let error):
            return .failure(error)
          }
        case .failure(let error):
          return .failure(error)
        }
      }
    switch result {
    case let .success((elements, mapping_)):
      return (.success(.tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier1 ?? lifetimeSpecifier2)), mapping_)
    case .failure(let error):
      return (.failure(error), mapping)
    }
  }

  func match(
    strict: Bool,
    typeVariables1: TypeVariables, lhs: SimpleType,
    typeVariables2: TypeVariables, rhs: SimpleType,
    lifetimeSpecifier1: String?,
    lifetimeSpecifier2: String?,
    mapping: Mapping
  ) -> ResultMappingPair {
    let resultType: SimpleType
    var mapping = mapping

    switch (lhs, rhs) {
    case (.atomic(let l, let ls1), .atomic(let r, let ls2)) where l == r:
      resultType = lhs
    case (.unit, .unit):
      resultType = .unit
    case (.unit, .tuple([], _)), (.tuple([], _), .unit): // TODO: - fix!!!
      resultType = .unit
    case let (.typeVariable(name1, _), .typeVariable(name2, _)):
      if let existing = mapping[name1] {
        guard TCType.simple(existing).asPolymorphic.isLessSpecific(than: TCType.simple(rhs).asPolymorphic) else {
          return (.failure(.typeMismatch(.match(lhs: lhs, rhs: rhs))), mapping)
        }
        mapping[name1] = rhs
        resultType = rhs
      } else {
        mapping[name1] = rhs
        resultType = rhs
      }
    case (.typeVariable(let name, _), _):
      if let existing = mapping[name] {
        guard TCType.simple(existing).asPolymorphic.isLessSpecific(than: TCType.simple(rhs).asPolymorphic) else {
          return (.failure(.typeMismatch(.match(lhs: lhs, rhs: rhs))), mapping)
        }
        mapping[name] = rhs
        resultType = rhs
      } else {
        mapping[name] = rhs
        resultType = rhs
      }
    case (_, .typeVariable(let name, _)):
      guard !strict else {
        return (.failure(.typeMismatch(.match(lhs: lhs, rhs: rhs))), mapping)
      }
      if let existing = mapping[name] {
        guard TCType.simple(existing).asPolymorphic.isLessSpecific(than: TCType.simple(lhs).asPolymorphic) else {
          return (.failure(.typeMismatch(.match(lhs: lhs, rhs: rhs))), mapping)
        }
        mapping[name] = lhs
        resultType = lhs
      } else {
        mapping[name] = lhs
        resultType = lhs
      }
    case let (.typeConstructor(name1, types1, lifetimeSpecifiers1, ls1),
              .typeConstructor(name2, types2, lifetimeSpecifiers2, ls2)) where name1 == name2:
      let (result, mapping_) = matchTuples(
        strict: strict,
        typeVariables1: typeVariables1, elements1: types1,
        typeVariables2: typeVariables2, elements2: types2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: mapping)
      switch result {
      case let .success(.tuple(types, ls)):
        resultType = .typeConstructor(
          name: name1,
          types: types,
          lifetimeSpecifiers: lifetimeSpecifiers1,
          lifetimeSpecifier: ls)
        mapping = mapping_
      case let .success(type):
        resultType = .typeConstructor(name: name1, types: [type], lifetimeSpecifiers: lifetimeSpecifiers1, lifetimeSpecifier: ls1 ?? ls2)
        mapping = mapping_
      case .failure(let error):
        return (.failure(error), mapping_)
      }
    case let (.arrow(arg1, ret1, ls1), .arrow(arg2, ret2, ls2)):
      let (result, mapping_) = matchArrows(
        strict: strict,
        typeVariables1: typeVariables1, arg1: arg1, ret1: ret1,
        typeVariables2: typeVariables2, arg2: arg2, ret2: ret2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: mapping)
      switch result {
      case .success(let type):
        resultType = type
        mapping = mapping_
      case .failure(let error):
        return (.failure(error), mapping_)
      }
    case let (.list(l, ls1), .list(r, ls2)):
      let (result, mapping_) = matchListsOrReferences(
        strict: strict,
        typeVariables1: typeVariables1, element1: l,
        typeVariables2: typeVariables2, element2: r,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: mapping,
        constructor: { .list(element: $0, lifetimeSpecifier: ls1 ?? ls2) })
      switch result {
      case .success(let type):
        resultType = type
        mapping = mapping_
      case .failure(let error):
        return (.failure(error), mapping_)
      }
    case let (.tuple(el1, ls1), .tuple(el2, ls2)):
      let (result, mapping_) = matchTuples(
        strict: strict,
        typeVariables1: typeVariables1, elements1: el1,
        typeVariables2: typeVariables2, elements2: el2,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: mapping)
      switch result {
      case .success(let type):
        resultType = type
        mapping = mapping_
      case .failure(let error):
        return (.failure(error), mapping_)
      }
    case let (.reference(l, ls1), .reference(type: r, ls2)):
      let (result, mapping_) = matchListsOrReferences(
        strict: strict,
        typeVariables1: typeVariables1, element1: l,
        typeVariables2: typeVariables2, element2: r,
        lifetimeSpecifier1: ls1,
        lifetimeSpecifier2: ls2,
        mapping: mapping,
        constructor: { .reference(type: $0, lifetimeSpecifier: ls1 ?? ls2) })
      switch result {
      case .success(let type):
        resultType = type
        mapping = mapping_
      case .failure(let error):
        return (.failure(error), mapping_)
      }
    case _:
      return (.failure(.typeMismatch(.match(lhs: lhs, rhs: rhs))), mapping)
    }

    return (.success(resultType), mapping)
  }
}

private func isCorrectionNeeded(
  _ mapping1: TCType.Mapping,
  _ mapping2: TCType.Mapping
) -> Bool {
  for (key, value) in (mapping1.count > mapping2.count ? mapping1 : mapping2) {
    guard let map2 = mapping2[key] else { continue }
    if value != map2 { return true }
  }
  return false
}

extension Dictionary where Value == TCType, Key == Value {
  func customMerge(_ other: [TCType: TCType]) -> [TCType: TCType] {
    var res = self
    for (key, value) in other {
      if res[key] == nil {
        res[key] = value
        continue
      }
      let currentValue = res[key]!
      res[currentValue] = value
    }
    return res
  }
}
