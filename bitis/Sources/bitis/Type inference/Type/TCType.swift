//
// Created by Toni K. Turk on 02/11/2020.
//

import Foundation

public indirect enum TCType: Equatable, Hashable, CustomStringConvertible, CustomDebugStringConvertible {
  public typealias TypeVariables = Set<String>
  public typealias Class = String

  case simple(SimpleType)
  case polymorphic(
    typeVariables: TypeVariables,
    context: Context,
    type: SimpleType)
  case data(
    name: String,
    constructors: [Constructor],
    genericParams: TCAst.Binding.GenericParams)

  public var description: String {
    switch self {
    case let .simple(simple):
      return simple.description
    case let .polymorphic(typeVariables, context, type):
      return "∀\(typeVariables.sorted().joined(separator: ",")).\(context.description) => \(type.description)"
    case let .data(name, constructors, genericParams):
      return name + ": " + constructors.map { $0.description }.joined(separator: " | ")
    }
  }
  
  public var debugDescription: String { description }
}

public extension TCType {
  struct Context: Equatable, CustomStringConvertible, Hashable {
    public let context: [String: Class]

    public static var empty: Context {
      .init(context: [:])
    }

    public var description: String {
      context.keys
        .sorted()
        .map { context[$0]! + " " + $0 }
        .joined(separator: ",")
    }
  }
  
  struct Constructor: Equatable, Hashable, CustomStringConvertible, CustomDebugStringConvertible {
    public let name: String
    public let type: TCType // arrow
    
    public var description: String {
      "\(name): \(type.description)"
    }
    
    public var debugDescription: String {
      description
    }
  }
}

extension TCType: ExpressibleByStringLiteral {
  public init(stringLiteral value: StringLiteralType) {
    self = .polymorphic(
      typeVariables: [value],
      context: .empty,
      type: .typeVariable(name: value, lifetimeSpecifier: nil))
  }
}

public extension TCType {
  indirect enum SimpleType: Equatable, CustomStringConvertible, Hashable {
    case atomic(Atomic, lifetimeSpecifier: String?)
    case arrow(arg: SimpleType, ret: SimpleType, lifetimeSpecifier: String?)
    case typeVariable(name: String, lifetimeSpecifier: String?)
    case list(element: SimpleType, lifetimeSpecifier: String?)
    case tuple(elements: [SimpleType], lifetimeSpecifier: String?)
    case reference(type: SimpleType, lifetimeSpecifier: String?)
    case unit // NOTE: - Is unit perhaps an `Atomic` type as well?
    case typeConstructor(name: String, types: [SimpleType], lifetimeSpecifiers: [String], lifetimeSpecifier: String?)
    
    public var description: String {
      switch self {
      case let .arrow(.arrow(arg, ret1, _), ret, _):
        return "(\(arg) -> \(ret1)) -> \(ret)"
      case let .arrow(arg, ret, _):
        return "\(arg) -> \(ret)"
      case let .atomic(atomic, lifetimeSpecifier):
        if let lifetime = lifetimeSpecifier {
          return "\(lifetime) \(atomic.description)"
        }
        return atomic.description
      case let .typeVariable(name, lifetimeSpecifier):
        if let lifetime = lifetimeSpecifier {
          return "\(lifetime) \(name)"
        }
        return name
      case let .list(type, lifetimeSpecifier):
        if let lifetime = lifetimeSpecifier {
          return "\(lifetime) [\(type)]"
        }
        return "[\(type)]"
      case let .tuple(types, lifetimeSpecifier):
        if let lifetime = lifetimeSpecifier {
          return "\(lifetime) (\(types.map { $0.description }.joined(separator: ",")))"
        }
        return "(\(types.map { $0.description }.joined(separator: ",")))"
      case .unit:
        return "()"
      case let .reference(type, lifetime):
        return "&\(lifetime.map { "\($0) " } ?? "")(\(type.description))"
      case let .typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier):
        let lifetimes     = lifetimeSpecifiers.joined(separator: ",")
        let types         = types.map { $0.description }.joined(separator: ",")
        let genericParams = lifetimes.isEmpty && types.isEmpty
        ? ""
        : types.isEmpty ? lifetimes : lifetimes.isEmpty ? types : "\(lifetimes),\(types)"
        if let lifetime = lifetimeSpecifier {
          return "\(lifetime) \(name) [\(genericParams)]"
        }
        return name + " [" + genericParams + "]"
      }
    }
    
    public static func == (lhs: Self, rhs: Self) -> Bool {
      switch (lhs, rhs) {
      case let (.atomic(lhs, lhsLS), .atomic(rhs, rhsLS)):
        return lhs == rhs && lhsLS == rhsLS
      case let (.arrow(arg1, ret1, ls1), .arrow(arg2, ret2, ls2)):
        return arg1 == arg2 && ret1 == ret2 && ls1 == ls2
      case let (.typeVariable(name1, ls1), .typeVariable(name2, ls2)):
        return name1 == name2 && ls1 == ls2
      case let (.list(el1, ls1), .list(el2, ls2)):
        return el1 == el2 && ls1 == ls2
      case let (.tuple(els1), .tuple(els2)):
        return els1 == els2
      case let (.reference(type1, ls1), .reference(type2, ls2)):
        return type1 == type2 && ls1 == ls2
      case (.unit, .unit),
           (.unit, .tuple([], _)),
           (.tuple([], _), .unit):
        return true
      case let (.typeConstructor(name1, types1, lifetimeSpecifiers1, ls1), .typeConstructor(name2, types2, lifetimeSpecifiers2, ls2)):
        return name1 == name2 && types1 == types2 && lifetimeSpecifiers1 == lifetimeSpecifiers2 && ls1 == ls2
      case _:
        return false
      }
    }
    
    public var isTypeVariable: Bool {
      switch self {
      case .typeVariable:
        return true
      case _:
        return false
      }
    }
    
    public var isTuple: Bool {
      switch self {
      case .tuple:
        return true
      case _:
        return false
      }
    }
    
    public var isTypeConstructor: Bool {
      switch self {
      case .typeConstructor:
        return true
      case _:
        return false
      }
    }
    
    public var asPolymorphic: TCType {
      switch self {
      case let .typeVariable(name, lifetimeSpecifier):
        return .polymorphic(
          typeVariables: [name],
          context: .empty,
          type: .typeVariable(name: name, lifetimeSpecifier: lifetimeSpecifier))
      case let .list(element, lifetimeSpecifier):
        switch element.asPolymorphic {
        case let .polymorphic(typeVariables, context, type):
          return .polymorphic(
            typeVariables: typeVariables,
            context: context,
            type: .list(element: type, lifetimeSpecifier: lifetimeSpecifier))
        case _:
          return .simple(self)
        }
      case let .tuple(elements, lifetimeSpecifier):
        let (vars, types) = elements.reduce(into: (TypeVariables(), [SimpleType]())) { result, type in
          switch type.asPolymorphic {
          case let .polymorphic(typeVariables, _, _):
            result.0.formUnion(typeVariables)
            result.1.append(type)
          case _:
            result.1.append(type)
          }
        }
        if vars.isEmpty {
          return .simple(.tuple(elements: types, lifetimeSpecifier: lifetimeSpecifier))
        }
        return .polymorphic(typeVariables: vars, context: .empty, type: .tuple(elements: types, lifetimeSpecifier: lifetimeSpecifier))
      case let .arrow(arg, ret, lifetimeSpecifier):
        switch (arg.asPolymorphic, ret.asPolymorphic) {
        case let (.polymorphic(typeVariables1, context1, arg),
                  .polymorphic(typeVariables2, _, ret)):
          return .polymorphic(
            typeVariables: typeVariables1.union(typeVariables2),
            context: context1,
            type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
        case let (.polymorphic(typeVariables, context, arg),
                  .simple(ret)),
             let (.simple(arg),
                  .polymorphic(typeVariables, context, ret)):
          return .polymorphic(
            typeVariables: typeVariables,
            context: context,
            type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
        case let (.simple(arg), .simple(ret)):
          return .arrow(arg: arg, ret: ret)
        case _:
          fatalError()
        }
      case let .reference(type, lifetime):
        switch type.asPolymorphic {
        case let .polymorphic(typeVariables, context, type):
          return .polymorphic(typeVariables: typeVariables, context: context, type: .reference(type: type, lifetimeSpecifier: lifetime))
        case _:
          return .simple(self)
        }
      case let .typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier):
        let (vars, types) = types.reduce(into: (TypeVariables(), [SimpleType]())) { result, type in
          switch type.asPolymorphic {
          case let .polymorphic(typeVariables, _, _):
            result.0.formUnion(typeVariables)
            result.1.append(type)
          case _:
            result.1.append(type)
          }
        }
        if vars.isEmpty {
          return .simple(.typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: lifetimeSpecifier))
        }
        return .polymorphic(
          typeVariables: vars,
          context: .empty,
          type: .typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: lifetimeSpecifier))
      case _:
        return .simple(self)
      }
    }
  }
}

public extension TCType.SimpleType {
  static var int: TCType.SimpleType {
    .atomic(.numeric(.int), lifetimeSpecifier: nil)
  }
  
  static var float: TCType.SimpleType {
    .atomic(.numeric(.float), lifetimeSpecifier: nil)
  }
  
  static var bool: TCType.SimpleType {
    .atomic(.bool, lifetimeSpecifier: nil)
  }
  
  static var string: TCType.SimpleType {
    .atomic(.string, lifetimeSpecifier: nil)
  }
  
  static var char: TCType.SimpleType {
    .atomic(.char, lifetimeSpecifier: nil)
  }
  
  static func typeVariable(name: String) -> Self {
    .typeVariable(name: name, lifetimeSpecifier: nil)
  }
  
  static func atomic(_ type: Self.Atomic) -> Self {
    .atomic(type, lifetimeSpecifier: nil)
  }
  
  static func tuple(elements: [TCType.SimpleType]) -> TCType.SimpleType {
    .tuple(elements: elements, lifetimeSpecifier: nil)
  }
  
  static func list(element: TCType.SimpleType) -> TCType.SimpleType {
    .list(element: element, lifetimeSpecifier: nil)
  }
  
  static func arrow(arg: Self, ret: Self) -> Self {
    .arrow(arg: arg, ret: ret, lifetimeSpecifier: nil)
  }
  
  static func typeConstructor(name: String, types: [Self], lifetimeSpecifiers: [String]) -> Self {
    .typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: nil)
  }
}

extension TCType.SimpleType: ExpressibleByStringLiteral {
  public init(stringLiteral value: StringLiteralType) {
    self = .typeVariable(name: value, lifetimeSpecifier: nil)
  }
}

public extension TCType.SimpleType {
  enum Atomic: Equatable, CustomStringConvertible, Hashable {
    case numeric(Numeric)
    case bool
    case string
    case char

    public var description: String {
      switch self {
      case .numeric(let numeric):
        return numeric.description
      case .bool:
        return "Bool"
      case .string:
        return "String"
      case .char:
        return "Char"
      }
    }
  }
}

public extension TCType.SimpleType.Atomic {
  enum Numeric: Equatable, CustomStringConvertible, Hashable {
    case int
    case float

    public var description: String {
      switch self {
      case .int:
        return "Int"
      case .float:
        return "Float"
      }
    }
  }
}

public extension TCType {
  var asData: (name: String, constructors: [Constructor], genericParams: TCAst.Binding.GenericParams)? {
    switch self {
    case let .data(name, constructors, genericParams):
      return (name, constructors, genericParams)
    case _:
      return nil
    }
  }
  
  var isReference: Bool {
    switch self {
    case .simple(.reference),
         .polymorphic(_, _, .reference):
      return true
    case _:
      return false
    }
  }
  
  var isSimple: Bool {
    switch self {
    case .simple:
      return true
    case _:
      return false
    }
  }
  
  var dereference: TCType? {
    func withLifetime(type: TCType.SimpleType, lifetime: String?) -> TCType.SimpleType {
      switch type {
      case .list(let element, _):
        return .list(element: element, lifetimeSpecifier: lifetime)
      case .tuple(let elements, _):
        return .tuple(elements: elements, lifetimeSpecifier: lifetime)
      case let .arrow(arg, ret, _):
        return .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetime)
      case let .reference(type, _):
        return .reference(type: type, lifetimeSpecifier: lifetime)
      case let .typeConstructor(name, types, lifetimeSpecifiers, _):
        return .typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: lifetime)
      case let .atomic(atomic, _):
        return .atomic(atomic, lifetimeSpecifier: lifetime)
      case let .typeVariable(name, _):
        return .typeVariable(name: name, lifetimeSpecifier: lifetime)
      case .unit:
        return .unit
      }
    }
    
    
    switch self {
    case let .simple(.reference(type, lifetime)),
         let .polymorphic(_, _, .reference(type, lifetime)):
      return .simple(withLifetime(type: type, lifetime: lifetime)).asPolymorphic
    case _:
      return nil
    }
  }
  
  func wrapWithReference(lifetimeSpecifier: String?) -> TCType {
    switch self {
    case .simple(let simple),
         .polymorphic(_, _, let simple):
      return simple.wrapWithReference(lifetime: lifetimeSpecifier).asPolymorphic
    case _:
      return self
    }
  }
  
  func injectLifetimeToReference(lifetimeSpecifier: String?) -> TCType {
    switch self {
    case let .polymorphic(typeVariables, context, type):
      return .polymorphic(
        typeVariables: typeVariables,
        context: context,
        type: .reference(type: type, lifetimeSpecifier: lifetimeSpecifier))
    case .simple(let simple):
      return .reference(simple, lifetimeSpecifier: lifetimeSpecifier)
    case _:
      fatalError("Invalid type (probably due to a syntax error ... try to wrap type constructor with parentheses)")
    }
  }
  
  var asSimple: SimpleType? {
    switch self {
    case .simple(let simple):
      return simple
    case let .polymorphic(_, _, type):
      return type
    case _:
      return nil
    }
  }
  
  var asRef: SimpleType? {
    switch self {
    case let .simple(.reference(type, _)),
         let .polymorphic(_, _, .reference(type, _)):
      return type
    case _:
      return nil
    }
  }
  
  var isArrow: Bool {
    switch self {
    case .simple(.arrow):
      return true
    case .polymorphic(_, _, .arrow):
      return true
    case _:
      return false
    }
  }

  var asArrow: (arg: TCType, ret: TCType)? {
    switch self {
    case let .simple(.arrow(arg, ret, _)): // TODO: - Lifetime specifier
      return (.simple(arg), .simple(ret))
    case let .polymorphic(_, _, .arrow(arg, ret, _)):
      return (arg.asPolymorphic, ret.asPolymorphic)
    case _:
      return nil
    }
  }
  
  var asSimpleArrow: (arg: TCType.SimpleType, ret: TCType.SimpleType)? {
    switch self {
    case let .simple(.arrow(arg, ret, _)): // TODO: - Lifetime specifier
      return (arg, ret)
    case let .polymorphic(_, _, .arrow(arg, ret, _)):
      return (arg, ret)
    case _:
      return nil
    }
  }

  var asTuple: [TCType]? {
    switch self {
    case .polymorphic(_, _, .tuple(let elements, _)),
         .simple(.tuple(let elements, _)):
      return elements.map { $0.asPolymorphic }
    case .simple(.reference(.tuple(let elements, _), let lifetime)),
         .polymorphic(_, _, type: .reference(.tuple(let elements, _), let lifetime)):
      return elements.map { .reference($0, lifetimeSpecifier: lifetime).asPolymorphic }
    case .unit, .simple(.reference(.unit, _)):
      return []
    case _:
      return nil
    }
  }
  
  var asTypeConstructor: (name: String, types: [SimpleType], lifetimeSpecifiers: [String], lifetimeSpecifier: String?)? {
    switch self {
    case let .simple(.typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier)),
         let .polymorphic(_, _, type: .typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier)):
      return (name, types, lifetimeSpecifiers, lifetimeSpecifier)
    case _:
      return nil
    }
  }
  
  var asPolymorphic: TCType {
    switch self {
    case .simple(let simple):
      return simple.asPolymorphic
    case let .polymorphic(_, _, type):
      return type.asPolymorphic
    case _:
      return self
    }
  }
  
  var typeVariables: TypeVariables {
    switch self {
    case .polymorphic(let typeVariables, _, _):
      return typeVariables
    case _:
      return .empty()
    }
  }
  
  var isPolymorphic: Bool {
    switch self {
    case .polymorphic:
      return true
    case _:
      return false
    }
  }

  var asList: SimpleType? {
    switch self {
    case let .simple(.list(element, _)),
         let .polymorphic(_, _, .list(element, _)):
      return element
    case _:
      return nil
    }
  }

  var isInt: Bool {
    switch self {
    case .simple(.atomic(.numeric(.int), _)):
      return true
    case _:
      return false
    }
  }
  
  var isUnit: Bool {
    switch self {
    case .simple(.unit):
      return true
    case _:
      return false
    }
  }

  var isBool: Bool {
    switch self {
    case .simple(.atomic(.bool, nil)):
      return true
    case _:
      return false
    }
  }
  
  var isTypeVariable: Bool {
    switch self {
    case .simple(.typeVariable),
         .polymorphic(_, _, .typeVariable):
      return true
    case _:
      return false
    }
  }
  
  var isTypeConstructor: Bool {
    switch self {
    case .simple(.typeConstructor), .polymorphic(_, _, .typeConstructor):
      return true
    case _:
      return false
    }
  }
  
  var isTuple: Bool {
    switch self {
    case .simple(.tuple), .polymorphic(_, _, .tuple):
      return true
    case _:
      return false
    }
  }
  
  func containsTypeVariable(name: String) -> Bool {
    switch self {
    case .polymorphic(let typeVariables, _, _):
      return typeVariables.contains(name)
    case let .simple(.typeVariable(name2, _)):
      return name == name2
    case _:
      return false
    }
  }
  
  var curriedArrowArguments: [TCType] {
    guard let (arg, ret) = asArrow else { return [] }
    return [arg] + ret.curriedArrowArguments
  }
  
  var curriedArrow: [TCType] {
    guard let (arg, ret) = asArrow else { return [self] }
    return [arg] + ret.curriedArrow
  }
  
  var lifetime: LAAst.Lifetime? {
    switch self {
    case .simple(let simple),
         .polymorphic(_, _, let simple):
      return simple.lifetime
    case _:
      return nil
    }
  }
  
  func withLifetime(_ lifetime: String?) -> TCType {
    guard let lifetime = lifetime else {
      return self
    }

    switch self {
    case let .polymorphic(typeVariables, context, type):
      return .polymorphic(
        typeVariables: typeVariables,
        context: context,
        type: TCType.simple(type).withLifetime(lifetime).asSimple!)
    case let .simple(.reference(type, _)):
      return .simple(.reference(type: type, lifetimeSpecifier: lifetime))
    case let .simple(.atomic(atomic, _)):
      return .simple(.atomic(atomic, lifetimeSpecifier: lifetime))
    case let .simple(.typeVariable(name, _)):
      return .simple(.typeVariable(name: name, lifetimeSpecifier: lifetime))
    case let .simple(.arrow(arg, ret, _)):
      return .simple(.arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetime))
    case let .simple(.list(element, _)):
      return .simple(.list(element: element, lifetimeSpecifier: lifetime))
    case let .simple(.tuple(elements, _)):
      return .simple(.tuple(elements: elements, lifetimeSpecifier: lifetime))
    case let .simple(.typeConstructor(name, types, lifetimeSpecifiers, _)):
      return .simple(.typeConstructor(name: name, types: types, lifetimeSpecifiers: lifetimeSpecifiers, lifetimeSpecifier: lifetime))
    case .simple(.unit):
      return .simple(.unit)
    case .data:
      return self
    }
  }
}

public extension TCType {
  static func arrow(arg: SimpleType, ret: SimpleType, lifetimeSpecifier: String? = nil) -> TCType {
    .simple(.arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
  }
  
  static func list(element: SimpleType, lifetimeSpecifier: String? = nil) -> TCType {
    .simple(.list(element: element, lifetimeSpecifier: lifetimeSpecifier))
  }
  
  static func tuple(elements: [SimpleType], lifetimeSpecifier: String? = nil) -> TCType {
    assert(!elements.isEmpty)
    return .simple(.tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier))
  }
  
  static func reference(_ type: SimpleType, lifetimeSpecifier: String? = nil) -> TCType {
    .simple(.reference(type: type, lifetimeSpecifier: lifetimeSpecifier))
  }
  
  static func atomic(_ type: SimpleType.Atomic, lifetimeSpecifier: String? = nil) -> TCType {
    .simple(.atomic(type, lifetimeSpecifier: lifetimeSpecifier))
  }
  
  static var unit: TCType {
    .simple(.unit)
  }
  
  static var bool: TCType {
    .atomic(.bool)
  }
  
  static var int: TCType {
    .atomic(.numeric(.int))
  }
  
  static var float: TCType {
    .atomic(.numeric(.float))
  }
  
  static var char: TCType {
    .atomic(.char)
  }
  
  static var string: TCType {
    .atomic(.string)
  }
  
  static func arrow(arg: SimpleType, ret: SimpleType) -> TCType {
    .arrow(arg: arg, ret: ret, lifetimeSpecifier: nil)
  }
}

public extension TCType {
  static func createArrow(arg: TCType, ret: TCType, lifetimeSpecifier: String? = nil) -> TCType {
    func convertType(_ type: TCType) -> TCType {
      switch type {
      /// TODO: - What is this code doing ???????
      case let .data(name, _, genericParams):
        // TODO: -
        return .simple(.typeConstructor(
          name: name,
          types: [],
          lifetimeSpecifiers: [],
          lifetimeSpecifier: nil)) // TODO: -
      case _:
        return type
      }
    }
    
    switch (convertType(arg), convertType(ret)) {
    case let (.polymorphic(typeVariables1, context1, type1),
              .polymorphic(typeVariables2, _, type2)):
      return .polymorphic(
        typeVariables: typeVariables1.union(typeVariables2),
        context: context1,
        type: .arrow(arg: type1, ret: type2, lifetimeSpecifier: lifetimeSpecifier))
    case let (.polymorphic(typeVariables, context, arg),
              .simple(ret)),
         let (.simple(arg),
              .polymorphic(typeVariables, context, ret)):
      return .polymorphic(
        typeVariables: typeVariables,
        context: context,
        type: .arrow(arg: arg, ret: ret, lifetimeSpecifier: lifetimeSpecifier))
    case let (.simple(arg), .simple(ret)):
      return .arrow(arg: arg, ret: ret)
    case _:
      // TODO: - Should probably return an error instead
      fatalError("TCType.createArrow: Invalid type combination!")
    }
  }
  
  static func createTuple(elements: [TCType], lifetimeSpecifier: String? = nil) -> TCType {
    assert(!elements.isEmpty)
    let (elements, typeVariables) = elements.reduce(into: ([SimpleType](), TypeVariables())) { acc, type in
      switch type {
      case .simple(let simple):
        acc.0.append(simple)
      case let .polymorphic(typeVariables_, _, simple):
        acc.0.append(simple)
        acc.1.formUnion(typeVariables_)
      case _:
        // TODO: - Should probably return an error instead
        fatalError("TCType.createTuple: Invalid type combination!")
      }
    }
    switch typeVariables.isEmpty {
    case true:
      return .tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier)
    case false:
      return .polymorphic(
        typeVariables: typeVariables,
        context: .empty,
        type: .tuple(elements: elements, lifetimeSpecifier: lifetimeSpecifier))
    }
  }
  
  static func createList(element: TCType, lifetimeSpecifier: String? = nil) -> TCType {
    switch element {
    case .simple(let simple):
      return .list(element: simple)
    case let .polymorphic(typeVariables, context, type):
      return .polymorphic(
        typeVariables: typeVariables,
        context: context,
        type: .list(element: type, lifetimeSpecifier: lifetimeSpecifier))
    case _:
      // TODO: - Should probably return an error instead
      fatalError("TCType.createList: Invalid type combination!")
    }
  }
  
  func addReturnType(_ type: SimpleType) -> TCType {
    switch self {
    case let .polymorphic(typeVariables, context, t):
      return .polymorphic(typeVariables: typeVariables, context: context, type: t.addReturnType(type))
    case let .simple(arrow):
      return .simple(arrow.addReturnType(type))
    case _:
      fatalError("TCType.addReturnType: Invalid type!")
    }
  }
  
  var fullyAppliedReturnType: TCType? {
    guard let (_, ret) = asArrow else { return nil }
    return ret.fullyAppliedReturnType ?? ret
  }
  
  var partiallyAppliedReturnType: TCType? {
    asArrow?.ret
  }
}

public extension TCType.SimpleType {
  func addReturnType(_ type: TCType.SimpleType) -> TCType.SimpleType {
    switch self {
    case let .arrow(arg, ret, lifetimeSpecifier):
      return .arrow(arg: arg, ret: ret.addReturnType(type), lifetimeSpecifier: lifetimeSpecifier)
    case _:
      return .arrow(arg: self, ret: type, lifetimeSpecifier: nil)
    }
  }
  
  var asArrow: (TCType.SimpleType, TCType.SimpleType)? {
    switch self {
    case let .arrow(arg, ret, _): // TODO: - Lifetime specifier
      return (arg, ret)
    case _ :
      return nil
    }
  }
  
  var isReference: Bool {
    switch self {
    case .reference:
      return true
    case _:
      return false
    }
  }
  
  var lifetime: LAAst.Lifetime? {
    switch self {
    case let .reference(_, lifetimeSpecifier?):
      return lifetimeSpecifier.toLifetime
    case let .reference(type, nil):
      return type.lifetime
    case let .arrow(arg, ret, lifetimeSpecifier):
      if let lifetime = lifetimeSpecifier?.toLifetime {
        return lifetime
      }
      // TODO: - Should we return `nil` for arrow?
      return .composite(lifetimes: [arg.lifetime, ret.lifetime].compactMap { $0 })
    case let .tuple(elements, lifetimeSpecifier):
      if let lifetime = lifetimeSpecifier?.toLifetime {
        return lifetime
      }
      return .composite(lifetimes: elements.compactMap { $0.lifetime })
    case let .list(element, lifetimeSpecifier):
      return lifetimeSpecifier?.toLifetime ?? element.lifetime
    case let .typeConstructor(_, _, lifetimeSpecifiers, lifetimeSpecifier):
      if let lifetime = lifetimeSpecifier?.toLifetime {
        return lifetime
      }
//      return .composite(lifetimes: lifetimeSpecifiers.map { $0.toLifetime })
      return nil
    case let .atomic(_, lifetimeSpecifier),
         let .typeVariable(_, lifetimeSpecifier):
      return lifetimeSpecifier?.toLifetime
    case .unit:
      return nil
    }
  }
  
  func wrapWithReference(lifetime: String?) -> Self { // TODO: - This implementation doesn't seem right ...
    switch self {
    case .atomic, .unit, .typeVariable:
      return .reference(type: self, lifetimeSpecifier: lifetime)
    case let .reference(type, lifetimeSpecifier):
      return .reference(type: type.wrapWithReference(lifetime: lifetime), lifetimeSpecifier: lifetimeSpecifier)
    case .typeConstructor:
      return .reference(type: self, lifetimeSpecifier: lifetime)
    case let .arrow(arg, ret, lifetimeSpecifier):
      return .arrow(
        arg: arg.wrapWithReference(lifetime: lifetime),
        ret: ret.wrapWithReference(lifetime: lifetime),
        lifetimeSpecifier: lifetimeSpecifier)
    case let .list(element, lifetimeSpecifier):
      return .list(
        element: element.wrapWithReference(lifetime: lifetime),
        lifetimeSpecifier: lifetimeSpecifier)
    case let .tuple(elements, lifetimeSpecifier):
      return .tuple(
        elements: elements.map { $0.wrapWithReference(lifetime: lifetime) },
        lifetimeSpecifier: lifetimeSpecifier)
    }
  }
}

public extension Collection where Element == TCType {
  var formArrow: TCType {
    switch count {
    case 0:
      return .unit
    case 1:
      return first!
    case 2:
      return .createArrow(arg: first!, ret: dropFirst().first!)
    case _:
      return .createArrow(arg: first!, ret: dropFirst().formArrow)
    }
  }
}

public func ==(lhs: TCType.SimpleType, rhs: TCType) -> Bool {
  rhs.asSimple == lhs
}

public func ==(lhs: TCType, rhs: TCType.SimpleType) -> Bool {
  rhs == lhs.asSimple
}

fileprivate extension String {
  var toLifetime: LAAst.Lifetime {
    switch self {
    case "'STATIC":
      return .static(id: -1)
    case _ where self.allSatisfy { $0.isLowercase }:
      return .init(scope: .local(.init(identifier: self)))
    case _:
      return .init(scope: .generic(.init(identifier: self)))
    }
  }
}
