import Foundation
import Bow
import SwiftParsec
import OrderedCollections

internal var CURRENT_FILE_BEING_COMPILED: String = ""

public func >>-<A, B, C>(
  _ f1: @escaping (A) -> Bow.Either<Error, B>,
  _ f2: @escaping (B) -> Bow.Either<Error, C>
) -> (A) -> Bow.Either<Error, C> {
  {
    let r1 = f1($0)
    switch r1.isLeft {
    case true:
      return .left(r1.leftValue)
    case false:
      return f2(r1.rightValue)
    }
  }
}

public func parse(
  fileName: String,
  using parser: Parser
) -> (String) -> Bow.Either<Error, AST> {
  CURRENT_FILE_BEING_COMPILED = fileName
  return {
    switch parser.parse(sourceName: fileName, input: $0) {
    case .left(let error):
      return .left(error)
    case .right(let ast):
      return .right(ast)
    }
  }
}

public func parse(
  fileName: String,
  using parser: @escaping (String) -> Bow.Either<Error, AST>
) -> (String) -> Bow.Either<Error, AST> {
  CURRENT_FILE_BEING_COMPILED = fileName
  return parser
}

public func resolveImports(
  compilerPipeline pipeline: @escaping (String) throws -> Bow.Either<Error, AST>
) -> (AST) -> Bow.Either<Error, AST> {
  { ast in
    // @TODO: - Optimise!
    
    var imports = OrderedSet<String>()
    var bindings = [AST.Binding]()
    bindings.reserveCapacity(ast.bindings.count)
    for binding in ast.bindings {
      switch binding {
      case .include(let module):
        imports.append(module)
      case _:
        bindings.append(binding)
      }
    }
    
    guard !imports.isEmpty else {
      return .right(ast)
    }
    
    do {
      let pipeline = { (file: String) throws -> Bow.Either<Error, AST> in
        CURRENT_FILE_BEING_COMPILED = file
        return try pipeline(file)
      }
      let results: [Bow.Either<Error, AST>] = try imports.map {
        let current = CURRENT_FILE_BEING_COMPILED
        return try pipeline($0).map { ast in
          CURRENT_FILE_BEING_COMPILED = current
          return ast
        }^
      }
      var asts = [AST]()
      asts.reserveCapacity(imports.count)
      for result in results {
        guard result.isRight else {
          return result
        }
        asts.append(result.rightValue)
      }
      var allBindings = asts.reduce(into: []) { $0.append($1.bindings) }.flatMap { $0 }
      allBindings.append(contentsOf: bindings)
      return .right(.init(bindings: allBindings))
    } catch {
      return .raiseError(error)^
    }
  }
}

public func typeChecker(ast: AST) -> Bow.Either<Error, AST> {
  ast
    .typeChecker()
    .run()^
    .mapLeft { $0 }
}

public func lifetimeAnalyser(ast: AST) -> Bow.Either<Error, AST> {
  ast
    .lifetimeAnalyser()
    .run()^
    .mapLeft { $0 }
}

public func borrowChecker(discard: Bool = false) -> (AST) -> Bow.Either<Error, AST> {
  { ast in
    ast
      .borrowChecker()
      .run()^
      .map { discard ? ast : $0 }^
      .mapLeft { $0 }
  }
}

public func liftCases(ast: AST) -> Bow.Either<Error, AST> {
  let liftCases = LiftCases()
  let transformedAst = liftCases.transformAST(ast)
  return .right(transformedAst)
}

public func stackFrames(ast: AST) -> Bow.Either<Error, (AST, ARState)> {
  let (env, _) = ast
    .frameEvaluator()
    .run(.empty)
  return .right((ast, env))
}

public func generateIR(
  memorySize: Byte,
  config: IRState.Configuration = .default,
  optimizer: Optimizer 
) -> (
  AST,
  ARState
) -> Bow.Either<Error, (Set<IRState.CodeChunk>, LabelStreamEnv, TempStreamEnv, MemoryEnv)> {
  { ast, env in
    let initialState = IRState(
      fme: env.frameMappingEnv,
      ame: env.accessMappingEnv,
      lse: env.labelStreamEnv,
      tse: env.tempStreamEnv,
      memorySize: memorySize,
      config: config
    )
    let (_, (chunks, state)) = ast
      .generateIR()
      .map { Set($0.map(optimizer.optimize)) }^
      .withState()
      .run(initialState)
    return .right((chunks, state.lse, state.tse, state.mem))
  }
}

public func generateIR(
  memory: Interpreter.Memory,
  config: IRState.Configuration = .default,
  optimizer: Optimizer
) -> (
  AST,
  ARState
) -> Bow.Either<Error, (Set<IRState.CodeChunk>, LabelStreamEnv, TempStreamEnv, MemoryEnv)> {
  { ast, env in
    let initialState = IRState(
      fme: env.frameMappingEnv,
      ame: env.accessMappingEnv,
      lse: env.labelStreamEnv,
      tse: env.tempStreamEnv,
      memory: memory,
      config: config
    )
    let (_, (chunks, state)) = ast
      .generateIR()
      .map { Set($0.map(optimizer.optimize)) }^
      .withState()
      .run(initialState)
    return .right((chunks, state.lse, state.tse, state.mem))
  }
}

public func linearizeCode(input: (Set<IRState.CodeChunk>, LabelStreamEnv, TempStreamEnv, MemoryEnv)) -> Bow.Either<Error, (CodeGenerator.CodeChunk, Interpreter.Memory)> {
  let codeGenerator = CodeGenerator(
    tempStreamEnv: input.2,
    labelStreamEnv: input.1,
    memory: input.3.memory)
  let mainChunk = codeGenerator.linearizeAndGenerateCode(chunks: input.0)
  return .right((mainChunk, codeGenerator.memory))
}

public func interpret(
  outputStream: TextOutputStream,
  traceOutputHandle: FileHandle?,
  executionMode: Interpreter.ExecutionMode,
  isRunningTests: Bool = false,
  isRunningREPL: Bool,
  runtimeInfo: Box<Interpreter.RuntimeInfo>? = nil
) -> (CodeGenerator.CodeChunk, Interpreter.Memory) -> Bow.Either<Error, ()> {
  { chunk, memory in
    do {
      let interpreter = Interpreter(
        memory: memory,
        stdout: outputStream
      )
      interpreter.trace = traceOutputHandle
      interpreter.executionMode = executionMode
      try interpreter.interpret(chunk: chunk)
      if isRunningTests {
        print("Delta:", (interpreter.runtimeInfo.allocatedMemory - interpreter.runtimeInfo.deallocatedMemory).bytes)
      } else if !isRunningREPL && executionMode == .debug {
        let delta = interpreter.runtimeInfo.allocatedMemory - interpreter.runtimeInfo.deallocatedMemory
        print("\n--------------------------------------------\n")
        print("ℹ️  Allocated memory: \(String(format: "%dB [%@]", interpreter.runtimeInfo.allocatedMemory, interpreter.runtimeInfo.allocatedMemory.bytes).magenta)")
        print("ℹ️  Freed memory    : \(String(format: "%dB [%@]", interpreter.runtimeInfo.deallocatedMemory, interpreter.runtimeInfo.deallocatedMemory.bytes).magenta)")
        print("ℹ️  Delta           :", "\(delta)B [\(delta.bytes)] \((1.0 - Double(delta) / Double(interpreter.runtimeInfo.allocatedMemory)).formatted(.percent))".magenta)
        print("ℹ️  Peak used memory: \(String(format: "%dB [%@]", interpreter.runtimeInfo.peakAllocatedMemory, interpreter.runtimeInfo.peakAllocatedMemory.bytes).magenta)")
        print("ℹ️  Avg used memory : \(String(format: "%dB [%@]", interpreter.runtimeInfo.averageAllocatedMemory, interpreter.runtimeInfo.averageAllocatedMemory.bytes).magenta)")
      }
      runtimeInfo?.value = interpreter.runtimeInfo
      return .right(())
    } catch {
      return .left(error)
    }
  }
}

public func prettyPrint(ast: AST) -> Bow.Either<Error, AST> {
  print(ast.prettyDescription)
  return .right(ast)
}

public func prettyPrint<A>(pair: (AST, A)) -> Bow.Either<Error, (AST, A)> {
  print(pair.0.prettyDescription)
  print(pair.1)
  return .right(pair)
}

public func prettyPrint<A, B>(tuple: (AST, A, B)) -> Bow.Either<Error, (AST, A, B)> {
  print(tuple.0.prettyDescription)
  print(tuple.1)
  print(tuple.2)
  return .right(tuple)
}

//func prettyPrint(chunks: [IRState.CodeChunk]) -> Bow.Either<Error, [IRState.CodeChunk]> {
//  for (id, chunk) in chunks.enumerated() {
//    print(id)
//    print(chunk.stmt.prettyDescription)
//  }
//  return .right(chunks)
//}

public func XCTestDump(ast: AST) -> Bow.Either<Error, AST> {
  print(XCTestDump(ast: ast) as String)
  return .right(ast)
}

public func XCTestDump<A>(pair: (AST, A)) -> Bow.Either<Error, (AST, A)> {
  print(XCTestDump(ast: pair.0) as String)
  return .right(pair)
}

public func XCTestDump<A, B>(tuple: (AST, A, B)) -> Bow.Either<Error, (AST, A, B)> {
  print(XCTestDump(ast: tuple.0) as String)
  return .right(tuple)
}

public func abort<A>(_ any: A) -> Bow.Either<Error, A> {
  exit(0)
}

extension Byte {
  var bytes: String {
    formatted(.byteCount(style: .memory))
  }
}
