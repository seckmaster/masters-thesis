//
//  XCTestDump.swift
//  
//
//  Created by Toni K. Turk on 27/12/2021.
//

import Foundation

func XCTestDump(ast: AST, indent: Int = 0) -> String {
  let bindings = ast.bindings
    .map { XCTestDump(binding: $0, indent: indent + 2) }
    .joined(separator: ",\n")
  return """
  .init( // NOTE: - Generated with XCTestDump
    bindings: [
  \(bindings.with(indent: indent + 2))
    ]
  )
  """.with(indent: indent)
}

func XCTestDump(binding: AST.Binding, indent: Int = 0) -> String {
  switch binding {
  case let .data(_, name, params, constructors, type):
    return """
    .data(
      position: .init(),
      name: "\(name)",
      params: \(XCTestDump(params: params, indent: 0)),
      constructors: [
    \(XCTestDump(constructors: constructors, indent: 0).with(indent: indent + 2))
      ],
      tcType: \(XCTestDump(type: type, indent: 0)))
    """.with(indent: indent)
  case let .prototype(_, name, type, tcType):
    return """
    .prototype(
      position: .init(),
      name: "\(name)",
      type: \(XCTestDump(type: type, indent: indent).with(indent: indent)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent)))
    """.with(indent: indent)
  case let .binding(_, name, cases, tcType, _, _):
    return """
    .binding(
      position: .init(),
      name: "\(name)",
      cases: [
    \(XCTestDump(cases: cases, indent: 0).with(indent: indent + 2))
      ],
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent)),
      lifetime: nil,
      borrowLifetime: nil)
    """
  case _:
    return ""
  }
}

func XCTestDump(params: AST.Binding.GenericParams, indent: Int = 0) -> String {
  let lifetimes = params.lifetimeParameters.map { "\"\($0)\"" }.joined(separator: ", ")
  let types = params.typeParameters.map { "\"\($0)\"" }.joined(separator: ", ")
  return ".init(lifetimeParameters: [\(lifetimes)], typeParameters: [\(types)])".with(indent: indent)
}

func XCTestDump(constructors: [AST.Binding.Constructor], indent: Int = 0) -> String {
  """
  \(constructors.map { XCTestDump(constructor: $0, indent: indent) }.joined(separator: ",\n"))
  """.with(indent: indent)
}

func XCTestDump(constructor: AST.Binding.Constructor, indent: Int = 0) -> String {
  """
  .init(
    position: .init(),
    name: "\(constructor.name)",
    types: [
  \(constructor.types.map { XCTestDump(type: $0, indent: indent + 2) }.joined(separator: ",\n").with(indent: indent + 2))
    ],
    type: \(XCTestDump(type: constructor.type)))
  """.with(indent: indent)
}

func XCTestDump(cases: [AST.Binding.Case], indent: Int = 0) -> String {
  cases
    .map { XCTestDump(case: $0, indent: indent) }
    .joined(separator: ",\n")
}

func XCTestDump(`case`: AST.Binding.Case, indent: Int = 0) -> String {
  let params = `case`.parameters.isEmpty
    ? "[]"
  : """
  [
    \(`case`.parameters.map { XCTestDump(pattern: $0, indent: indent + 2) }.joined(separator: ",\n").with(indent: indent + 2))
  ]
  """
  return """
  .init(
    name: "\(`case`.name)",
    position: .init(),
    parameters: \(params),
    body: \(XCTestDump(expression: `case`.body, indent: indent + 2).with(indent: indent + 2)),
    type: \(XCTestDump(type: `case`.type, indent: indent + 2).with(indent: indent + 2)),
    lifetime: nil)
  """.with(indent: indent)
}

func XCTestDump(type: AST.AType, indent: Int = 0) -> String {
  switch type {
  case let .name(_, name, lifetimeSpecifier, tcType):
    return """
    .name(
      position: .init(),
      name: "\(name)",
      lifetimeParameter: \(XCTestDump(lifetime: lifetimeSpecifier)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent)))
    """.with(indent: indent)
  case let .typeConstructor(_, name, types, lifetimeSpecifiers, lifetimeSpecifier, tcType):
    return """
    .typeConstructor(
      position: .init(),
      name: "\(name)",
      types: [
    \(types.map { XCTestDump(type: $0, indent: indent) }.joined(separator: "\n").with(indent: indent))
      ],
      lifetimeSpecifiers: [\(lifetimeSpecifiers.map { "\"\($0)\"" }.joined(separator: ", "))],
      lifetimeParameter: \(XCTestDump(lifetime: lifetimeSpecifier)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent)))
    """.with(indent: indent)
  case let .ref(_, type, lifetimeParameter, tcType):
    return """
    .ref(
      position: .init(),
      type: \(XCTestDump(type: type, indent: indent).with(indent: indent)),
      lifetimeParameter: \(lifetimeParameter.map { "\"\($0)\"" } ?? "nil"),
      tcType:
    \(XCTestDump(type: tcType, indent: indent).with(indent: indent)))
    """.with(indent: indent)
  case let .function(_, arg, ret, lifetimeSpecifier, tcType):
    return """
    .function(
      position: .init(),
      arg: \(XCTestDump(type: arg, indent: indent).with(indent: indent)),
      ret: \(XCTestDump(type: ret, indent: indent).with(indent: indent)),
      lifetimeParameter: \(XCTestDump(lifetime: lifetimeSpecifier)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent)))
    """.with(indent: indent)
  case let .tuple(position, types, lifetimeParameter, type):
    return """
    .tuple(
      position: .init(),
      types: [
    \(types.map { XCTestDump(type: $0, indent: indent) }.joined(separator: "\n").with(indent: indent))
      ],
      lifetimeParameter: \(XCTestDump(lifetime: lifetimeParameter)),
      tcType: \(XCTestDump(type: type)))
    """
  case _:
    return ""
  }
}

func XCTestDump(expression: AST.Expression, indent: Int = 0) -> String {
  switch expression {
  case let .name(_, identifier, tcType, lifetime, _):
    return """
    .name(
      position: .init(),
      identifier: \"\(identifier)\",
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil
    )
    """.with(indent: indent)
  case let .literal(_, literal, tcType, lifetime):
    return """
    .literal(
      position: .init(),
      literal: \(XCTestDump(literal: literal)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
      lifetime: nil)
    """
  case let .let(_, bindings, expression, tcType, lifetime):
    return """
    .let(
      position: .init(),
      bindings: [
    \(bindings.map { XCTestDump(binding: $0, indent: indent + 2) }.joined(separator: ",\n"))
      ],
      expression: \(XCTestDump(expression: expression, indent: indent).with(indent: indent + 2)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
      lifetime: nil)
    """
  case let .lambda(_, parameter, body, tcType, lifetime):
    return """
    .lambda(
      position: .init(),
      parameter: \(XCTestDump(pattern: parameter, indent: indent).with(indent: indent + 2)),
      body: \(XCTestDump(expression: body, indent: indent).with(indent: indent + 2)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
      lifetime: nil)
    """
  case let .list(_, expressions, tcType, lifetime):
    return """
    .list(
      position: .init(),
      expressions: [
      \(expressions.map { XCTestDump(expression: $0) }.joined(separator: ",\n"))
      ],
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
      lifetime: nil)
    """
  case let .ref(_, expression, tcType, lifetime):
    return """
    .ref(
      position: .init(),
      expression: \(XCTestDump(expression: expression, indent: indent).with(indent: indent + 2)),
      tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
      lifetime: nil)
    """
  case let .binary(.application(_, fn, arg, tcType, lifetime)):
    return """
    .binary(
      .application(
        position: .init(),
        fn: \(XCTestDump(expression: fn, indent: indent).with(indent: indent + 2)),
        arg: \(XCTestDump(expression: arg, indent: indent).with(indent: indent + 2)),
        tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
        lifetime: nil))
    """
  case let .binary(.ifThenElse(_, condition, `true`, `false`, tcType, lifetime)):
    return """
    .binary(
      .ifThenElse(
        position: .init(),
        condition: \(XCTestDump(expression: condition, indent: indent).with(indent: indent + 2)),
        true: \(XCTestDump(expression: `true`, indent: indent).with(indent: indent + 2)),
        false: \(XCTestDump(expression: `false`, indent: indent).with(indent: indent + 2)),
        tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
        lifetime: nil))
    """
  case let .binary(.arithmetic(_, op, left, right, tcType, lifetime)):
    return """
    .binary(
      .arithmetic(
        position: .init(),
        operator: \(XCTestDump(op: op)),
        left: \(XCTestDump(expression: left, indent: indent).with(indent: indent + 2)),
        right: \(XCTestDump(expression: right, indent: indent).with(indent: indent + 2)),
        tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
        lifetime: nil))
    """
  case let .binary(.logical(_, op, left, right, tcType, lifetime)):
    return """
    .binary(
      .logical(
        position: .init(),
        operator: \(XCTestDump(op: op)),
        left: \(XCTestDump(expression: left, indent: indent).with(indent: indent + 2)),
        right: \(XCTestDump(expression: right, indent: indent).with(indent: indent + 2)),
        tcType: \(XCTestDump(type: tcType, indent: indent).with(indent: indent + 2)),
        lifetime: nil))
    """
  case let .case(_, expression, matches, type, lifetime):
    return """
    .case(
      position: .init(),
      expression: \(XCTestDump(expression: expression)),
      matches: [
    \(matches.map { XCTestDump(match: $0, indent: indent) }.joined(separator: ",\n").with(indent: indent))
      ],
      tcType: \(XCTestDump(type: type)),
      lifetime: nil)
    """
  case _:
    return "todo \(expression.description)"
  }
}

func XCTestDump(match: AST.Expression.Match, indent: Int) -> String {
  """
  .init(
    position: .init(),
    pattern: \(XCTestDump(pattern: match.pattern, indent: indent)),
    expression: \(XCTestDump(expression: match.expression)))
  """
}

func XCTestDump(op: Lexer.Token.Operator.Arithmetic) -> String {
  switch op {
  case .plus:
    return ".plus"
  case .mul:
    return ".mul"
  case .div:
    return ".div"
  case .minus:
    return ".minus"
  }
}

func XCTestDump(op: Lexer.Token.Operator.Logical) -> String {
  switch op {
  case .and:
    return ".and"
  case .or:
    return ".or"
  case .lowerThan:
    return ".lowerThan"
  case .greaterThan:
    return ".greaterThan"
  case .greaterOrEqualThan:
    return ".greaterOrEqualThan"
  case .lowerOrEqualThan:
    return ".lowerOrEqualThan"
  case .equal:
    return ".equal"
  case .notEqual:
    return ".notEqual"
  }
}

func XCTestDump(literal: Lexer.Token.Literal) -> String {
  switch literal {
  case .logical(.true):
    return ".logical(.true)"
  case .logical(.false):
    return ".logical(.false)"
  case .int(let val):
    return ".int(\(val))"
  case .char(let val):
    return ".char(\(val))"
  case .float(let val):
    return ".float(\(val))"
  case .string(let val):
    return ".string(\"\(val)\")"
  }
}

func XCTestDump(pattern: AST.Pattern, indent: Int = 0) -> String {
  switch pattern {
  case let .wildcard(_, tcType):
    return """
    .wildcard(
      position: .init(),
      tcType: \(XCTestDump(type: tcType, indent: indent)))
    """.with(indent: indent)
  case let .deconstruct(_, constructor, patterns, tcType, lifetime):
    return """
    .deconstruct(
      position: .init(),
      constructor: "\(constructor)",
      patterns: [
    \(patterns.map { XCTestDump(pattern: $0, indent: indent) }.joined(separator: ",\n").with(indent: indent))
      ],
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil)
    """.with(indent: indent)
  case let .identifier(_, identifier, tcType, lifetime):
    return """
    .identifier(
      position: .init(),
      identifier: "\(identifier)",
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil)
    """.with(indent: indent)
  case let .literal(_, literal, tcType, lifetime):
    return """
    .literal(
      position: .init(),
      literal: \(XCTestDump(literal: literal)),
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil)
    """.with(indent: indent)
  case let .ref(_, pattern, tcType, lifetime):
    return """
    .ref(
      position: .init(),
      pattern: \(XCTestDump(pattern: pattern, indent: indent)),
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil)
    """.with(indent: indent)
  case let .tuple(_, patterns, tcType, lifetime):
    return """
    .tuple(
      position: .init(),
      patterns: [
    \(patterns.map { XCTestDump(pattern: $0, indent: 0) }.joined(separator: ",\n").with(indent: indent)),
      ]
      tcType: \(XCTestDump(type: tcType, indent: indent)),
      lifetime: nil)
    """.with(indent: indent)
  case let .list(.empty(_, tcType, lifetime)):
    return """
    .list(
      .empty(
        position: .init(),
        tcType: \(XCTestDump(type: tcType, indent: indent + 2)),
        lifetime: nil))
    """.with(indent: indent)
  case let .list(.cons(_, head, tail, tcType, lifetime)):
    return """
    .list(
      .cons(
        position: .init(),
        head: \(XCTestDump(pattern: head, indent: indent + 2).with(indent: indent)),
        tail: \(XCTestDump(pattern: tail, indent: indent + 2).with(indent: indent)),
        tcType: \(XCTestDump(type: tcType, indent: indent + 2).with(indent: indent)),
        lifetime: nil))
    """.with(indent: indent)
  }
}

func XCTestDump(type: TCType?, indent: Int = 0) -> String {
  guard let type = type else { return "nil" }
  switch type {
  case let .polymorphic(typeVariables, _, type):
    return """
    .polymorphic(
      typeVariables: [\(typeVariables.map { "\"\($0)\"" }.joined(separator: ", "))],
      context: .empty,
      type: \(XCTestDump(type: type, indent: indent)))
    """.with(indent: indent)
//  case let .simple(.atomic(atomic, ls)): // TODO: - Dump `ls`
//    switch atomic {
//    case .string:
//      return ".string".with(indent: indent)
//    case .bool:
//      return ".bool".with(indent: indent)
//    case .char:
//      return ".char".with(indent: indent)
//    case .numeric(.int):
//      return ".int".with(indent: indent)
//    case .numeric(.float):
//      return ".float".with(indent: indent)
//    }
  case let .simple(type):
    return """
    .simple(\(XCTestDump(type: type, indent: indent)))
    """.with(indent: indent)
  case let .data(name, constructors, genericParams):
    return """
    .data(
      name: "\(name)",
      constructors: [
    \(constructors.map { XCTestDump(constructor: $0, indent: indent) }.joined(separator: ",\n").with(indent: indent))
      ],
      genericParams: \(XCTestDump(params: genericParams, indent: 0)))
    """.with(indent: indent)
  }
}

func XCTestDump(lifetime: String?) -> String {
  lifetime.map { "\"\($0)\"" } ?? "nil"
}

func XCTestDump(type: TCType.SimpleType?, indent: Int = 0) -> String {
  guard let type = type else { return "nil" }
  switch type {
  case let .atomic(.bool, lifetimeSpecifier):
    return ".atomic(.bool, lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))".with(indent: indent)
  case let .atomic(.string, lifetimeSpecifier):
    return ".atomic(.string, lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))".with(indent: indent)
  case let .atomic(.char, lifetimeSpecifier):
    return ".atomic(.char, lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))".with(indent: indent)
  case let .atomic(.numeric(.float), lifetimeSpecifier):
    return ".atomic(.numeric(.float), lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))".with(indent: indent)
  case let .atomic(.numeric(.int), lifetimeSpecifier):
    return ".atomic(.numeric(.int), lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))".with(indent: indent)
  case let .reference(type, lifetimeSpecifier):
    return """
    .reference(
      type: \(XCTestDump(type: type, indent: indent).with(indent: indent)),
      lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))
    """.with(indent: indent)
  case let .arrow(arg, ret, lifetimeSpecifier):
    return """
    .arrow(
      arg: \(XCTestDump(type: arg, indent: indent).with(indent: indent + 2)),
      ret: \(XCTestDump(type: ret, indent: indent).with(indent: indent + 2)),
      lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))
    """.with(indent: indent)
  case let .list(element, lifetimeSpecifier):
    return """
    .list(
      element: \(XCTestDump(type: element, indent: indent).with(indent: indent)),
      lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))
    """.with(indent: indent)
  case let .tuple(elements, lifetimeSpecifier):
    return """
    .tuple(
      elements: [
    \(elements.map { XCTestDump(type: $0, indent: 0) }.joined(separator: ",\n").with(indent: indent + 2))
      ], lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))
    """.with(indent: indent)
  case let .typeVariable(name, _):
    return ".typeVariable(name: \"\(name)\")".with(indent: indent)
  case let .typeConstructor(name, types, lifetimeSpecifiers, lifetimeSpecifier):
    return """
    .typeConstructor(
      name: \"\(name)\",
      types: [
    \(types.map { XCTestDump(type: $0, indent: indent) }.joined(separator: ",\n").with(indent: indent))
      ],
      lifetimeSpecifiers: [\(lifetimeSpecifiers.map { "\"\($0)\"" }.joined(separator: ", "))],
      lifetimeSpecifier: \(XCTestDump(lifetime: lifetimeSpecifier)))
    """.with(indent: indent)
  case .unit:
    return ".unit".with(indent: indent)
  }
}

func XCTestDump(constructor: TCType.Constructor, indent: Int = 0) -> String {
  """
  .init(
    name: "\(constructor.name)",
    type: \(XCTestDump(type: constructor.type, indent: indent).with(indent: indent)))
  """
}
