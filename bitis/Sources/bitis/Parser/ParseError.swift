//
//  ParseError.swift
//  
//
//  Created by Toni K. Turk on 14/01/2022.
//

import Foundation
import SwiftParsec

public struct ParseError: Error, HasPosition {
  public let fullFilePath: String
  public let description: String
  public let position: Lexer.Position
  
  init(error: SwiftParsec.ParseError) {
    let description = String(error.description.split(separator: "\n")[1])
    // @FIXME: - An ad-hoc hack for a quick win!
    // @TODO: - In future, implement in a correct manner!
    self.fullFilePath = CURRENT_FILE_BEING_COMPILED
    self.description = description
    self.position = error.pos
  }
}

extension SwiftParsec.ParseError {
  var pos: Lexer.Position {
    .init(
      start: .init(line: position.line, column: position.column),
      end: .init(line: position.line, column: position.column + 1)
    )
  }
}
