//
// Created by Toni K. Turk on 29/10/2020.
//

import Foundation

public typealias AST = AbstractSyntaxTree

public struct AbstractSyntaxTree {
  public typealias Position = Lexer.Position
  public typealias Lifetime = DefaultLifetime
  
  /// @TODO: - Rename to `statements`.
  public let bindings: [Binding]
  
  public init(bindings: [Binding]) {
    self.bindings = bindings
  }
}

extension AST: Equatable, CustomStringConvertible {
  public var description: String {
    bindings
      .map { $0.description }
      .joined(separator: "\n")
  }
}

// MARK: - Bindings
public extension AST {
  /// @TODO: - Rename to `Statement`
  indirect enum Binding: CustomStringConvertible, Equatable, Hashable {
    case prototype( // <-- rename to `typeSignature` ...
      position: Position,
      name: String,
      type: AType,
      tcType: TCType? = nil)
    case binding(
      position: Position,
      name: String,
      cases: [Case],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil,
      borrowLifetime: AST.Lifetime? = nil)
    case data(
      position: Position,
      name: String,
      params: GenericParams,
      constructors: [Constructor],
      tcType: TCType? = nil)
    case print(
      position: Position,
      expression: AST.Expression,
      tcType: TCType? = nil)
    case include(module: String)

    public var description: String {
      switch self {
      case let .prototype(_, name, type, _):
        return "\(name) :: \(type.description)"
      case let .binding(_, _, cases, _, _, _):
        return "\(cases.map { $0.description }.joined(separator: " | "))"
      case let .data(_, name, params, constructors, _):
        return "\(name) [\(params.description)] \(constructors.map { $0.description })"
      case let .print(_, expression, _):
        return "print \(expression.description)"
      case let .include(module):
        return "import \(module)"
      }
    }

    public var position: Lexer.Position {
      switch self {
      case .prototype(let position, _, _, _),
           .binding(let position, _, _, _, _, _),
           .data(let position, _, _, _, _),
           .print(let position, _, _):
        return position
      case .include:
        fatalError()
      }
    }
    
    public var name: String {
      switch self {
      case .prototype(_, let name, _, _),
           .binding(_, let name, _, _, _, _),
           .data(_, let name, _, _, _):
        return name
      case .print:
        return "print"
      case .include:
        return "import"
      }
    }
    
    public var bindingExpressions: [AST.Expression] {
      switch self {
      case .binding(_, _, let cases, _, _, _):
        return cases.map { $0.body }
      case _:
        return []
      }
    }
    
    public var hashValue: Int {
      switch self {
      case let .binding(_, name, _, _, lifetime?, _):
        let fstHash = lifetime.hashValue
        let sndHash = name.hashValue
        return fstHash << MemoryLayout<LAAst.Lifetime>.size ^ sndHash // TODO: - Better hash combiner
      case _:
        return position.hashValue
      }
    }
  }
}

public extension AST.Binding {
  struct Case: CustomStringConvertible, Equatable, Hashable {
    public let name: String
    public let position: AST.Position
    public let parameters: [AST.Pattern]
    public let body: AST.Expression
    public let type: TCType?
    public let lifetime: LAAst.Lifetime?

    public var description: String {
      if parameters.isEmpty {
        return "\(name) = \(body.description)"
      }
      return "\(name) \(parameters.map { $0.description }) = \(body.description)"
    }
    
    public func withType(_ type: TCType) -> Self {
      .init(
        name: name,
        position: position,
        parameters: parameters,
        body: body,
        type: type,
        lifetime: lifetime
      )
    }
    
    public var hashValue: Int {
      let fstHash = lifetime?.hashValue ?? position.hashValue
      let sndHash = name.hashValue
      return fstHash << MemoryLayout<LAAst.Lifetime>.size ^ sndHash // TODO: - Better hash combiner
    }
  }

  struct Constructor: CustomStringConvertible, Equatable, Hashable {
    public let position: AST.Position
    public let name: String
    public let types: [AST.AType]
    public let type: TCType?
    public let lifetime: AST.Lifetime?

    public var description: String {
      "\(name) \(types.map { $0.description }.joined(separator: ","))"
    }
    
    public var hashValue: Int {
      let fstHash = lifetime!.hashValue
      let sndHash = name.hashValue
      return fstHash << MemoryLayout<LAAst.Lifetime>.size ^ sndHash // TODO: - Better hash combiner
    }
  }
  
  struct GenericParams: CustomStringConvertible, Equatable, Hashable {
    public let lifetimeParameters: [String]
    public let typeParameters: [String]
    
    public var description: String {
      "\(lifetimeParameters.joined(separator: ",")) \(typeParameters.joined(separator: ","))"
    }
  }
}

public extension AST.Binding.GenericParams {
  init() {
    self.lifetimeParameters = []
    self.typeParameters = []
  }
}

public extension AST.Binding.Case {
  init(
    name: String,
    position: AST.Position,
    parameters: [AST.Pattern],
    body: AST.Expression
  ) {
    self.name = name
    self.position = position
    self.parameters = parameters
    self.body = body
    self.type = nil
    self.lifetime = nil
  }
  
  init(
    name: String,
    position: AST.Position,
    parameters: [AST.Pattern],
    body: AST.Expression,
    type: TCType
  ) {
    self.name = name
    self.position = position
    self.parameters = parameters
    self.body = body
    self.type = type
    self.lifetime = nil
  }
}

public extension AST.Binding.Constructor {
  init(
    position: AST.Position,
    name: String,
    types: [AST.AType]
  ) {
    self.position = position
    self.name = name
    self.types = types
    self.type = nil
    self.lifetime = nil
  }
  
  init(
    position: AST.Position,
    name: String,
    types: [AST.AType],
    type: TCType
  ) {
    self.position = position
    self.name = name
    self.types = types
    self.type = type
    self.lifetime = nil
  }
}

// MARK: - Types
public extension AST {
  indirect enum AType: CustomStringConvertible, Equatable {
    case name(
      position: Position,
      name: String,
      lifetimeParameter: String? = nil, // TODO: - Should not be nil by default (for the sake of rapidly continuing with progress)
      tcType: TCType? = nil)
    case function(
      position: Position,
      arg: AType,
      ret: AType,
      lifetimeParameter: String? = nil,
      tcType: TCType? = nil)
    case tuple(
      position: Position,
      types: [AType],
      lifetimeParameter: String? = nil,
      tcType: TCType? = nil)
    case list(
      position: Position,
      element: AType,
      lifetimeParameter: String? = nil,
      tcType: TCType? = nil)
    case typeConstructor(
      position: Position,
      name: String,
      types: [AST.AType],
      lifetimeSpecifiers: [String],
      lifetimeParameter: String? = nil,
      tcType: TCType? = nil)
    case ref(
      position: Position,
      type: AType,
      lifetimeParameter: String? = nil,
      tcType: TCType? = nil)

    public var description: String {
      switch self {
      case let .name(_, name, lifetime, _):
        return "\(lifetime.map { $0 + " " } ?? "")\(name)"
      case let .function(_, arg, ret, lifetime, _):
        return "\(lifetime.map { $0 + " " } ?? "")(\(arg.description) -> \(ret.description))"
      case let .tuple(_, types, lifetime, _):
        return "\(lifetime.map { $0 + " " } ?? "")(\(types.map { $0.description }.joined(separator: ",")))"
      case let .list(_, element, lifetime, _):
        return "\(lifetime.map { $0 + " " } ?? "")[\(element.description)]"
      case let .typeConstructor(_, name, types, specifiers, lifetime, _):
        let types = types.map { $0.description }.joined(separator: " ")
        let genericList = specifiers.isEmpty
        ? types
        : "[\(specifiers.joined(separator: " ")) \(types)]"
        return "\(lifetime.map { $0 + " " } ?? "")\(name) \(genericList)"
      case let .ref(_, type, lifetime, _):
        return "&\(lifetime.map { $0 + " " } ?? "")\(type.description)"
      }
    }

    public var position: Position {
      switch self {
      case .name(let position, _, _, _),
           .function(let position, _, _, _, _),
           .tuple(let position, _, _, _),
           .list(let position, _, _, _),
           .typeConstructor(let position, _, _, _, _, _),
           .ref(let position, _, _, _):
        return position
      }
    }
    
    public var lifetimeParameter: String? {
      switch self {
      case .function(_, _, _, let lifetimeParameter, _),
          .list(_, _, let lifetimeParameter, _),
          .name(_, _, let lifetimeParameter, _),
          .ref(_, _, let lifetimeParameter, _),
          .tuple(_, _, let lifetimeParameter, _),
          .typeConstructor(_, _, _, _, let lifetimeParameter, _):
        return lifetimeParameter
      }
    }
  }
}

// MARK: - Patterns
public extension AST {
  indirect enum Pattern: CustomStringConvertible, Equatable, Hashable {
    case identifier(
      position: Position,
      identifier: String,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case literal(
      position: Position,
      literal: Lexer.Token.Literal,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case wildcard(
      position: Position,
      tcType: TCType? = nil)
    case tuple(
      position: Position,
      patterns: [Pattern],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case list(List)
    case ref(
      position: Position,
      pattern: Pattern,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case deconstruct(
      position: Position,
      constructor: String,
      patterns: [Pattern],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    
    public func withType(_ type: TCType) -> Pattern {
      switch self {
      case let .identifier(position, identifier, _, lifetime):
        return .identifier(position: position, identifier: identifier, tcType: type, lifetime: lifetime)
      case let .literal(position, literal, _, lifetime):
        return .literal(position: position, literal: literal, tcType: type, lifetime: lifetime)
      case let .tuple(position, patterns, _, lifetime):
        return .tuple(position: position, patterns: patterns, tcType: type, lifetime: lifetime)
      case let .list(.cons(position, head, tail, _, lifetime)):
        return .list(.cons(position: position, head: head, tail: tail, tcType: type, lifetime: lifetime))
      case let .list(.empty(position, _, lifetime)):
        return .list(.empty(position: position, tcType: type, lifetime: lifetime))
      case let .wildcard(position, _):
        return .wildcard(position: position, tcType: type)
      case let .ref(position, pattern, _, lifetime):
        return .ref(position: position, pattern: pattern, tcType: type, lifetime: lifetime)
      case let .deconstruct(position, constructor, patterns, _, lifetime):
        return .deconstruct(position: position, constructor: constructor, patterns: patterns, tcType: type, lifetime: lifetime)
      }
    }
    
    public func withLifetime(_ lifetime: AST.Lifetime) -> Pattern {
      switch self {
      case let .identifier(position, identifier, type, _):
        return .identifier(position: position, identifier: identifier, tcType: type, lifetime: lifetime)
      case let .literal(position, literal, type, _):
        return .literal(position: position, literal: literal, tcType: type, lifetime: lifetime)
      case let .tuple(position, patterns, type, _):
        return .tuple(position: position, patterns: patterns, tcType: type, lifetime: lifetime)
      case let .list(.cons(position, head, tail, type, _)):
        return .list(.cons(position: position, head: head, tail: tail, tcType: type, lifetime: lifetime))
      case let .list(.empty(position, type, _)):
        return .list(.empty(position: position, tcType: type, lifetime: lifetime))
      case let .wildcard(position, type):
        return .wildcard(position: position, tcType: type)
      case let .ref(position, pattern, type, _):
        return .ref(position: position, pattern: pattern, tcType: type, lifetime: lifetime)
      case let .deconstruct(position, constructor, patterns, type, _):
        return .deconstruct(position: position, constructor: constructor, patterns: patterns, tcType: type, lifetime: lifetime)
      }
    }
    
    public func withLifetime(_ lifetime: AST.Lifetime?) -> Pattern {
      guard let lifetime = lifetime else {
        return self
      }
      return withLifetime(lifetime)
    }

    public var description: String {
      switch self {
      case let .identifier(_, identifier, _, _):
        return "\(identifier)"
      case .wildcard:
        return "_"
      case let .tuple(_, patterns, _, _):
        return "(\(patterns.map { $0.description }.joined(separator: ",")))"
      case let .list(list):
        return list.description
      case let .literal(_, literal, _, _):
        return literal.description
      case let .ref(_, pattern, _, _):
        return "&\(pattern.description)"
      case let .deconstruct(_, constructor, patterns, _, _):
        return "$(\(constructor) \(patterns.map { $0.description }.joined(separator: " ")))"
      }
    }

    public var position: AST.Position {
      switch self {
      case .identifier(let position, _, _, _),
           .wildcard(let position, _),
           .tuple(let position, _, _, _),
           .literal(let position, _, _, _),
           .ref(let position, _, _, _),
           .deconstruct(let position, _, _, _, _):
        return position
      case let .list(list):
        return list.position
      }
    }

    var identifiers: Set<String> {
      switch self {
      case .tuple(_, let patterns, _, _),
           .deconstruct(_, _, let patterns, _, _):
        return patterns
          .map { $0.identifiers }
          .reduce(into: Set()) { $0.formUnion($1) }
      case .list(.cons(_, let head, let tail, _, _)):
        return head.identifiers.union(tail.identifiers)
      case .ref(_, let pattern, _, _):
        return pattern.identifiers
      case .identifier(_, let identifier, _, _):
        return [identifier]
      case .literal, .wildcard, .list(.empty):
        return []
      }
    }
    
    public var hashValue: Int {
      switch self {
      case let .identifier(_, identifier, _, lifetime):
        let fstHash = lifetime!.hashValue
        let sndHash = identifier.hashValue
        return fstHash << MemoryLayout<LAAst.Lifetime>.size ^ sndHash // TODO: - Better hash combiner
      case _:
        return lifetime?.hashValue ?? position.hashValue
      }
    }
    
    var isIdentifier: Bool {
      switch self {
      case .identifier:
        return true
      case _:
        return false
      }
    }
  }
}

// MARK: - List pattern
public extension AST.Pattern {
  enum List: CustomStringConvertible, Equatable {
    case empty(
      position: AST.Position,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case cons(
      position: AST.Position,
      head: AST.Pattern,
      tail: AST.Pattern,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)

    public var description: String {
      switch self {
      case .empty:
        return "[]"
      case let .cons(_, head, tail, _, _):
        return "\(head.description):\(tail.description)"
      }
    }

    public var position: AST.Position {
      switch self {
      case let .empty(position, _, _),
           let .cons(position, _, _, _, _):
        return position
      }
    }
  }
}

// MARK: - Expressions
public extension AST {
  indirect enum Expression: CustomStringConvertible, Equatable, Hashable {
    case binary(Binary)
    case literal(
      position: Position,
      literal: Lexer.Token.Literal,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case lambda(
      position: Position,
      parameter: Pattern,
      body: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case tuple(
      position: Position,
      expressions: [Expression],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case name(
      position: Position,
      identifier: String,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil,
      bindingLifetime: AST.Lifetime? = nil)
    case list(
      position: Position,
      expressions: [Expression],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case `let`(
      position: Position,
      bindings: [Binding],
      expression: Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case `case`(
      position: Position,
      expression: Expression,
      matches: [Match],
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case ref(
      position: Position,
      expression: Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case deref(
      position: Position,
      expression: Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)

    public var position: Position {
      switch self {
      case .binary(let binary):
        return binary.position
      case .literal(let position, _, _, _),
           .lambda(let position, _, _, _, _),
           .tuple(let position, _, _, _),
           .name(let position, _, _, _, _),
           .list(let position, _, _, _),
           .let(let position, _, _, _, _),
           .case(let position, _, _, _, _),
           .ref(let position, _, _, _),
           .deref(let position, _, _, _):
        return position
      }
    }

    public var description: String {
      switch self {
      case let .literal(_, literal, _, _):
        return "\(literal)"
      case .binary(let binary):
        return binary.description
      case let .lambda(_, parameter, body, _, _):
        return "λ\(parameter) -> \(body)"
      case let .tuple(_, expressions, _, _):
        return "(\(expressions.map { $0.description }.joined(separator: ",")))"
      case let .list(_, expressions, _, _):
        return "[\(expressions.map { $0.description }.joined(separator: ","))]"
      case let .name(_, identifier, _, _, _):
        return identifier
      case let .let(_, bindings, expression, _, _):
        return "let \(bindings.map { $0.description }.joined(separator: ",")) in \(expression.description)"
      case let .case(_, match, cases, _, _):
        return "case \(match) of \(cases.map { $0.description }.joined(separator: " | "))"
      case let .ref(_, expression, _, _):
        return "&\(expression.description)"
      case let .deref(_, expression, _, _):
        return "*\(expression.description)"
      }
    }
    
    public var lifetime: LAAst.Lifetime? {
      switch self {
      case let .name(_, _, _, lifetime?, _),
           let .let(_, _, _, _, lifetime?),
           let .case(_, _, _, _, lifetime?),
           let .deref(_, _, _, lifetime?),
           let .ref(_, _, _, lifetime?),
           let .list(_, _, _, lifetime?),
           let .literal(_, _, _, lifetime?),
           let .lambda(_, _, _, _, lifetime?),
           let .tuple(_, _, _, lifetime?):
        return lifetime
      case let .binary(expr):
        return expr.lifetime
      case _:
        return nil
      }
    }
    
    public func withLifetime(_ lifetime: AST.Lifetime?) -> Expression {
      switch self {
      case let .name(position, name, type, _, bindingLifetime):
        return .name(position: position, identifier: name, tcType: type, lifetime: lifetime, bindingLifetime: bindingLifetime)
      case let .let(position, bindings, expression, type, _):
        return .let(position: position, bindings: bindings, expression: expression, tcType: type, lifetime: lifetime)
      case let .case(position, expression, matches, type, _):
        return .case(position: position, expression: expression, matches: matches, tcType: type, lifetime: lifetime)
      case let .deref(position, expression, type, _):
        return .deref(position: position, expression: expression, tcType: type, lifetime: lifetime)
      case let .ref(position, expression, type, _):
        return .ref(position: position, expression: expression, tcType: type, lifetime: lifetime)
      case let .list(position, expressions, type, _):
//        switch lifetime.scope {
//        case .composite(let lifetimes):
//          return .list(position: position, expressions: zip(expressions, lifetimes).map { $0.0.withLifetime($0.1) }, tcType: type, lifetime: lifetime)
//        case _:
          return .list(position: position, expressions: expressions, tcType: type, lifetime: lifetime)
//        }
      case let .literal(position, literal, type, _):
        return .literal(position: position, literal: literal, tcType: type, lifetime: lifetime)
      case let .lambda(position, parameter, expression, type, _):
        return .lambda(position: position, parameter: parameter, body: expression, tcType: type, lifetime: lifetime)
      case let .tuple(position, expressions, type, _):
//        switch lifetime.scope {
//        case .composite(let lifetimes):
//          return .tuple(position: position, expressions: zip(expressions, lifetimes).map { $0.0.withLifetime($0.1) }, tcType: type, lifetime: lifetime)
//        case _:
          return .tuple(position: position, expressions: expressions, tcType: type, lifetime: lifetime)
//        }
      case let .binary(.arithmetic(position, op, left, right, type, _)):
//        switch lifetime.scope {
//        case .composite(let lifetimes):
//          return .binary(.arithmetic(position: position, operator: op, left: left.withLifetime(lifetimes[0]), right: right.withLifetime(lifetimes[1]), tcType: type, lifetime: lifetime))
//        case _:
          return .binary(.arithmetic(position: position, operator: op, left: left, right: right, tcType: type, lifetime: lifetime))
//        }
      /// TODO: - Recursively withLifetime
      case let .binary(.logical(position, op, left, right, type, _)):
        return .binary(.logical(position: position, operator: op, left: left, right: right, tcType: type, lifetime: lifetime))
      case let .binary(.ifThenElse(position, condition, tr, fl, type, _)):
        return .binary(.ifThenElse(position: position, condition: condition, true: tr, false: fl, tcType: type, lifetime: lifetime))
      case let .binary(.concat(position, left, right, type, _)):
        return .binary(.concat(position: position, left: left, right: right, tcType: type, lifetime: lifetime))
      case let .binary(.application(position, fn, arg, type, _)):
        return .binary(.application(position: position, fn: fn, arg: arg, tcType: type, lifetime: lifetime))
      }
    }
    
    public var hashValue: Int {
      let fstHash: Int
      let sndHash: Int
      switch self {
      case let .literal(_, literal, _, _):
        return literal.hashValue
      case let .name(_, name, _, _, lifetime):
        fstHash = lifetime!.hashValue
        sndHash = name.hashValue
      case _:
        fstHash = lifetime?.hashValue ?? 0
        sndHash = position.hashValue
      }
      return fstHash << MemoryLayout<LAAst.Lifetime>.size ^ sndHash // TODO: - Better hash combiner
    }
  }
}

// MARK: - Binary expression
public extension AST.Expression {
  enum Binary: CustomStringConvertible, Equatable {
    case arithmetic(
      position: AST.Position,
      operator: Lexer.Token.Operator.Arithmetic,
      left: AST.Expression,
      right: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case logical(
      position: AST.Position,
      operator: Lexer.Token.Operator.Logical,
      left: AST.Expression,
      right: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case ifThenElse(
      position: AST.Position,
      condition: AST.Expression,
      true: AST.Expression,
      false: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case concat(
      position: AST.Position,
      left: AST.Expression,
      right: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)
    case application(
      position: AST.Position,
      fn: AST.Expression,
      arg: AST.Expression,
      tcType: TCType? = nil,
      lifetime: AST.Lifetime? = nil)

    public var description: String {
      switch self {
      case let .ifThenElse(_, c, t, f, _, _):
        return "if \(c) then \(t) else \(f)"
      case let .arithmetic(_, op, l, r, _, _):
        return "(\(op) \(l) \(r))"
      case let .logical(_, op, l, r, _, _):
        return "\(l) \(op) \(r)"
      case let .concat(_, left, right, _, _):
        return "\(left) ++ \(right)"
      case let .application(_, fn, arg, _, _):
        switch (fn, arg) {
        case (.name, .name), (.literal, .literal):
          return "(\(fn) \(arg))"
        case (.name, _), (.literal, _):
          return "(\(fn) (\(arg)))"
        case (_, .name), (_, .literal):
          return "((\(fn)) \(arg))"
        case _:
          return "((\(fn)) (\(arg)))"
        }
      }
    }

    public var position: AST.Position {
      switch self {
      case .arithmetic(let position, _, _, _, _, _),
           .logical(let position, _, _, _, _, _),
           .ifThenElse(let position, _, _, _, _, _),
           .concat(let position, _, _, _, _),
           .application(let position, _, _, _, _):
        return position
      }
    }
    
    public var lifetime: LAAst.Lifetime? {
      switch self {
      case .arithmetic(_, _, _, _, _, let lifetime),
           .logical(_, _, _, _, _, let lifetime),
           .ifThenElse(_, _, _, _, _, let lifetime),
           .concat(_, _, _, _, let lifetime),
           .application(_, _, _, _, let lifetime):
        return lifetime
      }
    }
  }
}
// MARK: - Match
public extension AST.Expression {
  struct Match: Equatable, CustomStringConvertible {
    public let position: AST.Position
    public let pattern: AST.Pattern
    public let expression: AST.Expression

    public var description: String {
      "\(pattern.description) -> \(expression.description)"
    }
  }
}

public protocol HasPosition {
  var position: AST.Position { get }
}

extension AST.Expression: HasPosition {}
extension AST.Expression.Match: HasPosition {}
extension AST.Binding: HasPosition {}
extension AST.Binding.Case: HasPosition {}
extension AST.Pattern: HasPosition {}
extension AST.AType: HasPosition {}
extension Lexer.Token: HasPosition {}
extension AST.Binding.Constructor: HasPosition {}
