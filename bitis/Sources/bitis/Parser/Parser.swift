//
// Created by Toni K. Turk on 29/10/2020.
//

import Foundation
import SwiftParsec

public struct Parser {
  public typealias Parser<Output> = Lexer.Parser<Output>
  
  let lexer: Lexer

  public init(lexer: Lexer = .init()) {
    self.lexer = lexer
  }
}

// MARK: - Public
public extension Parser {
  func parse(sourceName: String, input: String) -> Either<ParseError, AST> {
    bitis
      .runSafe(userState: (), sourceName: sourceName, input: input)
      .mapLeft { ParseError(error: $0) }
  }
  
  func parseOrThrow(sourceName: String, input: String) throws -> AST {
    switch parse(sourceName: sourceName, input: input) {
    case .left(let error):
      throw error
    case .right(let ast):
      return ast
    }
  }
}

// MARK: - Internal
extension Parser {
  var bitis: Parser<AST> {
    statements.map(AST.init) <* StringParser.eof
  }
  
  var statements: Parser<[AST.Binding]> {
    lexer.newLine *> statement.dividedBy(lexer.newLine) <* lexer.newLine
  }
}

// MARK: - Bindings
public extension Parser {
  var statement: Parser<AST.Binding> {
    dataBinding <|> bindingOrPrototype <|> printStatement <|> importStatement
  }
  
  var bindingOrPrototype: Parser<AST.Binding> {
    func binding(ident i: Lexer.Token) -> Parser<AST.Binding> {
      func caseParser() -> Parser<AST.Binding.Case> {
        pattern.many >>- { p in
          lexer.assign *> expression >>- {
            let case_ = AST.Binding.Case(
              name: i.identifier,
              position: position(i, $0),
              parameters: p,
              body: $0)
            return .init(result: case_)
          }
        } <?> "'identifier'"
      }
      
      func casesParser() -> Parser<[AST.Binding.Case]> {
        caseParser() >>- { `case` in
          let otherCases = (lexer.newLine *> lexer.symbol(i.identifier + " ")).attempt *> casesParser() >>- {
            .init(result: [`case`] + $0)
          }
          if `case`.parameters.isEmpty {
            return .init(result: [`case`])
          }
          return otherCases <|> .init(result: [`case`])
        }
      }
      
      return casesParser()
        .map {
          .binding(
            position: position(i, $0.last!),
            name: i.identifier,
            cases: $0
          )
        }
    }
    
    func prototype(ident i: Lexer.Token) -> Parser<AST.Binding> {
      type.map {
        .prototype(
          position: position(i, $0),
          name: i.identifier,
          type: $0)
      } <?> "`type`"
    }
    
    return lexer.identifier >>- {
      (lexer.hasType *> prototype(ident: $0)) <|> binding(ident: $0)
    }
  }
  
  var dataBinding: Parser<AST.Binding> {
    func genericParamsParser() -> Parser<AST.Binding.GenericParams> {
      lexer.leftBracket >>- { l in
        (lifetimeSpecifier <|> .init(result: "*!!*")).separatedBy(lexer.comma) >>- { lp in
          lexer.identifier.separatedBy(lexer.comma) >>- { tp in
            lexer.rightBracket >>- { r in
              let params = AST.Binding.GenericParams(
                lifetimeParameters: lp.filter { $0 != "*!!*" }, // FIXME: This is a hack, try to avoid it ...
                typeParameters: tp.map { $0.identifier })
              return .init(result: params)
            }
          }
        }
      }
    }
    
    return lexer.data >>- { d in
      lexer.identifier >>- { name in
        genericParamsParser() <|> .init(result: .init()) >>- { gp in
          lexer.assign *> constructor.separatedBy1(lexer.symbol("|")) >>- { c in
            let binding = AST.Binding.data(
              position: position(d, c.last!),
              name: name.identifier,
              params: gp,
              constructors: c)
            return .init(result: binding)
          }
        }
      }
    }
  }
  
  var constructor: Parser<AST.Binding.Constructor> {
    lexer.identifier >>- { i in
      type.many >>- { t in
        let constr = AST.Binding.Constructor(
          position: position(i, t.last ?? i),
          name: i.identifier,
          types: t)
        return .init(result: constr)
      }
    }
  }
  
  var lifetimeSpecifier: Parser<String> {
    (lexer.symbol("'") >>- { s in
      lexer.uppercaseLetterSequence >>- { i in
        .init(result: "'" + i.identifier)
      }
    }) <* lexer.whiteSpace
  }
  
  var lifetimeSpecifierOrNil: Parser<String?> {
    lifetimeSpecifier.map(Optional.some) <|> .init(result: nil)
  }
  
  var printStatement: Parser<AST.Binding> {
    lexer.print >>- { print in
      expression >>- { expr in
        .init(result: .print(
          position: position(print, expr),
          expression: expr
        ))
      }
    }
  }
  
  var importStatement: Parser<AST.Binding> {
    lexer.include >>- { include in
      lexer.identifier >>- { module in
        .init(result: .include(
          module: module
        ))
      }
    }
  }
}

// MARK: - Expressions
public extension Parser {
  var expression: Parser<AST.Expression> {
    .recursive {
      let orParser = or(expression: $0)
      let lambdaParser = lambda(expression: $0)
      let ifParser = `if`(expression: $0)
      let letParser = `let`(expression: $0)
      let caseParser = `case`(expression: $0)
      let expressionParser = lambdaParser <|> caseParser <|> letParser <|> ifParser <|> orParser
      return expressionParser
    }
  }

  func `if`(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    lexer.if.attemptOr(lexer.newLine) >>- { t1 in
      expression.attemptOr(lexer.newLine) >>- { e1 in
        lexer.then.attemptOr(lexer.newLine) *> expression.attemptOr(lexer.newLine) >>- { e2 in
          lexer.else.attemptOr(lexer.newLine) *> expression.attemptOr(lexer.newLine) >>- { e3 in
            let expr = AST.Expression.Binary.ifThenElse(
              position: self.position(t1, e3),
              condition: e1,
              true: e2,
              false: e3)
            return .init(result: AST.Expression.binary(expr))
          }
        }
      }
    }
  }

  func lambda(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    lexer.lambda.attemptOr(lexer.newLine) >>- { l in
      self.pattern.attemptOr(lexer.newLine) >>- { par in
        self.lexer.rightArrow.attemptOr(lexer.newLine) *> expression.attemptOr(lexer.newLine) >>- { body in
          let expr = AST.Expression.lambda(
            position: position(l, body),
            parameter: par,
            body: body)
          return .init(result: expr)
        }
      }
    }
  }

  func `let`(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    let separator = lexer.newLine.map { _ in () }.attemptOr(lexer.comma.map { _ in () })
    let bindings = lexer.newLine *> statement.dividedBy(separator) <* lexer.newLine
    return lexer.let.attemptOr(lexer.newLine) >>- { l in
      bindings.attemptOr(lexer.newLine) >>- { b in
        lexer.in.attemptOr(lexer.newLine) *> expression >>- { e in
          let expr = AST.Expression.let(
            position: position(l, e),
            bindings: b,
            expression: e)
          return .init(result: expr)
        }
      }
    }
  }

  func `case`(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    func parseMatch(expression: Parser<AST.Expression>) -> Parser<AST.Expression.Match> {
      pattern >>- { p in
        lexer.rightArrow *> expression >>- { e in
          let case_ = AST.Expression.Match(
            position: position(p, e),
            pattern: p,
            expression: e)
          return .init(result: case_)
        }
      }
    }

    return lexer.case.attemptOr(lexer.newLine) >>- { c in
      expression.attemptOr(lexer.newLine) >>- { e in
        lexer.of.attemptOr(lexer.newLine) *> parseMatch(expression: expression).attemptOr(lexer.newLine).separatedBy1(lexer.symbol("|")) >>- { cases in
          let expr = AST.Expression.case(
            position: position(c, cases.last!),
            expression: e,
            matches: cases)
          return .init(result: expr)
        }
      }
    }
  }

  func or(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    let lookAhead =
      (lexer.leftParent
        <|> lexer.leftBracket
        <|> lexer.numericLiteral
        <|> lexer.logicalLiteral
        <|> lexer.stringLiteral
        <|> lexer.let
        <|> lexer.identifier
        <|> lexer.mul
        <|> lexer.ref
      ).lookAhead
    return lookAhead *> and(expression: expression) >>- {
      or_(expression: expression, parsed: $0)
    }
  }

  func or_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let orParser: Parser<AST.Expression> = lexer.or >>- { op in
      expression >>- { e in
        let expr = AST.Expression.Binary.logical(
          position: position(parsed, e),
          operator: op.logicalOperator,
          left: parsed,
          right: e)
        return .init(result: .binary(expr))
      }
    }
    return (lexer.newLine.lookAhead.noOccurence *> orParser) <|> .init(result: parsed)
  }

  func and(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    cmp(expression: expression) >>- {
      and_(expression: expression, parsed: $0)
    }
  }

  func and_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let andParser: Parser<AST.Expression> = lexer.and >>- { op in
      expression >>- { e in
        let expr = AST.Expression.Binary.logical(
          position: position(parsed, e),
          operator: op.logicalOperator,
          left: parsed,
          right: e)
        return .init(result: AST.Expression.binary(expr))
      }
    }
    return (lexer.newLine.lookAhead.noOccurence *> andParser) <|> .init(result: parsed)
  }

  func cmp(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    add(expression: expression) >>- { e in
      cmp_(expression: expression, parsed: e)
    }
  }

  func cmp_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let cmpCombinator =
      lexer.equal
      <|> lexer.notEqual
      <|> lexer.lowerThan
      <|> lexer.greaterThan
      <|> lexer.lowerOrEqualThan
      <|> lexer.greaterOrEqualThan
    let cmpParser: Parser<AST.Expression> = cmpCombinator >>- { op in
      expression >>- { e in
        let expr = AST.Expression.Binary.logical(
          position: position(parsed, e),
          operator: op.logicalOperator,
          left: parsed,
          right: e)
        return .init(result: AST.Expression.binary(expr))
      }
    }
    return (lexer.newLine.lookAhead.noOccurence *> cmpParser) <|> .init(result: parsed)
  }

  func add(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    mul(expression: expression) >>- { e in
      add_(expression: expression, parsed: e)
    }
  }

  func add_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let addOrSub: Parser<AST.Expression> = lexer.plus <|> lexer.minus >>- { op in
      expression >>- { e in
        let expr = AST.Expression.Binary.arithmetic(
          position: position(parsed, e),
          operator: op.arithmeticOperator,
          left: parsed,
          right: e)
        return .init(result: AST.Expression.binary(expr))
      }
    }
    return (lexer.newLine.lookAhead.noOccurence *> addOrSub) <|> .init(result: parsed)
  }

  func mul(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    prefix(expression: expression) >>- { e in
      mul_(expression: expression, parsed: e)
    }
  }

  func mul_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let mulOrDiv: Parser<AST.Expression> = lexer.mul <|> lexer.div >>- { op in
      expression >>- { e in
        let expr = AST.Expression.Binary.arithmetic(
          position: position(parsed, e),
          operator: op.arithmeticOperator,
          left: parsed,
          right: e)
        return .init(result: AST.Expression.binary(expr))
      }
    }
    return (lexer.newLine.lookAhead.noOccurence *> mulOrDiv) <|> .init(result: parsed)
  }

  func prefix(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    func reference(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
      lexer.ref >>- { _ in
        expression >>- {
          let ref = AST.Expression.ref(
            position: $0.position,
            expression: $0)
          return .init(result: ref)
        }
      }
    }
    
    func dereference(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
      lexer.mul >>- { _ in
        expression >>- {
          let ref = AST.Expression.deref(
            position: $0.position,
            expression: $0)
          return .init(result: ref)
        }
      }
    }
    
    return .recursive {
      reference(expression: $0)
        <|> dereference(expression: $0)
        <|> postfix(expression: expression)
    }
  }

//  func prefix_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
//    .init(result: parsed)
//  }

  func postfix(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    concat(expression: expression) /*>>- { e in
      postfix_(expression: expression, parsed: e)
    }*/
  }

//  func postfix_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
//    .init(result: parsed)
//  }

  // TODO: - concat binds too strong
  func concat(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    application(expression: expression) >>- { e in
      concat_(expression: expression, parsed: e)
    }
  }
  
  func concat_(expression: Parser<AST.Expression>, parsed: AST.Expression) -> Parser<AST.Expression> {
    let concat: Parser<AST.Expression> = lexer.concat.lookAhead *> expression >>- { e in
      let expr = AST.Expression.Binary.concat(
        position: position(parsed, e),
        left: parsed,
        right: e)
      return .init(result: .binary(expr))
    }
    return (lexer.newLine.lookAhead.noOccurence *> concat) <|> .init(result: parsed)
  }
  
  func application(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    let op: Parser<(AST.Expression, AST.Expression) -> AST.Expression> = Parser { e1, e2 in
      let expr = AST.Expression.Binary.application(
        position: position(e1, e2),
        fn: e1,
        arg: e2)
      return .binary(expr)
    }
    return (lexer.newLine.lookAhead.noOccurence *> atom(expression: expression) <* lexer.newLine.lookAhead.noOccurence).chainLeft1(op)
  }

  func atom(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    tuple(expression: expression)
      <|> list(expression: expression)
      <|> literal
      <|> lexer.identifier.map { .name(position: $0.position, identifier: $0.identifier) }
  }

  func tuple(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    items( // @TODO: - Multi-line tuple expression.
      repeating: lexer.parentheses(expression.separatedBy1(lexer.symbol(","))),
      emptyParser: lexer.emptyTuple,
      constructor: { AST.Expression.tuple(position: $0, expressions: $1) }
    ).map {
      switch $0 {
      case .tuple(_, let expressions, _, _) where expressions.count == 1:
        return expressions[0]
      case let expr:
        return expr
      }
    }
  }

  func list(expression: Parser<AST.Expression>) -> Parser<AST.Expression> {
    items(
      repeating: lexer.brackets(expression.separatedBy1(lexer.symbol(","))),
      emptyParser: lexer.emptyList,
      constructor: { AST.Expression.list(position: $0, expressions: $1) }
    )
  }
  
  var literal: Parser<AST.Expression> {
    (lexer.numericLiteral
      <|> lexer.logicalLiteral
      <|> lexer.stringLiteral
      <|> lexer.characterLiteral
    ).map {
      .literal(
        position: $0.position,
        literal: $0.literal)
    }
  }
}

// MARK: - Patterns
extension Parser {
  var pattern: Parser<AST.Pattern> {
    .recursive { atom(pattern: $0) }
  }

  func atom(pattern: Parser<AST.Pattern>) -> Parser<AST.Pattern> {
    let allPatterns =
      reference(pattern: pattern)
      <|> tuple(pattern: pattern)
      <|> deconstruct(pattern: pattern)
      <|> literalPattern
      <|> lexer.identifier.map { .identifier(position: $0.position, identifier: $0.identifier) }
      <|> lexer.wildcard.map { .wildcard(position: $0.position) }
      <|> lexer.emptyList.map { .list(.empty(position: $0.position)) }
    
    return allPatterns >>- {
      atom_(pattern: pattern, parsed: $0)
    }
  }

  func atom_(pattern: Parser<AST.Pattern>, parsed: AST.Pattern) -> Parser<AST.Pattern> {
    let list: Parser<AST.Pattern> = lexer.colon >>- { c in
      pattern >>- { p in
        let list = AST.Pattern.list(.cons(
          position: position(parsed, p),
          head: parsed,
          tail: p))
        return .init(result: list)
      }
    }
    return list <|> .init(result: parsed)
  }
  
  func deconstruct(pattern: Parser<AST.Pattern>) -> Parser<AST.Pattern> {
    lexer.doll >>- { _ in
      lexer.leftParent >>- { _ in
        lexer.identifier >>- { identifier in
          pattern.many >>- { patterns in
            lexer.rightParent >>- { _ in
              switch patterns.isEmpty {
              case true:
                return .init(result: .deconstruct(
                  position: identifier.position,
                  constructor: identifier.identifier,
                  patterns: []))
              case false:
                return .init(result: .deconstruct(
                  position: position(patterns.first!, patterns.last!),
                  constructor: identifier.identifier,
                  patterns: patterns))
              }
            }
          }
        }
      }
    }
  }

  var literalPattern: Parser<AST.Pattern> {
    lexer.anyLiteral
      .map {
        .literal(
          position: $0.position,
          literal: $0.literal)
      }
  }

  func tuple(pattern: Parser<AST.Pattern>) -> Parser<AST.Pattern> {
    items(
      repeating: lexer.parentheses(pattern.separatedBy1(lexer.symbol(","))),
      emptyParser: lexer.emptyTuple,
      constructor: { AST.Pattern.tuple(position: $0, patterns: $1) }
    ).map {
      switch $0 {
      case .tuple(_, let patterns, _, _) where patterns.count == 1:
        return patterns[0]
      case let expr:
        return expr
      }
    }
  }
  
  func reference(pattern: Parser<AST.Pattern>) -> Parser<AST.Pattern> {
    lexer.ref >>- { ref in
      pattern >>- {
        let ref = AST.Pattern.ref(
          position: position(ref, $0),
          pattern: $0)
        return .init(result: ref)
      }
    }
  }
}

// MARK: - Types
extension Parser {
  var type: Parser<AST.AType> {
    .recursive { reference(type: $0) <|> atom(type: $0) }
  }
  
  func reference(type: Parser<AST.AType>) -> Parser<AST.AType> {
    lexer.ref >>- { ref in
      lifetimeSpecifierOrNil >>- { lifetime in
        reference(type: type) <|> atom(type: type, parseTail: false) >>- { type in
          let refType = AST.AType.ref(
            position: type.position,
            type: type,
            lifetimeParameter: lifetime)
          return .init(result: refType)
        }
      }
    } >>- { atomType_(type: type, parsed: $0) }
  }

  func atom(type: Parser<AST.AType>, parseTail: Bool = true) -> Parser<AST.AType> {
    lifetimeSpecifierOrNil >>- { lifetime in
      let atom =
      tuple(type: type, lifetime: lifetime)
      <|> list(type: type, lifetime: lifetime)
      <|> name(type: type, lifetime: lifetime)
      <|> typeConstructor(type: type, lifetime: lifetime)
      
      return atom >>- {
        parseTail
          ? atomType_(type: type, parsed: $0)
          : .init(result: $0)
      }
    }
  }
  
  func name(type: Parser<AST.AType>, lifetime: String?) -> Parser<AST.AType> {
    lexer.identifier.map { .name(position: $0.position, name: $0.identifier, lifetimeParameter: lifetime) }
  }

  func atomType_(type: Parser<AST.AType>, parsed: AST.AType) -> Parser<AST.AType> {
    let fn: Parser<AST.AType> = lexer.rightArrow *> type >>- { ret in
      let functionType = AST.AType.function(
        position: position(parsed, ret),
        arg: parsed,
        ret: ret,
        lifetimeParameter: nil) // TODO: -
      return .init(result: functionType)
    }
    return (lexer.newLine.lookAhead.noOccurence *> fn) <|> .init(result: parsed)
  }

  func tuple(type: Parser<AST.AType>, lifetime: String?) -> Parser<AST.AType> {
    items(
      repeating: lexer.parentheses(type.separatedBy1(lexer.symbol(","))),
      emptyParser: lexer.emptyTuple,
      extractPosition: { $0.position },
      constructor: { AST.AType.tuple(position: $0, types: $1, lifetimeParameter: lifetime) }
    ).map {
      switch $0 {
      case let .tuple(_, types, _, _) where types.count == 1:
        return types[0]
      case let type:
        return type
      }
    }.attempt
  }

  func list(type: Parser<AST.AType>, lifetime: String?) -> Parser<AST.AType> {
    lexer
      .brackets(type)
      .map { .list(position: $0.position, element: $0, lifetimeParameter: lifetime) }
  }

  func typeConstructor(type: Parser<AST.AType>, lifetime: String?) -> Parser<AST.AType> {
    func genericParamsParser() -> Parser<([AST.AType], [String], AST.Position)> {
      (lexer.leftBracket >>- { l in
        (lifetimeSpecifier <|> .init(result: "*!!*")).separatedBy(lexer.comma) >>- { lp in
          type.separatedBy(lexer.comma) >>- { types in
            lexer.rightBracket >>- { r in
              .init(result: (types, lp, r.position))
            }
          }
        }
      }).map { ($0, $1.filter { $0 != "*!!*" }, $2) } // FIXME: - A hack, try to fix it
    }
    func parser(lifetime: String?) -> Parser<AST.AType> {
      lexer.identifier >>- { identifier in
        genericParamsParser() <|> (type.many1.map { ($0, [], $0.last?.position ?? identifier.position) }) >>- { genericParams in
          let typeConstr = AST.AType.typeConstructor(
            position: .init(start: identifier.position.start, end: genericParams.2.end),
            name: identifier.identifier,
            types: genericParams.0,
            lifetimeSpecifiers: genericParams.1,
            lifetimeParameter: lifetime)
          return .init(result: typeConstr)
        }
      }
    }
    return lexer.parentheses(parser(lifetime: lifetime))
  }
}

// MARK: - Private
private extension Parser {
  private func items<T, U>(
    repeating: Parser<[T]>,
    emptyParser: Lexer.TokenParser,
    extractPosition: @escaping (T) -> AST.Position,
    constructor: @escaping (AST.Position, [T]) -> U
  ) -> Parser<U> {
    let empty = { _ in [T]() } <^> emptyParser
    return .sourcePosition >>- { p in
      (empty <|> repeating).map { expressions -> U in
        if expressions.isEmpty {
          return constructor(
            .init(
              start: .init(line: p.line, column: p.column),
              end: .init(line: p.line, column: p.column + 1)),
            expressions)
        }
        let p2 = extractPosition(expressions.last!)
        return constructor(
          .init(
            start: .init(line: p.line, column: p.column),
            end: .init(line: p2.end.line, column: p2.end.column + 1)),
          expressions)
      }
    }
  }

  private func items(
    repeating: Parser<[AST.Expression]>,
    emptyParser: Lexer.TokenParser,
    constructor: @escaping (AST.Position, [AST.Expression]) -> AST.Expression
  ) -> Parser<AST.Expression> {
    items(
      repeating: repeating,
      emptyParser: emptyParser,
      extractPosition: { $0.position },
      constructor: constructor)
  }

  private func items<U>(
    repeating: Parser<[AST.Binding]>,
    emptyParser: Lexer.TokenParser,
    constructor: @escaping (AST.Position, [AST.Binding]) -> U
  ) -> Parser<U> {
    items(
      repeating: repeating,
      emptyParser: emptyParser,
      extractPosition: { $0.position },
      constructor: constructor)
  }

  private func items<U>(
    repeating: Parser<[AST.Pattern]>,
    emptyParser: Lexer.TokenParser,
    constructor: @escaping (AST.Position, [AST.Pattern]) -> U
  ) -> Parser<U> {
    items(
      repeating: repeating,
      emptyParser: emptyParser,
      extractPosition: { $0.position },
      constructor: constructor)
  }
}

// MARK: - Helpers
extension Parser {
  static func position(_ a: HasPosition, _ b: HasPosition) -> AST.Position {
    .init(start: a.position.start, end: b.position.end)
  }
  
  func position(_ a: HasPosition, _ b: HasPosition) -> AST.Position {
    Self.position(a, b)
  }
}

extension Lexer.Token {
  var literal: Lexer.Token.Literal {
    switch self {
    case .literal(let literal, _):
      return literal
    case _:
      fatalError("Lexer.Token: not a literal!")
    }
  }

  var identifier: String {
    switch self {
    case .identifier(let identifier, _):
      return identifier
    case _:
      fatalError("Lexer.Token: not an identifier!")
    }
  }

  var arithmeticOperator: Lexer.Token.Operator.Arithmetic {
    switch self {
    case .operator(.arithmetic(.plus), _):
      return .plus
    case .operator(.arithmetic(.minus), _):
      return .minus
    case .operator(.arithmetic(.mul), _):
      return .mul
    case .operator(.arithmetic(.div), _):
      return .div
    case _:
      fatalError("Lexer.Token: not an arithmetic operator!")
    }
  }

  var logicalOperator: Lexer.Token.Operator.Logical {
    switch self {
    case .operator(.logical(.equal), _):
      return .equal
    case .operator(.logical(.notEqual), _):
      return .notEqual
    case .operator(.logical(.lowerThan), _):
      return .lowerThan
    case .operator(.logical(.lowerOrEqualThan), _):
      return .lowerOrEqualThan
    case .operator(.logical(.greaterThan), _):
      return .greaterThan
    case .operator(.logical(.greaterOrEqualThan), _):
      return .greaterOrEqualThan
    case .operator(.logical(.and), _):
      return .and
    case .operator(.logical(.or), _):
      return .or
    case _:
      fatalError("Lexer.Token: not an logical operator!")
    }
  }
}

extension GenericParser {
  func attemptOr<T>(_ parser: GenericParser<StreamType, UserState, T>) -> GenericParser {
    (self <* parser).attempt <|> self
  }
}

extension SwiftParsec.Either {
  func mapLeft<A>(_ f: (L) -> A) -> SwiftParsec.Either<A, R> {
    switch self {
    case .left(let l):
      return .left(f(l))
    case .right(let r):
      return .right(r)
    }
  }
}
