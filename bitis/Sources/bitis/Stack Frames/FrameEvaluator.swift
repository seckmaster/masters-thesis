//
//  FrameEvaluator.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation
import Bow
import SwiftParsec

// TODO: - !!! move into designated env !!!
var bindingNameStack: [String] = []

extension ARAst: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    bindings.frameEvaluator()
  }
}

extension ARAst.Binding: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    switch self {
    case .data(_, _, _, let constructors, _):
      return constructors.map {
        ARState.ActionsAME.bind(
          access: .init(
            definitionDescription: $0.description,
            kind: .global(.init(name: $0.name))),
          for: .constructor($0))
      }
      .sequence()
      .map { _ in () }^
    case .prototype:
      return .pure(())^
    case let .binding(_, name, cases, _, _, _):
      guard cases.count == 1 else {
        fatalError("Perform case lifting!")
      }
      guard cases[0].parameters.isEmpty else {
        fatalError("Perform case lifting!")
      }
      return ARState.ActionsFBE.addLocalVariable(name: name) >>- {
        bindingNameStack.append(name)
        return cases.frameEvaluator() >>- {
          _ = bindingNameStack.popLast()
          return ARState.ActionsFBE.access(
            for: name,
            description: description
          ) >>- {
            ARState.ActionsAME.bind(access: $0, for: .binding(self))
          }
        }
      }
    case let .print(_, expression, _):
      return expression.frameEvaluator()
    case .include:
      return .pure(())^
    }
  }
}

extension ARAst.Binding.Case: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    parameters
      .map { $0.frameEvaluator() }
      .sequence()^
    *> body.frameEvaluator()
  }
}

extension ARAst.Expression: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    let children: [ARAnalysable]
    
    switch self {
    case .literal:
      children = []
    case .name:
      children = []
    case let .let(_, bindings, expression, _, _):
      children = bindings + [expression]
    case let .case(_, expression, matches, _, _):
      children = matches + [expression]
    case let .ref(_, expression, _, _):
      children = [expression]
    case let .deref(_, expression, _, _):
      children = [expression]
    case let .tuple(_, expressions, _, _):
      children = expressions
    case let .list(_, expressions, _, _):
      children = expressions
    case let .binary(.application(_, fn, arg, _, _)):
      children = [fn, arg]
    case let .binary(.arithmetic(_, _, left, right, _, _)):
      children = [left, right]
    case let .binary(.concat(_, left, right, _, _)):
      children = [left, right]
    case let .binary(.ifThenElse(_, condition, `true`, `false`, _, _)):
      children = [condition, `true`, `false`]
    case let .binary(.logical(_, _, left, right, _, _)):
      children = [left, right]
    case let .lambda(_, parameter, body, _, _):
      return (ARState.Actions
        .inNewScope {
          ARState.ActionsFBE.initialize(
            description: description + "~[fn]",
            parameterCount: 3 // SL, thunk, lambda parameter @TODO: - Only two actually needed!
          ) *> parameter.frameEvaluator(isParameter: true) *> body.frameEvaluator()
        } >>- { _, frame in
          ARState.ActionsFME.bind(
            frame: frame,
            for: .expression(self)
          )
        }) >>- {
          ARState.Actions.inNewScope {
            ARState.ActionsFBE.initialize(
              description: description + "~[thunk]",
              parameterCount: 2
            )
          } >>- { _, frame in
            ARState.ActionsFME.bind(
              frame: frame,
              for: .pattern(parameter)
            )
          }
        }
    }
    return evaluateFrames(analysables: children)
  }
}

extension AST.Expression {
  func evaluateFrames(
    analysables: [ARAnalysable]
  ) -> ActivationRecordsM<()> {
    ARState.Actions
      .inNewScope {
        ARState.ActionsFBE.initialize(
          description: self.description
        ) >>- {
          analysables.map { $0.frameEvaluator() }.sequence().map { _ in () }^
        }
      } >>- { _, frame in
        ARState.ActionsFME.bind(
          frame: frame,
          for: .expression(self)
        )
      }
  }
}

extension ARAst.Expression.Match: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    pattern.frameEvaluator() *> expression.frameEvaluator()
  }
}

// MARK: - Patterns
extension LAAst.Pattern/*: ARAnalysable*/ {
  public func frameEvaluator(isParameter: Bool = false) -> ActivationRecordsM<()>  {
    ARState.ActionsFBE.peek >>- { scope in
      switch self {
      case let .identifier(_, identifier, _, _):
        switch isParameter {
        case true:
          let access = Access(
            definitionDescription: description,
            kind: .parameter(
              staticLevel: scope.staticLevel!,
              offset: wordSize*2))
          return ARState.ActionsAME.bind(
            access: access,
            for: .pattern(self)
          )
        case false:
          return ARState.ActionsFBE.addLocalVariable(
            name: identifier
          ) *> ARState.ActionsFBE.access(
            for: identifier,
            description: description
          ) >>- { access in
            ARState.ActionsAME.bind(access: access, for: .pattern(self))
          }
        }
      case .list:
        fatalError()
      case .list(.empty):
        fatalError()
      case let .tuple(_, patterns, _, _):
        return patterns
          .map { $0.frameEvaluator() }
          .sequence()
          .map { _ in () }^
      case let .deconstruct(_, _, patterns, _, _):
        return patterns
          .map { $0.frameEvaluator() }
          .sequence()
          .map { _ in () }^
      case let .ref(_, pattern, _, _):
        return pattern.frameEvaluator(isParameter: isParameter)
      case .literal, .wildcard:
        return .pure(())^
      }
    }
  }
}

extension Collection where Element: ARAnalysable {
  public func frameEvaluator() -> ActivationRecordsM<()> {
    map { $0.frameEvaluator() }
      .sequence()
      .map { _ in () }^
  }
}
