//
//  FrameBuilderEnv.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct FrameBuilderEnv: HasScope {
  typealias Scope = Builder
  
  var scopes: [Scope]
  
  init() {
    var staticScope = Scope()
    staticScope.staticLevel = 0
    scopes = [staticScope]
  }
  
  init(scopes: [Scope]) {
    self.scopes = scopes
  }
  
  public func pushScope() -> FrameBuilderEnv {
    var scope = Builder()
    scope.staticLevel = scopes.last!.staticLevel.map { $0 + 1 }
    return .init(scopes: scopes.appending(scope))
  }
  
  public func popScope() -> FrameBuilderEnv {
    .init(scopes: scopes.dropLast())
  }
}

extension FrameBuilderEnv {
  struct Builder {
    var description: String?
    var staticLevel: UInt?
    var entry: FrameLabel?
    var end: FrameLabel?
    var localVariables: [Frame.Access] = []
    var framePointer: FrameTemp?
    var returnValue: FrameTemp?
    var parameterCount: UInt?
  }
}

extension FrameBuilderEnv {
  mutating func setDescription(_ description: String) {
    scopes[scopes.count - 1].description = description
  }
  
//  mutating func setStaticLevel(_ level: UInt) {
//    scopes[scopes.count - 1].staticLevel = level
//  }
  
  mutating func addLocalVariable(name: String) {
    scopes[scopes.count - 1].addLocalVariable(name: name)
  }
  
  mutating func setEntryLabel(_ label: FrameLabel) {
    scopes[scopes.count - 1].entry = label
  }
  
  mutating func setEndLabel(_ label: FrameLabel) {
    scopes[scopes.count - 1].end = label
  }
  
  mutating func setFramePointer(_ ptr: FrameTemp) {
    scopes[scopes.count - 1].framePointer = ptr
  }
  
  mutating func setReturnValue(_ value: FrameTemp) {
    scopes[scopes.count - 1].returnValue = value
  }
  
  mutating func setParameterCount(_ value: UInt?) {
    scopes[scopes.count - 1].parameterCount = value
  }
  
  func build() -> Frame {
    scopes.last!.build
  }
}

extension FrameBuilderEnv.Builder {
  mutating func addLocalVariable(name: String) {
    let offset = (localVariables.count + 1) * Int(wordSize)
    localVariables.append(.init(
      bindingName: name,
      offset: -offset)
    )
  }
}

extension FrameBuilderEnv.Builder {
  var build: Frame {
    if let parameterCount = parameterCount {
      return .init(
        description: description!,
        staticLevel: staticLevel!,
        entry: entry!,
        end: end!,
        localVariables: localVariables,
        framePointer: framePointer!,
        returnValue: returnValue!,
        parameterCount: parameterCount
      )
    } else {
      return .init(
        description: description!,
        staticLevel: staticLevel!,
        entry: entry!,
        end: end!,
        localVariables: localVariables,
        framePointer: framePointer!,
        returnValue: returnValue!
      )
    }
  }
}
