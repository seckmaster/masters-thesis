//
//  ARState.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation
import Bow
import SwiftParsec

public typealias FrameMappingEnv = ASTMap<Frame>
public typealias AccessMappingEnv = ASTMap<Access>

public struct ARState: HasScope {
  public var frameMappingEnv: FrameMappingEnv
  public var accessMappingEnv: AccessMappingEnv
  public var frameBuilderEnv: FrameBuilderEnv
  public var labelStreamEnv: LabelStreamEnv
  public var tempStreamEnv: TempStreamEnv
  
  public static var empty: ARState {
    .init(
      frameMappingEnv: .init(),
      accessMappingEnv: .init(),
      frameBuilderEnv: .init(),
      labelStreamEnv: .init(anonymousLabelCount: 0),
      tempStreamEnv: .init(count: 0)
    )
  }
  
  public func pushScope() -> ARState {
    .init(
      frameMappingEnv: frameMappingEnv,
      accessMappingEnv: accessMappingEnv,
      frameBuilderEnv: frameBuilderEnv.pushScope(),
      labelStreamEnv: labelStreamEnv,
      tempStreamEnv: tempStreamEnv
    )
  }
  
  public func popScope() -> ARState {
    .init(
      frameMappingEnv: frameMappingEnv,
      accessMappingEnv: accessMappingEnv,
      frameBuilderEnv: frameBuilderEnv.popScope(),
      labelStreamEnv: labelStreamEnv,
      tempStreamEnv: tempStreamEnv
    )
  }
}

public extension ARState {
  enum Actions {
    static func inNewScope<A>(f: () -> ActivationRecordsM<A>) -> ActivationRecordsM<(A, Frame)> {
      let push: ActivationRecordsM<()> = .init { env -> (ARState, ()) in
        (env.pushScope(), ())
      }
      let pop: ActivationRecordsM<()> = .init { env -> (ARState, ()) in
        (env.popScope(), ())
      }
      let doWork = f().withState().map { ($0.0, $0.1.frameBuilderEnv.build()) }^
      return push *> doWork <* pop
    }
  }
}

public extension ARState {
  enum ActionsFME {
    static func bind(frame: Frame, for node: FrameMappingEnv.Node) -> ActivationRecordsM<()> {
      .init { env -> (ARState, ()) in
        var env = env
        if env.frameMappingEnv.scope[node] != nil {
          warning("ARState.ActionsAME.bind(frame:for): environment already contains a node with the same hash!")
        }
        env.frameMappingEnv.scope[node] = frame
        return (env, ())
      }
    }
  }
}

public extension ARState {
  enum ActionsAME {
    static func bind(access: Access, for node: AccessMappingEnv.Node) -> ActivationRecordsM<()> {
      .init { env -> (ARState, ()) in
        var env = env
        if env.accessMappingEnv.scope[node] != nil {
          warning("ARState.ActionsAME.bind(access:for): environment already contains a node with the same hash!")
        }
        env.accessMappingEnv.scope[node] = access
        return (env, ())
      }
    }
  }
}

public extension ARState {
  enum ActionsFBE {
    static func increaseStaticLevel<T>(do work: () -> ActivationRecordsM<T>) -> ActivationRecordsM<T> {
      let incr: ActivationRecordsM<()> = .init { env -> (ARState, ()) in
        var env = env
        env.frameBuilderEnv.scopes[env.frameBuilderEnv.scopes.count - 1].staticLevel! += 1
        return (env, ())
      }
      let decr: ActivationRecordsM<()> = .init { env -> (ARState, ()) in
        var env = env
        env.frameBuilderEnv.scopes[env.frameBuilderEnv.scopes.count - 1].staticLevel! -= 1
        return (env, ())
      }
      return incr *> work() <* decr
    }
        
    static func addLocalVariable(name: String, size: Byte = wordSize) -> ActivationRecordsM<()> {
      .init { env -> (ARState, ()) in
        var env = env
        env.frameBuilderEnv.addLocalVariable(name: name)
        return (env, ())
      }
    }
    
    static func initialize(
      description: String,
      parameterCount: UInt? = nil
    ) -> ActivationRecordsM<()> {
      ActionsLSE.anonymous >>- { entryLabel in
        ActionsLSE.anonymous >>- { endLabel in
          ActionsTSE.temps(count: 2) >>- { temps in
            .init { env -> (ARState, ()) in
              var env = env
              env.frameBuilderEnv.setEntryLabel(entryLabel)
              env.frameBuilderEnv.setEndLabel(endLabel)
              env.frameBuilderEnv.setDescription(description)
              env.frameBuilderEnv.setFramePointer(temps[0])
              env.frameBuilderEnv.setReturnValue(temps[1])
              env.frameBuilderEnv.setParameterCount(parameterCount)
              return (env, ())
            }
          }
        }
      }
    }
    
    static func access(
      for name: String,
      description: String,
      on previousFrame: () = ()
    ) -> ActivationRecordsM<Access> {
      .init { env -> (ARState, Access) in
        let access: Access
        switch env.frameBuilderEnv.scopes.last!.staticLevel! {
        case 0:
          access = .init(
            definitionDescription: description,
            kind: .global(.init(name: name))
          )
        case 1...:
          let localAccess = env.frameBuilderEnv.scopes.last!.localVariables.first { $0.bindingName == name }!
          access = .init(
            definitionDescription: description,
            kind: .local(
              localAccess,
              frameStaticLevel: env.frameBuilderEnv.scopes.last!.staticLevel!
            )
          )
        case _:
          fatalError()
        }
        return (env, access)
      }
    }
    
    static var staticLevel: ActivationRecordsM<UInt> {
      .init { env -> (ARState, UInt) in
        (env, env.frameBuilderEnv.scopes.last!.staticLevel!)
      }
    }
    
    static var peek: ActivationRecordsM<FrameBuilderEnv.Scope> {
      .init { env -> (ARState, FrameBuilderEnv.Scope) in
        (env, env.frameBuilderEnv.scopes.last!)
      }
    }
  }
}

public extension ARState {
  enum ActionsLSE {
    static var anonymous: ActivationRecordsM<FrameLabel> {
      .init { env -> (ARState, FrameLabel) in
        let label = FrameLabel(name: "L\(env.labelStreamEnv.anonymousLabelCount)")
        var env = env
        env.labelStreamEnv.anonymousLabelCount += 1
        return (env, label)
      }
    }
  }
}

public extension ARState {
  enum ActionsTSE {
    static var temp: ActivationRecordsM<FrameTemp> {
      .init { env -> (ARState, FrameTemp) in
        let temp = FrameTemp(integerLiteral: env.tempStreamEnv.count)
        var env = env
        env.tempStreamEnv.count += 1
        return (env, temp)
      }
    }
    
    static func temps(count: Int) -> ActivationRecordsM<[FrameTemp]> {
      .init { env -> (ARState, [FrameTemp]) in
        let temps = (0..<count)
          .map { FrameTemp(integerLiteral: $0 + env.tempStreamEnv.count) }
        var env = env
        env.tempStreamEnv.count += count
        return (env, temps)
      }
    }
  }
}
