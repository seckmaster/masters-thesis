//
//  LabelStreamEnv.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct LabelStreamEnv {
  var anonymousLabelCount: Int
}
