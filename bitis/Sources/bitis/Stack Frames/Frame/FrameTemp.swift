//
//  FrameTemp.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct FrameTemp: Equatable, Hashable {
  // NOTE: - Negative temps are reserved for the compiler
  public let identifier: Int
}

extension FrameTemp: CustomStringConvertible, CustomDebugStringConvertible {
  public var description: String {
    "Temp [\(identifier)]"
  }
  
  public var debugDescription: String {
    description
  }
}

extension FrameTemp: ExpressibleByIntegerLiteral {
  public init(integerLiteral value: IntegerLiteralType) {
    self.identifier = value
  }
}
