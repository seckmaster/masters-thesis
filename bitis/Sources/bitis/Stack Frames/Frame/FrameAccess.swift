//
//  FrameAccess.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct Access: Equatable {
  let definitionDescription: String
  let kind: Kind
}

public extension Access {
  enum Kind: Equatable {
    /// Access a variable declared on the global scope.
    case global(FrameLabel)
    
    /// Access a local value.
    ///
    /// Local values (locals) are located just bellow the frame pointer on the stack.
    case local(
      Frame.Access,
      frameStaticLevel: UInt
    )
    
    /// Access a parameter.
    ///
    /// Parameters are located just above the frame pointer on the stack.
    /// The first paramater is (usually) the static link, followed by n arguments.
    /// Note that every function in Bitis has exactly one argument.
    case parameter(
      staticLevel: UInt,
      offset: Byte
    )
    
    var offset: Int {
      switch self {
      case .global:
        fatalError("Global access does not have `offset`!")
      case .local(let access, _):
        return access.offset
      case .parameter(_, let offset):
        return Int(offset)
      }
    }
  }
  
  var staticLevel: UInt? {
    switch kind {
    case .global:
      return nil
    case .local(_, let staticLevel):
      return staticLevel
    case .parameter(let staticLevel, _):
      return staticLevel
    }
  }
}

extension Access: CustomStringConvertible, CustomDebugStringConvertible {
  public var description: String {
    switch kind {
    case .global(let label):
      return "Global [\(label.description)]"
    case let .local(access, _):
      return "Local [\(access.description)]"
    case let .parameter(lvl, offset):
      return "Parameter [Lvl: \(lvl), Offset: \(offset)]"
    }
  }
  
  public var debugDescription: String {
    description
  }
}
