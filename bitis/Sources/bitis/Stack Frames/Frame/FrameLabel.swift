//
//  FrameLabel.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct FrameLabel: Equatable, Hashable {
  public let name: String
}

extension FrameLabel: CustomStringConvertible {
  public var description: String {
    name
  }
}

extension FrameLabel: ExpressibleByStringLiteral {
  public init(stringLiteral value: StringLiteralType) {
    name = value
  }
}
