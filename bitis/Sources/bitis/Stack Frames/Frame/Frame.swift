//
//  Frame.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public typealias Byte = UInt

public let x86: Byte = 4
public let x64: Byte = 8
public let wordSize: Byte = x64 // TODO: - We are targeting x64 arch ATM
public typealias WordSizeType = Double

// NOTE: - This stack frame representation is language specific
public struct Frame: Equatable {
  public var __description__: String
  public var staticLevel: UInt
  public var entry: FrameLabel
  public var end: FrameLabel
  public var localVariables: [Access]
  public var framePointer: FrameTemp
  public var returnValue: FrameTemp
  public var parameterCount: Byte
  
  public static func ==(lhs: Frame, rhs: Frame) -> Bool {
    lhs.entry == rhs.entry
  }
}

extension Frame {
  init(
    description: String,
    staticLevel: UInt,
    entry: FrameLabel,
    end: FrameLabel,
    localVariables: [Access],
    framePointer: FrameTemp,
    returnValue: FrameTemp
  ) {
    self.__description__ = description
    self.staticLevel = staticLevel
    self.entry = entry
    self.end = end
    self.localVariables = localVariables
    self.framePointer = framePointer
    self.returnValue = returnValue
    self.parameterCount = 2
  }
  
  init(
    description: String,
    staticLevel: UInt,
    entry: FrameLabel,
    end: FrameLabel,
    localVariables: [Access],
    framePointer: FrameTemp,
    returnValue: FrameTemp,
    parameterCount: UInt
  ) {
    self.__description__ = description
    self.staticLevel = staticLevel
    self.entry = entry
    self.end = end
    self.localVariables = localVariables
    self.framePointer = framePointer
    self.returnValue = returnValue
    self.parameterCount = parameterCount
  }
}

public extension Frame {
  var size: Byte {
    stackSize
  }
  
  var stackSize: Byte {
    stackLocalsSize + blockSizeForFPRA + parametersSize + blockSizeForRegisters
  }
  
  var blockSizeForFPRA: Byte { wordSize * 2 }
  
  var blockSizeForRegisters: Byte { 0 }
  
  var parametersSize: Byte { parameterCount * wordSize }
  
  var stackLocalsSize: Byte { Byte(localVariables.count) * wordSize }
}

extension Frame: CustomStringConvertible {
  public var description: String {
    """
    Frame (\(__description__)):
      - size         : \(size),
      - stack size   : \(stackSize),
      - static level : \(staticLevel),
      - entry label  : \(entry.description),
      - end label    : \(end.description),
      - params count : \(parameterCount),
      - params size  : \(parametersSize),
      - local vars   : \(localVariables.isEmpty ? "" : "\n" + localVariables.map { "    - " + $0.description }.joined(separator: ",\n"))
      - size for vars: \(stackLocalsSize)
      - size for regs: \(blockSizeForRegisters),
      - FP           : \(framePointer.description),
      - RV           : \(returnValue.description),
    """
  }
}

/// Frame variable access

public extension Frame {
  struct Access: Equatable, CustomStringConvertible {
    let bindingName: String
    let offset: Int // signed because it's negative
    
    public var description: String {
      "\(bindingName) [\(offset)]"
    }
  }
}

extension Frame {
  func access(for name: String) -> Access? {
    localVariables
      .reversed()
      .first(where: { $0.bindingName == name })
  }
}
