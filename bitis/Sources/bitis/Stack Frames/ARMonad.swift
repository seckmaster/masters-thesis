//
//  ARMonad.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Bow
import SwiftParsec
import Foundation

public typealias ActivationRecordsM<A> = State<ARState, A>

public protocol ARAnalysable {
  func frameEvaluator() -> ActivationRecordsM<()>
}

public typealias ARAst = AST

extension State where A == () {
  var unit: State<S, ()> {
    .pure(())^
  }
}
