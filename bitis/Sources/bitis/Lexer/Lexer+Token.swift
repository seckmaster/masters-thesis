//
// Created by Toni K. Turk on 30/10/2020.
//

import Foundation

// MARK: - Token
public extension Lexer {
  enum Token: CustomStringConvertible, Equatable {
    case `operator`(Operator, Position)
    case keyword(Keyword, Position)
    case identifier(String, Position)
    case literal(Literal, Position)

    public var description: String {
      switch self {
      case .operator(let op, _):
        return op.description
      case .keyword(let keyword, _):
        return keyword.description
      case .identifier(let ident, _):
        return ident
      case .literal(let literal, _):
        return literal.description
      }
    }

    public var position: Position {
      switch self {
      case .literal(_, let position),
           .operator(_, let position),
           .keyword(_, let position),
           .identifier(_, let position):
        return position
      }
    }
  }
}

// MARK: - Operators
public extension Lexer.Token {
  enum Operator: CustomStringConvertible, Equatable {
    case assign
    case dot
    case concat
    case lambda
    case emptyTuple
    case emptyList
    case rightArrow
    case hasType
    case ref
    case doll
    case bracket(Bracket)
    case arithmetic(Arithmetic)
    case logical(Logical)

    public var description: String {
      switch self {
      case .assign:
        return "="
      case .dot:
        return "."
      case .concat:
        return "++"
      case .lambda:
        return "λ"
      case .emptyTuple:
        return "()"
      case .emptyList:
        return "[]"
      case .rightArrow:
        return "->"
      case .hasType:
        return "::"
      case .ref:
        return "&"
      case .doll:
        return "$"
      case .arithmetic(let op):
        return op.description
      case .bracket(let bracket):
        return bracket.description
      case .logical(let op):
        return op.description
      }
    }
  }
}

// MARK: - Brackets
public extension Lexer.Token.Operator {
  enum Bracket: CustomStringConvertible, Equatable {
    case leftParent
    case rightParent
    case leftBrace
    case rightBrace
    case leftBracket
    case rightBracket

    public var description: String {
      switch self {
      case .leftParent:
        return "("
      case .rightParent:
        return ")"
      case .leftBrace:
        return "{"
      case .rightBrace:
        return "}"
      case .leftBracket:
        return "["
      case .rightBracket:
        return "]"
      }
    }
  }
}

// MARK: - Arithmetic operators
public extension Lexer.Token.Operator {
  enum Arithmetic: CustomStringConvertible, Equatable {
    case plus
    case minus
    case mul
    case div

    public var description: String {
      switch self {
      case .plus:
        return "+"
      case .minus:
        return "-"
      case .mul:
        return "*"
      case .div:
        return "/"
      }
    }
  }
}

// MARK: - Logical operators
public extension Lexer.Token.Operator {
  enum Logical: CustomStringConvertible, Equatable {
    case equal
    case notEqual
    case lowerThan
    case greaterThan
    case lowerOrEqualThan
    case greaterOrEqualThan
    case and
    case or

    public var description: String {
      switch self {
      case .equal:
        return "=="
      case .notEqual:
        return "/="
      case .lowerThan:
        return "<"
      case .greaterThan:
        return ">"
      case .lowerOrEqualThan:
        return "<="
      case .greaterOrEqualThan:
        return ">="
      case .or:
        return "||"
      case .and:
        return "&&"
      }
    }
  }
}

// MARK: - Keywords
public extension Lexer.Token {
  enum Keyword: CustomStringConvertible, Equatable {
    case `let`
    case `if`
    case then
    case print
    case `else`
    case data
    case type
    case `case`
    case `in`
    case `_`
    case of
    case include

    public var description: String {
      switch self {
      case .let:
        return "let"
      case .if:
        return "if"
      case .then:
        return "then"
      case .print:
        return "print"
      case .else:
        return "else"
      case .data:
        return "data"
      case .type:
        return "type"
      case .case:
        return "case"
      case .in:
        return "in"
      case ._:
        return "_"
      case .of:
        return "of"
      case .include:
        return "import"
      }
    }
  }
}

// MARK: - Literals
public extension Lexer.Token {
  enum Literal: CustomStringConvertible, Equatable, Hashable {
    case logical(Logical)
    case string(String)
    case int(Int)
    case float(Double)
    case char(Character)

    public var description: String {
      switch self {
      case .logical(let logical):
        return logical.description
      case .string(let string):
        return "\"\(string)\""
      case .int(let int):
        return int.description
      case .float(let float):
        return float.description
      case .char(let char):
        return "'\(char.description)'"
      }
    }
  }
}

// MARK: - Logical literals
public extension Lexer.Token.Literal {
  enum Logical: ExpressibleByStringLiteral, CustomStringConvertible, Equatable, Hashable {
    case `true`
    case `false`

    public init(stringLiteral value: String) {
      switch value {
      case "true":
        self = .true
      case "false":
        self = .false
      case _:
        fatalError("Lexer.Token.Literal.Logical: invalid value '\(value)'.")
      }
    }

    public var description: String {
      switch self {
      case .true:
        return "true"
      case .false:
        return "false"
      }
    }
  }
}
