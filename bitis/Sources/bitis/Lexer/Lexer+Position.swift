//
// Created by Toni K. Turk on 30/10/2020.
//

import Foundation

// MARK: - Position
public extension Lexer {
  struct Position: CustomStringConvertible, Equatable, Hashable {
    public let start: Location
    public let end: Location

    public init(
      start: Location,
      end: Location
    ) {
      self.start = start
      self.end = end
    }

    public var description: String {
      if start == end { return start.description }
      return start.description + "-" + end.description
    }

    public static var zero: Position {
      .init(start: .zero, end: .zero)
    }

    #if DEBUG
    public static func ==(_ lhs: Self, _ rhs: Self) -> Bool {
      if lhs.start.line == dummyValue || rhs.start.line == dummyValue {
        return true
      }
      return lhs.start == rhs.start && lhs.end == rhs.end
    }
    #endif
  }

  struct Location: CustomStringConvertible, Equatable, Hashable {
    public let line: Int
    public let column: Int

    public init(
      line: Int,
      column: Int
    ) {
      self.line = line
      self.column = column
    }

    public var description: String {
      "[\(line),\(column)]"
    }

    public static var zero: Location {
      .init(line: 0, column: 0)
    }

    public static func +(_ lhs: Location, _ rhs: Int) -> Location {
      .init(line: lhs.line, column: lhs.column + rhs)
    }

    public static func -(_ lhs: Location, _ rhs: Int) -> Location {
      .init(line: lhs.line, column: lhs.column - rhs)
    }
  }
}

extension Lexer.Position {
  #if DEBUG
  static var dummyValue = -0x224466

  init() {
    self.start = .init(line: Lexer.Position.dummyValue, column: Lexer.Position.dummyValue)
    self.end = .init(line: Lexer.Position.dummyValue, column: Lexer.Position.dummyValue)
  }
  #endif
}

func ==(_ lhs: Lexer.Position, _ rhs: Int) -> Bool {
  lhs.start.line == rhs && lhs.start.column == rhs && lhs.end.line == rhs && lhs.end.column == rhs
}
