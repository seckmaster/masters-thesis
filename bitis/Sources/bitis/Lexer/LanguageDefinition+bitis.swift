//
// Created by Toni K. Turk on 29/10/2020.
//

import Foundation
import SwiftParsec

extension LanguageDefinition {
  static var bitis: LanguageDefinition {
    var bitisLanguageSpec = empty
    bitisLanguageSpec.commentStart = "{-"
    bitisLanguageSpec.commentEnd   = "-}"
    bitisLanguageSpec.commentLine  = "--"
    bitisLanguageSpec.reservedNames = [
      "let", "in", "case", "if", "then", "else", "data", "type", "true", "false", "_", "of", "print", "import",
    ]
    bitisLanguageSpec.operatorLetter = .oneOf("|=")
    bitisLanguageSpec.reservedOperators = [
      "+", "-", "*", "/", "=", "==", "/=", "\\", "(", ")", "[", "]", "{", "}", "<", ">",
      "<=", ">=", "||", "&&", "()", "[]", "++", ".", "->", "::", "λ", "$"
    ]
    return bitisLanguageSpec
  }
}
