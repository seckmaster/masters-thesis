//
// Created by Toni K. Turk on 29/10/2020.
//

import SwiftParsec
import Foundation

public struct Lexer: TokenParser {
  public typealias UserState = ()
  public typealias Parser<Output> = GenericParser<String, UserState, Output>
  public typealias TokenParser = Parser<Token>
  public let languageDefinition: LanguageDefinition<UserState> = .bitis
  
  public init() {}
}

// MARK: - Identifier and Reserved words
public extension Lexer {
  var identifier: TokenParser {
    withPosition(wrap: identifier) {
      .identifier($0, $1)
    }
  }
  
  var uppercaseLetterSequence: TokenParser {
    withPosition(wrap: StringParser.uppercase.many1) {
      .identifier(.init($0), $1)
    }
  }

  var `let`: TokenParser {
    withPosition(keyword: "let") {
      .keyword(.let, $0)
    }
  }

  var `if`: TokenParser {
    withPosition(keyword: "if") {
      .keyword(.if, $0)
    }
  }

  var then: TokenParser {
    withPosition(keyword: "then") {
      .keyword(.then, $0)
    }
  }
  
  var print: TokenParser {
    withPosition(keyword: "print") {
      .keyword(.print, $0)
    }
  }
  
  var include: TokenParser {
    withPosition(keyword: "import") {
      .keyword(.include, $0)
    }
  }

  var `else`: TokenParser {
    withPosition(keyword: "else") {
      .keyword(.else, $0)
    }
  }

  var data: TokenParser {
    withPosition(keyword: "data") {
      .keyword(.data, $0)
    }
  }

  var type: TokenParser {
    withPosition(keyword: "type") {
      .keyword(.type, $0)
    }
  }

  var `case`: TokenParser {
    withPosition(keyword: "case") {
      Token.keyword(.case, $0)
    }
  }

  var `in`: TokenParser {
    withPosition(keyword: "in") {
      .keyword(.in, $0)
    }
  }

  var wildcard: TokenParser {
    withPosition(keyword: "_") {
      .keyword(._, $0)
    }
  }

  var of: TokenParser {
    withPosition(keyword: "of") {
      .keyword(.of, $0)
    }
  }
}

// MARK: - Operators
public extension Lexer {
  // MARK: - Arithmetic
  var plus: TokenParser {
    withPosition(operator: "+") {
      .operator(.arithmetic(.plus), $0)
    }
  }

  var minus: TokenParser {
    withPosition(operator: "-") {
      .operator(.arithmetic(.minus), $0)
    }
  }

  var mul: TokenParser {
    withPosition(operator: "*") {
      .operator(.arithmetic(.mul), $0)
    }
  }

  var div: TokenParser {
    withPosition(operator: "/") {
      .operator(.arithmetic(.div), $0)
    }
  }
  
  // MARK: - Logical
  var equal: TokenParser {
    withPosition(operator: "==") {
      .operator(.logical(.equal), $0)
    }
  }

  var notEqual: TokenParser {
    withPosition(operator: "/=") {
      .operator(.logical(.notEqual), $0)
    }
  }

  var lowerThan: TokenParser {
    withPosition(operator: "<") {
      .operator(.logical(.lowerThan), $0)
    }
  }

  var greaterThan: TokenParser {
    withPosition(operator: ">") {
      .operator(.logical(.greaterThan), $0)
    }
  }

  var lowerOrEqualThan: TokenParser {
    withPosition(operator: "<=") {
      .operator(.logical(.lowerOrEqualThan), $0)
    }
  }

  var greaterOrEqualThan: TokenParser {
    withPosition(operator: ">=") {
      .operator(.logical(.greaterOrEqualThan), $0)
    }
  }

  var or: TokenParser {
    withPosition(operator: "||") {
      .operator(.logical(.or), $0)
    }
  }

  var and: TokenParser {
    withPosition(operator: "&&") {
      .operator(.logical(.and), $0)
    }
  }

  // MARK: - Brackets
  var leftParent: TokenParser {
    withPosition(operator: "(") {
      .operator(.bracket(.leftParent), $0)
    }
  }

  var rightParent: TokenParser {
    withPosition(operator: ")") {
      .operator(.bracket(.rightParent), $0)
    }
  }

  var leftBrace: TokenParser {
    withPosition(operator: "{") {
      .operator(.bracket(.leftBrace), $0)
    }
  }

  var rightBrace: TokenParser {
    withPosition(operator: "}") {
      .operator(.bracket(.rightBrace), $0)
    }
  }

  var leftBracket: TokenParser {
    withPosition(operator: "[") {
      .operator(.bracket(.leftBracket), $0)
    }
  }

  var rightBracket: TokenParser {
    withPosition(operator: "]") {
      .operator(.bracket(.rightBracket), $0)
    }
  }

  // MARK: - Dot
  var dot: TokenParser {
    withPosition(operator: ".") {
      .operator(.dot, $0)
    }
  }

  // MARK: - Assign
  var assign: TokenParser {
    withPosition(operator: "=") {
      .operator(.assign, $0)
    }
  }

  // MARK: - Lambda
  var lambda: TokenParser {
    withPosition(operator: "\\") {
      .operator(.lambda, $0)
    } <|> withPosition(operator: "λ") {
      .operator(.lambda, $0)
    }
  }

  // MARK: - Concat
  var concat: TokenParser {
    withPosition(operator: "++") {
      .operator(.concat, $0)
    }
  }

  // MARK: - Concat
  var emptyTuple: TokenParser {
    withPosition(operator: "()") {
      .operator(.emptyTuple, $0)
    }
  }

  // MARK: - Concat
  var emptyList: TokenParser {
    withPosition(operator: "[]") {
      .operator(.emptyList, $0)
    }
  }

  // MARK: - ->
  var rightArrow: TokenParser {
    withPosition(operator: "->") {
      .operator(.rightArrow, $0)
    }
  }

  // MARK: - Has type (::)
  var hasType: TokenParser {
    withPosition(operator: "::") {
      .operator(.hasType, $0)
    }
  }
  
  // MARK: - Reference/Dereference
  var ref: TokenParser {
    withPosition(operator: "&") {
      .operator(.ref, $0)
    }
  }
  
  // MARK: Misc
  var doll: TokenParser {
    withPosition(operator: "$") {
      .operator(.doll, $0)
    }
  }
}

// MARK: - Logical literals
public extension Lexer {
  var `true`: TokenParser {
    withPosition(wrap: identifier.noOccurence *> symbol("true")) {
      .literal(.logical(.true), $1)
    }
  }
  
  var `false`: TokenParser {
    withPosition(wrap: identifier.noOccurence *> symbol("false")) {
      .literal(.logical(.false), $1)
    }
  }
  
  var logicalLiteral: TokenParser {
    self.true <|> self.false
  }
}

// MARK: - Newline
public extension Lexer {
  var newLine: Parser<()> {
    (whiteSpace *> GenericParser.newLine.many1 <* whiteSpace).many >>- { _ in
      .init(result: ())
    }
  }
}

// MARK: - Numerical literals
public extension Lexer {
  var intLiteral: TokenParser {
    let integer: Parser<Int> = Lexer.sign() >>- { s in
      s <^> Lexer.naturalNumber <* whiteSpace
    }
    return withPosition(wrap: integer) {
      .literal(.int($0), $1)
    }
  }

  var floatLiteral: TokenParser {
    withPosition(wrap: float) {
      .literal(.float($0), $1)
    }
  }

  var numericLiteral: TokenParser {
    floatLiteral <|> intLiteral
  }

  var anyLiteral: TokenParser {
    numericLiteral <|> characterLiteral <|> stringLiteral <|> logicalLiteral
  }

  private static func sign<Number: SignedNumeric>() -> Parser<(Number) -> Number> {
    .character("-") *> Parser(result: -)
      <|> .character("+") *> Parser { $0 }
      <|> .init { $0 }
  }

  private static var naturalNumber: Parser<Int> {
    let zeroNumber = GenericParser.character("0") *>
      (hexadecimal
        <|> octal
        <|> decimal
        <|> GenericParser(result: 0)
      ) <?> ""
    return zeroNumber <|> decimal
  }
}

// MARK: - String and Character literals
public extension Lexer {
  var characterLiteral: TokenParser {
    withPosition(wrap: characterLiteral) {
      .literal(.char($0), $1)
    }
  }

  var stringLiteral: TokenParser {
    withPosition(wrap: stringLiteral) {
      .literal(.string($0), $1)
    }
  }
}

// MARK: - Private
private extension Lexer {
  func withPosition<A, B>(
    wrap parser: Parser<A>,
    transform: @escaping (A, Position) -> B
  ) -> Parser<B> where A: CustomStringConvertible {
    sourceLocation >>- { start in
      parser.attempt >>- { a in
        .init(result: transform(a, .init(start: start, end: start + a.description.count)))
      }
    }
  }

  func withPosition<A, B>(
    wrap parser: Parser<A>,
    offset: Int,
    transform: @escaping (A, Position) -> B
  ) -> Parser<B> {
    sourceLocation >>- { start in
      parser.attempt >>- { a in
        .init(result: transform(a, .init(start: start, end: start + offset)))
      }
    }
  }

  func withPosition<B>(
    keyword: String,
    transform: @escaping (Position) -> B
  ) -> Parser<B> {
    withPosition(
      wrap: reservedName(keyword),
      offset: keyword.count,
      transform: { _, pos in transform(pos) })
  }

  func withPosition<B>(
    operator op: String,
    transform: @escaping (Position) -> B
  ) -> Parser<B> {
    withPosition(
      wrap: reservedOperator(op).attempt,
      offset: op.count,
      transform: { _, pos in transform(pos) })
  }

  var sourceLocation: Parser<Location> {
    .sourcePosition
      .map { .init(line: $0.line, column: $0.column) }
  }
}
