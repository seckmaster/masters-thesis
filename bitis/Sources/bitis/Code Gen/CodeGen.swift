//
//  CodeGen.swift
//  
//
//  Created by Toni K. Turk on 15/01/2022.
//

import Foundation

public class CodeGenerator {
  public var tempStreamEnv: TempStreamEnv
  public var labelStreamEnv: LabelStreamEnv
  public var memory: Interpreter.Memory
  
  public init(
    tempStreamEnv: TempStreamEnv,
    labelStreamEnv: LabelStreamEnv,
    memory: Interpreter.Memory
  ) {
    self.tempStreamEnv = tempStreamEnv
    self.labelStreamEnv = labelStreamEnv
    self.memory = memory
  }
}

public extension CodeGenerator {
  class CodeChunk: PrettyStringConvertible, CustomStringConvertible, Equatable {
    public let frame: Frame
    public let code: [IR.Statement]
    
    init(frame: Frame, code: [IR.Statement]) {
      self.frame = frame
      self.code = code
    }
    
    public var prettyDescription: String {
      """
      \(description)
      \(code.prettyDescription)
      """
    }
    
    public var description: String {
      "Code chunk [\(frame.entry.name)]: \(frame.__description__)"
    }
    
    public static func == (lhs: CodeGenerator.CodeChunk, rhs: CodeGenerator.CodeChunk) -> Bool {
      lhs.frame == rhs.frame && lhs.code == rhs.code
    }
  }
  
  /// Get rid of ESEQs and SEQs.
  ///
  /// Code chunks are stored into the memory.
  ///
  /// - Returns: The `main` code chunk.
  func linearizeAndGenerateCode(chunks: Set<IRState.CodeChunk>) -> CodeChunk {
    let linearChunks = chunks.map {
      CodeChunk(frame: $0.frame, code: linearize(stmt: $0.stmt))
    }
    var mainChunk: CodeChunk!
    for chunk in linearChunks {
      switch chunk.frame.entry {
      case AST.mainLabel:
        mainChunk = chunk
      case _:
        try! memory.stM(label: chunk.frame.entry, value: chunk)
      }
    }
    return mainChunk
  }
}

extension CodeGenerator {
  public typealias SEQ  = [IR.Statement]
  public typealias ESEQ = (SEQ, IR.Expression)
  
  func linearize(stmt: IR.Statement) -> SEQ {
    switch stmt {
    case let .move(dst, src):
      let (seq1, expr1) = linearize(expr: dst)
      let (seq2, expr2) = linearize(expr: src)
      var seq = SEQ()
      seq.append(contentsOf: seq1)
      seq.append(contentsOf: seq2)
      seq.append(.move(dst: expr1, src: expr2))
      return seq
    case let .cjump(cond, `true`, `false`):
      var seq = SEQ()
      let (seq1, expr1) = linearize(expr: cond)
      seq.append(contentsOf: seq1)
      seq.append(.cjump(cond: expr1, true: `true`, false: `false`))
      return seq
    case .exp(let expr):
      let (seq, expr) = linearize(expr: expr)
      var lin = SEQ()
      lin.append(contentsOf: seq)
      lin.append(.exp(expr))
      return lin
    case .seq(let stmts):
      var seq = SEQ()
      for stmt in stmts {
        let lin = linearize(stmt: stmt)
        seq.append(contentsOf: lin)
      }
      return seq
    case .label, .jump, .trap:
      return [stmt]
    case .marker:
      return []
    }
  }
  
  func linearize(expr: IR.Expression) -> ESEQ {
    switch expr {
    case .name, .constant, .temp, .malloc, .dealloc:
      return ([], expr)
    case let .binop(op, lhs, rhs):
      /// @NOTE:
      /// Due to an issue with lazy evaluation and recursion,
      /// the way the code for `binop` is generated is changed.
      ///
      /// Because if the side-effects of `lhs` and `rhs`
      /// effect one another, recursion won't work correctly; we therefore do not completely
      /// remove `eseq` and `seq` . By making this change, the
      /// left branch of the binary expression is completely reduced; hence if the right branch
      /// uses the same register as the left branch, the side-effects won't interfere.
      
      /// The changes were performed by the commit:
      /// `76ba20f391d5fc72590bbab045a96a1a6096943e`
      ///
      /// The "correct" linearization of `binop`:
      ///
      ///      let (stmt1, expr1) = linearize(expr: lhs)
      ///      let (stmt2, expr2) = linearize(expr: rhs)
      ///      var seq = SEQ()
      ///      seq.append(contentsOf: stmt1)
      ///      seq.append(contentsOf: stmt2)
      ///      return (seq, .binop(
      ///        op,
      ///        lhs: expr1,
      ///        rhs: expr2))
      
//      let (stmt1, expr1) = linearize(expr: lhs)
//      let (stmt2, expr2) = linearize(expr: rhs)
//      var seq = SEQ()
//      seq.append(contentsOf: stmt1)
//      seq.append(contentsOf: stmt2)
//      return (seq, .binop(
//        op,
//        lhs: expr1,
//        rhs: expr2))
      
      let (stmt1, expr1) = linearize(expr: lhs)
      let (stmt2, expr2) = linearize(expr: rhs)

      let expr11: IR.Expression
      switch stmt1.count {
      case 0:
        expr11 = expr1
      case 1:
        expr11 = .eseq(stmt1[0], expr1)
      case _:
        expr11 = .eseq(.seq(stmt1), expr1)
      }

      let expr22: IR.Expression
      switch stmt2.count {
      case 0:
        expr22 = expr2
      case 1:
        expr22 = .eseq(stmt2[0], expr2)
      case _:
        expr22 = .eseq(.seq(stmt2), expr2)
      }

      return ([], .binop(
        op,
        lhs: expr11,
        rhs: expr22))
    case let .eseq(stmt, expr):
      var seq = linearize(stmt: stmt)
      let (seq1, expr) = linearize(expr: expr)
      seq.append(contentsOf: seq1)
      return (seq, expr)
    case let .call(fn, args):
      var seq = SEQ()
      let (fnStmts, fnExpr) = linearize(expr: fn)
      seq.append(contentsOf: fnStmts)
      var argTemps = [IR.Expression]()
      for arg in args {
        let temp = tempStreamEnv.next()
        let (stmts, expr) = linearize(expr: arg)
        seq.append(contentsOf: stmts)
        seq.append(.move(dst: .temp(temp), src: expr))
        argTemps.append(.temp(temp))
      }
      let call = IR.Expression.call(
        fn: fnExpr,
        args: argTemps)
      let temp = tempStreamEnv.next()
      seq.append(.move(dst: .temp(temp), src: call))
      return (seq, .temp(temp))
    case .mem(let expr):
      let (seq, expr) = linearize(expr: expr)
      return (seq, .mem(expr))
    case .print(let expr):
      let (stmt, expr) = linearize(expr: expr)
      return (stmt, .print(expr))
    }
  }
}

fileprivate extension TempStreamEnv {
  mutating func next() -> FrameTemp {
    let temp = FrameTemp(identifier: count)
    count += 1
    return temp
  }
}

fileprivate extension LabelStreamEnv {
  mutating func next() -> FrameLabel {
    let temp = FrameLabel(name: "L\(anonymousLabelCount)")
    anonymousLabelCount += 1
    return temp
  }
}
