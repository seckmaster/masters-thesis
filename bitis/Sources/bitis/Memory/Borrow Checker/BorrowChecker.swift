//
//  BorrowChecker.swift
//  
//
//  Created by Toni K. Turk on 30/07/2021.
//

import Foundation
import Bow
import SwiftParsec

extension BCAst: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<BCAst> {
    bindings.borrowChecker().map { .init(bindings: $0) }^
  }
}

extension BCAst.Binding: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<AST.Binding> {
    switch self {
    case let .data(_, _, _, constructors, type):
      let constructorsM = constructors
        .map { BCState.Actions.bind(type: type!, for: $0.name) }
        .sequence()^
      return BCState.Actions.bind(type: type!, for: name) *> constructorsM *> .pure(self)^
    case .prototype:
      return .pure(self)^
    case let .binding(position, name, cases, type, lifetime, borrowLifetime):
      return BCState.ActionsBCE.MoveEnv.insertName(name) *>
        cases.borrowChecker() >>- { cases in
          BCState.ActionsLME.bind(name: name, for: lifetime!) >>- {
            let binding = BCAst.Binding.binding(
              position: position,
              name: name,
              cases: cases,
              tcType: type,
              lifetime: lifetime,
              borrowLifetime: borrowLifetime)
            return .pure(binding)^
          }
        }
    case let .print(position, expression, type):
      return expression.borrowChecker() >>- { expr in
        let binding = AST.Binding.print(
          position: position,
          expression: expr,
          tcType: type)
        return .pure(binding)^
      }
    case let .include(module):
      return .pure(.include(module: module))^
    }
  }
}

extension BCAst.Binding.Case: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<AST.Binding.Case> {
    BCState.ActionsBCE.BorrowEnv.isBorrowed(name: name) >>- { isCurrentlyBorrowed in
      let allIdentifiers = parameters.reduce(into: Set()) { $0.formUnion($1.identifiers) }
      return BCState.ActionsBCE.MoveEnv.insertNames(allIdentifiers) *> body.borrowChecker() >>- { body in
        BCState.ActionsEDE.dependencies(for: body) >>- { dependencies in
          let dependencies = dependencies.filter { !allIdentifiers.contains($0) }
          let nodes = dependencies.map(DependencyTreeEnv.Node.init(name:))
          return DependencyTreeEnv.Actions.connect(node: .init(name: name), with: nodes) >>- {
            DependencyTreeEnv.Actions.ensureNoStrongCycles >>- {
              BCState.ActionsEDE.addDependencies(dependencies, binding: self) >>- {
                let removeSelfBorrowM = isCurrentlyBorrowed
                  ? .pure(())^
                  : BCState.ActionsBCE.BorrowEnv.removeBorrow(name: name)
                return removeSelfBorrowM >>- {
                  let `case` = BCAst.Binding.Case(
                    name: name,
                    position: position,
                    parameters: parameters,
                    body: body,
                    type: type,
                    lifetime: lifetime)
                  return .pure(`case`)^
                }
              }
            }
          }
        }
      }
    }
  }
}

extension BCAst.Expression: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<AST.Expression> {
    switch self {
    case .literal:
      return .pure(self)^
    case let .name(_, identifier, type, _, _):
      guard !type!.isArrow else { return .pure(self)^ } // TODO: - Handle functions in a correct manner
      
      return BCState.ActionsBCE.MoveEnv.ensureNotAlreadyMoved(identifier) >>- {
        BCState.ActionsBCE.ActiveLifetimeEnv.current >>- { activeLifetime in
          BCState.ActionsIIRS.isInsideReference >>- { isInsideReference in
            guard !type!.isReference else {
              // TODO: - Is this actually correct?
              return .pure(self)^
            }
            
            switch (isInsideReference, activeLifetime) {
            case (_, let activeLifetime?):
              guard let activeLifetime = activeLifetime else {
                if isInsideReference {
                  return .pure(self)^
                }
                // TODO: - Code dupl.
                return BCState.ActionsBCE.BorrowEnv.ensureNotBorrowed(name: identifier) *>
                BCState.ActionsBCE.MoveEnv.markAsMoved(name: identifier) *>
                BCState.ActionsEDE.addDependencies([identifier], for: self) *> .pure(self)^
              }
              if lifetime!.hasDependency(on: activeLifetime, checkStaticId: true) {
                return BCState.ActionsBCE.BorrowEnv.insertName(identifier) *> .pure(self)^
              }
              return .pure(self)^
            case (true, nil):
              return BCState.ActionsBCE.BorrowEnv.insertName(identifier) *> .pure(self)^
            case (false, nil):
              return BCState.ActionsBCE.BorrowEnv.ensureNotBorrowed(name: identifier) *>
              BCState.ActionsBCE.MoveEnv.markAsMoved(name: identifier) *>
              BCState.ActionsEDE.addDependencies([identifier], for: self) *> .pure(self)^
            }
          }
        }
      }
    case let .let(_, bindings, expression, _, _):
      return BCState.Actions.inNewScope {
        bindings.borrowChecker() >>- { bindings in
          expression.borrowChecker() >>- { expression in
            let allExpressions = bindings.flatMap { $0.bindingExpressions } + [expression]
            return BCState.ActionsEDE.computeAndStoreDependencies(
              for: allExpressions,
              parentExpression: self,
              filter: .init(bindings.map { $0.name })
            ) *> .pure(self)^
          }
        }
      } >>- { (letExpression: LAAst.Expression) -> BorrowCheckM<LAAst.Expression> in
        /// This code handles the case where a borrow escapes its scope
        /// by being returned out of the`let` expression.
        
        guard let lifetime = letExpression.lifetime else {
          return .pure(self)^
        }
        return BCState.ActionsLME.names(for: lifetime) >>- { borrowedNames in
          BCState.ActionsBCE.BorrowEnv.insertNames(borrowedNames) *> .pure(self)^
        }
      }
    case let .lambda(_, parameter, body, _, _):
      return BCState.ActionsBCE.MoveEnv.inNewScope {
        BCState.ActionsBCE.MoveEnv.insertNames(
          parameter.identifiers
        ) *>
        body.borrowChecker() >>- { body in
          BCState.ActionsEDE.computeAndStoreDependencies(
            for: body,
            parentExpression: self,
            filter: parameter.identifiers
          ) *> .pure(self)^
        }
      }
    case let .case(_, expression, matches, _, _):
      let allIdentifiers = matches.reduce(into: Set()) { $0.formUnion($1.pattern.identifiers) }
      return expression.borrowChecker() *> (BCState.ActionsBCE.Actions.performInParallel(
        matches.map { $0.borrowChecker }
      )) *>
          BCState.ActionsEDE.computeAndStoreDependencies(
            for: expression,
            parentExpression: self,
            filter: allIdentifiers
          ) *> .pure(self)^
    case let .ref(_, expression, _, _):
      func handleError(_ error: BorrowError) -> BorrowError {
        switch error {
        case .alreadyMoved(let name):
          return .borrowFromMovedValue(name: name)
        case _:
          return error
        }
      }
      return BCState.ActionsIIRS.performWorkInsideReference {
        expression
          .borrowChecker()
          .mapError(handleError) 
        *> .pure(self)^
      }
    case let .deref(_, expression, _, _):
      return expression.borrowChecker() >>- { expr in
        /// @NOTE: - Tuples cannot be moved out of borrow because memory management wouldn't work correctly!
        /// That is, becaue pattern matching removes moved objects out of the destructor of the tuple, memory would be leaked.
        /// Consider the program
        ///
        ///     todo: add example
        let isTupleOrTypeConstructor = expr.type!.asRef?.isTuple ?? false || expr.type!.asRef?.isTypeConstructor ?? false
        let isRef = expr.type!.isReference
//        let isRefRef = expr.type!.asRef?.isReference ?? false
        
        
        if !isTupleOrTypeConstructor && isRef {
          return BCState.ActionsEDE.computeAndStoreDependencies(
            for: expr,
            parentExpression: self,
            filter: .empty()
          ) *> .pure(self)^
        }
        return .error(.cannotMoveOutOfBorrow)
      }
    case let .tuple(_, expressions, _, _):
      return expressions.borrowChecker() *>
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: expressions,
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
    case let .list(_, expressions, _, _):
      return expressions.borrowChecker() *>
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: expressions,
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
    // MARK: - Application
    case let .binary(.application(_, fn, arg, _, _)):
      func dependenciesM(fn: BCAst.Expression, arg: BCAst.Expression) -> BorrowCheckM<()> {
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: [fn, arg],
          parentExpression: self,
          filter: .empty()
        )
      }
      return fn.borrowChecker() >>- { fn in
        let arrow = fn.type!.curriedArrow
        let activeLifetime = lifetimeIntersection(
          returnTypeLifetime: arrow.last!.lifetime?.simplify,
          argTypeLifetime: arrow.first!.lifetime?.simplify,
          givenLifetime: arg.lifetime
        )
        return BCState.ActionsBCE.ActiveLifetimeEnv.performWork({
          (arg.borrowChecker() >>- { dependenciesM(fn: fn, arg: $0) }) *> .pure(self)^
        }, with: activeLifetime)
      }
    case let .binary(.arithmetic(_, _, left, right, _, _)):
      return BCState.ActionsBCE.Actions.performInParallelWeak(
        left.borrowChecker,
        right.borrowChecker
      ) >>- { branches in
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: branches,
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
      }
    case let .binary(.concat(_, left, right, _, _)):
      return BCState.ActionsBCE.Actions.performInParallelWeak(
        left.borrowChecker,
        right.borrowChecker
      ) >>- { branches in
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: branches,
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
      }
    case let .binary(.ifThenElse(_, condition, `true`, `false`, _, _)):
      return BCState.ActionsBCE.Actions.performInParallelWeak(
        { condition.borrowChecker().map { [$0] }^ },
        {
          BCState.ActionsBCE.Actions.performInParallel(
            `true`.borrowChecker,
            `false`.borrowChecker
          )
        }
      ) >>- { branches in
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: branches.flatMap { $0 },
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
      }
    case let .binary(.logical(_, _, left, right, _, _)):
      return BCState.ActionsBCE.Actions.performInParallelWeak(
        left.borrowChecker,
        right.borrowChecker
      ) >>- { branches in
        BCState.ActionsEDE.computeAndStoreDependencies(
          for: branches,
          parentExpression: self,
          filter: .empty()
        ) *> .pure(self)^
      }
    }
  }
}

extension BCAst.Expression.Match: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<AST.Expression.Match> {
    BCState.Actions.inNewScope {
      BCState.ActionsBCE.MoveEnv.insertNames(pattern.identifiers) *>
      expression.borrowChecker() >>- { expression in
        let match = BCAst.Expression.Match(
          position: position,
          pattern: pattern,
          expression: expression)
        return .pure(match)^
      }
    }
  }
}

extension Collection where Element: BorrowCheckable {
  public func borrowChecker() -> BorrowCheckM<[Element]> {
    map { $0.borrowChecker() }.sequence()^
  }
}

extension Swift.Optional where Wrapped == LAAst.Lifetime {
  func hasDependency(on other: LAAst.Lifetime?) -> Bool {
    guard let lhs = self, let rhs = other else {
      return false
    }
    return lhs.hasDependency(on: rhs)
  }
}
