//
//  DependencyTreeEnv.swift
//  
//
//  Created by Toni K. Turk on 06/12/2021.
//

import Foundation

public struct DependencyTreeEnv {
  public typealias Scope = Graph<Node>
  public var scopes: [Scope]
}

public extension DependencyTreeEnv {
  struct Node: Hashable, Comparable, ExpressibleByStringLiteral, CustomStringConvertible, CustomDebugStringConvertible {
    let name: String
    //    let position: AST.Position
    
    public init(stringLiteral value: StringLiteralType) {
      self.name = value
    }
    
    public init(name: StringLiteralType) {
      self.name = name
    }
    
    public var description: String {
      name
    }
    
    public var debugDescription: String {
      description
    }
    
    public static func < (lhs: DependencyTreeEnv.Node, rhs: DependencyTreeEnv.Node) -> Bool {
      lhs.name < rhs.name
    }
  }
}

public extension DependencyTreeEnv {
  static var empty: Self { .init(scopes: [.init(isDirected: true)]) }
  
  func connect(node: Node, with nodes: [Node]) -> Self {
    guard !scopes.isEmpty else {
      fatalError("DependencyTreeEnv.connect: no scopes!")
    }
    var env = self
    nodes.forEach {
      env.scopes[env.scopes.count - 1].addEdge(from: node, to: $0)
    }
    return env
  }
  
  var cycles: [[Node]] {
    guard !scopes.isEmpty else {
      fatalError("DependencyTreeEnv.connect: no scopes!")
    }
    func isCycle(nodes: Set<Graph.Node>) -> Bool {
      precondition(!nodes.isEmpty)
      guard nodes.count == 1 else {
        return true
      }
      // singleton cycles are "real" cycles only
      // if the node is connected to itself
      return scopes.last!.neighbours(of: nodes.first!).contains(nodes.first!)
    }
    return scopes.last!
      .stronglyConnectedComponents
      .filter(isCycle)
      .map { $0.map { scopes.last!.indexes[$0]! } }
  }
}

extension DependencyTreeEnv: HasScope {
  public func pushScope() -> DependencyTreeEnv {
    guard !scopes.isEmpty else {
      fatalError("Cannot push when the environment is empty.")
    }
    var env = self
    env.scopes.append(scopes.last!)
    return env
  }
  
  public func popScope() -> DependencyTreeEnv {
    guard scopes.count >= 1 else {
      fatalError("Cannot pop when the environment has less than one scope.")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}

// MARK: - Actions

public extension DependencyTreeEnv {
  enum Actions {
    static func connect(node: Node, with dependencies: [Node]) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.dte = env.dte.connect(node: node, with: dependencies)
        return .pure((env, ()))
      }
    }
    
    /// Make sure there are no (strong) cycles.
    static var ensureNoStrongCycles: BorrowCheckM<()> {
      .init { env in
        let cycles = env.dte.cycles
        guard cycles.isEmpty else {
          precondition(cycles.count == 1)
          return .raiseError(.init(
            backtrace: env.backtrace,
            error: .cycleDetected(cycles.first!.sorted(by: <)) // sort so that testing is easier
          ))
        }
        return .pure((env, ()))
      }
    }
  }
}

/// Modified graph data structure with additional capabilities for
/// borrow checking requirements:
///   - Connect vertices by their ID
///
/// TODO: - Optimise for performance.
public struct Graph<ID: Hashable> {
  var inAdjacencyList: [[Node]]
  var outAdjacencyList: [[Node]]
  var values: [ID: Node]
  var indexes: [Node: ID]
  public internal(set) var isDirected: Bool
  
  public init(isDirected: Bool) {
    self.isDirected = isDirected
    self.inAdjacencyList = .empty()
    self.outAdjacencyList = .empty()
    self.values = .empty()
    self.indexes = .empty()
  }
}

public extension Graph {
  typealias Node = Int
  
  internal struct EdgeKey: Hashable {
    let from: Node
    let to: Node
  }
}

public extension Graph {
  var count: Int {
    inAdjacencyList.count
  }
  
//  var edgesCount: Int {
//  }
  
  mutating func addEdge(from: ID, to: ID) {
    let fromNode: Node
    let toNode: Node
    if let existing = node(for: from) {
      fromNode = existing
    } else {
      fromNode = addNode(for: from)
    }
    if let existing = node(for: to) {
      toNode = existing
    } else {
      toNode = addNode(for: to)
    }
    
    outAdjacencyList[fromNode].append(toNode)
    if isDirected {
      inAdjacencyList[toNode].append(fromNode)
    } else {
      outAdjacencyList[toNode].append(fromNode)
    }
  }
  
  func neighbours(of node: Node) -> [Node] {
    outAdjacencyList[node]
  }
  
  func node(for value: ID) -> Node? {
    values[value]
  }
  
  var transpose: Graph {
    guard isDirected else { return self }
    var transposed = self
    transposed.outAdjacencyList = self.inAdjacencyList
    transposed.inAdjacencyList = self.outAdjacencyList
    return transposed
  }
  
  func weaklyConnectedComponent(for node: Node) -> Set<Node> {
    var nodes = Set<Node>()
    (0..<count).forEach { nodes.insert($0) }
    return weaklyConnectedComponent(for: node, nodes: &nodes)
  }
  
  var weaklyConnectedComponents: [Set<Node>] {
    var components = [Set<Node>]()
    var nodes = Set<Node>()
    (0..<count).forEach { nodes.insert($0) }
    while !nodes.isEmpty {
      let node = nodes.first!
      let component = weaklyConnectedComponent(for: node, nodes: &nodes)
      components.append(component)
    }
    return components
  }
  
  func stronglyConnectedComponent(for node: Node) -> Set<Node> {
    precondition(isDirected)
    var nodes = Set<Node>()
    (0..<count).forEach { nodes.insert($0) }
    return stronglyConnectedComponent(for: node, nodes: &nodes)
  }
  
  var stronglyConnectedComponents: [Set<Node>] {
    precondition(isDirected)
    var components = [Set<Node>]()
    var nodes = Set<Node>()
    (0..<count).forEach { nodes.insert($0) }
    while !nodes.isEmpty {
      let node = nodes.first!
      let component = stronglyConnectedComponent(for: node, nodes: &nodes)
      components.append(component)
    }
    return components
  }
  
  private func weaklyConnectedComponent(for node: Node, nodes: inout Set<Node>) -> Set<Node> {
    var component = Set<Node>()
    var stack = [Node]()
    nodes.remove(node)
    component.insert(node)
    neighbours(of: node).forEach { stack.append($0) }
    while !stack.isEmpty {
      let current = stack.popLast()!
      component.insert(current)
      nodes.remove(current)
      neighbours(of: current).forEach {
        if nodes.remove($0) == nil { return }
        stack.append($0)
      }
    }
    return component
  }
  
  private func stronglyConnectedComponent(for node: Node, nodes: inout Set<Node>) -> Set<Node> {
    let wcc1 = weaklyConnectedComponent(for: node)
    let wcc2 = transpose.weaklyConnectedComponent(for: node)
    let scc = wcc1.intersection(wcc2)
    scc.forEach { nodes.remove($0) }
    return scc
  }
  
  private mutating func addNode(for value: ID) -> Node {
    outAdjacencyList.append(.empty())
    inAdjacencyList.append(.empty())
    values[value] = outAdjacencyList.count - 1
    indexes[outAdjacencyList.count - 1] = value
    return outAdjacencyList.count - 1
  }
}

extension Graph: Equatable {
  static public func == (lhs: Self, rhs: Self) -> Bool {
    lhs.isDirected == rhs.isDirected &&
    lhs.inAdjacencyList == rhs.inAdjacencyList &&
    lhs.outAdjacencyList == rhs.outAdjacencyList
  }
}
