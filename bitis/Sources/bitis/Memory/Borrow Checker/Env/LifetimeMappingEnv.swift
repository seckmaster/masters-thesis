//
//  LifetimeMappingEnv.swift
//  
//
//  Created by Toni K. Turk on 11/12/2021.
//

import Foundation

public struct LifetimeMappingEnv {
  public typealias Scope = [LAAst.Lifetime: Set<String>]
  var scopes: [Scope]
}

public extension LifetimeMappingEnv {
  mutating func bind(name: String, for lifetime: AST.Lifetime) {
    bind(names: [name], for: lifetime)
  }
  
  mutating func bind(names: Set<String>, for lifetime: AST.Lifetime) {
    guard !scopes.isEmpty else {
      fatalError("LifetimeEnv: no scopes!")
    }
    if scopes[scopes.count - 1][lifetime] == nil {
      scopes[scopes.count - 1][lifetime] = names
    } else {
      scopes[scopes.count - 1][lifetime]!.formUnion(names)
    }
  }
  
  func names(for lifetime: LAAst.Lifetime) -> Set<String> {
    var allNames = Set<String>()
    for scope in scopes.reversed() {
      guard let names = scope[lifetime] else { continue }
      allNames.formUnion(names)
    }
    return allNames
  }
}

extension LifetimeMappingEnv: HasScope {
  public func pushScope() -> LifetimeMappingEnv {
    var env = self
    env.scopes.append(.empty())
    return env
  }
  
  public func popScope() -> LifetimeMappingEnv {
    guard scopes.count > 1 else {
      fatalError("LifetimeEnv.popScope: not allowed to pop when count < 2!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}
