//
//  BorrowCheckerEnv.swift
//  
//
//  Created by Toni K. Turk on 06/12/2021.
//

import Foundation

public struct BorrowCheckerEnv: HasScope {
  var borrowEnv: BorrowEnv
  var moveEnv: MoveEnv
  var activeLiftimeStack: [BCAst.Lifetime?] // the lifetime at the top of the stack is the current active lifetime for application
  
  public func pushScope() -> BorrowCheckerEnv {
    .init(
      borrowEnv: borrowEnv.pushScope(),
      moveEnv: moveEnv.pushScope(),
      activeLiftimeStack: activeLiftimeStack
    )
  }
  
  public func popScope() -> BorrowCheckerEnv {
    .init(
      borrowEnv: borrowEnv.popScope(),
      moveEnv: moveEnv.popScope(),
      activeLiftimeStack: activeLiftimeStack
    )
  }
}

public extension BorrowCheckerEnv {
  static var empty: Self {
    .init(borrowEnv: .empty, moveEnv: .empty, activeLiftimeStack: [])
  }
}

// MARK: - Move Environment
public extension BorrowCheckerEnv {
  struct MoveEnv {
    public typealias Scope = [String: Bool] // true: moved, false: not moved
    var scopes: [Scope]
    var writeToTheLastScope: [Bool]
  }
}

public extension BorrowCheckerEnv.MoveEnv {
  static var empty: Self {
    .init(scopes: [.empty()], writeToTheLastScope: [])
  }
  
  mutating func insert(name: String) {
    precondition(!scopes.isEmpty, "BorrowCheckerEnv.insert(name:): empty env!")
    scopes[scopes.count - 1][name] = false
  }
  
  mutating func markAsMoved(name: String) {
    precondition(!scopes.isEmpty, "BorrowCheckerEnv.markAsMoved(name:): empty env!")
    if writeToTheLastScope.last == true {
      scopes[scopes.count - 1][name] = true
    } else {
      for (idx, scope) in scopes.reversed().enumerated() {
        if scope[name] == nil { continue }
        scopes[scopes.count - idx - 1][name] = true
        break
      }
    }
  }
  
  mutating func markAsMoved(names: Set<String>) {
    names.forEach { markAsMoved(name: $0) }
  }
  
  func isMoved(name: String) -> Bool {
    for scope in scopes.reversed() {
      guard let isMoved = scope[name] else { continue }
      return isMoved
    }
    return false
  }
  
  func isMoved<C: Collection>(any names: C) -> String? where C.Element == String {
    for name in names {
      if isMoved(name: name) { return name }
    }
    return nil
  }
}

extension BorrowCheckerEnv.MoveEnv: HasScope {
  public func pushScope() -> Self {
    var env = self
    env.scopes.append(.empty())
    return env
  }
  
  public func popScope() -> Self {
    guard scopes.count >= 1 else {
      fatalError("Cannot pop scope when only one is present!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}

// MARK: - Borrow Environment
public extension BorrowCheckerEnv {
  struct BorrowEnv {
    public typealias Scope = Set<String>
    var scopes: [Scope]
  }
}

public extension BorrowCheckerEnv.BorrowEnv {
  static var empty: Self {
    .init(scopes: [.empty()])
  }
  
  mutating func insert(name: String) {
    precondition(!scopes.isEmpty, "BorrowCheckerEnv.insert(names:): empty env!")
    scopes[scopes.count - 1].insert(name)
  }
  
  mutating func remove(name: String) {
    precondition(!scopes.isEmpty, "BorrowCheckerEnv.remove(names:): empty env!")
    scopes[scopes.count - 1].remove(name)
  }
  
  func contains(name: String) -> Bool {
    scopes
      .reversed()
      .contains(where: { $0.contains(name) })
  }
  
  func contains<C: Collection>(any names: C) -> String? where C.Element == String {
    for name in names {
      if contains(name: name) { return name }
    }
    return nil
  }
}

extension BorrowCheckerEnv.BorrowEnv: HasScope {
  public func pushScope() -> Self {
    var env = self
    env.scopes.append(.empty())
    return env
  }
  
  public func popScope() -> Self {
    guard scopes.count >= 1 else {
      fatalError("Cannot pop scope when only one is present!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}
