//
//  ExpressionDependencyEnv.swift
//  
//
//  Created by Toni K. Turk on 11/12/2021.
//

import Foundation

public struct ExpressionDependencyEnv {
  typealias Node = ASTMap<Set<String>>.Node
  
  var scope: ASTMap<Set<String>>
}
