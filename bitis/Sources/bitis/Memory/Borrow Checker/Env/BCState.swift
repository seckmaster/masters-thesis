//
//  BCState.swift
//  
//
//  Created by Toni K. Turk on 08/12/2021.
//

import Foundation
import SwiftParsec
import Bow

public struct BCState: HasScope {
  var bce: BorrowCheckerEnv
  var dte: DependencyTreeEnv
  var lme: LifetimeMappingEnv
  public var ede: ExpressionDependencyEnv // TODO: - `public` because required for stack-frames?
  var lte: LVE
  var backtrace: Backtrace
  var isInsideReferenceStack: [Bool]
  
  public static var empty: Self {
    .init(
      bce: .empty,
      dte: .empty,
      lme: .init(scopes: [.empty()]),
      ede: .init(scope: .init(scope: [:])),
      lte: .empty,
      backtrace: .init(),
      isInsideReferenceStack: [false]
    )
  }
  
  public func pushScope() -> BCState {
    .init(
      bce: bce.pushScope(),
      dte: dte.pushScope(),
      lme: lme.pushScope(),
      ede: ede,
      lte: lte.pushScope(),
      backtrace: backtrace,
      isInsideReferenceStack: isInsideReferenceStack
    )
  }
  
  public func popScope() -> BCState {
    .init(
      bce: bce.popScope(),
      dte: dte.popScope(),
      lme: lme.popScope(),
      ede: ede,
      lte: lte.pushScope(),
      backtrace: backtrace,
      isInsideReferenceStack: isInsideReferenceStack
    )
  }
}

// MARK: - Monadic actions on the state
public extension BCState {
  enum Actions {
    static func pushScope() -> BorrowCheckM<()> {
      .init { env in
        let env = env.pushScope()
        return .pure((env, ()))
      }
    }
    
    static func popScope() -> BorrowCheckM<()> {
      .init { env in
        let env = env.popScope()
        return .pure((env, ()))
      }
    }
    
    static func inNewScope<A>(_ f: () -> BorrowCheckM<A>) -> BorrowCheckM<A> {
      pushScope() *> f() <* popScope()
    }
    
    
    // MARK: - Local Type Env
    // TODO: - Create a separate actions extension
    
    static func bind(type: TCType, for name: String) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.lte = env.lte.bind(name: name, with: type)
        return .pure((env, ()))
      }
    }
    
    static func type(for name: String) -> BorrowCheckM<TCType> {
      .init { env in
        guard let type = env.lte.queryType(for: name, currentScopeOnly: false) else {
          fatalError()
        }
        return .pure((env, type))
      }
    }
  }
}

// MARK: - Monadic actions

public extension BCState {
  // MARK: - BCE
  enum ActionsBCE {}
  
  // MARK: - LME
  enum ActionsLME {
    static func bind(name: String, for lifetime: AST.Lifetime) -> BorrowCheckM<()> {
      .init {
        var env = $0
        env.lme.bind(name: name, for: lifetime)
        return .pure((env, ()))
      }
    }
    
    static func names(for lifetime: LAAst.Lifetime) -> BorrowCheckM<Set<String>> {
      .init { env in
        let names = lifetime
          .decomposeLifetime
          .reduce(into: Set()) { $0.formUnion(env.lme.names(for: $1)) }
        return .pure((env, .init(names)))
      }
    }
  }
}

extension BCState.ActionsBCE {
  enum Actions {
    /// Use this action when the given you want to "execute" the given tasks
    /// in such a way that they are not executed one after the other, but in "parallel" fashion.
    /// This means that they can produce the same side-effects (for instance, two tasks can
    /// move the same variable) without causing an error.
    /// After the tasks are executed, a union of their side-effects is persisted.
    ///
    /// An example use-case is the `if-then-else` expr for analysing the branches.
    static func performInParallel<A>(_ tasks: [() -> BorrowCheckM<A>]) -> BorrowCheckM<[A]> {
      performInParallel(tasks) { moveScopes, borrowScope in
        .init { env in
          var env = env
          for scope in moveScopes {
            for (name, isMoved) in scope {
              guard isMoved else { continue }
              env.bce.moveEnv.markAsMoved(name: name)
            }
          }
          for borrow in borrowScope {
            env.bce.borrowEnv.scopes[env.bce.borrowEnv.scopes.count - 1].insert(borrow)
          }
          return .pure((env, ()))
        }
      }
    }
    static func performInParallel<A>(_ tasks: (() -> BorrowCheckM<A>)...) -> BorrowCheckM<[A]> {
      performInParallel(tasks)
    }
    
    /// This method is similar to `performInParallel`; the difference is that this method
    /// does not allow more than one task to move the same variable. What it allows is that
    /// one task moves a variable and the other borrows it.
    ///
    /// An example use-case are binary operations:
    ///
    /// ```
    /// x = 10
    /// y = &x + x
    /// ```
    static func performInParallelWeak<A>(_ tasks: (() -> BorrowCheckM<A>)...) -> BorrowCheckM<[A]> {
      func validateMoveScopes(_ scopes: [BorrowCheckerEnv.MoveEnv.Scope]) -> String? {
        var alreadyMovedVariables = Set<String>()
        for scope in scopes {
          for (name, isMoved) in scope {
            guard isMoved else { continue }
            if alreadyMovedVariables.contains(name) {
              return name
            }
            alreadyMovedVariables.insert(name)
          }
        }
        return nil
      }
      return performInParallel(tasks) { moveScopes, borrowScope in
        if let alreadyMoved = validateMoveScopes(moveScopes) {
          return .error(.alreadyMoved(name: alreadyMoved))
        }
        return .init { env in
          var env = env
          for scope in moveScopes {
            for (name, isMoved) in scope {
              guard isMoved else { continue }
              env.bce.moveEnv.markAsMoved(name: name)
            }
          }
          for borrow in borrowScope {
            env.bce.borrowEnv.scopes[env.bce.borrowEnv.scopes.count - 1].insert(borrow)
          }
          return .pure((env, ()))
        }
      }
    }
    
    private static func performInParallel<A>(
      _ tasks: [() -> BorrowCheckM<A>],
      persistChanges: @escaping ([BorrowCheckerEnv.MoveEnv.Scope], BorrowCheckerEnv.BorrowEnv.Scope) -> BorrowCheckM<()>
    ) -> BorrowCheckM<[A]> {
      func performTask(_ task: @escaping () -> BorrowCheckM<A>) -> BorrowCheckM<(A, BorrowCheckerEnv.MoveEnv.Scope, BorrowCheckerEnv.BorrowEnv.Scope)> {
        let push: BorrowCheckM<()> = .init { env in
          var env = env
          env.bce = env.bce.pushScope()
          env.bce.moveEnv.writeToTheLastScope.append(true)
          return .pure((env, ()))
        }
        let pop: BorrowCheckM<()> = .init { env in
          var env = env
          env.bce = env.bce.popScope()
          _ = env.bce.moveEnv.writeToTheLastScope.popLast()
          return .pure((env, ()))
        }
        return push *> task().withState().map {
          ($0.0, $0.1.bce.moveEnv.scopes.last!, $0.1.bce.borrowEnv.scopes.last!)
        }^ <* pop
      }
      
      return tasks.map(performTask).sequence()^ >>- { results in
        let borrowCheckedTasks = results.map { $0.0 }
        let moveScopes = results.map { $0.1 }
        let borrowScope = results
          .map { $0.2 }
          .reduce(into: BorrowCheckerEnv.BorrowEnv.Scope()) { $0.formUnion($1) }
        return persistChanges(moveScopes, borrowScope) *> .pure(borrowCheckedTasks)^
      }
    }
  }
}

// MARK: - MoveEnv
extension BCState.ActionsBCE {
  enum MoveEnv {
    static func pushScope() -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.moveEnv = env.bce.moveEnv.pushScope()
        return .pure((env, ()))
      }
    }
    
    static func popScope() -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.moveEnv = env.bce.moveEnv.popScope()
        return .pure((env, ()))
      }
    }
    
    static func inNewScope<A>(_ f: () -> BorrowCheckM<A>) -> BorrowCheckM<A> {
      pushScope() *> f() <* popScope()
    }
    
    static func insertName(_ name: String) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.moveEnv.insert(name: name)
        return .pure((env, ()))
      }
    }
    
    static func insertNames(_ names: Set<String>) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        names.forEach { env.bce.moveEnv.insert(name: $0) }
        return .pure((env, ()))
      }
    }
    
    static func ensureNotAlreadyMoved(_ names: Set<String>) -> BorrowCheckM<()> {
      .init { env in
#if DEBUG
        if let movedName = env.bce.moveEnv.isMoved(any: names.sorted(by: <)) {
          return .raiseError(.init(backtrace: env.backtrace, error: .alreadyMoved(name: movedName)))
        }
#else
        if let movedName = env.bce.moveEnv.isMoved(any: names) {
          return .raiseError(.init(backtrace: env.backtrace, error: .alreadyMoved(name: movedName)))
        }
#endif
        return .pure((env, ()))
      }
    }
    
    static func ensureNotAlreadyMoved(_ name: String) -> BorrowCheckM<()> {
      .init { env in
        if env.bce.moveEnv.isMoved(name: name) {
          return .raiseError(.init(backtrace: env.backtrace, error: .alreadyMoved(name: name)))
        }
        return .pure((env, ()))
      }
    }
    
    static func markAsMoved(names: Set<String>) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.moveEnv.markAsMoved(names: names)
        return .pure((env, ()))
      }
    }
    
    static func markAsMoved(name: String) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.moveEnv.markAsMoved(name: name)
        return .pure((env, ()))
      }
    }
  }
}

// MARK: - BorrowEnv
extension BCState.ActionsBCE {
  enum BorrowEnv {
    static func insertName(_ name: String) -> BorrowCheckM<()> {
      .init { env in
        var env_ = env
        env_.bce.borrowEnv.insert(name: name)
        return .pure((env_, ()))
      }
    }
    
    static func insertNames(_ names: Set<String>) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        names.forEach { env.bce.borrowEnv.insert(name: $0) }
        return .pure((env, ()))
      }
    }
    
    static func ensureNotBorrowed(name: String) -> BorrowCheckM<()> {
      .init { env in
        guard !env.bce.borrowEnv.contains(name: name) else {
          return .raiseError(.init(
            backtrace: env.backtrace,
            error: .moveWhileBorrowed(name: name)
          ))
        }
        return .pure((env, ()))
      }
    }
    
    static func isBorrowed(name: String) -> BorrowCheckM<Bool> {
      .init { env in
      .pure((env, env.bce.borrowEnv.contains(name: name)))
      }
    }
    
    static func removeBorrow(name: String) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.borrowEnv.remove(name: name)
        return .pure((env, ()))
      }
    }
  }
  
  enum ActiveLifetimeEnv {
    static func push(lifetime: BCAst.Lifetime?) -> BorrowCheckM<()> {
      .init { env in
        var env = env
        env.bce.activeLiftimeStack.append(lifetime)
        return .pure((env, ()))
      }
    }
    
    static func pop() -> BorrowCheckM<()> {
      .init { env in
        var env = env
        _ = env.bce.activeLiftimeStack.popLast()
        return .pure((env, ()))
      }
    }
    
    static func performWork<A>(_ work: () -> BorrowCheckM<A>, with lifetime: BCAst.Lifetime?) -> BorrowCheckM<A> {
      push(lifetime: lifetime) *> work() <* pop()
    }
    
    static var current: BorrowCheckM<BCAst.Lifetime??>{
      .init { env in
        .pure((env, env.bce.activeLiftimeStack.last))
      }
    }
  }
}

// MARK: - EDE
extension BCState {
  enum ActionsEDE {
    static func addDependencies(
      _ dependencies: Set<String>,
      binding: BCAst.Binding.Case
    ) -> BorrowCheckM<()> {
      .init { env in
        let node = ExpressionDependencyEnv.Node.case(binding)
        var env = env
        if env.ede.scope[node] == nil {
          env.ede.scope[node] = dependencies
        } else {
          env.ede.scope[node]!.formUnion(dependencies)
        }
        return .pure((env, ()))
      }
    }
    
    static func dependencies(binding: BCAst.Binding.Case) -> BorrowCheckM<Set<String>> {
      .init { env in
        // TODO: - Checking whether the type is reference
        // should not be necessary. Check on other places as well.
        let dependencies = binding.type!.isReference
          ? .empty() // borrowing does not create a dependency
          : env.ede.scope[.case(binding)] ?? .empty()
        return .pure((env, dependencies))
      }
    }
    
    ///
    
    static func addDependencies(
      _ dependencies: Set<String>,
      for expresion: BCAst.Expression
    ) -> BorrowCheckM<()> {
      .init { env in
        let node = ExpressionDependencyEnv.Node.expression(expresion)
        var env = env
        if env.ede.scope[node] == nil {
          env.ede.scope[node] = dependencies
        } else {
          env.ede.scope[node]!.formUnion(dependencies)
        }
        return .pure((env, ()))
      }
    }
    
    static func dependencies(for expression: BCAst.Expression) -> BorrowCheckM<Set<String>> {
      .init { env in
        let dependencies: Set<String>
        switch expression {
        case let .name(_, identifier, _, _, _):
          if expression.type!.isReference {
            dependencies = .empty()
          } else if expression.type!.isArrow {
            let node = ExpressionDependencyEnv.Node.expression(expression)
            dependencies = env.ede.scope[node] ?? .empty()
          } else {
            dependencies = [identifier]
          }
        case _:
          dependencies = expression.type!.isReference
          ? .empty() // borrowing does not create a dependency
          : env.ede.scope[.expression(expression)] ?? .empty()
        }
        return .pure((env, dependencies))
      }
    }
    
    static func dependencies(for expressions: [BCAst.Expression]) -> BorrowCheckM<Set<String>> {
      expressions
        .map { dependencies(for: $0) }
        .sequence()
        .map { $0.reduce(into: Set()) { $0.formUnion($1) } }^
    }
    
    static func computeAndStoreDependencies(
      for expression: BCAst.Expression,
      parentExpression: BCAst.Expression,
      filter: Set<String>
    ) -> BorrowCheckM<()> {
      // TODO: - Optimise !!
      dependencies(for: expression) >>- { dependencies in
        guard !dependencies.isEmpty else {
          return .pure(())^
        }
        let filteredDependecies = dependencies.filter { !filter.contains($0) }
        return addDependencies(.init(filteredDependecies), for: parentExpression)
      }
    }
    
    static func computeAndStoreDependencies(
      for expressions: [BCAst.Expression],
      parentExpression: BCAst.Expression,
      filter: Set<String>
    ) -> BorrowCheckM<()> {
      // TODO: - Optimise !!
      dependencies(for: expressions) >>- { dependencies in
        guard !dependencies.isEmpty else {
          return .pure(())^
        }
        let filteredDependecies = dependencies.filter { !filter.contains($0) }
        return addDependencies(.init(filteredDependecies), for: parentExpression)
      }
    }
  }
}

// MARK: - IIRS
extension BCState {
  enum ActionsIIRS {
    static func performWorkInsideReference<A>(work: () -> BorrowCheckM<A>) -> BorrowCheckM<A> {
      let set: BorrowCheckM<()> = .init { env in
        var env = env
        env.isInsideReferenceStack.append(true)
        return .pure((env, ()))
      }
      let reset: BorrowCheckM<()> = .init { env in
        var env = env
        _ = env.isInsideReferenceStack.popLast()
        return .pure((env, ()))
      }
      return set *> work() <* reset
    }
    
    static var isInsideReference: BorrowCheckM<Bool> {
      .init { env in
        .pure((env, env.isInsideReferenceStack.last!))
      }
    }
  }
}
