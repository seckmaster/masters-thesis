//
//  BCMonad.swift
//  
//
//  Created by Toni K. Turk on 06/12/2021.
//

import Foundation
import Bow
import SwiftParsec

public typealias BorrowCheckM<A> = StateT<EitherPartial<BCError>, BCState, A>

public protocol BorrowCheckable {
  func borrowChecker() -> BorrowCheckM<Self>
}

public typealias BCAst = AST

public extension StateT where F == EitherPartial<BCError>, S == BCState, A == BCAst {
  func run() -> EitherOf<BCError, BCAst> {
    runA(.empty)
  }
}

public extension StateT where F == EitherPartial<BCError>, S == BCState {
  static func error(_ error: BorrowError) -> StateT<EitherPartial<BCError>, BCState, A> {
    .init { state in
      .raiseError(.init(backtrace: state.backtrace, error: error))
    }
  }
  
  static func backtrace<A: HasPosition>(_ hasPosition: A) -> BorrowCheckM<A> {
    .init { env in
      var env = env
      env.backtrace.trace.append(hasPosition)
      return .pure((env, hasPosition))
    }
  }
  
  func failWith(_ error: BorrowError) -> StateT<EitherPartial<BCError>, BCState, A> {
    self <|> .init { env in
      .raiseError(.init(backtrace: env.backtrace, error: error))^
    }
  }
  
  func mapError(_ f: @escaping (BorrowError) -> BorrowError) -> BorrowCheckM<A> {
    handleErrorWith {
      .raiseError(.init(backtrace: $0.backtrace, error: f($0.error)))
    }^
  }
}

public func >>-<A: HasPosition, B>(
  _ m: StateT<EitherPartial<BCError>, BCState, A>,
  _ f: @escaping (A) -> StateT<EitherPartial<BCError>, BCState, B>
) -> StateT<EitherPartial<BCError>, BCState, B> {
  m
    .flatMap(BorrowCheckM<A>.backtrace)
    .flatMap(f)^
}
