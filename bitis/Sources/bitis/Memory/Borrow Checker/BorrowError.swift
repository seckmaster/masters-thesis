//
//  BorrowError.swift
//  
//
//  Created by Toni K. Turk on 22/12/2021.
//

import Foundation

public typealias BCError = BacktraceErrorImpl<BorrowError>

public enum BorrowError: Error, Equatable, CustomStringConvertible {
  case cycleDetected([DependencyTreeEnv.Node])
  case alreadyMoved(name: String)
  case moveWhileBorrowed(name: String)
  case borrowFromMovedValue(name: String)
  case cannotMoveOutOfBorrow
  
  public var description: String {
    switch self {
    case .cannotMoveOutOfBorrow:
      return "Cannot move out of borrow!"
    case let .alreadyMoved(name):
      return "Variable '\(name)' already moved!"
    case let .borrowFromMovedValue(name):
      return "Variable '\(name)' already moved! Cannot borrow from a moved value!"
    case let .moveWhileBorrowed(name):
      return "Variable '\(name)' is borrowed! Cannot move a borrowed value!"
    case let .cycleDetected(nodes):
      return """
             A strong cycle detected!
             \(nodes.map { $0.description }.joined(separator: " -> "))
             """
    }
  }
}
