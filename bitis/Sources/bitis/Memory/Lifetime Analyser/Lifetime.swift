//
//  Lifetime.swift
//  
//
//  Created by Toni K. Turk on 31/07/2021.
//

import Foundation

public protocol ParentChildRelation {
  func isParent(of other: Self) -> Bool
  func isChild(of other: Self) -> Bool
  func commonParent(_ other: Self) -> Self?
  var parent: Self { get }
}

public protocol TreeSetAlgebra {
}

public struct Lifetime<ID: Hashable & ExpressibleByStringLiteral> {
  public let scope: Scope
  
  public init(scope: Scope) {
    self.scope = scope
  }
}

public extension Lifetime {
  enum Scope {
    case `static`(Int)
    case local(ID)
    case generic(ID)
    case composite([Lifetime])
  }
  
  static func `static`(id: Int) -> Lifetime {
    .init(scope: .static(id))
  }
  
  static func local(_ id: ID) -> Lifetime { .init(scope: .local(id)) }
  static func generic(_ id: ID) -> Lifetime { .init(scope: .generic(id)) }
  
  static func composite(lifetimes: [Lifetime]) -> Lifetime? {
    guard !lifetimes.isEmpty else { return nil }
    guard lifetimes.count > 1 else { return lifetimes.first! }
    return .init(scope: .composite(lifetimes))
  }
  
  static func composite(lifetimes: Lifetime...) -> Lifetime? {
    composite(lifetimes: Array(lifetimes))
  }
  
  static func composite(lifetimes: Lifetime?...) -> Lifetime? {
    composite(lifetimes: lifetimes.compactMap { $0 })
  }
}

public extension Lifetime {
  var isStatic: Bool {
    switch self.scope {
    case .static:
      return true
    case _:
      return false
    }
  }
  
  var isLocal: Bool {
    switch self.scope {
    case .local:
      return true
    case _:
      return false
    }
  }
  
  var isGeneric: Bool {
    switch self.scope {
    case .generic:
      return true
    case _:
      return false
    }
  }
  var decomposeLifetime: Set<Lifetime> {
    switch self.scope {
    case .composite(let lifetimes):
      return lifetimes.reduce(into: Set(arrayLiteral: self)) { $0.formUnion($1.decomposeLifetime) }
    case _:
      return .init(arrayLiteral: self)
    }
  }
}

public extension Lifetime where ID: CustomStringConvertible & ExpressibleByStringLiteral, ID.StringLiteralType == String {
  var identifier: ID {
    switch self.scope {
    case .local(let id),
         .generic(let id):
      return id
    case .static(let id):
      return .init(stringLiteral: "'STATIC @ \(id)")
    case .composite(let lifetimes):
      let identifiers = lifetimes.map { $0.identifier.description }.joined(separator: ",")
      return .init(stringLiteral: identifiers)
    }
  }
}

extension Lifetime where ID: Equatable {
  func hasDependency(on other: Lifetime, checkStaticId: Bool = false) -> Bool {
    switch (self.scope, other.scope) {
    case let (.composite(lifetimes1), .composite(lifetimes2)):
      return lifetimes1.contains { l1 in lifetimes2.contains { l1.hasDependency(on: $0) } }
    case let (.composite(lifetimes), .local),
         let (.local, .composite(lifetimes)),
         let (.composite(lifetimes), .generic),
         let (.generic, .composite(lifetimes)),
         let (.composite(lifetimes), .static),
         let (.static, .composite(lifetimes)):
      return lifetimes.contains { $0.hasDependency(on: self) }
    case let (.static(id1), .static(id2)) where checkStaticId:
      return id1 == id2
    case (.static, .static):
      return true
    case _:
      return self == other
    }
  }
}

extension Lifetime.Scope: Equatable where ID: Equatable {}
extension Lifetime.Scope: Hashable where ID: Hashable {}
extension Lifetime: Equatable where ID: Equatable {}
extension Lifetime: Hashable where ID: Hashable {}

extension Lifetime: CustomStringConvertible where ID: CustomStringConvertible {
  public var description: String {
    switch scope {
    case .static(let id):
      return "'STATIC @ \(id)"
    case .local(let id):
      return "'" + id.description
    case .generic(let id):
      return "∀\(id.description)"
    case .composite(let lifetimes):
      return "(" + lifetimes.map { $0.description }.joined(separator: ",") + ")"
    }
  }
}

extension Lifetime: PrettyStringConvertible where ID: PrettyStringConvertible {
  public var prettyDescription: String {
    switch self.scope {
    case .static(let id):
      return "'STATIC [\(id)]"
    case .local(let id):
      return "'" + id.prettyDescription
    case .generic(let id):
      return "∀\(id.prettyDescription)"
    case .composite(let lifetimes):
      return "(" + lifetimes.map { $0.prettyDescription }.joined(separator: ",") + ")"
    }
  }
}

extension Lifetime: ParentChildRelation where ID: ParentChildRelation & Comparable {
  public func isParent(of other: Lifetime) -> Bool {
    switch (self.scope, other.scope) {
    case (.static, _):
      return true
    case let (.local(left), .local(right)):
      return left.isParent(of: right) || (left.commonParent(right) != nil && left < right)
    case (_, .static):
      return false
    case (.generic, _),
         (_, .generic):
      return false
    case let (.composite(lifetimes), _):
      return lifetimes.allSatisfy { $0.isParent(of: other) }
    case _:
      return false
    }
  }
  
  public func isChild(of other: Lifetime<ID>) -> Bool {
    switch (self.scope, other.scope) {
    case (_, .static):
      return true
    case let (.local(left), .local(right)):
      return left.isChild(of: right)
    case (.static, _):
      return false
    case (.generic, _),
         (_, .generic):
      return false
    case (.composite, _), (_, .composite):
      return false
    }
  }
  
  public func commonParent(_ other: Lifetime<ID>) -> Lifetime<ID>? {
    switch (self.scope, other.scope) {
    case (.static(let id), _),
         (_, .static(let id)):
      return .static(id: id)
    case let (.local(left), .local(right)):
      return left.commonParent(right)
        .map { Lifetime(scope: .local($0)) }
    case (.generic, _),
         (_, .generic):
      return nil
    case (.composite, _), (_, .composite):
      return nil
    }
  }
  
  public var parent: Lifetime<ID> {
    switch self.scope{
    case .local(let id):
      return .local(id.parent)
    case .composite(let lifetimes):
      return .composite(lifetimes: lifetimes.map { $0.parent })!
    case _:
      return self
    }
  }
}

extension Lifetime: TreeSetAlgebra where ID: ParentChildRelation, ID: Comparable {
}

extension Lifetime: ExpressibleByExtendedGraphemeClusterLiteral where ID: ExpressibleByExtendedGraphemeClusterLiteral {
  public init(extendedGraphemeClusterLiteral value: ID.ExtendedGraphemeClusterLiteralType) {
    self = .init(scope: .local(.init(extendedGraphemeClusterLiteral: value)))
  }
}

extension Lifetime: ExpressibleByUnicodeScalarLiteral where ID: ExpressibleByUnicodeScalarLiteral {
  public init(unicodeScalarLiteral value: ID.UnicodeScalarLiteralType) {
    self = .init(scope: .local(.init(unicodeScalarLiteral: value)))
  }
}

extension Lifetime: ExpressibleByStringLiteral where ID: ExpressibleByStringLiteral {
  public init(stringLiteral value: ID.StringLiteralType) {
    self = .init(scope: .local(.init(stringLiteral: value)))
  }
}

/// ------

// MARK: - Lifetime Identifier

public struct LifetimeIdentifier: Equatable, Hashable {
  public let identifier: String
  
  public init(identifier: String) {
    self.identifier = identifier
  }
}

extension LifetimeIdentifier: Comparable {
  public static func < (lhs: LifetimeIdentifier, rhs: LifetimeIdentifier) -> Bool {
    lhs.identifier < rhs.identifier
  }
}

extension LifetimeIdentifier: ExpressibleByStringLiteral {
  public init(stringLiteral value: String) {
    self.identifier = value
  }
}

extension LifetimeIdentifier: CustomStringConvertible {
  public var description: String { identifier }
}

extension LifetimeIdentifier: PrettyStringConvertible {
  public var prettyDescription: String { description }
}

extension LifetimeIdentifier: ParentChildRelation {
  public func isParent(of other: LifetimeIdentifier) -> Bool {
    self != other && other.identifier.hasPrefix(self.identifier)
  }
  
  public func isChild(of other: LifetimeIdentifier) -> Bool {
    self != other && self.identifier.hasPrefix(other.identifier)
  }
  
  public func commonParent(_ other: LifetimeIdentifier) -> LifetimeIdentifier? {
    let prefix = self.identifier.commonPrefix(with: other.identifier)
    if prefix.isEmpty {
      return nil
    }
    return .init(identifier: prefix)
  }
  
  public var parent: LifetimeIdentifier {
    .init(identifier: String(identifier.dropLast()))
  }
}

public typealias DefaultLifetime = Lifetime<LifetimeIdentifier>
