//
//  LifetimeEnv+LifetimeMappingEnv.swift
//  
//
//  Created by Toni K. Turk on 01/01/2022.
//

import Foundation

extension LifetimeEnv {
  public struct LifetimeMappingEnv: HasScope {
    typealias Scope = [LAAst.Lifetime: LAAst.Lifetime]
    var scopes: [Scope]
    
    public func pushScope() -> LifetimeMappingEnv {
      var scopes = self.scopes
      scopes.append([:])
      return .init(scopes: scopes)
    }
    
    public func popScope() -> LifetimeMappingEnv {
      precondition(scopes.count > 1)
      var scopes = self.scopes
      _ = scopes.popLast()
      return .init(scopes: scopes)
    }
  }
}

extension LifetimeEnv.LifetimeMappingEnv {
  subscript(from: LAAst.Lifetime) ->  LAAst.Lifetime? {
    get {
      for scope in scopes.reversed() {
        guard let to = scope[from] else { return nil }
        return to
      }
      return nil
    }
    set {
      scopes[scopes.count - 1][from] = newValue
    }
  }
}
