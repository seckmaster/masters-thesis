//
//  LAState.swift
//  
//
//  Created by Toni K. Turk on 29/12/2021.
//

import Foundation
import Bow
import SwiftParsec

public struct LAState: HasScope {
  var lifetimeEnv: LifetimeEnv
  var localTypeEnv: LVE
  var backtrace: Backtrace
  var isInsideReferenceStack: [Bool]
  var lifetimeMappingEnv: LifetimeEnv.LifetimeMappingEnv
  
  static var empty: LAState {
    .init(
      lifetimeEnv: .static,
      localTypeEnv: .empty,
      backtrace: .init(),
      isInsideReferenceStack: [false],
      lifetimeMappingEnv: .init(scopes: [.empty()])
    )
  }
  
  public func pushScope() -> LAState {
    .init(
      lifetimeEnv: lifetimeEnv.pushScope(),
      localTypeEnv: localTypeEnv.pushScope(),
      backtrace: backtrace,
      isInsideReferenceStack: isInsideReferenceStack,
      lifetimeMappingEnv: lifetimeMappingEnv.pushScope()
    )
  }
  
  public func popScope() -> LAState {
    .init(
      lifetimeEnv: lifetimeEnv.popScope(),
      localTypeEnv: localTypeEnv.popScope(),
      backtrace: backtrace,
      isInsideReferenceStack: isInsideReferenceStack,
      lifetimeMappingEnv: lifetimeMappingEnv.popScope()
    )
  }
}

extension LAState {
  enum ActionsIIRS {
    static func inNewScope<A>(_ work: () -> LifetimeAnalyseM<A>) -> LifetimeAnalyseM<A> {
      let push: LifetimeAnalyseM<()> = .init { env in
        var env = env
        env.isInsideReferenceStack.append(false)
        return .pure((env, ()))
      }
      let pop: LifetimeAnalyseM<()> = .init { env in
        var env = env
        _ = env.isInsideReferenceStack.popLast()
        return .pure((env, ()))
      }
      return push *> work() <* pop
    }
    
    static func performWorkInsideReference<A>(work: () -> LifetimeAnalyseM<A>) -> LifetimeAnalyseM<A> {
      let set: LifetimeAnalyseM<()> = .init { env in
        var env = env
        env.isInsideReferenceStack.append(true)
        return .pure((env, ()))
      }
      let reset: LifetimeAnalyseM<()> = .init { env in
        var env = env
        _ = env.isInsideReferenceStack.popLast()
        return .pure((env, ()))
      }
      return set *> work() <* reset
    }
    
    static var isInsideReference: LifetimeAnalyseM<Bool> {
      .init { env in
        .pure((env, env.isInsideReferenceStack.last!))
      }
    }
  }
}

extension LAState {
  enum ActionsLME {
    static func associateLifetime(_ lifetime: LAAst.Lifetime?, with targetLifetime: LAAst.Lifetime?) -> LifetimeAnalyseM<()> {
      .init { env in
        guard let lifetime = lifetime?.simplify, let targetLifetime = targetLifetime?.simplify else { return .pure((env, ())) }
        
        var env = env
        if let existingLifetime = env.lifetimeMappingEnv[lifetime] {
          guard targetLifetime == existingLifetime else {
            return .raiseError(.init(backtrace: env.backtrace, error: .incompatibleLifetimes(expected: existingLifetime, got: targetLifetime)))
          }
        } else {
          env.lifetimeMappingEnv[lifetime] = targetLifetime
        }
        return .pure((env, ()))
      }
    }
  }
}
