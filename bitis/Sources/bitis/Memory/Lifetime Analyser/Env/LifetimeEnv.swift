//
//  LifetimeEnv.swift
//  
//
//  Created by Toni K. Turk on 31/07/2021.
//

import Foundation
import Bow
import SwiftParsec

public struct LifetimeEnv {
  public typealias Lifetime = AST.Lifetime
  
  internal private(set) var scopes: [Scope]
}

internal extension LifetimeEnv {
  struct Scope {
    let bindingsCount: Int
    let parentLifetime: AST.Lifetime
    let mapping: [String: (lifetime: Lifetime, borrowLifetime: Lifetime?)]
  }
}

internal func +(_ lhs: LifetimeEnv.Scope, _ x: Int) -> LifetimeEnv.Scope {
  .init(
    bindingsCount: lhs.bindingsCount + 1,
    parentLifetime: lhs.parentLifetime,
    mapping: lhs.mapping
  )
}

public extension LifetimeEnv {
  static var `static`: LifetimeEnv {
    .init(scopes: [
      .init(bindingsCount: 0, parentLifetime: .static(id: 0), mapping: [:])
    ])
  }
  
  func current() -> Lifetime {
    scopes.last!.parentLifetime.next(scopes.last!.bindingsCount)
  }
  
  func parent() -> Lifetime {
    switch scopes.last!.parentLifetime.scope {
    case .static:
      return .static(id: scopes.last!.bindingsCount)
    case _:
      return scopes.last!.parentLifetime
    }
  }
  
  mutating func next() -> Lifetime {
    guard let current = scopes.last else {
      fatalError("LifetimeEnv: no scopes!")
    }
    scopes[scopes.endIndex - 1] = current + 1
    let next = scopes[scopes.endIndex - 1].parentLifetime.next(scopes[scopes.endIndex - 1].bindingsCount - 1)
    return next
  }
  
  mutating func bind(lifetime: (lifetime: Lifetime, borrowLifetime: Lifetime?), for name: String) {
    guard let current = scopes.last else {
      fatalError("LifetimeEnv: no scopes!")
    }
//    precondition(current.mapping[name].map { $0.lifetime == lifetime.lifetime } ?? true)
    
    var mapping = current.mapping
    mapping[name] = lifetime
    scopes[scopes.endIndex - 1] = .init(
      bindingsCount: current.bindingsCount,
      parentLifetime: current.parentLifetime,
      mapping: mapping
    )
  }
  
  mutating func bind(inPreviousScope lifetime: (lifetime: Lifetime, borrowLifetime: Lifetime?), for name: String) {
    let current = scopes[scopes.count - 2]
    var mapping = current.mapping
    mapping[name] = lifetime
    scopes[scopes.endIndex - 1] = .init(
      bindingsCount: current.bindingsCount,
      parentLifetime: current.parentLifetime,
      mapping: mapping
    )
  }
  
  func lifetime(for name: String) -> (lifetime: Lifetime, borrowLifetime: Lifetime?)? {
    for scope in scopes.reversed() {
      guard let lifetimePair = scope.mapping[name] else { continue }
      return lifetimePair
    }
    return nil
  }
}

extension LifetimeEnv: HasScope {
  public func pushScope() -> LifetimeEnv {
    var env = self
    let newScope = Scope(
      bindingsCount: 0,
      parentLifetime: env.scopes.last!.parentLifetime.push(env.scopes.last!.bindingsCount),
      mapping: [:]
    )
    env.scopes.append(newScope)
    return env
  }
  
  public func popScope() -> LifetimeEnv {
    guard scopes.count > 1 else {
      fatalError("LifetimeEnv.popScope: not allowed to pop when count < 2!")
    }
    var env = self
    _ = env.scopes.popLast()
    return env
  }
}

internal extension AST.Lifetime {
  func next(_ i: Int) -> Lifetime {
    switch scope {
    case .static:
      return .static(id: i)
    case let .local(id):
      return .init(scope: .local(.init(identifier: id.identifier + String(i.ascii))))
    case .generic:
      return self // TODO: - ???
    case .composite:
      return self // TODO: - ???
    }
  }
  
  func push(_ i: Int) -> AST.Lifetime {
    switch scope {
    case .static:
      return .init(scope: .local(.init(identifier: String(i.ascii))))
    case let .local(id):
      return .init(scope: .local(.init(identifier: id.identifier + String(i.ascii))))
    case .generic:
      return self // TODO: - ???
    case .composite:
      return self // TODO: - ???
    }
  }
}

extension Int {
  var ascii: UnicodeScalar {
    let ascii = UInt32(self) % 25 + UnicodeScalar("a").value
    let scalar = UnicodeScalar(ascii)!
    return scalar
  }
}

// MARK: - Monadic actions
public extension LifetimeEnv {
  enum Actions {
    static var parent: LifetimeAnalyseM<Lifetime> {
      .init {
        .pure(($0, $0.lifetimeEnv.parent()))
      }
    }
    
    static var current: LifetimeAnalyseM<Lifetime> {
      .init {
        .pure(($0, $0.lifetimeEnv.current()))
      }
    }
    
    static var next: LifetimeAnalyseM<Lifetime> {
      .init {
        var env = $0
        let lifetime = env.lifetimeEnv.next()
        return .pure((env, lifetime))
      }
    }
    
    static func bind(lifetime: (lifetime: Lifetime, borrowLifetime: Lifetime?), for name: String) -> LifetimeAnalyseM<()> {
      .init {
        var env = $0
        env.lifetimeEnv.bind(lifetime: lifetime, for: name)
        return .pure((env, ()))
      }
    }
    
    static func bind(inPreviousScope lifetime: (lifetime: Lifetime, borrowLifetime: Lifetime?), for name: String) -> LifetimeAnalyseM<()> {
      .init {
        var env = $0
        env.lifetimeEnv.bind(inPreviousScope : lifetime, for: name)
        return .pure((env, ()))
      }
    }
    
    static func lifetime(for name: String) -> LifetimeAnalyseM<(lifetime: Lifetime, borrowLifetime: Lifetime?)?> {
      .init {
        .pure(($0, $0.lifetimeEnv.lifetime(for: name)))
      }
    }
    
    static func lifetimeOrRaise(for name: String) -> LifetimeAnalyseM<(lifetime: Lifetime, borrowLifetime: Lifetime?)> {
      .init {
        guard let lifetime = $0.lifetimeEnv.lifetime(for: name) else {
          fatalError("LifetimeEnv: lifetime not found for \"\(name)\"!")
        }
        return .pure(($0, lifetime))
      }
    }
    
    // MARK: - Scopes
    static func pushScope() -> LifetimeAnalyseM<()> {
      .init {
        .pure(($0.pushScope(), ()))
      }
    }
    
    static func popScope() -> LifetimeAnalyseM<()> {
      .init {
        .pure(($0.popScope(), ()))
      }
    }
    
    static func inNewScope<A>(_ f: @autoclosure () -> LifetimeAnalyseM<A>) -> LifetimeAnalyseM<A> {
      pushScope() *> f() <* popScope()
    }
    
    static func inNewScope<A>(_ f: () -> LifetimeAnalyseM<A>) -> LifetimeAnalyseM<A> {
      pushScope() *> f() <* popScope()
    }
    
    // MARK: - Local Type Env
    
    static func bind(type: TCType, for name: String) -> LifetimeAnalyseM<()> {
      .init { env in
        var env = env
        env.localTypeEnv = env.localTypeEnv.bind(name: name, with: type)
        return .pure((env, ()))
      }
    }
    
    static func type(for name: String) -> LifetimeAnalyseM<TCType> {
      .init { env in
        guard let type = env.localTypeEnv.queryType(for: name, currentScopeOnly: false) else {
          fatalError()
        }
        return .pure((env, type))
      }
    }
    
    static func typeOptional(for name: String) -> LifetimeAnalyseM<TCType?> {
      .init { env in
        let type = env.localTypeEnv.queryType(for: name, currentScopeOnly: false)
        return .pure((env, type))
      }
    }
  }
}
