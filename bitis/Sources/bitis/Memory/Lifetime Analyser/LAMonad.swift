//
//  LAMonad.swift
//  
//
//  Created by Toni K. Turk on 31/07/2021.
//

import Foundation
import Bow
import SwiftParsec

public typealias LifetimeAnalyseM<A> = StateT<EitherPartial<LAError>, LAState, A>

public protocol LifetimeAnalysable {
  func lifetimeAnalyser() -> LifetimeAnalyseM<Self>
  var lifetime: LAAst.Lifetime? { get }
}

public extension LifetimeAnalysable {
  var lifetime: LAAst.Lifetime? { nil }
}

public typealias LAAst = AST

public extension StateT where F == EitherPartial<LAError>, S == LAState, A == LAAst {
  func run() -> EitherOf<LAError, LAAst> {
    runA(.empty)
  }
}

public extension StateT where F == EitherPartial<LAError>, S == LAState {
  static func error(_ error: LifetimeError) -> StateT<EitherPartial<LAError>, LAState, A> {
    .init { state in
      .raiseError(.init(backtrace: state.backtrace, error: error))
    }
  }
  
  static func backtrace<A: HasPosition>(_ hasPosition: A) -> LifetimeAnalyseM<A> {
    .init { env in
      var env = env
      env.backtrace.trace.append(hasPosition)
      return .pure((env, hasPosition))
    }
  }
}

public func >>-<A: HasPosition, B>(
  _ m: StateT<EitherPartial<LAError>, LAState, A>,
  _ f: @escaping (A) -> StateT<EitherPartial<LAError>, LAState, B>
) -> StateT<EitherPartial<LAError>, LAState, B> {
  m
    .flatMap(LifetimeAnalyseM<A>.backtrace)
    .flatMap(f)^
}
