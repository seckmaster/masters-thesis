//
//  LAError.swift
//  
//
//  Created by Toni K. Turk on 22/12/2021.
//

import Foundation

public typealias LAError = BacktraceErrorImpl<LifetimeError>

public enum LifetimeError: Error, Equatable, CustomStringConvertible {
  case incompatibleLifetimes(expected: LAAst.Lifetime, got: LAAst.Lifetime)
  case cannotReference(name: String)
  case cannotReferenceLiteral
  case cannotReferenceLocalValue
  case referenceWithoutLifetimeSpecifier
  case referenceWithLifetimeSpecifier
  
  public var description: String {
    switch self {
    case .cannotReferenceLocalValue:
      return "Cannot reference a value defined in local scope. The reference would outlive the referent!"
    case .cannotReference(let name):
      return "Cannot reference '\(name)'!"
    case .cannotReferenceLiteral:
      return "Cannot reference a literal!"
    case let .incompatibleLifetimes(expected, got):
      return "Lifetime '\(got.description)' is not compatible with lifetime '\(expected)'!"
    case .referenceWithLifetimeSpecifier:
      return "A lifetime specifier is not allowed in this context!"
    case .referenceWithoutLifetimeSpecifier:
      return "A lifetime specifier is expected in this context!"
    }
  }
}
