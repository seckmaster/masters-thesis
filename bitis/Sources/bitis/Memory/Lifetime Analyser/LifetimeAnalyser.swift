//
//  LifetimeAnalyser.swift
//  
//
//  Created by Toni K. Turk on 31/07/2021.
//

import Foundation
import Bow
import SwiftParsec

// MARK: - AST
extension LAAst: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<LAAst> {
    bindings
      .map { $0.lifetimeAnalyser() <* LifetimeEnv.Actions.next }
      .sequence()
      .map { .init(bindings: $0) }^
  }
}

// MARK: - Binding
extension LAAst.Binding: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<LAAst.Binding> {
    switch self {
    case let .data(position, name, params, constructors, type):
      let constructorsM = constructors
        .map { LifetimeEnv.Actions.bind(type: type!, for: $0.name) }
        .sequence()^
      let lifetimesM = constructors
        .map { constructor in
          LifetimeEnv.Actions.next >>- { lifetime in
            LifetimeEnv.Actions.bind(
              lifetime: (lifetime, nil),
              for: constructor.name
            ) >>- {
              let constr = LAAst.Binding.Constructor(
                position: constructor.position,
                name: constructor.name,
                types: constructor.types,
                type: constructor.type,
                lifetime: lifetime
              )
              return .pure(constr)^
            }
          }
        }
        .sequence()
        .map {
          LAAst.Binding.data(
            position: position,
            name: name,
            params: params,
            constructors: $0,
            tcType: type
          )
        }^
      return LifetimeEnv.Actions.bind(type: type!, for: name) *> constructorsM *> lifetimesM
    case let .prototype(_, name, type, tcType):
      guard !tcType!.isArrow || type.ensureAllReferencesHaveLifetimeSpecifiers else {
        return .error(.referenceWithoutLifetimeSpecifier)
      }
      guard tcType!.isArrow || type.ensureNoReferenceHasLifetimeSpecifiers else {
        return .error(.referenceWithLifetimeSpecifier)
      }
      return LifetimeEnv.Actions.lifetime(for: name) >>- { lifetime in
        guard lifetime == nil else { return .pure(self)^ }
        return LifetimeEnv.Actions.next >>- { current in
          LifetimeEnv.Actions.bind(lifetime: (current, nil), for: name) *> .pure(self)^
        }
      }
    // 1st phase
    case let .binding(position, name, cases, type, nil, nil):
      return cases.lifetimeAnalyser() >>- { cases in
        let lifetime = LAAst.Lifetime
          .composite(lifetimes: cases.compactMap { $0.lifetime })!
          .simplify
        let borrowLifetime = LAAst.Lifetime
          .composite(lifetimes: cases.compactMap { $0.body.lifetime } )?
          .removeGenericLifetimes?
          .simplify
        
        let binding = LAAst.Binding.binding(
          position: position,
          name: name,
          cases: cases,
          tcType: type,
          lifetime: lifetime,
          borrowLifetime: borrowLifetime
        )
        return LifetimeEnv.Actions.bind(
          lifetime: (lifetime, borrowLifetime),
          for: name
        ) *> .pure(binding)^
      }
    // 2nd phase
    case let .binding(position, name, cases, type, _, _):
      let la = cases.map { $0.secondPassLifetimeAnalyser() }.sequence()^
      return la >>- { cases in
        let lifetime = LAAst.Lifetime
          .composite(lifetimes: cases.compactMap { $0.lifetime })!
          .simplify
        let borrowLifetime = LAAst.Lifetime
          .composite(lifetimes: cases.compactMap { $0.body.lifetime } )?
          .removeGenericLifetimes?
          .simplify
        let binding = LAAst.Binding.binding(
          position: position,
          name: name,
          cases: cases,
          tcType: type,
          lifetime: lifetime,
          borrowLifetime: borrowLifetime)
        return LifetimeEnv.Actions.bind(
          lifetime: (lifetime, borrowLifetime),
          for: name
        ) *> .pure(binding)^
      }
    case let .print(position, expression, type):
      return expression.lifetimeAnalyser() >>- { expression in
        .pure(.print(
          position: position,
          expression: expression,
          tcType: type
        ))^
      }
    case let .include(module):
      return .pure(.include(module: module))^
    }
  }
  
  public var lifetime: LAAst.Lifetime? {
    switch self {
    case .prototype,
         .data,
         .print:
      return nil
    case let .binding(_, _, _, _, lifetime, _):
      return lifetime
    case .include:
      fatalError()
    }
  }
}

// TODO: - !!!!!
var activeLifetime: LAAst.Lifetime?

// MARK: - Case
extension LAAst.Binding.Case: LifetimeAnalysable {
  // 1st pass
  public func lifetimeAnalyser() -> LifetimeAnalyseM<LAAst.Binding.Case> {
    LifetimeEnv.Actions.parent >>- { parentLifetime in
      LifetimeEnv.Actions.inNewScope {
        LAState.ActionsIIRS.inNewScope {
          LifetimeEnv.Actions.current >>- { currentLifetime in
            activeLifetime = type!.curriedArrow.last!.lifetime
            return parameters.lifetimeAnalyser() >>- { parameters in
              body.lifetimeAnalyser() >>- { body in
                activeLifetime = nil
                return LifetimeEnv.Actions.lifetime(for: name) >>- { existingLifetime in
                  type!.updateLifetimeSpecifiersM() >>- { type in
                    let existingOrCurrentLifetime = existingLifetime?.lifetime ?? (parentLifetime.isStatic ? parentLifetime : currentLifetime)
                    
                    guard let lifetime = body.lifetime?.simplify else {
                      let `case` = LAAst.Binding.Case(
                        name: name,
                        position: position,
                        parameters: parameters,
                        body: body,
                        type: type,
                        lifetime: existingOrCurrentLifetime)
                      return .pure(`case`)^
                    }
                    
                    let isSelfBorrow = lifetime == existingLifetime?.lifetime
                    let isValid = lifetime.isParent(of: currentLifetime) || isSelfBorrow
                    
                    guard isValid else {
                      return .error(.cannotReferenceLocalValue)
                    }
                    
                    let body = isSelfBorrow ? body.withLifetime(nil) : body
                    let `case` = LAAst.Binding.Case(
                      name: name,
                      position: position,
                      parameters: parameters,
                      body: body,
                      type: type,
                      lifetime: existingOrCurrentLifetime)
                    return .pure(`case`)^
                  }
                }
              }
            }
          }
        }
      }
    }
  }
  
  // 2nd pass
  public func secondPassLifetimeAnalyser() -> LifetimeAnalyseM<LAAst.Binding.Case> {
    LifetimeEnv.Actions.inNewScope {
      LAState.ActionsIIRS.inNewScope {
        LifetimeEnv.Actions.lifetime(for: name) >>- { existingLifetime in
          activeLifetime = type!.curriedArrow.last!.lifetime
          return parameters.lifetimeAnalyser() >>- { parameters in
            activeLifetime = nil
            return body.lifetimeAnalyser() >>- { body in
              let isSelfBorrow = body.lifetime?.simplify == existingLifetime?.lifetime
              let body = isSelfBorrow ? body.withLifetime(nil) : body
              let `case` = LAAst.Binding.Case(
                name: name,
                position: position,
                parameters: parameters,
                body: body,
                type: type,
                lifetime: lifetime)
              return .pure(`case`)^
            }
          }
        }
      }
    }
  }
}

// MARK: - Expression
extension LAAst.Expression: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<LAAst.Expression> {
    switch self {
    case .literal:
      return .pure(self)^
    case let .binary(expr):
      return expr
        .lifetimeAnalyser()
        .map(LAAst.Expression.binary)^
    // MARK: - Lambda
    case let .lambda(position, parameter, body, type, nil):
      return LifetimeEnv.Actions.inNewScope {
        parameter.lifetimeAnalyser() >>- { parameter in
          body.lifetimeAnalyser() >>- { body in
            let lambda = LAAst.Expression.lambda(
              position: position,
              parameter: parameter,
              body: body,
              tcType: type,
              lifetime: body.lifetime)
            return .pure(lambda)^
          }
        }
      }
    case .lambda:
      return .pure(self)^
    case let .tuple(_, expressions, _, nil) where expressions.isEmpty:
      return .pure(self)^
    case let .tuple(position, expressions, type, nil):
      return expressions.lifetimeAnalyser() >>- { expressions in
        let tuple = LAAst.Expression.tuple(
          position: position,
          expressions: expressions,
          tcType: type,
          lifetime: .composite(lifetimes: expressions.compactMap(\.lifetime))
        )
        return .pure(tuple)^
      }
    case .tuple:
      return .pure(self)^
    // MARK: - Name
    case let .name(position, identifier, type, _, nil):
      return LifetimeEnv.Actions.lifetimeOrRaise(for: identifier) >>- { lifetimePair in
        LAState.ActionsIIRS.isInsideReference >>- { isInsideReference in
          let lifetime = isInsideReference
            ? lifetimePair.lifetime
            : lifetimePair.borrowLifetime
          let name = LAAst.Expression.name(
            position: position,
            identifier: identifier,
            tcType: type,
            lifetime: lifetime,
            bindingLifetime: lifetimePair.lifetime
          )
          return .pure(name)^
        }
      }
    case let .name(position, identifier, type, _, .some):
      return LifetimeEnv.Actions.lifetimeOrRaise(for: identifier) >>- { lifetimePair in
        LAState.ActionsIIRS.isInsideReference >>- { isInsideReference in
          let lifetime = isInsideReference ? lifetimePair.lifetime : lifetimePair.borrowLifetime
          let name = LAAst.Expression.name(
            position: position,
            identifier: identifier,
            tcType: type,
            lifetime: lifetime,
            bindingLifetime: lifetimePair.lifetime
          )
          return .pure(name)^
        }
      }
    case let .list(position, expressions, type, _):
      return expressions.lifetimeAnalyser() >>- { expressions in
        let list = LAAst.Expression.list(
          position: position,
          expressions: expressions,
          tcType: type,
          lifetime: .composite(lifetimes: expressions.compactMap(\.lifetime)))
        return .pure(list)^
      }
      // MARK: - Let
    case let .let(position, bindings, expression, type, _):
      return LifetimeEnv.Actions.current >>- { lifetime in
        LifetimeEnv.Actions.inNewScope {
          func bindingsM1(_ bindings: [LAAst.Binding]) -> LifetimeAnalyseM<[LAAst.Binding]> {
            bindings.map {
              $0.lifetimeAnalyser() >>- {
                LifetimeEnv.Actions.next *> .pure($0)^
              }
            }.sequence()^
          }
          func bindingsM2(_ bindings: [LAAst.Binding]) -> LifetimeAnalyseM<[LAAst.Binding]> {
            bindings.map {
              $0.lifetimeAnalyser() >>- {
                .pure($0)^
              }
            }.sequence()^
          }
          return bindingsM1(bindings) >>- { bindings in
            bindingsM2(bindings) >>- { bindings in
              expression.lifetimeAnalyser() >>- { expression in
                let `let` = LAAst.Expression.let(
                  position: position,
                  bindings: bindings,
                  expression: expression,
                  tcType: type,
                  lifetime: expression.lifetime)
                return .pure(`let`)^
              }
            }
          }
        }
      }
    // MARK: - Case
    case let .case(position, expression, matches, type, _):
      return expression.lifetimeAnalyser() >>- { expression in
        matches.lifetimeAnalyser() >>- { matches in
          var allLifetimes = matches
            .compactMap { $0.expression.lifetime }
          expression.lifetime.map { allLifetimes.append($0) }
          let `case` = LAAst.Expression.case(
            position: position,
            expression: expression,
            matches: matches,
            tcType: type,
            lifetime: .composite(lifetimes: allLifetimes)
          )
          return .pure(`case`)^
        }
      }
    case .ref(_, .literal, _, _):
      return .error(.cannotReferenceLiteral)
    case .ref(_, .tuple(_, let expressions, _, nil), _, _) where expressions.isEmpty:
      return .error(.cannotReferenceLiteral)
    case let .ref(position, expression, type, _):
      return LAState.ActionsIIRS.performWorkInsideReference {
        expression.lifetimeAnalyser() >>- { expression in
          let ref = LAAst.Expression.ref(
            position: position,
            expression: expression,
            tcType: type,
            lifetime: expression.lifetime)
          return .pure(ref)^
        }
      }
    case let .deref(position, expression, type, _):
      return expression.lifetimeAnalyser() >>- { expression in
        let deref = LAAst.Expression.deref(
          position: position,
          expression: expression,
          tcType: type,
          lifetime: expression.lifetime
        )
        return .pure(deref)^
      }
    }
  }
}

// MARK: - Binary
extension LAAst.Expression.Binary: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<AST.Expression.Binary> {
    switch self {
    case let .arithmetic(position, `operator`, left, right, type, _):
      return left.lifetimeAnalyser() >>- { left in
        right.lifetimeAnalyser() >>- { right in
          .pure(
            .arithmetic(
              position: position,
              operator: `operator`,
              left: left,
              right: right,
              tcType: type,
              lifetime: .composite(lifetimes: left.lifetime, right.lifetime)
            )
          )^
        }
      }
    case let .logical(position, `operator`, left, right, type, _):
      return left.lifetimeAnalyser() >>- { left in
        right.lifetimeAnalyser() >>- { right in
          .pure(
            .logical(
              position: position,
              operator: `operator`,
              left: left,
              right: right,
              tcType: type,
              lifetime: .composite(lifetimes: left.lifetime, right.lifetime)
            )
          )^
        }
      }
    case let .ifThenElse(position, condition, `true`, `false`, type, _):
      return condition.lifetimeAnalyser() >>- { condition in
        `true`.lifetimeAnalyser() >>- { `true` in
          `false`.lifetimeAnalyser() >>- { `false` in
            .pure(
              .ifThenElse(
                position: position,
                condition: condition,
                true: `true`,
                false: `false`,
                tcType: type,
                lifetime: .composite(lifetimes: condition.lifetime, `true`.lifetime, `false`.lifetime)
              )
            )^
          }
        }
      }
    case let .concat(position, left, right, type, _):
      return left.lifetimeAnalyser() >>- { left in
        right.lifetimeAnalyser() >>- { right in
          .pure(
            .concat(
              position: position,
              left: left,
              right: right,
              tcType: type,
              lifetime: .composite(lifetimes: left.lifetime, right.lifetime)
            )
          )^
        }
      }
    // MARK: - Application
    case let .application(position, fn, originalArg, _, _):
      let curriedArrow = fn.type!.curriedArrow
      let argType = curriedArrow[0]
      let originalArgLifetime = curriedArrow.first!.lifetime
      return fn.lifetimeAnalyser() >>- { fn in
        originalArg.lifetimeAnalyser() >>- { arg in
//          LAState.ActionsIIRS.performWorkInsideReference { // `incompatibleLifetimes`
//            originalArg.lifetimeAnalyser() >>- { arg2 in
//              validateFunctionArgumentLifetime(expectedLifetime: originalArgLifetime?.simplify, argLifetime: arg.lifetime?.simplify) >>- {
//                LAState.ActionsLME.associateLifetime(originalArgLifetime, with: arg.lifetime) >>- {
                  
                  let intersection = lifetimeIntersection(
                    returnTypeLifetime: curriedArrow.last!.lifetime,
                    argTypeLifetime: argType.lifetime?.simplify,
                    givenLifetime: arg.lifetime
                  )
                  
                  let application = LAAst.Expression.Binary.application(
                    position: position,
                    fn: fn,
                    arg: arg,
                    tcType: type!.replaceLifetimeSpecifier(
                      originalArgLifetime,
                      with: intersection),
                    lifetime: .composite(lifetimes: fn.lifetime, intersection)
                  )
                  return .pure(application)^
//                }
//              }
//            }
//          }
        }
      }
    }
  }
}

// MARK: - Match
extension LAAst.Expression.Match: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<LAAst.Expression.Match> {
    pattern.lifetimeAnalyser() >>- { pattern in
      expression.lifetimeAnalyser() >>- { expression in
        let match = LAAst.Expression.Match(
          position: position,
          pattern: pattern,
          expression: expression
        )
        return .pure(match)^
      }
    }
  }
}

// MARK: - Pattern
extension LAAst.Pattern: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<AST.Pattern> {
    switch self {
    case let .identifier(position, identifier, type, _):
      return LifetimeEnv.Actions.next >>- { lifetime in
        let borrowLifetime = activeLifetime
          .flatMap { type!.lifetime?.hasDependency(on: $0) }
          .map { $0 ? nil : lifetime } ?? nil
        return LifetimeEnv.Actions.bind(
          lifetime: (lifetime, borrowLifetime),
          for: identifier
        )
        .map {
          .identifier(
            position: position,
            identifier: identifier,
            tcType: type,
            lifetime: lifetime
          )
        }^
      }
    case let .list(.cons(position, head, tail, type, _)):
      return head.lifetimeAnalyser() >>- { head in
        tail.lifetimeAnalyser() >>- { tail in
          let list = LAAst.Pattern.list(.cons(
            position: position,
            head: head,
            tail: tail,
            tcType: type,
            lifetime: .composite(lifetimes: head.lifetime, tail.lifetime)))
          return .pure(list)^
        }
      }
    case .list(.empty):
      return .pure(self)^
    case let .tuple(position, patterns, type, _):
      return patterns.lifetimeAnalyser() >>- { patterns in
        let tuple = LAAst.Pattern.tuple(
          position: position,
          patterns: patterns,
          tcType: type,
          lifetime: .composite(lifetimes: patterns.compactMap { $0.lifetime }))
        return .pure(tuple)^
      }
    case let .deconstruct(position, constructor, patterns, type, _):
      return patterns.lifetimeAnalyser() >>- { patterns in
        let deconstruct = LAAst.Pattern.deconstruct(
          position: position,
          constructor: constructor,
          patterns: patterns,
          tcType: type,
          lifetime: type!.lifetime)
        return .pure(deconstruct)^
      }
    case let .ref(position, pattern, type, _):
      return pattern.lifetimeAnalyser() >>- { pattern in
        let ref = LAAst.Pattern.ref(
          position: position,
          pattern: pattern,
          tcType: type,
          lifetime: pattern.lifetime)
        return .pure(ref)^
      }
    case .literal, .wildcard:
      return .pure(self)^
    }
  }
  
  public var lifetime: LAAst.Lifetime? {
    switch self {
    case .identifier(_, _, _, let lifetime),
         .tuple(_, _, _, let lifetime),
         .literal(_, _, _, let lifetime),
         .ref(_, _, _, let lifetime),
         .deconstruct(_, _, _, _, let lifetime),
         .list(.cons(_, _, _, _, let lifetime)),
         .list(.empty(_, _, let lifetime)):
      return lifetime
    case _:
      return nil
    }
  }
}

// MARK: - Helpers

func lifetimeIntersection(returnTypeLifetime: LAAst.Lifetime?, argTypeLifetime: LAAst.Lifetime?, givenLifetime: LAAst.Lifetime?) -> LAAst.Lifetime? {
  guard let returnTypeLifetime = returnTypeLifetime, let argTypeLifetime = argTypeLifetime, let givenLifetime = givenLifetime else {
    return nil
  }
  
  let decomposed = returnTypeLifetime.decomposeLifetime
  
  func intersection(l1: LAAst.Lifetime, l2: LAAst.Lifetime) -> LAAst.Lifetime? {
    guard l1 != l2 else { return l1 }
    switch (l1.scope, l2.scope) {
    case (.generic, _):
      return decomposed.contains(l1) ? l2 : nil
    case (.composite(let ls1), .composite(let ls2)):
      precondition(ls1.count == ls2.count)
      return .composite(lifetimes: zip(ls1, ls2).compactMap { intersection(l1: $0, l2: $1) })
    case (.composite(let lifetimes), .local):
      return .composite(lifetimes: lifetimes.compactMap { intersection(l1: $0, l2: l2) })
    case (.local, .composite(let lifetimes)):
      return .composite(lifetimes: lifetimes.compactMap { intersection(l1: $0, l2: l1) })
    case (.local, .local):
      warning("This is due to an implementation error!")
      return l1.commonParent(l2)
    case let (.static(id1), .static(id2)):
      return .static(id: max(id1, id2))
    case _:
      fatalError()
    }
  }
  
  return intersection(l1: argTypeLifetime, l2: givenLifetime)
}

extension Array: LifetimeAnalysable where Element: LifetimeAnalysable {
  public func lifetimeAnalyser() -> LifetimeAnalyseM<[Element]> {
    map { $0.lifetimeAnalyser() }.sequence()^
  }
}

func validateFunctionArgumentLifetime(
  expectedLifetime: LAAst.Lifetime?,
  argLifetime: LAAst.Lifetime?
) -> LifetimeAnalyseM<()> {
  func validate(expected: LAAst.Lifetime?, arg: LAAst.Lifetime?) -> Bool {
    switch (expected?.scope, arg?.scope) {
    case (.static?, .static?),
         (.generic?, _),
         (nil, _):
      return true
    case (.composite(let lifetimes)?, _):
      return lifetimes.allSatisfy {
        validate(expected: $0, arg: arg)
      }
    case _:
      return false
    }
  }
  
  guard validate(expected: expectedLifetime, arg: argLifetime) else {
    return .error(.incompatibleLifetimes(
      expected: expectedLifetime!,
      got: argLifetime!
    ))
  }
  return .pure(())^
}

extension LAAst.AType {
  var ensureAllReferencesHaveLifetimeSpecifiers: Bool {
    switch self {
    case .ref(_, _, nil, _):
      return false
    case .name,
         .typeConstructor:
      return true
    case let .ref(_, type, _, _),
         let .list(_, type, _, _):
      return type.ensureAllReferencesHaveLifetimeSpecifiers
    case let .function(_, arg, ret, _, _):
      return arg.ensureAllReferencesHaveLifetimeSpecifiers && ret.ensureAllReferencesHaveLifetimeSpecifiers
    case let .tuple(_, types, _, _):
      return types.allSatisfy { $0.ensureAllReferencesHaveLifetimeSpecifiers }
    }
  }
  
  var ensureNoReferenceHasLifetimeSpecifiers: Bool {
    switch self {
    case .ref(_, let type, nil, _):
      return type.ensureNoReferenceHasLifetimeSpecifiers
    case .ref(_, _, .some, _):
      return false
    case .name,
         .typeConstructor:
      return true
    case let .list(_, type, _, _):
      return type.ensureNoReferenceHasLifetimeSpecifiers
    case let .function(_, arg, ret, _, _):
      return arg.ensureNoReferenceHasLifetimeSpecifiers && ret.ensureNoReferenceHasLifetimeSpecifiers
    case let .tuple(_, types, _, _):
      return types.allSatisfy { $0.ensureNoReferenceHasLifetimeSpecifiers }
    }
  }
}

extension Collection where Element: Equatable {
  func replacing(occurencesOf of: Element, with element: Element) -> [Element] {
    var replaced = [Element]()
    replaced.reserveCapacity(count)
    for el in self {
      if el == of {
        replaced.append(element)
      } else {
        replaced.append(el)
      }
    }
    return replaced
  }
}

extension LAAst.Lifetime {
  var removeGenericLifetimes: Self? {
    switch self.scope {
    case .generic:
      return nil
    case .composite(let lifetimes):
      return .composite(lifetimes: lifetimes.compactMap { $0.removeGenericLifetimes })
    case _:
      return self
    }
  }
}

extension TCType {
  func replaceLifetimeSpecifier(_ specifier: String?, with newSpecifier: String?) -> Self {
    guard let specifier = specifier, let newSpecifier = newSpecifier else {
      return self
    }
    return replaceLifetimeSpecifier(specifier, with: newSpecifier)
  }
  
  func replaceLifetimeSpecifier(_ specifier: String, with newSpecifier: String) -> TCType {
    switch self {
    case let .data(name, constructors, genericParams):
      return .data(
        name: name,
        constructors: constructors.map { $0.replaceLifetimeSpecifier(specifier, with: newSpecifier) },
        genericParams: genericParams)
    case let .polymorphic(typeVariables, context, type):
      return .polymorphic(typeVariables: typeVariables, context: context, type: type.replaceLifetimeSpecifier(specifier, with: newSpecifier))
    case .simple(let type):
      return .simple(type.replaceLifetimeSpecifier(specifier, with: newSpecifier))
    }
  }
  
  func replaceLifetimeSpecifier(_ specifier: LAAst.Lifetime?, with newSpecifier: LAAst.Lifetime?) -> TCType {
    guard let specifier = specifier?.identifier.identifier, let newSpecifier = newSpecifier?.identifier.identifier else {
      return self
    }
    return replaceLifetimeSpecifier(specifier, with: newSpecifier)
  }
  
  func updateLifetimeSpecifiersM() -> LifetimeAnalyseM<TCType> {
    .init { env in
      // TODO: - Optimise for performance
      var alreadyUpdated = Set<LAAst.Lifetime>()
      var type = self
      for scope in env.lifetimeMappingEnv.scopes.reversed() {
        for (from, to) in scope {
          guard !alreadyUpdated.contains(from) else { continue }
          type = type.replaceLifetimeSpecifier(
            from.identifier.identifier,
            with: to.identifier.identifier)
          alreadyUpdated.insert(from)
        }
      }
      return .pure((env, type))
    }
  }
}

extension TCType.SimpleType {
  func replaceLifetimeSpecifier(_ specifier: String, with newSpecifier: String) -> TCType.SimpleType {
    switch self {
    case let .reference(type: type, lifetime) where specifier == lifetime:
      return .reference(
        type: type.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: newSpecifier)
      
    case let .reference(type: type, lifetime):
      return .reference(
        type: type.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: lifetime)
      
      
    case let .arrow(arg, ret, lifetime) where specifier == lifetime:
      return .arrow(
        arg: arg.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        ret: ret.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: newSpecifier)
      
    case let .arrow(arg, ret, lifetime):
      return .arrow(
        arg: arg.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        ret: ret.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: lifetime)
      
      
    case let .list(element, lifetime) where specifier == lifetime:
      return .list(
        element: element.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: newSpecifier)
      
    case let .list(element, lifetime):
      return .list(
        element: element.replaceLifetimeSpecifier(specifier, with: newSpecifier),
        lifetimeSpecifier: lifetime)
      
      
    case let .tuple(elements, lifetime) where specifier == lifetime:
      return .tuple(
        elements: elements.map { $0.replaceLifetimeSpecifier(specifier, with: newSpecifier) },
        lifetimeSpecifier: newSpecifier)
      
    case let .tuple(elements, lifetime):
      return .tuple(
        elements: elements.map { $0.replaceLifetimeSpecifier(specifier, with: newSpecifier) },
        lifetimeSpecifier: lifetime)
      
      
    case let .typeConstructor(name, types, lifetimeSpecifiers, lifetime) where specifier == lifetime:
      return .typeConstructor(
        name: name,
        types: types.map { $0.replaceLifetimeSpecifier(specifier, with: newSpecifier) },
        lifetimeSpecifiers: lifetimeSpecifiers.replacing(occurencesOf: specifier, with: newSpecifier),
        lifetimeSpecifier: newSpecifier)
      
    case let .typeConstructor(name, types, lifetimeSpecifiers, lifetime):
      return .typeConstructor(
        name: name,
        types: types.map { $0.replaceLifetimeSpecifier(specifier, with: newSpecifier) },
        lifetimeSpecifiers: lifetimeSpecifiers.replacing(occurencesOf: specifier, with: newSpecifier),
        lifetimeSpecifier: lifetime)
      
      
    case let .typeVariable(name, lifetime) where specifier == lifetime:
      return .typeVariable(name: name, lifetimeSpecifier: newSpecifier)
      
    case let .typeVariable(name: name, lifetime):
      return .typeVariable(name: name, lifetimeSpecifier: lifetime)
      
      
    case let .atomic(atomic, lifetime) where specifier == lifetime:
      return .atomic(atomic, lifetimeSpecifier: newSpecifier)
      
    case let .atomic(atomic, lifetime):
      return .atomic(atomic, lifetimeSpecifier: lifetime)
      
    case .unit:
      return self
    }
  }
}

extension TCType.Constructor {
  func replaceLifetimeSpecifier(_ specifier: String, with newSpecifier: String) -> Self {
    .init(
      name: name,
      type: type.replaceLifetimeSpecifier(specifier, with: newSpecifier)
    )
  }
}

extension LAAst.Lifetime {
  var simplify: Self {
    switch self.scope {
    case .composite(let lifetimes):
      let lifetimes = lifetimes.map { $0.simplify }
      return .composite(lifetimes: Array(Set(lifetimes)))!
    case _:
      return self
    }
  }
}
