//
//  ExpressionTree.swift
//  
//
//  Created by Toni K. Turk on 09/01/2022.
//

import Foundation

public typealias IR = ExpressionTree

// Intermediate representation
public enum ExpressionTree: Equatable {
  case stmt(Statement)
  case expr(Expression)
}

public extension ExpressionTree {
  indirect enum Statement: Equatable {
    /// MOVE(TEMP(t), e) - Evaluate e and move it to temporary t
    /// MOVE(MEM(e1), e2) - Evaluate e1, yielding address a: evaluate e2
    /// and store wordSize bytes of memory starting at a.
    case move(dst: IR.Expression, src: IR.Expression)
    
    /// EXP(e) - Evaluate e and drop the result
    case exp(IR.Expression)
    
    /// JUMP(e) - Transfer control (jump) to address e. The address is either a label,
    /// or and address calculated by any kind of expression.
    case jump(FrameLabel)
    
    /// CJUMP(c, t, f) - Evaluate c, jump to location given by t if the condition is true,
    /// otherwise jump to addres calculated by f.
    case cjump(cond: IR.Expression, `true`: FrameLabel, `false`: FrameLabel)
    
    /// SEQ(s1, s2) - The statement s1 is followed by statement s2.
    case seq([Statement])
    
    /// LABEL(n) - Define the constant value of name n to be the
    /// current machine code address.
    case label(FrameLabel)
    
    /// TRAP(r) - Stop executing the interpreter and report an error.
    case trap(reason: String)
    
    /// This is used for closure generation. We mark the spot in the code
    /// which should be overriden by closure initialisation code.
    case marker(AST.Expression)
    
    ///
    
    static func seq(_ stmts: [[Statement]]) -> Self {
      .seq(stmts.flatMap { $0 })
    }
    
    var asSEQ: [Statement] {
      switch self {
      case .seq(let stmts):
        return stmts
      case _:
        fatalError()
      }
    }
  }
}

public extension ExpressionTree {
  indirect enum Expression: Equatable {
    /// CONST(i) - A constant i.
    case constant(WordSizeType)
    
    /// NAME(n) - The symbolic constant n (corresponding to an assembly
    /// language label).
    case name(FrameLabel)
    
    /// TEMP(t) - Temporary t. A temporary in the abstract machine is similar
    /// to a register in a real machine. Hovever, the abstract machine has an infinite
    /// number of temporaries.
    case temp(FrameTemp)
    
    /// BINOP(o, e1, e2) - The application of binary operator o to operands e1 and
    /// e2. Subexpression e1 is evaluated before e2.
    case binop(Operator, lhs: Expression, rhs: Expression)
    
    /// MEM(e) - The contents of wordSize bytes of memory starting at address
    /// e. When MEM is used as the left child of a MOVE, it means "store". Otherwise,
    /// it means "fetch".
    case mem(Expression)
    
    /// CALL(f, e) - The application of a function f to the argument list l.
    /// The expression f is evaluated before the arguments which are
    /// evaluated left-to-right.
    case call(fn: Expression, args: [Expression])
    
    /// ESEQ(s, e) - The statement s is evauated for side effects,
    /// then e is evaluated for a result.
    case eseq(IR.Statement, Expression)

    /// MALLOC(b) - Allocates b bytes on the heap and returns
    /// the address of allocated space.
    case malloc(bytes: Byte, runtimeInfo: AnyHashable? = nil)
    
    /// DEALLOC(e) - Deallocates the allocated memory at the address
    /// given by evaluating e.
    case dealloc(Expression, bytes: Byte)
    
    /// 
    case print(Expression)
    
    static func constant(_ constant: UInt64) -> Self {
      .constant(WordSizeType(constant))
    }
    
    static func constant(_ constant: UInt) -> Self {
      .constant(WordSizeType(constant))
    }
    
    static func constant(_ constant: Int) -> Self {
      .constant(WordSizeType(constant))
    }
    
    static func constant(_ constant: Bool) -> Self {
      .constant(constant ? 1 : 0)
    }
    
    var asEseq: (stmt: IR.Statement, expr: IR.Expression) {
      switch self {
      case let .eseq(stmt, expr):
        return (stmt, expr)
      case _:
        return (.seq([ExpressionTree.Statement].empty()), self)
      }
    }
  }
}

public extension ExpressionTree.Expression {
  enum Operator: Equatable {
    case arithmetic(Lexer.Token.Operator.Arithmetic)
    case logical(Lexer.Token.Operator.Logical)
    case bitwise(Bitwise)
  }
}

public extension ExpressionTree.Expression.Operator {
  enum Bitwise {
    case shiftLeft
    case shiftRight
    case and
    case or
  }
}

extension ExpressionTree.Expression {
  static func binop(
    _ op: Lexer.Token.Operator.Arithmetic,
    lhs: ExpressionTree.Expression,
    rhs: ExpressionTree.Expression
  ) -> ExpressionTree.Expression {
    .binop(.arithmetic(op), lhs: lhs, rhs: rhs)
  }
  
  static func binop(
    _ op: Lexer.Token.Operator.Logical,
    lhs: ExpressionTree.Expression,
    rhs: ExpressionTree.Expression
  ) -> ExpressionTree.Expression {
    .binop(.logical(op), lhs: lhs, rhs: rhs)
  }
}
