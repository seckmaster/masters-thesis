//
//  ExpressionTree+Pretty.swift
//  
//
//  Created by Toni K. Turk on 14/01/2022.
//

import Foundation

extension ExpressionTree: PrettyStringConvertible {
  public var prettyDescription: String {
    switch self {
    case .stmt(let stmt):
      return pretty(stmt, indent: 0)
    case .expr(let expr):
      return pretty(expr, indent: 0)
    }
  }
}

extension ExpressionTree.Statement: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(self, indent: 0)
  }
}

extension ExpressionTree.Expression: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(self, indent: 0)
  }
}

func pretty(_ stmt: ExpressionTree.Statement, indent: Int) -> String {
  switch stmt {
  case .seq(let stmts):
    return """
    \("SEQ:".with(indent: indent))
    \(stmts.map { pretty($0, indent: indent + 2) }.joined(separator: "\n"))
    """
  case let .cjump(cond, `true`, `false`):
    return """
    \("CJUMP:".with(indent: indent))
    \(pretty(cond, indent: indent + 2))
    \(pretty(`true`, indent: indent + 2))
    \(pretty(`false`, indent: indent + 2))
    """
  case let .exp(expr):
    return """
    \("EXP:".with(indent: indent))
    \(pretty(expr, indent: indent + 2))
    """
  case let .jump(expr):
    return """
    \("JUMP:".with(indent: indent))
    \(pretty(expr, indent: indent + 2))
    """
  case let .label(label):
    return "LABEL: \(label.description)".with(indent: indent)
  case let .move(dst, src):
    return """
    \("MOVE:".with(indent: indent))
    \(pretty(dst, indent: indent + 2))
    \(pretty(src, indent: indent + 2))
    """
  case let .trap(reason):
    return "TRAP (\(reason))".with(indent: indent)
  case .marker(let ast):
    return "MARKER (\(ast.description)".with(indent: indent)
  }
}

func pretty(_ expr: ExpressionTree.Expression, indent: Int) -> String {
  switch expr {
  case .temp(let temp):
    return "TEMP: \(temp.description)".with(indent: indent)
  case let .constant(constant):
    return "CONST: \(constant)".with(indent: indent)
  case let .binop(op, lhs, rhs):
    return """
    \("BINOP \(op):".with(indent: indent))
    \(pretty(lhs, indent: indent + 2))
    \(pretty(rhs, indent: indent + 2))
    """
  case let .call(fn, args):
    return """
    \("CALL:".with(indent: indent))
    \(pretty(fn, indent: indent + 2))
    \(args.map { pretty($0, indent: indent + 2) }.joined(separator: "\n"))
    """
  case let .eseq(stmt, expr):
    return """
    \("ESEQ:".with(indent: indent))
    \(pretty(stmt, indent: indent + 2))
    \(pretty(expr, indent: indent + 2))
    """
  case let .malloc(bytes, _):
    return "MALLOC: \(bytes)".with(indent: indent)
  case let .dealloc(expr, bytes):
    return """
    \("DEALLOC[\(bytes)]:".with(indent: indent))
    \(pretty(expr, indent: indent + 2))
    """
  case .name(let label):
    return "NAME: \(label.description)".with(indent: indent)
  case .mem(let expr):
    return """
    \("MEM:".with(indent: indent))
    \(pretty(expr, indent: indent + 2))
    """
  case .print(let expr):
    return """
    \("PRINT".with(indent: indent))
    \(pretty(expr, indent: indent + 2))
    """
  }
}

func pretty(_ label: FrameLabel, indent: Int) -> String {
  label.description.with(indent: indent)
}

extension Int {
  var indent: String {
    (0..<self).reduce(into: "") { acc, _ in acc.append(" ") }
  }
}
