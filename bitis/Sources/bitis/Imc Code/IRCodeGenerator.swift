//
//  IRCodeGenerator.swift
//  
//
//  Created by Toni K. Turk on 09/01/2022.
//

import Foundation
import Bow
import SwiftParsec

extension AST: IRConvertible {
  public func generateIR() -> IntermediateReprM<Set<IRState.CodeChunk>> {
    generateExtract() *>
    bindings
      .map {
        let code = $0.generateIR()
        switch $0 {
        case .binding(_, _, let cases, _, _, _):
          precondition(cases.count == 1)
          return IRState.ActionsDE.removeDependencies(cases[0].body.strongDependencies) *> code
        case .print(_, .name, _):
          return code
        case .print(_, let expression, _):
          return IRState.ActionsDE.removeDependencies(expression.strongDependencies) *> code
        case _:
          return code
        }
      }
      .sequence()
      .flatMap(__main__)
      .flatMap(IRState.ActionsCHNK.addChunk)^
      .withState() >>- { _, state in .pure(state.chunks)^ }
  }
  
  private func __main__(_ code: [IR.Statement?]) -> IntermediateReprM<IRState.CodeChunk> {
    let envokeDestructorsForConstructors: IntermediateReprM<CodeGenerator.SEQ> = IRState.ActionsDB.allConstructors >>- { constructors in
      constructors.map {
        envokeDestructor(closureAddress: .mem(.name(.init(name: $0))), isPartial: false)
          .map { .exp($0) }
      }
      .sequence()^
    }
    
    return envokeDestructorsForActiveBindings() >>- { envokeDestructors in
      envokeDestructorsForConstructors >>- { envokeDestructorsForConstructors in
        IRState.ActionsCFG.config >>- { config in
          let frame = Frame(
            description: Self.mainLabel.description,
            staticLevel: 0,
            entry: Self.mainLabel,
            end: Self.mainLabel,
            localVariables: [],
            framePointer: -2,
            returnValue: -3
          )
          var code = code.compactMap { $0 }
          if !config.isRunningInREPL {
            code.append(contentsOf: envokeDestructors)
            code.append(contentsOf: envokeDestructorsForConstructors)
          }
          code.append(.move(
            dst: .temp(frame.returnValue),
            src: .constant(0)))
          let chunk = IRState.CodeChunk(
            frame: frame,
            stmt: .seq(code)
          )
          return .pure(chunk)^
        }
      }
    }
  }
  
  /// Extract the value from a thunk.
  private func generateExtract() -> IntermediateReprM<()> {
    /// `__extract__` returns the item by dereferencing the
    /// pointer in the memo slot of the thunk
    IRState.ActionsTSE.temps(count: 2) >>- { temps in
      IRState.ActionsMEM.allocateMemory(
        for: Self.extractLabel,
        bytes: wordSize
      ) >>- { _ in
        let frame = Frame(
          description: Self.extractLabel.description,
          staticLevel: 0,
          entry: Self.extractLabel,
          end: Self.extractLabel,
          localVariables: [],
          framePointer: temps[0],
          returnValue: temps[1],
          parameterCount: 4)
        let code = IR.Statement.seq([
          .move( // 1. dereference the pointer inside the first slot of the thunk
            dst: .temp(frame.returnValue),
            src: .mem(.EP))
        ])
        let chunk = IRState.CodeChunk(
          frame: frame,
          stmt: code)
        return IRState.ActionsCHNK.addChunk(chunk)
      }
    }
  }
  
  static let mainLabel   : FrameLabel = "__main__"
  static let extractLabel: FrameLabel = "__extract__"
}

extension AST.Binding: IRConvertible {
  public func generateIR() -> IntermediateReprM<IR.Statement?> {
    switch self {
    // MARK: - Data
    case let .data(_, _, _, constructors, _):
      return constructors
        .enumerated()
        .map { $0.element.generateIR(constructorId: $0.offset) }
        .sequence()
        .map { .seq($0) }^
    case .prototype:
      return .pure(nil)^
    case let .binding(_, _, cases, _, _, _):
      precondition(cases.count == 1, "Perform `case lifting` before generating IR!")
      precondition(cases[0].parameters.isEmpty, "Perform `case lifting` before generating IR!")
      return cases[0].generateIR() >>- { code in
        IRState.ActionsAME.access(for: .binding(self)) >>- { access in
          switch access.kind {
          case .global(let label):
            return IRState.ActionsMEM.allocateMemory(
              for: label,
              bytes: wordSize,
              andStore: 0x0
            ) >>- { _ in
              let move = IR.Statement.move(
                dst: .mem(.name(label)),
                src: code)
              return .pure(move)^
            }
          case let .local(access, _):
            let move = IR.Statement.move(
              dst: .mem(.binop(
                .plus,
                lhs: .FP,
                rhs: .constant(access.offset))),
              src: code)
            return .pure(move)^
          case _:
            // TODO: -
            fatalError()
          }
        }
      }
    // MARK: - Print
    case let .print(_, expression, _):
      return IRState.ActionsTSE.temp >>- { temp in
        expression.generateIR() >>- { expressionCode in
          evaluateThunk(expr: .temp(temp)) >>- { evaluated in
            IRState.ActionsCFG.config >>- { config in
              envokeDestructor(closureAddress: .temp(temp), isPartial: false) >>- { envokeDestructorCode in
                var code: CodeGenerator.SEQ = [
                  .move(dst: .temp(temp), src: expressionCode),
                  .exp(.print(evaluated)),
                ]
                if config.envokeDestructors {
                  switch expression {
                  case .name:
                    break
                  case _:
                    code.append(.exp(envokeDestructorCode))
                  }
                }
                return .pure(.seq(code))^
              }
            }
          }
        }
      }
    case .include:
      fatalError("Imports should have already been resolved!")
    }
  }
}

// MARK: - Constructor
extension AST.Binding.Constructor {
  public func generateIR(
    constructorId: Int
  ) -> IntermediateReprM<IR.Statement> {
    IRState.ActionsDB.register(
      id: constructorId,
      for: name
    ) >>- {
      let lambdaExpr = generateLambdaExpression(
        constructorId: constructorId,
        constructorName: name,
        elementTypes: type!.curriedArrow
      )
      let label = FrameLabel(name: name)
      return frameEvaluator(
        lambdaExpr,
        staticLevel: 0
      ) >>- { innerMostFrame in
        lambdaExpr.generateIR() >>- { lambdaExpr in
          IRState.ActionsMEM.allocateMemory(
            for: label,
            bytes: wordSize
          ) >>- { address in
            let move = IR.Statement.move(
              dst: .mem(.name(label)),
              src: lambdaExpr)
            return .pure(move)^
          }
        }
      }
    }
  }
  
  // @NOTE: - Bind a frame for each nested lambda-expr.
  func frameEvaluator(
    _ expression: AST.Expression,
    staticLevel: UInt,
    frames: [Frame] = [],
    offset: Byte = wordSize
  ) -> IntermediateReprM<Frame> {
    func storeAccess(
      expr: AST.Expression,
      frame: Frame
    ) -> IntermediateReprM<()> {
      IRState.ActionsAME.bind(
        access: .init(
          definitionDescription: expr.description,
          kind: .parameter(
            staticLevel: frame.staticLevel,
            offset: 2*wordSize)),
        for: .expression(expr))
    }
    
    return IRState.ActionsLSE.manyAnonymous(count: 2) >>- { entryLabels in
      IRState.ActionsTSE.temps(count: 4) >>- { temps in
        let frame = Frame(
          description: expression.description+"~[thunk]",
          staticLevel: staticLevel,
          entry: entryLabels[0],
          end: entryLabels[0],
          localVariables: [],
          framePointer: temps[0],
          returnValue: temps[1])
        switch expression {
        case .lambda(_, let pattern, let body, _, _):
          let outerFrame = Frame(
            description: expression.description+"~[fn]",
            staticLevel: staticLevel,
            entry: entryLabels[1],
            end: entryLabels[1],
            localVariables: [],
            framePointer: temps[2],
            returnValue: temps[3],
            parameterCount: 3) // SL, thunk, lambda parameter
          return IRState.ActionsFME.bind(
            frame: frame,
            for: .pattern(pattern)
          ) *> IRState.ActionsFME.bind(
            frame: outerFrame,
            for: .expression(expression)
          ) *> frameEvaluator(
            body,
            staticLevel: staticLevel + 1,
            frames: frames.appending(outerFrame),
            offset: offset + wordSize
          )
        case .tuple(_, let expressions, _, _):
          precondition(frames.count == expressions.count - 1)
          return IRState.ActionsFME.bind(
            frame: frame,
            for: .expression(expression)
          ) *> zip(expressions.dropFirst(), frames)
            .map { storeAccess(expr: $0.0, frame: $0.1) }
            .sequence()
            .map { _ in frames.last! }^
        case _:
          fatalError()
        }
      }
    }
  }
  
  func generateLambdaExpression(
    constructorId: Int,
    constructorName: String,
    elementTypes: [TCType]
  ) -> AST.Expression {
    let types = types.isEmpty
      ? [.tuple(position: .next, types: [])]
      : types
    
    /// Generate lambda expression for the constructor:
    ///
    /// ```
    /// data List a = Nil () | Cons a (List a)
    /// Cons a (List a) =>
    ///   λx -> λxs -> (0, x, xs)
    ///                 ^ data type instance identifier
    /// Nil =>
    ///   λ() -> (1)
    ///           ^ data type instance identifier
    /// ```
    let lambdaExpr: AST.Expression = types
      .dropLast()
      .reversed()
      .enumerated()
      .reduce(.lambda(
        position: .next,
        parameter: .identifier(
          position: .next,
          identifier: "__\(constructorName)\(types.count - 1)__",
          lifetime: .local(.init(identifier: constructorName+"!\(types.count - 1)"))),
        body: .tuple(
          position: .next,
          expressions: [
            .literal(
              position: .init(
                start: .init(line: -1, column: -1),
                end: .init(line: -1, column: -1)),
              literal: .int(constructorId))
          ] + types.enumerated().map {
            .name(
              position: .next,
              identifier: "__\(constructorName)\($0.offset)__",
              tcType: elementTypes[$0.offset],
              bindingLifetime: .local(.init(identifier: constructorName+"!\($0.offset)")))
          },
          tcType: type?.curriedArrow.last,
          lifetime: .local(.init(identifier: constructorName+"!!!d\(types.count - 1)"))),
        lifetime: .local(.init(identifier: constructorName+"!!d\(types.count - 1)")))
      ) { acc, el -> AST.Expression in
          .lambda(
            position: .next,
            parameter: .identifier(
              position: .next,
              identifier: "__\(constructorName)\(el.offset)__",
              lifetime: .local(.init(identifier: constructorName+"!\(el.offset)"))),
            body: acc,
            lifetime: .local(.init(identifier: constructorName+"!!d\(el.offset)")))
      }
    return lambdaExpr
  }
}

extension AST.Binding.Case: IRConvertible {
  public func generateIR() -> IntermediateReprM<ExpressionTree.Expression> {
    IRState.ActionsBS.addBinding(self)
    *> IRState.ActionsDE.addBinding(self)
    *> IRState.ActionsDE.inNewScope(do: body.generateIR)
  }
}

extension AST.Expression: IRConvertible {
  public func generateIR() -> IntermediateReprM<ExpressionTree.Expression> {
    switch self {
    // MARK: - Literal
    case let .literal(position, .int(constructorId), _, _) where position == .init(start: .init(line: -1, column: -1), end: .init(line: -1, column: -1)):
      return .pure(.constant(constructorId))^
    case let .literal(_, literal, _, _):
      return IRState.ActionsCME.query(node: .expression(self)) >>- { codeOrNil in
        if let code = codeOrNil {
          return .pure(code)^
        }
        return IRState.ActionsCFG.config >>- { config in
          let literalCode = literal.generateCode()
          if config.shouldAutomaticallyEvaluateConstants {
            return literalCode.flatMap { createEvaluatedThunk(code: $0, expression: self) }^ >>- { thunk in
              /*IRState.ActionsCME.store(code: thunk, for: .expression(self)) *> */ .pure(thunk)^
            }
          }
          return inNewScope {
            createThunk(
              code: literalCode,
              expression: self
            ) >>- { thunk in
              /*IRState.ActionsCME.store(code: thunk, for: .expression(self)) *>*/ .pure(thunk)^
            }
          }
        }
      }
    // MARK: - Name
    case .name(_, let name, _, _, _):
      func generateCode() -> IntermediateReprM<IR.Expression> {
        IRState.ActionsAME.access(for: .expression(self)) >>- { access in
          switch access.kind {
          case .global(let label):
            let code: IR.Expression = .mem(.name(label))
            return .pure(code)^
          case let .parameter(staticLevel, offset):
            return IRState.ActionsFS.peek >>- { frame in
              precondition(frame.staticLevel >= staticLevel)
              
              let diff = frame.staticLevel - staticLevel
              if diff == 0 { // Stored on the stack!
                let expr = IR.Expression.mem(.binop(
                  .plus,
                  lhs: .FP,
                  rhs: .constant(offset)))
                return .pure(expr)^
              }
              
              // Stored in the closure!
              return IRState.ActionsCBE.registerOffset(
                for: name,
                access: access
              ) >>- { offsetInClosure, register in
                let expr = IR.Expression.mem(.binop(
                  .plus,
                  lhs: .EP,
                  rhs: .constant(offsetInClosure)
                ))
                return .pure(expr)^
              }
            }
          case let .local(localAccess, variableStaticLevel): 
            return IRState.ActionsFS.peek >>- { frame in
              precondition(frame.staticLevel >= variableStaticLevel)
              
              let diff = frame.staticLevel - variableStaticLevel
              if diff == 0 { // Stored on the stack!
                let expr = IR.Expression.mem(.binop(
                  .plus,
                  lhs: .FP,
                  rhs: .constant(localAccess.offset)))
                return .pure(expr)^
              }
              
              // Stored in the closure!
              return IRState.ActionsCBE.registerOffset(
                for: name,
                access: access
              ) >>- { offsetInClosure, register in
                let expr = IR.Expression.mem(.binop(
                  .plus,
                  lhs: .EP,
                  rhs: .constant(offsetInClosure)
                ))
                return .pure(expr)^
              }
            }
          }
        }
      }
      
      let isArrow = type.map { $0.isArrow } ?? false
      if isArrow {
        return createEvaluatedThunk(
          code: evaluateThunk(expr: generateCode()),
          expression: self
        )
      }
      return generateCode()
    // MARK: - Let
    case let .let(_, bindings, expression, _, _):
      return inNewScope {
        IRState.ActionsBS.inNewScope {
          bindings
            .map { binding -> IntermediateReprM<IR.Statement?> in
              let code = binding.generateIR()
              switch binding {
              case .binding(_, _, let cases, _, _, _):
                precondition(cases.count == 1)
                return IRState.ActionsDE.removeDependencies(cases[0].body.strongDependencies) *> code
              case .print(_, .name, _):
                return code
              case .print(_, let expression, _):
                return IRState.ActionsDE.removeDependencies(expression.strongDependencies) *> code
              case _:
                return code
              }
            }
            .sequence()
            .flatMap { optionalCodes in
              expression.generateIR() >>- { nonEvaluated in
                createThunk(
                  expressions: [
                    .dummyLiteralNode(),
                  ],
                  join: { expressions, locations in
                    evaluateThunk(expr: locations[0]) >>- { evaluated in
                      evaluateThunk(expr: nonEvaluated) >>- { evaluated1 in
                        let code: CodeGenerator.SEQ
                        let exprCode: IR.Expression
                        if expression.isNameBehindReference {
                          code = []
                          exprCode = evaluated1
                        } else {
                          code = [.move(dst: locations[0], src: nonEvaluated)]
                          exprCode = evaluated
                        }
                        return .pure(.eseq(.seq(code), exprCode))^
                      }
                    }
                  },
                  beforeEvaluatingExpressionsDo: {
                    IRState.ActionsDE.removeDependencies(expression.strongDependencies) *> envokeDestructorsForActiveBindings() >>- { envokeDestructors in
                      var code = optionalCodes.compactMap { $0 }
                      code.append(contentsOf: envokeDestructors)
                      return .pure(code)^
                    }
                  },
                  modifyDestructorsCode: {
                    .pure($0)^
                  },
                  expression: self
                )
              }
            }^
        }
      }
    // MARK: - Case
    case let .case(_, expression, matches, _, _):
      let branchesStrongDependencies = matches.map { $0.expression.strongDependencies(ignoreIdentifiers: $0.pattern.identifiers) }
      let addDependenciesForEachBranch = branchesStrongDependencies.enumerated().map { idx, dependencies in
        let unionOfAllOthers = branchesStrongDependencies.enumerated().reduce(into: Set<String>()) { (acc, arg1) in
          let (idx2, d) = arg1
          if idx == idx2 { return }
          acc.formUnion(d)
        }
        return unionOfAllOthers.subtracting(dependencies)
      }
      let addDependenciesCodes = addDependenciesForEachBranch.map { branch in
        branch.map(IRState.ActionsBS.binding)
          .sequence()^
          .flatMap { cases in
            cases.map {
              AST.Expression.name(
                position: .next,
                identifier: $0.name,
                lifetime: $0.lifetime,
                bindingLifetime: $0.lifetime
              ).generateIR()
            }.sequence()^
          }
      }.sequence()^
      let dummyExpressions = Array(
        repeating: ThunkSubExpressionInfo.dummyLiteralNode(),
        count: addDependenciesForEachBranch.max(by: { $0.count < $1.count })?.count ?? 0)
      return inNewScope {
        createThunk(
          expressions: [.init(expression: expression, transformation: id, automaticallyMoveToClosure: false)] + matches.map { match in
            var info = ThunkSubExpressionInfo(
              expression: match.expression,
              transformation: id,
              automaticallyMoveToClosure: false)
            info.generateCode = {
              IRState.ActionsBS.inNewScope {
                IRState.ActionsBS.addBindings(match.pattern.createBindings) *> info.expression.generateIR()
              }
            }
            return info
          } + dummyExpressions,
          join: { expressions, locations in
            IRState.ActionsTSE.temp >>- { temp in
              IRState.ActionsLSE.anonymous >>- { caseExprEndLabel in
                addDependenciesCodes >>- { addDependenciesCodes in
                  var code = CodeGenerator.SEQ()
                  let matchedExpression: IR.Expression
                  if expression.isNameBehindReference {
                    matchedExpression = expressions[0]
                  } else {
                    matchedExpression = locations[0]
                    code.append(.move(dst: locations[0], src: expressions[0]))
                  }
                  return matches.enumerated()
                    .map { offset, el in
                      el.generateIR(
                        endLabel: caseExprEndLabel,
                        resultRegister: temp,
                        moveExpressionIntoClosureLocation: locations[offset + 1],
                        matchedExpressionClosureLocation: matchedExpression
                      ).map {
                        var code = $0
                        for (idx, strongReference) in addDependenciesCodes[offset].enumerated() {
                          code.insert(
                            .move(
                              dst: locations[idx + 1 + matches.count],
                              src: strongReference),
                            at: code.count - 2)
                        }
                        return code
                      }
                    }.sequence()^ >>- { matchesCode in
                      code.append(contentsOf: matchesCode.flatMap { $0 })
                      code.append(.trap(reason: "non-exhaustive patterns"))
                      code.append(.label(caseExprEndLabel))
                      let eseq = IR.Expression.eseq(.seq(code), .temp(temp))
                      return .pure(eseq)^
                    }
                }
              }
            }
          },
          expression: self)
      }
    // MARK: - Ref,Deref
    case let .deref(_, expression, _, _),
         let .ref(_, expression, _, _):
      return expression.generateIR() >>- { _ in // @NOTE: - For some reason generating IR for the expression like this solves some issues with infinite recursion and expressions such as `*&x`
        switch expression {
        case .name:
          return inNewScope {
            createThunk(
              expressions: [
                .init(expression: expression, transformation: id, automaticallyMoveToClosure: false),
              ],
              join: { expressions, _ in
                evaluateThunk(expr: expressions[0]).map { $0 }^
              },
              expression: self
            )
          }
        case _:
          return inNewScope {
            createThunk(
              expressions: [
                .init(expression: expression, transformation: id, automaticallyMoveToClosure: true),
              ],
              join: { _, locations in
                evaluateThunk(expr: locations[0]).map { $0 }^
              },
              expression: self
            )
          }
        }
      }
    // MARK: - Tuple
    case let .tuple(_, expressions, _, _):
      guard !expressions.isEmpty else {
        return IRState.ActionsCFG.config >>- { config in
          if config.shouldAutomaticallyEvaluateConstants {
            return createEvaluatedThunk(
              code: .constant(0),
              expression: self
            )
          }
          return inNewScope {
            createThunk(
              code: .pure(.constant(0))^,
              expression: self
            )
          }
        }
      }
      // @NOTE: - Allocate additional `wordSize` memory
      // for the back pointer into the owning thunk
      //              ~~~~~~~~
      let tupleSize = wordSize + wordSize * Byte(expressions.count)
      
      return inNewScope {
        IRState.ActionsTSE.temp >>- { temp in
          createThunk(
            expressions: expressions
              .map { .init(expression: $0, transformation: id, automaticallyMoveToClosure: false) },
            join: { expressionsCode, locations in
              let initialiseTuple: [IR.Statement] = expressionsCode
                .enumerated()
                .map { offset, code -> [IR.Statement] in
                  if expressions[offset].position == -1 {
                    // data binding constructor
                    return [
                      .move(
                        dst: .mem(.binop(
                          .plus,
                          lhs: .temp(temp),
                          rhs: .constant(Byte(offset) * wordSize))),
                        src: code
                      )
                    ]
                  }
                  guard !expressions[offset].isNameBehindReference else {
                    return [
                      .move(
                        dst: .mem(.binop(
                          .plus,
                          lhs: .temp(temp),
                          rhs: .constant(Byte(offset) * wordSize))),
                        src: code
                      )
                    ]
                  }
                  return [
                    .move(
                      dst: locations[offset],
                      src: code),
                    .move(
                      dst: .mem(.binop(
                        .plus,
                        lhs: .temp(temp),
                        rhs: .constant(Byte(offset) * wordSize))),
                      src: locations[offset]
                    )
                  ]
                }
                .flatMap { $0 }
              
              var seq = CodeGenerator.SEQ()
              seq.append(.move(
                dst: .temp(temp),
                src: .malloc(bytes: tupleSize, runtimeInfo: self.runtimeInfo)))
              seq.append(contentsOf: initialiseTuple)
              seq.append(.move( // Store the pointer to the thunk into the tuple
                dst: .mem(.binop(
                  .plus,
                  lhs: .temp(temp),
                  rhs: .constant(tupleSize - wordSize))),
                src: .EP))
              let eseq = IR.Expression.eseq(
                .seq(seq),
                .temp(temp))
              return .pure(eseq)^
            },
            modifyDestructorsCode: { code in
              IRState.ActionsLSE.manyAnonymous(count: 2) >>- { labels in
                var code = code
                code.insert(contentsOf: [
                  .cjump(
                    cond: .binop(.greaterThan, lhs: .mem(.EP), rhs: .constant(0)),
                    true: labels[0],
                    false: labels[1]),
                  .label(labels[0]),
                  .exp(.dealloc(.mem(.EP), bytes: tupleSize)),
                  .label(labels[1]),
                ], at: 0)
                return .pure(code)^
              }
            },
            expression: self
          )
        }
      }
    case .list:
      fatalError()
    // MARK: - Application
    case let .binary(.application(_, fn, arg, _, _)):
      return inNewScope {
        return arg.generateIR() >>- { argCode in
          fn.generateIR() >>- { fnCode in
            createThunk(
              expressions: [
                .dummyLiteralNode(),
                .dummyLiteralNode(),
                .dummyLiteralNode(),
              ],
              join: { expressions, locations in
                var code: CodeGenerator.SEQ = [
                  .move(dst: locations[0], src: fnCode),
                ]
                
                let shouldMoveIntoDestructor = (arg.type!.isReference && !arg.isName)
                /// @NOTE: - Uncommenting the line bellow will improve the memory
                /// cleanup of functions. For instance, ThesisAnalysis/program2.hs
                /// will get 100% of memory cleared. However, it introduces a double
                /// free issue when the same name is moved twice into the closure
                /// (`fmap (compose neg neg) (Just true)`).
                /// @TODO: - Re-implement functions; do not allow moving the same
                /// name twice, even if it's an arrow. Use borrowing instead when
                /// that is needed.
                || (arg.type!.isArrow /*&& arg.isName*/)
                
                let argCode_: IR.Expression
                if shouldMoveIntoDestructor {
                  code.append(.move(
                    dst: locations[2],
                    src: argCode)
                  )
                  argCode_ = locations[2]
                } else {
                  argCode_ = argCode
                }
                
                return evaluateThunk(
                  expr: evaluateThunk(expr: locations[0]),
                  lambdaParameter: argCode_
                ) >>- { nonEvaluatedFunction in
                  code.append(.move(
                    dst: locations[1],
                    src: nonEvaluatedFunction)
                  )
                  
                  return evaluateThunk(expr: locations[1]) >>- { applicationResult in
                    .pure(.eseq(.seq(code), applicationResult))^
                  }
                }
              },
              expression: self
            )
          }
        }
      }
    // MARK: - Arithmetic
    case let .binary(.arithmetic(_, op, left, right, _, _)):
      return inNewScope {
        createThunk(
          expressions: [
            .init(expression: left, transformation: id, automaticallyMoveToClosure: false),
            .init(expression: right, transformation: id, automaticallyMoveToClosure: false),
          ],
          join: { expressions, locations in
            evaluateThunk(expr: locations[0]) >>- { leftEvaled in
              evaluateThunk(expr: locations[1]) >>- { rightEvaled in
                evaluateThunk(expr: expressions[0]) >>- { leftEvaled1 in
                  evaluateThunk(expr: expressions[1]) >>- { rightEvaled1 in
                    var code: CodeGenerator.SEQ = []
                    let leftCode: IR.Expression
                    let rightCode: IR.Expression
                    if left.isNameBehindReference {
                      leftCode = leftEvaled1
                    } else {
                      code.append(.move(
                        dst: locations[0],
                        src: expressions[0])
                      )
                      leftCode = leftEvaled
                    }
                    if right.isNameBehindReference {
                      rightCode = rightEvaled1
                    } else {
                      code.append(.move(
                        dst: locations[1],
                        src: expressions[1])
                      )
                      rightCode = rightEvaled
                    }
                    return .pure(
                      .eseq(
                        .seq(code),
                        .binop(op, lhs: leftCode, rhs: rightCode)
                      )
                    )^
                  }
                }
              }
            }
          },
          expression: self
        )
      }
    // MARK: - Logical
    case let .binary(.logical(_, op, left, right, _, _)):
      return inNewScope {
        createThunk(
          expressions: [
            .init(expression: left, transformation: id, automaticallyMoveToClosure: false),
            .init(expression: right, transformation: id, automaticallyMoveToClosure: false),
          ],
          join: { expressions, locations in
            evaluateThunk(expr: locations[0]) >>- { leftEvaled in
              evaluateThunk(expr: locations[1]) >>- { rightEvaled in
                evaluateThunk(expr: expressions[0]) >>- { leftEvaled1 in
                  evaluateThunk(expr: expressions[1]) >>- { rightEvaled1 in
                    var code: CodeGenerator.SEQ = []
                    let leftCode: IR.Expression
                    let rightCode: IR.Expression
                    if left.isNameBehindReference {
                      leftCode = leftEvaled1
                    } else {
                      code.append(.move(
                        dst: locations[0],
                        src: expressions[0])
                      )
                      leftCode = leftEvaled
                    }
                    if right.isNameBehindReference {
                      rightCode = rightEvaled1
                    } else {
                      code.append(.move(
                        dst: locations[1],
                        src: expressions[1])
                      )
                      rightCode = rightEvaled
                    }
                    return .pure(
                      .eseq(
                        .seq(code),
                        .binop(op, lhs: leftCode, rhs: rightCode)
                      )
                    )^
                  }
                }
              }
            }
          },
          expression: self
        )
      }
    case .binary(.concat):
      fatalError()
    // MARK: - If-then-else
    case let .binary(.ifThenElse(_, condition, `true`, `false`, _, _)):
      let trueDependencies = `true`.strongDependencies
      let falseDependencies = `false`.strongDependencies
      let addDependenciesForFalseBranch = trueDependencies.subtracting(falseDependencies)
      let addDependenciesForTrueBranch = falseDependencies.subtracting(trueDependencies)
      let expandDestructorByCount = max(addDependenciesForFalseBranch.count, addDependenciesForTrueBranch.count)
      let dummyExpressions = Array(repeating: ThunkSubExpressionInfo.dummyLiteralNode(), count: expandDestructorByCount)
      return inNewScope {
        createThunk(
          expressions: [
            .init(expression: condition, transformation: id, automaticallyMoveToClosure: false),
            .init(expression: `true`, transformation: id, automaticallyMoveToClosure: false),
            .init(expression: `false`, transformation: id, automaticallyMoveToClosure: false),
          ] + dummyExpressions,
          join: { expressions, locations in
            IRState.ActionsLSE.manyAnonymous(count: 3) >>- { labels in
              IRState.ActionsTSE.temp >>- { res in
                evaluateThunk(expr: locations[0]) >>- { evaluatedConditionCodeLocation in
                  evaluateThunk(expr: expressions[0]) >>- { evaluatedConditionCodeLocation1 in
                    evaluateThunk(expr: .temp(res)) >>- { eval in
                      let trueBranchCodes: IntermediateReprM<[IR.Expression]> = addDependenciesForTrueBranch
                        .map { IRState.ActionsBS.binding(for: $0)}
                        .sequence()
                        .flatMap { cases in
                          cases.map {
                            AST.Expression.name(
                              position: .next,
                              identifier: $0.name,
                              lifetime: $0.lifetime,
                              bindingLifetime: $0.lifetime
                            ).generateIR()
                          }.sequence()^
                        }^
                      let falseBranchCodes: IntermediateReprM<[IR.Expression]> = addDependenciesForFalseBranch
                        .map { IRState.ActionsBS.binding(for: $0)}
                        .sequence()
                        .flatMap { cases in
                          cases.map {
                            AST.Expression.name(
                              position: .next,
                              identifier: $0.name,
                              lifetime: $0.lifetime,
                              bindingLifetime: $0.lifetime
                            ).generateIR()
                          }.sequence()^
                        }^
                      return trueBranchCodes >>- { trueBranchCodes in
                        falseBranchCodes >>- { falseBranchCodes in
                          var code: CodeGenerator.SEQ = []
                          
                          let conditionCode: IR.Expression
                          let leftCode: IR.Expression
                          let rightCode: IR.Expression
                          
                          if condition.isNameBehindReference {
                            conditionCode = evaluatedConditionCodeLocation1
                          }
                          else {
                            code.append(.move(
                              dst: locations[0],
                              src: expressions[0])
                            )
                            conditionCode = evaluatedConditionCodeLocation
                          }

                          // if-then-else
                          // condition
                          code.append(.cjump(
                            cond: conditionCode,
                            true: labels[0],
                            false: labels[1])
                          )
                          
                          // true branch
                          code.append(.label(labels[0]))
                          if `true`.isNameBehindReference {
                            leftCode = expressions[1]
                          } else {
                            code.append(.move(
                              dst: locations[1],
                              src: expressions[1])
                            )
                            leftCode = locations[1]
                          }
                          code.append(contentsOf: [
                            .move(
                              dst: .temp(res),
                              src: leftCode),
                          ])
                          
                          for (idx, strongReference) in trueBranchCodes.enumerated() {
                            code.append(.move(dst: locations[idx + 3], src: strongReference))
                          }
                          
                          code.append(.jump(labels[2]))
                          
                          // false branch
                          code.append(.label(labels[1]))
                          if `false`.isNameBehindReference {
                            rightCode = expressions[2]
                          } else {
                            code.append(.move(
                              dst: locations[2],
                              src: expressions[2])
                            )
                            rightCode = locations[2]
                          }
                          code.append(contentsOf: [
                            .move(
                              dst: .temp(res),
                              src: rightCode),
                          ])
                          
                          for (idx, strongReference) in falseBranchCodes.enumerated() {
                            code.append(.move(dst: locations[idx + 3], src: strongReference))
                          }
                          
                          code.append(.label(labels[2]))
                          
                          return .pure(.eseq(.seq(code), eval))^
                        }
                      }
                    }
                  }
                }
              }
            }
          },
          expression: self
        )
      }
    // MARK: - Lambda
    case let .lambda(_, parameter, body, _, _):
      // the inner thunk is the actual function
      let innerThunk = inNewScope {
        IRState.ActionsBS.inNewScope {
          IRState.ActionsBS.addBindings(parameter.createBindings) *> createThunk(
            target: .lambda(identifiers: parameter.identifiers),
            code: body.generateIR(),
            expression: self
          )
        }
      }
      // the outer thunk is the thunked lambda expression
      return IRState.ActionsFME.frameOrNil(
        for: .pattern(parameter)
      ) >>- { frame in
        inNewScope(frame: frame!) {
          innerThunk >>- { functionBody in
            createThunk(
              frame: frame!,
              expressions: [
                .dummyLiteralNode(),
              ],
              join: { expressions, locations in
                let code: CodeGenerator.SEQ = [
                  .move(
                    dst: locations[0],
                    src: functionBody
                  )
                ]
                return .pure(.eseq(.seq(code), locations[0]))^
              },
              expression: self
            )
          }
        }
      }
    }
  }
}

// MARK: - Match
extension AST.Expression.Match/*: IRConvertible*/ {
  public func generateIR(
    endLabel: FrameLabel,                             // the label which marks the end of the whole case expression
    resultRegister: FrameTemp,                        // register into which the matched expression should store its result
    moveExpressionIntoClosureLocation: IR.Expression, // the location into which the expression should be moved
                                                      // when this match is matched
    matchedExpressionClosureLocation: IR.Expression   // the location in the closure where the matched expression
                                                      // is located
  ) -> IntermediateReprM<CodeGenerator.SEQ> {
    IRState.ActionsBS.inNewScope {
      IRState.ActionsBS.addBindings(pattern.createBindings) *>
      IRState.ActionsLSE.anonymous >>- { matchLabel in
        pattern.generateIR(
          expression: self.expression,
          matchEndLabel: matchLabel,
          resultRegister: resultRegister,
          moveExpressionIntoClosureLocation: moveExpressionIntoClosureLocation,
          matchedExpressionClosureLocation: matchedExpressionClosureLocation
        ) >>- { code in
          var seq = code
          seq.append(.jump(endLabel))
          seq.append(.label(matchLabel))
          return .pure(seq)^
        }
      }
    }
  }
}

// MARK: - Patterns
extension AST.Pattern/*: IRConvertible*/ {
  public func generateIR(
    expression: AST.Expression,                       // the expression of the match RHS
    matchEndLabel: FrameLabel,                        // the label which marks the end of a single match
    resultRegister: FrameTemp,                        // register into which the result is stored
    moveExpressionIntoClosureLocation: IR.Expression, // the location into which the expression should be moved
                                                      // when this match is matched
    matchedExpressionClosureLocation: IR.Expression   // the location in the closure where the matched expression
                                                      // is located
  ) -> IntermediateReprM<CodeGenerator.SEQ> {
    func generateLiteralCode(
      lhs: IR.Expression,
      rhs: IR.Expression
    ) -> IntermediateReprM<CodeGenerator.SEQ> {
      IRState.ActionsLSE.anonymous >>- { trueLabel in
        let code: CodeGenerator.SEQ = [
          .cjump(
            cond: .binop(
              .equal,
              lhs: lhs,
              rhs: rhs),
            true: trueLabel,
            false: matchEndLabel),
          .label(trueLabel),
        ]
        return .pure(code)^
      }
    }
    func generateTupleCode(
      patterns: [AST.Pattern],
      expr: IR.Expression,
      isDeconstruct: Bool
    ) -> IntermediateReprM<CodeGenerator.SEQ> {
      IRState.ActionsTSE.temp >>- { temp in
        patterns
          .enumerated()
          .map { item -> IntermediateReprM<CodeGenerator.SEQ> in
            generateCode(
              for: item.element,
              dereference: .mem(.binop(
                .plus,
                lhs: .temp(temp),
                rhs: .constant(Byte(item.offset) * wordSize))
              )
            )
            .flatMap { code in
              if !expression.strongDependencies.intersection(item.element.identifiers).isEmpty {
                /// This value is moved to the resulting closure!
                /// Remove from this tuple's destructor!
                return IRState.ActionsTSE.temp >>- { innerTemp in
                  let parentClosure = ExpressionTree.Expression.mem(.binop(
                    .plus,
                    lhs: .temp(temp),
                    rhs: .constant(Byte(patterns.count) * wordSize)))
                  var code = code
                  code.append(contentsOf: [
                    /// 1. extract free variables count
                    /// `freeVars = header & 1 << (16 - 1)`
                    .move(
                      dst: .temp(innerTemp),
                      src: .binop(
                        .bitwise(.and),
                        lhs: .mem(.binop( /// header is the 4th element in the closure
                          .plus,
                          lhs: parentClosure,
                          rhs: .constant(3 * wordSize))),
                        rhs: .binop(
                          .minus,
                          lhs: .binop(
                            .bitwise(.shiftLeft),
                            lhs: .constant(1),
                            rhs: .constant(16)),
                          rhs: .constant(1)))),
                    .move( /// 2. override the strong reference to 0
                      dst: .mem(.binop(
                        .plus,
                        lhs: parentClosure,
                        rhs: .binop(
                          .plus,
                          lhs: .binop(.mul, lhs: .constant(wordSize), rhs: .temp(innerTemp)),
                          rhs: .constant(DEFAULT_THUNK_SIZE + Byte(item.offset) * wordSize + (isDeconstruct ? wordSize : 0))))),
                      src: .constant(0)),
                  ])
                  return .pure(code)^
                }
              }
              return .pure(code)^
            }^
          }
          .sequence()
          .map { $0.flatMap { $0 } }^ >>- { patternsCode in
            var code = patternsCode
            /// 1. Compute the expression and store it into a register.
            /// Otherwise it gets re-evaluated for each pattern inside the tuple.
            code.insert(
              .move(dst: .temp(temp), src: expr),
              at: 0)
            return .pure(code)^
          }
      }
    }
    func generateCode(
      for pattern: AST.Pattern,
      dereference: IR.Expression
    ) -> IntermediateReprM<CodeGenerator.SEQ> {
      evaluateThunk(expr: dereference) >>- { evaluatedCode in
        switch pattern {
        // MARK: - Identifier
        case .identifier:
          return IRState.ActionsAME.access(
            for: .pattern(pattern)
          ) >>- { access in
            switch access.kind {
            case .global:
              print(pattern.prettyDescription)
              fatalError("TODO")
            case _:
              let code: CodeGenerator.SEQ = [
                .move(
                  dst: .mem(.binop(
                    .plus,
                    lhs: .FP,
                    rhs: .constant(access.kind.offset))),
                  src: dereference), // @NOTE: - Move non-evaluated thunk!
              ]
              return .pure(code)^
            }
          }
        case .list:
          fatalError()
        // MARK: - Tuple
        case let .tuple(_, patterns, _, _):
          return generateTupleCode(
            patterns: patterns,
            expr: evaluatedCode,
            isDeconstruct: false
          )
        // MARK: - Deconstruct
        case let .deconstruct(_, constructor, patterns, _, _):
          return IRState.ActionsDB.id(for: constructor) >>- { id in
            IRState.ActionsTSE.temp >>- { temp in
              generateLiteralCode(
                lhs: .mem(.temp(temp)),
                rhs: .constant(id)
              ) >>- { compareIdCode in
                generateTupleCode(
                  patterns: patterns,
                  expr: .binop(
                    .plus,
                    lhs: .temp(temp),
                    rhs: .constant(wordSize)
                  ),
                  isDeconstruct: true
                ) >>- { tupleCode in
                  var code: CodeGenerator.SEQ = [
                    .move(dst: .temp(temp), src: evaluatedCode)
                  ]
                  code.append(contentsOf: compareIdCode)
                  code.append(contentsOf: tupleCode)
                  return .pure(code)^
                }
              }
            }
          }
        case let .ref(_, pattern, _, _):
          return generateCode(
            for: pattern,
            dereference: dereference)
        // MARK: - Literal
        case let .literal(_, literal, _, _):
          return literal.generateCode().map { $0 }^ >>- { literal in
            generateLiteralCode(
              lhs: evaluatedCode,
              rhs: literal
            )
          }
        case .wildcard:
          return .pure([])^
        }
      }
    }
    
    return generateCode(
      for: self,
      dereference: matchedExpressionClosureLocation
    ) >>- { code in
      expression.generateIR() >>- { nonEvaluatedExprCode in
        evaluateThunk(expr: moveExpressionIntoClosureLocation) >>- { evaluatedExpr in
          evaluateThunk(expr: nonEvaluatedExprCode) >>- { evaluatedExpr1 in
            var seq = code
            
            let finalExprCode: IR.Expression
            if expression.isNameBehindReference {
              finalExprCode = evaluatedExpr1
            } else {
              finalExprCode = evaluatedExpr
              seq.append(.move(
                dst: moveExpressionIntoClosureLocation,
                src: nonEvaluatedExprCode)
              )
            }
            if case .identifier = self, !expression.strongDependencies.intersection(self.identifiers).isEmpty {
              // @NOTE: - Prevent double free
              seq.append(contentsOf: [
                .move(
                  dst: matchedExpressionClosureLocation,
                  src: .constant(0)),
              ])
            }

            seq.append(.move(
              dst: .temp(resultRegister),
              src: finalExprCode)
            )
            return .pure(seq)^
          }
        }
      }
    }
  }
}

extension Lexer.Token.Literal {
  func generateCode() -> IntermediateReprM<IR.Expression> {
    switch self {
    case .int(let int):
      return .pure(.constant(int))^
    case .float(let float):
      return .pure(.constant(float))^
    case .char(let char):
      return .pure(.constant(WordSizeType(char.asciiValue!)))^
    case .logical(.true):
      return .pure(.constant(1))^
    case .logical(.false):
      return .pure(.constant(0))^
    case .string(let string):
      // TODO: -
      fatalError("todo")
    }
  }
}

enum ThunkTarget {
  case expression
  case lambda(identifiers: Set<String>)
  
  var identifiers: Set<String> {
    switch self {
    case .expression:
      return .empty()
    case .lambda(let identifiers):
      return identifiers
    }
  }
  
  var isLambda: Bool {
    switch self {
    case .lambda:
      return true
    case _:
      return false
    }
  }
}

//    Closure / Thunk memory layout:
//
// value ↘   ↙ eval code  ↙ destructor code         ~~~~~~~~~~~ strong references
//      [ ? *            *   h | v1 v2 v3 ... vn  | s1 s2 ... sn]
//        ~~~~~~~~~~~~~~~~       ~~~~~~~~~~~~~~~
//  thunk ^                  ^ header          ^ free (captured) variables
let DEFAULT_THUNK_SIZE = 4 * wordSize

struct ThunkSubExpressionInfo {
  let expression: AST.Expression
  let transformation: (IR.Expression) -> IntermediateReprM<IR.Expression>
  let automaticallyMoveToClosure: Bool // @TODO: - Remove in the future and not use it!
  var generateCode: (() -> IntermediateReprM<IR.Expression>)?
  
  var code: IntermediateReprM<IR.Expression> {
    generateCode?() ?? expression.generateIR()
  }
}

struct ThunkHeader {
  // @NOTE: - This limits the number of sub-expressions
  // in a closure to 2^16 (~65k).
  let freeVariablesCount: UInt16
  let strongReferencesCount: UInt16
  
  // 32 bits are free for additional data
  // ...
  
  /// ------------------------------
  
  var to64Bits: UInt64 {
    UInt64(freeVariablesCount) | UInt64(strongReferencesCount) << 16
  }
}

typealias Join = (_ expressions: [IR.Expression], _ locations: [IR.Expression]) -> IntermediateReprM<IR.Expression?>
typealias ThunkCode = () -> IntermediateReprM<CodeGenerator.SEQ>

/// @TODO: - The design of this method is a bit strange. Refactor in the future!
func createThunk(
  frame: Frame,                                         // frame
  target: ThunkTarget = .expression,                    // target for generator
  expressions: [ThunkSubExpressionInfo],                // sub-expressions
  join: @escaping Join,                                 // join sub-expressions codes into one code
  beforeEvaluatingExpressionsDo: ThunkCode? = nil,      // returns code that is evaluated before expressions
  // modify the destructor code if necessasry
  modifyDestructorsCode: ((CodeGenerator.SEQ) -> IntermediateReprM<CodeGenerator.SEQ>)? = nil,
  isEagerDestructorEnabled: Bool = false,
  expression: AST.Expression                           // expression for which the closure is being generated
) -> IntermediateReprM<IR.Expression> {
  func generateEvalCode(
    closure: IRState.ActionsCBE.ClosureInfo,
    expressionsCode: [IR.Expression],
    closureSize: Byte
  ) -> IntermediateReprM<CodeGenerator.SEQ> {
    let initialOffset: Byte = DEFAULT_THUNK_SIZE + closure.currentExpressionClosureSize
    
    let pipeline = expressions.enumerated()
      .map { index, expression -> IntermediateReprM<IR.Expression> in
        expression.transformation(expressionsCode[index])
      }
      .sequence()
      .map {
        (
          $0,
          $0.enumerated().map {
            subscriptIntoClosure(
              offset: initialOffset + Byte($0.offset) * wordSize,
              closureAddress: .EP
            )
          }
        )
      }
      .flatMap(join)^
    
    return pipeline >>- { code in
      (beforeEvaluatingExpressionsDo?() ?? .pure([]))^ >>- { beforeCode in
        // @FIXME: - Code duplication
        switch target {
        case .expression:
          return IRState.ActionsTSE.temp >>- { temp in
            IRState.ActionsCFG.config >>- { config in
              envokeDestructor(closureAddress: .temp(temp), isPartial: true) >>- { destructor in
                var evalCode: CodeGenerator.SEQ = [
                  .move( /// 1. read the argument and store it into a register
                    dst: .temp(temp),
                    src: .EP),
                ]
                
                /// 2. Evaluate any code that should be ran before the expressions code
                evalCode.append(contentsOf: beforeCode)
                
                if expressions.count > 0 {
                  /// 3. Store child sub-expressions.
                  let storeCode = generateStoreSubExpressionsCode(
                    initialOffset: DEFAULT_THUNK_SIZE + closure.currentExpressionClosureSize,
                    closureAddressRegister: closure.addressRegister,
                    expressions: expressions,
                    codes: expressionsCode)
                  evalCode.append(contentsOf: storeCode)
                }
                
                /// 4. Evaluate the code
                code.map {
                  evalCode.append(.move(
                    dst: .temp(frame.returnValue),
                    src: $0)
                  )
                }
                
                if config.isLazyEvaluationEnabled {
                  evalCode.append(contentsOf: [
                    .move( /// 5. store the result back into the thunk
                      dst: .mem(.temp(temp)),
                      src: .temp(frame.returnValue)),
                    .move( /// 6. replace eval function with `extract`
                      dst: .mem(.binop(
                        .plus,
                        lhs: .constant(wordSize),
                        rhs: .temp(temp))),
                      src: .name(AST.extractLabel)),
                  ])
                }
                
                /// 7.
                /// Envoke the (partial) destructor.
                ///
                /// This enables eager destruction.
                if isEagerDestructorEnabled {
                  evalCode.append(.exp(destructor))
                }
                
                /// A hackish solution.
                ///
                /// 8. Inject closure init code into correct spots.
                ///
                /// @TODO: - Very slow code!!!!
                var updatedEvalDode: CodeGenerator.SEQ = evalCode
                for childClosure in closure.childClosures {
                  updatedEvalDode = updatedEvalDode.replacing(
                    occurences: .marker(childClosure.expression),
                    with: .seq(childClosure.initialiseChildClosureCode)
                  )
                }
                
                return .pure(updatedEvalDode)^
              }
            }
          }
        case .lambda:
          return IRState.ActionsTSE.temp >>- { temp in
            var evalCode: CodeGenerator.SEQ = [
              .move( /// 1. read the argument and store it into a register
                dst: .temp(temp),
                src: .EP),
            ]
              
            /// 2. Evaluate any code that should be ran before the expressions code
            evalCode.append(contentsOf: beforeCode)

            if expressions.count > 0 {
              /// 3. Store child sub-expressions.
              let storeCode = generateStoreSubExpressionsCode(
                initialOffset: DEFAULT_THUNK_SIZE + closure.currentExpressionClosureSize,
                closureAddressRegister: closure.addressRegister,
                expressions: expressions,
                codes: expressionsCode)
              evalCode.append(contentsOf: storeCode)
            }
            
            /// 4. Evaluate the code
            evalCode.append(.move(
              dst: .temp(frame.returnValue),
              src: code!)
            )
            
            /// 5. Store free (captured) variables into the closure.
            if let childClosure = closure.childClosures.first {
              precondition(closure.childClosures.count == 1)
              evalCode.append(contentsOf: childClosure.initialiseChildClosureCode)
            }
            
            return .pure(evalCode)^
          }
        }
      }
    }
  }
  
  func generateStoreSubExpressionsCode(
    initialOffset: Byte,
    closureAddressRegister: FrameTemp,
    expressions: [ThunkSubExpressionInfo],
    codes: [IR.Expression]
  ) -> CodeGenerator.SEQ {
    expressions
      .enumerated()
      .compactMap { index, subExpr in
        guard subExpr.automaticallyMoveToClosure else { return nil }
        return .move(
          dst: subscriptIntoClosure(
            offset: initialOffset + Byte(index) * wordSize,
            closureAddress: .temp(closureAddressRegister)),
          src: codes[index])
      }
  }
  
  func subscriptIntoClosure(offset: Byte, closureAddress: IR.Expression) -> IR.Expression {
    .mem(.binop(
      .plus,
      lhs: closureAddress,
      rhs: .constant(offset))
    )
  }
  
  
  return IRState.ActionsMEM.allocateMemory( /// 0. Allocate memory for the evaluation function
    for: frame.entry,
    bytes: wordSize
  ) >>- { evalFunctionAddress in
    var variablesOnStack = target.identifiers
    for variable in frame.localVariables {
      variablesOnStack.insert(variable.bindingName)
    }
    
    let malloc: (Byte) -> IR.Expression = {
      .malloc(
        bytes: $0,
        runtimeInfo: expression.runtimeInfo
      )
    }
    
    return expressions.map { $0.code }.sequence()^ >>- { expressionsCode in
      IRState.ActionsCBE.generateClosure(
        variablesOnStack: variablesOnStack,
        staticLevel: frame.staticLevel
      ) >>- { closure in
        assert(UInt16.max > closure.currentExpressionClosureSize)
        assert(UInt16.max > expressions.count)
        
        
        ///                  4 * 8B                         +                     size of free variables                               +                 #strong references * 8B
        let closureSize = DEFAULT_THUNK_SIZE + closure.currentExpressionClosureSize + Byte(expressions.count) * wordSize
        let header = ThunkHeader(
          freeVariablesCount: UInt16(closure.currentExpressionClosureSize / wordSize),
          strongReferencesCount: UInt16(expressions.count)
        )
        
        return generateEvalCode(
          closure: closure,
          expressionsCode: expressionsCode,
          closureSize: closureSize
        ) >>- { evalCode in
          let evalChunk = IRState.CodeChunk(
            frame: frame,
            stmt: .seq(evalCode)
          )
          return generateDestructor(
            destructCount: expressions.count,
            initialOffset: DEFAULT_THUNK_SIZE + closure.currentExpressionClosureSize,
            closureSize: closureSize,
            expression: expression,
            modifyDestructorsCode: modifyDestructorsCode
          ) >>- { destructorAddress in
            IRState.ActionsCHNK.addChunk(evalChunk) >>- {
              var code: CodeGenerator.SEQ = [
                .move( /// 1. allocate memory for the closure/thunk
                  dst: .temp(closure.addressRegister),
                  src: malloc(closureSize)),
                .move( /// 2. store the eval function into the second slot
                  dst: .mem(.binop(
                    .plus,
                    lhs: .temp(closure.addressRegister),
                    rhs: .constant(wordSize))),
                  src: .constant(evalFunctionAddress)),
                .move( /// 3. store the destructor function into the third slot
                  dst: .mem(.binop(
                    .plus,
                    lhs: .temp(closure.addressRegister),
                    rhs: .constant(wordSize * 2))),
                  src: .constant(destructorAddress)),
                .move( /// 4. store header into the fourth slot
                  dst: .mem(.binop(
                    .plus,
                    lhs: .temp(closure.addressRegister),
                    rhs: .constant(wordSize * 3))),
                  src: .constant(header.to64Bits)),
              ]
              if closure.currentExpressionClosureSize > 0 {
                /// 4. Mark the spot where the closure initialising code should be injected
                code.append(.marker(expression))
              }
              return .pure(.eseq(
                .seq(code),
                .temp(closure.addressRegister))
              )^
            }
          }
        }
      }
    }
  }
}

func createThunk(
  target: ThunkTarget = .expression,                       // target for generator
  expressions: [ThunkSubExpressionInfo],                   // sub-expressions
  join: @escaping Join,                                    // join sub-expressions codes into one code
  beforeEvaluatingExpressionsDo: ThunkCode? = nil,
  modifyDestructorsCode: ((CodeGenerator.SEQ) -> IntermediateReprM<CodeGenerator.SEQ>)? = nil,
  isEagerDestructorEnabled: Bool = false, // @FIMXE: - Not used
  expression: AST.Expression                              // expression for which the closure is being generated
) -> IntermediateReprM<IR.Expression> {
  IRState.ActionsFME.frameOrNil(for: .expression(expression)) >>- { frame in
    createThunk(
      frame: frame!,
      target: target,
      expressions: expressions,
      join: join,
      beforeEvaluatingExpressionsDo: beforeEvaluatingExpressionsDo,
      modifyDestructorsCode: modifyDestructorsCode,
      isEagerDestructorEnabled: isEagerDestructorEnabled,
      expression: expression
    )
  }
}

func createThunk(
  target: ThunkTarget = .expression,                                     // target for generator
  expressions: [ThunkSubExpressionInfo],                                 // sub-expressions
  join: @escaping (([IR.Expression], [IR.Expression])) -> IR.Expression, // join sub-expressions codes into one code
  beforeEvaluatingExpressionsDo: ThunkCode? = nil,
  modifyDestructorsCode: ((CodeGenerator.SEQ) -> IntermediateReprM<CodeGenerator.SEQ>)? = nil,
  expression: AST.Expression                                            // expression for which the closure is being generated
) -> IntermediateReprM<IR.Expression> {
  IRState.ActionsFME.frameOrNil(for: .expression(expression)) >>- { frame in
    createThunk(
      frame: frame!,
      expressions: expressions,
      join: { .pure(join(($0, $1)))^ },
      beforeEvaluatingExpressionsDo: beforeEvaluatingExpressionsDo,
      modifyDestructorsCode: modifyDestructorsCode,
      expression: expression
    )
  }
}

func createThunk(
  frame: Frame,                                   // frame
  target: ThunkTarget = .expression,              // target for generator
  code: IntermediateReprM<IR.Expression>,         // code which evaluates the expression
  expression: AST.Expression                      // expression for which the closure is being generated
) -> IntermediateReprM<IR.Expression> {
  code >>- { code in
    createThunk(
      frame: frame,
      target: target,
      expressions: .empty(),
      join: { _,_  in .pure(code)^ },
      expression: expression
    )
  }
}

func createThunk(
  target: ThunkTarget = .expression,              // target for generator
  code: IntermediateReprM<IR.Expression>,         // code which evaluates the expression
  isEagerDestructorEnabled: Bool = false,         //
  expression: AST.Expression                      // expression for which the closure is being generated
) -> IntermediateReprM<IR.Expression> {
  IRState.ActionsFME.frameOrNil(for: .expression(expression)) >>- { frame in
    code >>- { code in
      createThunk(
        frame: frame!,
        target: target,
        expressions: .empty(),
        join: { _, _ in .pure(code)^ },
        isEagerDestructorEnabled: isEagerDestructorEnabled,
        expression: expression
      )
    }
  }
}

func generateDestructor(
  destructCount: Int,
  initialOffset: Byte,
  closureSize: Byte,
  expression: AST.Expression,
  modifyDestructorsCode: ((CodeGenerator.SEQ) -> IntermediateReprM<CodeGenerator.SEQ>)? = nil
) -> IntermediateReprM<Byte> {
  IRState.ActionsLSE.manyAnonymous(count: 1 + destructCount * 2 + 2) >>- { labels in
    IRState.ActionsTSE.temps(count: 2) >>- { temps in
      let destructorFrame = Frame(
        description: expression.description + "-[destructor]",
        staticLevel: 0,
        entry: labels[0],
        end: labels[0],
        localVariables: [],
        framePointer: temps[0],
        returnValue: temps[1],
        parameterCount: 2)
      return IRState.ActionsMEM.allocateMemory( /// 0. Allocate memory for the destructor
        for: destructorFrame.entry,
        bytes: wordSize
      ) >>- { destructorFunctionAddress in
        /// 1. Call children destructors
        let callChildDestructors: IntermediateReprM<CodeGenerator.SEQ> = (0..<destructCount).map { (index: Int) -> IntermediateReprM<CodeGenerator.SEQ> in
          let elementAddressInClosure: IR.Expression = .mem(.binop(
            .plus,
            lhs: .mem(.binop(
              .plus,
              lhs: .FP,
              rhs: .constant(wordSize))),
            rhs: .constant(initialOffset + Byte(index) * wordSize)))
          return envokeDestructor(
            closureAddress: elementAddressInClosure,
            isPartial: false
          ) >>- { envokeDestructorCode in
            let code: CodeGenerator.SEQ = [
              .exp(envokeDestructorCode),
              .move(
                dst: elementAddressInClosure,
                src: .constant(0)),
              .label(labels[2 + index * 2]),
            ]
            return .pure(code)^
          }
        }
          .sequence()^
          .map { $0.flatMap { $0 } }^
        
        return callChildDestructors >>- { callChildDestructors in
          var destructorCode: CodeGenerator.SEQ = callChildDestructors
          /// 2. Deallocate the memory of this thunk
          destructorCode.append(contentsOf: [
            .cjump( /// Delete memory of this thunk only if it's not a partial destructor.
              cond: .binop(.equal, lhs: .mem(.FP), rhs: .constant(1)),
              true: labels[labels.count - 2],
              false: labels[labels.count - 1]),
            .label(labels[labels.count - 2]),
            .exp(.dealloc(
              .mem(.binop(
                .plus,
                lhs: .FP,
                rhs: .constant(wordSize))),
              bytes: closureSize
            )),
            .label(labels[labels.count - 1]),
          ])
          destructorCode.append(.move(dst: .temp(destructorFrame.returnValue), src: .constant(0)))
          
          let modifiedCode = modifyDestructorsCode?(destructorCode) ?? .pure(destructorCode)^
          return modifiedCode >>- { modifiedCode in
            IRState.ActionsCHNK.addChunk(.init(
              frame: destructorFrame,
              stmt: .seq(modifiedCode))
            ) *> .pure(destructorFunctionAddress)^
          }
        }
      }
    }
  }
}

func envokeDestructor(
  closureAddress: IR.Expression,
  isPartial: Bool
) -> IntermediateReprM<IR.Expression> {
  IRState.ActionsLSE.manyAnonymous(count: 2) >>- { labels in
    let code: IR.Expression = .eseq(
      /// Do the if expression to handle the following cases:
      /// a :: &Int
      /// b :: &Int
      /// a = b
      /// b = a
      .seq([
        .cjump(
          cond: .binop(.logical(.notEqual), lhs: closureAddress, rhs: .constant(0)),
          true: labels[0],
          false: labels[1]),
        .label(labels[0]),
        .exp(.call(
          fn: .mem(.binop(
            .plus,
            lhs: closureAddress,
            rhs: .constant(wordSize * 2))),
          args: [
            .constant(!isPartial),
            closureAddress
          ])),
        .label(labels[1])
      ]),
      .constant(0)
    )
    return .pure(code)^
  }
}

func evaluateThunk(
  expr: IR.Expression,
  lambdaParameter: IR.Expression? = nil
) -> IntermediateReprM<IR.Expression> {
  IRState.ActionsTSE.temp >>- { temp in
    var args: [IR.Expression] = [
      .constant(0), // @TODO: - Remove, not used
      .temp(temp),
    ]
    if let lambdaParameter = lambdaParameter {
      args.append(lambdaParameter)
    }
    let eseq = IR.Expression.eseq(
      .seq([
        .move( // 1. evaluate the expression and store the
               // address of the thunk into a register
          dst: .temp(temp),
          src: expr),
      ]),
      .call(   // 3. evaluate the thunk (the function in the 2nd slot)
        fn: .mem(.binop(
          .plus,
          lhs: .temp(temp),
          rhs: .constant(wordSize))),
        args: args))
    return .pure(eseq)^
  }
}

func evaluateThunk(
  expr: IntermediateReprM<IR.Expression>,
  lambdaParameter: IR.Expression? = nil
) -> IntermediateReprM<IR.Expression> {
  expr >>- {
    evaluateThunk(
      expr: $0,
      lambdaParameter: lambdaParameter
    )
  }
}

func evaluateThunk(
  expr: IR.Expression,
  lambdaParameter: IntermediateReprM<IR.Expression>
) -> IntermediateReprM<IR.Expression> {
  lambdaParameter >>- { argCode in
    evaluateThunk(
      expr: expr,
      lambdaParameter: argCode
    )
  }
}

func evaluateThunk(
  expr: IR.Expression
) -> IntermediateReprM<IR.Expression> {
  evaluateThunk(
    expr: expr,
    lambdaParameter: nil
  )
}

func createEvaluatedThunk(
  code: IR.Expression,
  expression: AST.Expression
) -> IntermediateReprM<ExpressionTree.Expression> {
  generateDestructor(
    destructCount: 0,
    initialOffset: 0,
    closureSize: DEFAULT_THUNK_SIZE,
    expression: expression,
    modifyDestructorsCode: nil
  ) >>- { destructorAddess in
    IRState.ActionsTSE.temp >>- { temp in
      let code: CodeGenerator.SEQ = [
        .move( /// 1. allocate memory for the thunk
          dst: .temp(temp),
          src: .malloc(
            bytes: DEFAULT_THUNK_SIZE,
            runtimeInfo: expression.runtimeInfo)),
        .move( /// 2. store the value into the 1st slot
          dst: .mem(.temp(temp)),
          src: code),
        .move( /// 3. store __extract__ into the 2nd slot
          dst: .mem(.binop(
            .plus,
            lhs: .temp(temp),
            rhs: .constant(wordSize))),
          src: .name(AST.extractLabel)),
        .move( /// 4. store destructor into the 3rd slot
          dst: .mem(.binop(
            .plus,
            lhs: .temp(temp),
            rhs: .constant(wordSize * 2))),
          src: .constant(destructorAddess)),
      ]
      return .pure(.eseq(.seq(code), .temp(temp)))^
    }
  }
}

func createEvaluatedThunk(
  code: IntermediateReprM<IR.Expression>,
  expression: AST.Expression
) -> IntermediateReprM<ExpressionTree.Expression> {
  code >>- { code in
    createEvaluatedThunk(code: code, expression: expression)
  }
}

func envokeDestructorsForActiveBindings() -> IntermediateReprM<CodeGenerator.SEQ> {
  IRState.ActionsCFG.config >>- { config in
    IRState.ActionsDE.activeBindings() >>- { activeBindings in
      activeBindings
        .reversed()
        .filter { !$0.body.isNameBehindReference } // skip bindings such as: `x = &y`
        .map {
          AST.Expression.name(
            position: .next,
            identifier: $0.name,
            lifetime: $0.lifetime,
            bindingLifetime: $0.lifetime
          )
        }
      .map { $0.generateIR() }
      .sequence()^ >>- { bindingAccessCodes in
        bindingAccessCodes.map {
          envokeDestructor(closureAddress: $0, isPartial: false)
        }.sequence()^ >>- { code in
          if config.envokeDestructors {
            return .pure(code.map { .exp($0) })^
          }
          return .pure([])^
        }
      }
    }
  }
}


extension IR.Expression {
  static var FP: Self {
    .name("FP")
  }
  
  static var EP: Self {
    /// EP is located on the stack as the first argument of the
    /// closure currently being executed.
    .mem(.binop(.plus, lhs: .FP, rhs: .constant(wordSize)))
  }
}

extension AST.Expression {
  func inNewScope(
    do work: @escaping () -> IntermediateReprM<IR.Expression>
  ) -> IntermediateReprM<IR.Expression> {
    IRState.ActionsFME.frameOrNil(
      for: .expression(self)
    ) >>- { frame in
      inNewScope(frame: frame!, do: work)
    }
  }
  
  func inNewScope(
    frame: Frame,
    do work: @escaping () -> IntermediateReprM<IR.Expression>
  ) -> IntermediateReprM<IR.Expression> {
    IRState.ActionsCME.query(node: .expression(self)) >>- { codeOrNil in
      guard codeOrNil == nil else {
        return .pure(codeOrNil!)^
      }
      
      return IRState.ActionsCBE.inNewScope(
        do: {
          IRState.ActionsFS.pushing(
            frame: frame,
            do: work
          ) >>- { code in
            IRState.ActionsCME.store(
              code: code,
              for: .expression(self)
            ) *> .pure(code)^
          }
        },
        expression: self
      )
    }
  }
}

extension CodeGenerator.SEQ {
  func replacing(
    occurences of: IR.Statement,
    with statement: IR.Statement
  ) -> Self {
    map { $0.replacing(occurences: of, with: statement) }
  }
}
        
extension IR.Statement {
  func replacing(
    occurences of: IR.Statement,
    with statement: IR.Statement
  ) -> Self {
    if of == self {
      return statement
    }
    switch self {
    case .move(dst: let dst, src: let src):
      return .move(
        dst: dst.replacing(occurences: of, with: statement),
        src: src.replacing(occurences: of, with: statement))
    case .exp(let exp):
      return .exp(exp.replacing(occurences: of, with: statement))
    case let .cjump(cond, t, f):
      return .cjump(
        cond: cond.replacing(occurences: of, with: statement),
        true: t,
        false: f)
    case let .seq(seq):
      return .seq(seq.replacing(occurences: of, with: statement))
    case .jump, .label, .trap:
      return self
    case .marker:
      return self
    }
  }
}

extension IR.Expression {
  func replacing(
    occurences of: IR.Statement,
    with statement: IR.Statement
  ) -> Self {
    switch self {
    case .constant, .name, .temp, .malloc, .dealloc, .print:
      return self
    case .binop(let op, lhs: let lhs, rhs: let rhs):
      return .binop(
        op,
        lhs: lhs.replacing(occurences: of, with: statement),
        rhs: rhs.replacing(occurences: of, with: statement))
    case .mem(let exp):
      return .mem(exp.replacing(occurences: of, with: statement))
    case .call(fn: let fn, args: let args):
      return .call(
        fn: fn.replacing(occurences: of, with: statement),
        args: args.map { $0.replacing(occurences: of, with: statement) })
    case let .eseq(stmt, expr):
      return .eseq(
        stmt.replacing(occurences: of, with: statement),
        expr.replacing(occurences: of, with: statement))
    }
  }
}

extension Array where Element == IR.Expression {
  func replacing(
    occurences of: IR.Statement,
    with statement: IR.Statement
  ) -> Self {
    map { $0.replacing(occurences: of, with: statement) }
  }
}

func id<T>(_ val: T) -> IntermediateReprM<T> { .pure(val)^ }

extension AST.Expression {
  var strongDependencies: Set<String> {
    let dependencies = strongDependencies(ignoreIdentifiers: .empty())
    return dependencies
  }
  
  private func strongDependencies(ignoreIdentifiers: Set<String>) -> Set<String> {
    switch self {
    // ---------- special care ----------
    case let .name(_, identifier, type, _, _):
      guard !ignoreIdentifiers.contains(identifier) else { return .empty() }
      return (type!.isReference || type!.isArrow) ? [] : [identifier]
    case .ref:
      return .empty()
    case .deref:
      return .empty()
    // ----------------------------------
      
    case let .binary(.application(_, fn, arg, _, _)):
      return fn.strongDependencies(ignoreIdentifiers: ignoreIdentifiers).union(arg.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
    case let .binary(.arithmetic(_, _, left, right, _, _)):
      return left.strongDependencies(ignoreIdentifiers: ignoreIdentifiers).union(right.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
    case let .binary(.concat(_, left, right, _, _)):
      return left.strongDependencies(ignoreIdentifiers: ignoreIdentifiers).union(right.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
    case let .binary(.ifThenElse(_, condition, t, f, _, _)):
      return condition
        .strongDependencies(ignoreIdentifiers: ignoreIdentifiers)
        .union(t.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
        .union(f.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
    case let .binary(.logical(_, _, left, right, _, _)):
      return left
        .strongDependencies(ignoreIdentifiers: ignoreIdentifiers)
        .union(right.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
    case .literal:
      return .empty()
    case let .lambda(_, parameter, body, _, _):
      return body.strongDependencies(ignoreIdentifiers: ignoreIdentifiers + parameter.identifiers)
    case let .tuple(_, expressions, _, _):
      return expressions.reduce(into: Set<String>.empty()) {
        $0.formUnion($1.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
      }
    case let .list(_, expressions, _, _):
      return expressions.reduce(into: Set<String>.empty()) {
        $0.formUnion($1.strongDependencies(ignoreIdentifiers: ignoreIdentifiers))
      }
    case let .let(_, bindings, expression, _, _):
      let names = bindings.reduce(into: Set<String>.empty()) { $0.formUnion([$1.name]) }
      let ignoreIdentifiers = ignoreIdentifiers + names
      return expression.strongDependencies(ignoreIdentifiers: ignoreIdentifiers)
        .union(bindings.reduce(into: Set<String>.empty()) { $0.formUnion($1.expression?.strongDependencies(ignoreIdentifiers: ignoreIdentifiers) ?? .empty()) })
    case let .case(_, expression, matches, _, _):
      return expression.strongDependencies(ignoreIdentifiers: ignoreIdentifiers)
        .union(matches.reduce(into: Set<String>.empty()) {
          $0.formUnion($1.expression.strongDependencies(ignoreIdentifiers: ignoreIdentifiers + $1.pattern.identifiers))
        })
    }
  }
  
  var isName: Bool {
    switch self {
    case .name:
      return true
    case _:
      return false
    }
  }
  
  var isNameBehindReference: Bool {
    switch self {
    case .name where type!.isReference:
      return true
    case _:
      return false
    }
  }
}

extension AST.Binding {
  var expression: AST.Expression? {
    switch self {
    case .binding(_, _, let cases, _, _, _):
      precondition(cases.count == 1)
      return cases[0].body
    case _:
      return nil
    }
  }
}

extension ThunkSubExpressionInfo {
  static func dummyLiteralNode(
    transformation: @escaping (IR.Expression) -> IntermediateReprM<IR.Expression> = id,
    automaticallyMoveToClosure: Bool = false
  ) -> Self {
    .init(
      expression: .literal(
        position: .init(
          start: .init(line: -1, column: -1),
          end: .init(line: -1, column: -1)),
        literal: .int(0)),
      transformation: transformation,
      automaticallyMoveToClosure: automaticallyMoveToClosure
    )
  }
}

extension AST.Pattern {
  var createBindings: [AST.Binding.Case] {
    switch self {
    case let .identifier(position, identifier, type, lifetime):
      return [.init(
        name: identifier,
        position: position,
        parameters: [],
        body: .literal(position: .zero, literal: .int(0)),
        type: type,
        lifetime: lifetime)
      ]
    case .literal:
      return []
    case let .deconstruct(_, _, patterns, _, _):
      return patterns.flatMap { $0.createBindings }
    case .list:
      fatalError()
    case let .tuple(_, patterns, _, _):
      return patterns.flatMap { $0.createBindings }
    case let .ref(_, pattern, _, _):
      return pattern.createBindings
    case .wildcard:
      return []
    }
  }
}

extension AST.Expression {
  var runtimeInfo: String {
    self.description+(self.type.map { " :: "+$0.description} ?? "")
  }
}

func +<A>(_ lhs: Set<A>, _ rhs: Set<A>) -> Set<A> {
  lhs.union(rhs)
}
