//
//  IRMonad.swift
//  
//
//  Created by Toni K. Turk on 09/01/2022.
//

import Foundation
import Bow

public typealias IntermediateReprM<A> = State<IRState, A>

public protocol IRConvertible {
  associatedtype Result
  func generateIR() -> IntermediateReprM<Result>
}
