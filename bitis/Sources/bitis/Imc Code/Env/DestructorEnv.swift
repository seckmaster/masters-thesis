//
//  DestructorEnv.swift
//  
//
//  Created by Toni K. Turk on 28/06/2022.
//

import Foundation
import Collections

public struct DestructorEnv: HasScope {
  var scopes: [Scope]
  
  public func pushScope() -> DestructorEnv {
    .init(scopes: scopes.appending((.init())))
  }
  
  public func popScope() -> DestructorEnv {
    .init(scopes: scopes.dropLast())
  }
}

extension DestructorEnv {
  struct Scope {
    typealias Cases = OrderedSet<AST.Binding.Case>
    
    var activeBindings: Cases
    
    init() {
      activeBindings = .init()
    }
  }
}
