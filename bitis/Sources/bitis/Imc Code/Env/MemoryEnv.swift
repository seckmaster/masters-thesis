//
//  MemoryEnv.swift
//  
//
//  Created by Toni K. Turk on 14/01/2022.
//

import Foundation

public struct MemoryEnv {
  public var memory: Interpreter.Memory
  
  /// Initial offset from 0x00.
  init(heapPointer: Byte = wordSize, size: Byte) {
    self.memory = .init(size: size)
    self.memory.heapPointer = heapPointer
  }
  
  init(memory: Interpreter.Memory) {
    self.memory = memory
  }
}

extension MemoryEnv {
  func allocateMemory(for label: FrameLabel, bytes: Byte) -> Byte {
    if let address = memory.labelToAddressMapping[label] { return address }
    let address = memory.heapPointer
    memory.labelToAddressMapping[label] = address
    memory.heapPointer += bytes
    return address
  }
  
  func allocateMemory(bytes: Byte) -> Byte {
    let address = memory.heapPointer
    memory.heapPointer += bytes
    return address
  }
}
