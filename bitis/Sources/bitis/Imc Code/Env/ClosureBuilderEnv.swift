//
//  ClosureBuilderEnv.swift
//  
//
//  Created by Toni K. Turk on 21/01/2022.
//

import Foundation
import Collections

public struct ClosureBuilderEnv {
  typealias Element = (offsetInClosure: Int, access: Access)
  
  struct Scope {
    var mapping: OrderedDictionary<String, Element>
    let register: FrameTemp
    let expression: AST.Expression
    var childClosures: [ChildClosure]
    
    init(
      register: FrameTemp,
      expression: AST.Expression
    ) {
      self.mapping = .init()
      self.register = register
      self.expression = expression
      self.childClosures = .empty()
    }
  }
 
  var scopes: [Scope]
  
  static var empty: Self {
    .init(scopes: [])
  }
  
  public func pushScope(
    register: FrameTemp,
    expression: AST.Expression
  ) -> ClosureBuilderEnv {
    var env = self
    env.scopes.append(.init(
      register: register,
      expression: expression
    ))
    return env
  }
  
  public func popScope() -> ClosureBuilderEnv {
    var env = self
    _ = env.scopes.popLast()!
    return env
  }
}

extension ClosureBuilderEnv.Scope {
  struct ChildClosure {
    let addressRegister: FrameTemp
    var mustBeInitialisedByParent: [String: Byte]
    let expression: AST.Expression
  }
}
