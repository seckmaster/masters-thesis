//
//  IRState.swift
//  
//
//  Created by Toni K. Turk on 09/01/2022.
//

import Foundation
import SwiftParsec
import Bow
import OrderedCollections

public struct IRState {
  var fme: FrameMappingEnv
  var ame: AccessMappingEnv
  public var lse: LabelStreamEnv
  public var tse: TempStreamEnv
  public var mem: MemoryEnv
  public var chunks: Set<CodeChunk>
  var frameStack: [Frame]
  var cbe: ClosureBuilderEnv
  var dataBindings: [String: Int] // map constructor to its id
  var codeMappingEnv: ASTMap<IR.Expression>
  var destructorEnv: DestructorEnv
  var bindingsStack: [[String: AST.Binding.Case]]
  let config: Configuration
  
  public init(
    fme: FrameMappingEnv,
    ame: AccessMappingEnv,
    lse: LabelStreamEnv,
    tse: TempStreamEnv,
    memorySize: Byte,
    config: Configuration
  ) {
    self.fme = fme
    self.ame = ame
    self.lse = lse
    self.tse = tse
    self.mem = .init(size: memorySize)
    self.chunks = []
    self.frameStack = []
    self.cbe = .empty
    self.dataBindings = .empty()
    self.codeMappingEnv = .init()
    self.destructorEnv = .init(scopes: [.init()])
    self.config = config
    self.bindingsStack = [[:]]
  }
  
  public init(
    fme: FrameMappingEnv,
    ame: AccessMappingEnv,
    lse: LabelStreamEnv,
    tse: TempStreamEnv,
    memory: Interpreter.Memory,
    config: Configuration
  ) {
    self.fme = fme
    self.ame = ame
    self.lse = lse
    self.tse = tse
    self.mem = .init(memory: memory)
    self.chunks = []
    self.frameStack = []
    self.cbe = .empty
    self.dataBindings = .empty()
    self.codeMappingEnv = .init()
    self.destructorEnv = .init(scopes: [.init()])
    self.config = config
    self.bindingsStack = [[:]]
  }
}

public extension IRState {
  struct Configuration {
    /// Whether evaluating thunks should override them with computed value.
    public let isLazyEvaluationEnabled: Bool
    /// Should constants be evaluated by default.
    public let shouldAutomaticallyEvaluateConstants: Bool
    /// Should the destructors be envoked or not.
    public let envokeDestructors: Bool
    /// Is running in REPL
    public let isRunningInREPL: Bool
    
    public init(
      isLazyEvaluationEnabled: Bool,
      shouldAutomaticallyEvaluateConstants: Bool,
      envokeDestructors: Bool,
      isRunningInREPL: Bool
    ) {
      self.isLazyEvaluationEnabled = isLazyEvaluationEnabled
      self.shouldAutomaticallyEvaluateConstants = shouldAutomaticallyEvaluateConstants
      self.envokeDestructors = envokeDestructors
      self.isRunningInREPL = isRunningInREPL
    }
    
    public static var `default`: Configuration {
      .init(
        isLazyEvaluationEnabled: true,
        shouldAutomaticallyEvaluateConstants: true,
        envokeDestructors: true,
        isRunningInREPL: false
      )
    }
  }
}

public extension IRState {
  enum ActionsFME {
    static func frameOrNil(for node: FrameMappingEnv.Node) -> IntermediateReprM<Frame?> {
      .init { env -> (IRState, Frame?) in
        (env, env.fme.scope[node])
      }
    }
   
    static func bind(frame: Frame, for node: FrameMappingEnv.Node) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        env.fme.scope[node] = frame
        if env.fme.scope[node] != nil {
          warning("IRState.ActionsFME.bind(frame:for): environment already contains a node with the same hash!")
        }
        return (env, ())
      }
    }
  }
}

public extension IRState {
  enum ActionsAME {
    static func access(for node: AccessMappingEnv.Node) -> IntermediateReprM<Access> {
      .init { env -> (IRState, Access) in
        (env, env.ame.scope[node]!)
      }
    }
    
    static func bind(access: Access, for node: AccessMappingEnv.Node) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        if env.ame.scope[node] != nil {
          warning("IRState.ActionsAME.bind(access:for): environment already contains a node with the same hash!")
        }
        env.ame.scope[node] = access
        return (env, ())
      }
    }
  }
}

public extension IRState {
  enum ActionsLSE {
    static var anonymous: IntermediateReprM<FrameLabel> {
      .init { env -> (IRState, FrameLabel) in
        let label = FrameLabel(name: "L\(env.lse.anonymousLabelCount)")
        var env = env
        env.lse.anonymousLabelCount += 1
        return (env, label)
      }
    }
    
    static func manyAnonymous(count: Int) -> IntermediateReprM<[FrameLabel]> {
      .init { env -> (IRState, [FrameLabel]) in
        let labels = (0..<count)
          .map { FrameLabel(name: "L\(env.lse.anonymousLabelCount + $0)") }
        var env = env
        env.lse.anonymousLabelCount += count
        return (env, labels)
      }
    }
  }
}

public extension IRState {
  enum ActionsTSE {
    static var temp: IntermediateReprM<FrameTemp> {
      .init { env -> (IRState, FrameTemp) in
        let temp = FrameTemp(integerLiteral: env.tse.count)
        var env = env
        env.tse.count += 1
        return (env, temp)
      }
    }
    
    static func temps(count: Int) -> IntermediateReprM<[FrameTemp]> {
      .init { env -> (IRState, [FrameTemp]) in
        let temps = (0..<count)
          .map { FrameTemp(integerLiteral: $0 + env.tse.count) }
        var env = env
        env.tse.count += count
        return (env, temps)
      }
    }
  }
}

public extension IRState {
  enum ActionsMEM {
    static func allocateMemory(for label: FrameLabel, bytes: Byte) -> IntermediateReprM<Byte> {
      .init { env -> (IRState, Byte) in
        let address = env.mem.allocateMemory(for: label, bytes: bytes)
        return (env, address)
      }
    }
    
    static func allocateMemory(
      for label: FrameLabel,
      bytes: Byte,
      andStore value: Interpreter.Memory.Value
    ) -> IntermediateReprM<Byte> {
      .init { env -> (IRState, Byte) in
        if let address = env.mem.memory.labelToAddressMapping[label] {
          return (env, address)
        }
        let address = env.mem.allocateMemory(for: label, bytes: bytes)
        try! env.mem.memory.stM(address: address, value: value)
        return (env, address)
      }
    }
    
    static func allocateMemory(
      for label: FrameLabel,
      bytes: Byte,
      andStore chunk: CodeGenerator.CodeChunk
    ) -> IntermediateReprM<Byte> {
      .init { env -> (IRState, Byte) in
        let address = env.mem.allocateMemory(for: label, bytes: bytes)
        try! env.mem.memory.stM(address: address, value: chunk)
        return (env, address)
      }
    }
    
    static func allocateMemory(
      bytes: Byte,
      andStore value: Interpreter.Memory.Value
    ) -> IntermediateReprM<Byte> {
      .init { env -> (IRState, Byte) in
        let address = env.mem.allocateMemory(bytes: bytes)
        try! env.mem.memory.stM(address: address, value: value)
        return (env, address)
      }
    }
    
    static func allocateMemory(
      bytes: Byte,
      andStore chunk: CodeGenerator.CodeChunk
    ) -> IntermediateReprM<Byte> {
      .init { env -> (IRState, Byte) in
        let address = env.mem.allocateMemory(bytes: bytes)
        try! env.mem.memory.stM(address: address, value: chunk)
        return (env, address)
      }
    }
    
    static func store(
      value: Interpreter.Memory.Value,
      at address: Interpreter.Memory.Address
    ) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        try! env.mem.memory.stM(address: address, value: value)
        return (env, ())
      }
    }
  }
}

public extension IRState {
  struct CodeChunk: Hashable {
    public let frame: Frame
    public let stmt: IR.Statement
    
    public func hash(into hasher: inout Hasher) {
      hasher.combine(frame.entry)
    }
  }
  
  enum ActionsCHNK {
    static func addChunk(_ chunk: CodeChunk) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        env.chunks.insert(chunk)
        return (env, ())
      }
    }
  }
}

public extension IRState {
  enum ActionsFS {
    static func pushing<A>(frame: Frame, do work: () -> IntermediateReprM<A>) -> IntermediateReprM<A> {
      let push: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.frameStack.append(frame)
        return (env, ())
      }
      let pop: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        _ = env.frameStack.popLast()
        return (env, ())
      }
      return push *> work() <* pop
    }
    
    static var peek: IntermediateReprM<Frame> {
      .init { env -> (IRState, Frame) in
        (env, env.frameStack.last!)
      }
    }
    
    static var peekNextToLast: IntermediateReprM<Frame> {
      .init { env -> (IRState, Frame) in
        (env, env.frameStack.dropLast().last!)
      }
    }
    
    static var peekOrNil: IntermediateReprM<Frame?> {
      .init { env -> (IRState, Frame?) in
        (env, env.frameStack.last)
      }
    }
  }
}

extension IRState {
  enum ActionsCBE {
    typealias Closure = [(variable: String, access: ClosureBuilderEnv.Element)]
    
    struct ClosureInfo {
      // the size of the free variables (#free variables * wordSize) for
      // the expression generating the closure
      let currentExpressionClosureSize: Byte
      // the register which will contain the address of the closure
      let addressRegister: FrameTemp // @TODO: - Try to implement without temporaries ...
      // child closures
      let childClosures: [ChildClosure]
    }
    
    struct ChildClosure {
      // code that initialises the elements of the child thunk/closure
      let initialiseChildClosureCode: CodeGenerator.SEQ
      // expression for which this closure is generated
      let expression: AST.Expression
    }
    
    static func inNewScope<A>(
      do work: () -> IntermediateReprM<A>,
      expression: AST.Expression
    ) -> IntermediateReprM<A> {
      let push: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.cbe = env.cbe.pushScope(
          register: .init(identifier: env.tse.count),
          expression: expression
        )
        env.tse.count += 1
        return (env, ())
      }
      let pop: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.cbe = env.cbe.popScope()
        return (env, ())
      }
      return push *> work() <* pop
    }
    
    static func registerOffset(
      for name: String,
      access: Access
    ) -> IntermediateReprM<(Int, FrameTemp)> {
      .init { env -> (IRState, (Int, FrameTemp)) in
        let count = env.cbe.scopes.count
        precondition(env.cbe.scopes.indices.contains(count - 1))
        
        if let existing = env.cbe.scopes[count - 1].mapping[name] {
          return (env, (existing.offsetInClosure, env.cbe.scopes.last!.register))
        }
        
        var newEnv = env
        let offset = Int(env.cbe.scopes[env.cbe.scopes.count - 1].mapping
          .max { $0.value.offsetInClosure < $1.value.offsetInClosure }
          .map { Byte($0.value.offsetInClosure) + wordSize } ?? DEFAULT_THUNK_SIZE)
        
        newEnv.cbe.scopes[count - 1].mapping[name] = (
          offsetInClosure: offset,
          access: access)
        return (newEnv, (offset, env.cbe.scopes.last!.register))
      }
    }
    
    static func generateClosure(
      variablesOnStack: Set<String>,
      staticLevel: UInt
    ) -> IntermediateReprM<ClosureInfo> {
      func generateCode(
        for access: ClosureBuilderEnv.Element,
        offsetInChildClosure: Byte,
        closureAddressRegister: FrameTemp,
        fromStack: Bool
      ) -> IR.Statement {
        switch fromStack {
        case false:
          return .move(
            dst: .mem(.binop(
              .plus,
              lhs: .temp(closureAddressRegister),
              rhs: .constant(offsetInChildClosure))),
            src: .mem(.binop(
              .plus,
              lhs: .EP,
              rhs: .constant(access.offsetInClosure))))
        case true:
          let offset: Int
          
          switch access.access.kind {
          case let .parameter(_, offset_):
            offset = Int(offset_)
          case let .local(access, _):
            offset = access.offset
          case .global:
            fatalError("TODO")
          }
          
          return .move(
            dst: .mem(.binop(
              .plus,
              lhs: .temp(closureAddressRegister),
              rhs: .constant(offsetInChildClosure))),
            src: .mem(.binop(
              .plus,
              lhs: .FP,
              rhs: .constant(offset)))
          )
        }
      }
      
      return .init { env -> (IRState, ClosureInfo) in
        let scope = env.cbe.scopes.last!
        var env = env
        
        var childClosureInitCodes: [CodeGenerator.SEQ] = .init(
          repeating: .empty(),
          count: scope.childClosures.count)
        var mustBeInitialisedByParent: [String: Byte] = .empty()
        
        for (variable, access) in scope.mapping {
          /// 1.
          if access.access.staticLevel == staticLevel && variablesOnStack.contains(variable) {
            /// Variable lives on the stack of the current expression closure.
            /// Just generate the code for its initialisation and stop.
            
            for (index, childClosure) in scope.childClosures.enumerated() {
              guard let offset = childClosure.mustBeInitialisedByParent[variable] else { continue }
              let initCode = generateCode(
                for: access,
                offsetInChildClosure: offset,
                closureAddressRegister: childClosure.addressRegister,
                fromStack: true)
              childClosureInitCodes[index].append(initCode)
            }
            continue /// nothing more to be done
          }
          
          /// 2.
          for (index, childClosure) in scope.childClosures.enumerated() {
            guard let offset = childClosure.mustBeInitialisedByParent[variable] else { continue }
            /// The  child requested to initialise this variable, so lets do it ...
            let initCode = generateCode(
              for: access,
              offsetInChildClosure: offset,
              closureAddressRegister: childClosure.addressRegister,
              fromStack: false)
            childClosureInitCodes[index].append(initCode)
          }
          
          /// 3.
          /// Variable must be initialised by parent, so remember it ...
          if mustBeInitialisedByParent[variable] == nil {
            mustBeInitialisedByParent[variable] = Byte(access.offsetInClosure)
          } else {
            fatalError()
          }
          
          guard env.cbe.scopes.count >= 2 else { continue }
          if env.cbe.scopes[env.cbe.scopes.count - 2].mapping[variable] == nil {
            let newOffset = Int(env.cbe.scopes[env.cbe.scopes.count - 2].mapping
              .max { $0.value.offsetInClosure < $1.value.offsetInClosure }
              .map { Byte($0.value.offsetInClosure) + wordSize } ?? DEFAULT_THUNK_SIZE)
            
            env.cbe.scopes[env.cbe.scopes.count - 2].mapping[variable] = (
              offsetInClosure: newOffset,
              access: access.access
            )
          }
        }
          
        /// 4. Notifiy the parent that a new child closure exists.
        if env.cbe.scopes.count >= 2 && !mustBeInitialisedByParent.isEmpty {
          env.cbe.scopes[env.cbe.scopes.count - 2].childClosures.append(.init(
            addressRegister: scope.register,
            mustBeInitialisedByParent: mustBeInitialisedByParent,
            expression: scope.expression
          ))
        }
        
        let childClosures = scope.childClosures.enumerated().map {
          ChildClosure(
            initialiseChildClosureCode: childClosureInitCodes[$0.offset],
            expression: $0.element.expression
          )
        }
        let closureInfo = ClosureInfo(
          currentExpressionClosureSize: Byte(scope.mapping.count) * wordSize,
          addressRegister: scope.register,
          childClosures: childClosures)
        return (env, closureInfo)
      }
    }
  }
}

extension IRState {
  // MARK: - Data Bindings
  enum ActionsDB {
    static func register(id: Int, for constructor: String) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        guard env.dataBindings[constructor] == nil else {
          fatalError("IRState.ActionsDB.register(id:for): Constructor \(constructor) already exists!")
        }
        var env = env
        env.dataBindings[constructor] = id
        return (env, ())
      }
    }
    
    static func id(for constructor: String) -> IntermediateReprM<Int> {
      .init { env -> (IRState, Int) in
        guard let constructorId = env.dataBindings[constructor] else {
          fatalError("IRState.ActionsDB.id(for:): constructor '\(constructor)' not found!")
        }
        return (env, constructorId)
      }
    }
    
    static var allConstructors: IntermediateReprM<Dictionary<String, Int>.Keys> {
      .init { env -> (IRState, Dictionary<String, Int>.Keys) in
        (env, env.dataBindings.keys)
      }
    }
  }
}

public extension IRState {
  enum ActionsCME {
    static func store(code: IR.Expression, for node: ASTMap<IR.Expression>.Node) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        #if DEBUG
        if let existing = env.codeMappingEnv.scope[node], existing != code {
          warning("IRState.ActionsCME.bind(code:for): environment already contains a node with the same hash!", node.description)
        }
        #endif
        env.codeMappingEnv[node] = code
        return (env, ())
      }
    }
    
    static func query(node: ASTMap<IR.Expression>.Node) -> IntermediateReprM<IR.Expression?> {
      .init { env -> (IRState, IR.Expression?) in
        (env, env.codeMappingEnv[node])
      }
    }
  }
}

extension IRState {
  // MARK: - Config
  enum ActionsCFG {
    static var config: IntermediateReprM<Configuration> {
      .init { env -> (IRState, Configuration) in
        (env, env.config)
      }
    }
  }
}

public extension IRState {
  enum ActionsDE {
    static func inNewScope<T>(do work: () -> IntermediateReprM<T>) -> IntermediateReprM<T> {
      let push: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.destructorEnv = env.destructorEnv.pushScope()
        return (env, ())
      }
      let pop: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.destructorEnv = env.destructorEnv.popScope()
        return (env, ())
      }
      return push *> work() <* pop
    }
    
    static func addBinding(_ binding: AST.Binding.Case) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        env.destructorEnv.scopes[env.destructorEnv.scopes.count - 1].activeBindings.append(binding)
        return (env, ())
      }
    }
    
    static func addBindings<C: Collection>(_ bindings: C) -> IntermediateReprM<()> where C.Element == AST.Binding.Case {
      .init { env -> (IRState, ()) in
        var env = env
        for binding in bindings {
          env.destructorEnv.scopes[env.destructorEnv.scopes.count - 1].activeBindings.append(binding)
        }
        return (env, ())
      }
    }
    
    static func removeDependencies<C: Collection>(_ bindings: C) -> IntermediateReprM<()> where C.Element == String {
      .init { env -> (IRState, ()) in
        var env = env
        // @NOTE: - O(n^3)
        // @TODO: - Optimise
        for binding in bindings {
        loop: for (index, scope) in env.destructorEnv.scopes.enumerated().reversed() {
            for (index2, s) in scope.activeBindings.enumerated() {
              if s.name == binding {
                env.destructorEnv.scopes[index].activeBindings.remove(at: index2)
                break loop
              }
            }
          }
        }
        return (env, ())
      }
    }
    
    static func activeBindings() -> IntermediateReprM<DestructorEnv.Scope.Cases> {
      .init { env -> (IRState, DestructorEnv.Scope.Cases) in
        (env, env.destructorEnv.scopes.last!.activeBindings)
      }
    }
  }
}

public extension IRState {
  enum ActionsBS {
    static func inNewScope<T>(do work: () -> IntermediateReprM<T>) -> IntermediateReprM<T> {
      let push: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        env.bindingsStack.append(.empty())
        return (env, ())
      }
      let pop: IntermediateReprM<()> = .init { env -> (IRState, ()) in
        var env = env
        _ = env.bindingsStack.popLast()
        return (env, ())
      }
      return push *> work() <* pop
    }
    
    static func addBinding(_ binding: AST.Binding.Case) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        env.bindingsStack[env.bindingsStack.count - 1][binding.name] = binding
        return (env, ())
      }
    }
   
    static func addBindings(_ bindings: [AST.Binding.Case]) -> IntermediateReprM<()> {
      .init { env -> (IRState, ()) in
        var env = env
        for binding in bindings {
          env.bindingsStack[env.bindingsStack.count - 1][binding.name] = binding
        }
        return (env, ())
      }
    }
    
    static func binding(for name: String) -> IntermediateReprM<AST.Binding.Case> {
      .init { env -> (IRState, AST.Binding.Case) in
        for scope in env.bindingsStack.reversed() {
          if let binding = scope[name] {
            return (env, binding)
          }
        }
        fatalError()
      }
    }
  }
}
