//
// Created by Toni K. Turk on 29/10/2020.
//

import Foundation

public protocol PrettyStringConvertible {
  var prettyDescription: String { get }
}

extension AST: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}
extension AST.Binding: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}

extension AST.Expression: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}

extension AST.Binding.Case: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}

extension AST.AType: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}

extension AST.Pattern: PrettyStringConvertible {
  public var prettyDescription: String {
    pretty(ast: self, indent: 0)
  }
}

func pretty(ast: AST, indent: Int) -> String {
  ast.bindings
    .map { pretty(ast: $0, indent: indent) }
    .joined(separator: "\n")
}


// MARK: - Expression
func pretty(ast: AST.Expression, indent: Int) -> String {
  switch ast {
  case let .binary(.logical(position, op, left, right, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Logical expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(op.description.with(indent: indent))
           \(pretty(ast: left, indent: indent + 2))
           \(pretty(ast: right, indent: indent + 2))
           """
  case let .binary(.arithmetic(position, op, left, right, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Arithmetic expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(op.description.with(indent: indent))
           \(pretty(ast: left, indent: indent + 2))
           \(pretty(ast: right, indent: indent + 2))
           """
  case let .binary(.ifThenElse(position, condition, t, f, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("If expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: condition, indent: indent + 2))
           \(pretty(ast: t, indent: indent + 2))
           \(pretty(ast: f, indent: indent + 2))
           """
  case let .binary(.concat(position, left, right, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Concat expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: left, indent: indent + 2))
           \(pretty(ast: right, indent: indent + 2))
           """
  case let .binary(.application(position, fn, arg, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Function application") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: fn, indent: indent + 2))
           \(pretty(ast: arg, indent: indent + 2))
           """
  case let .tuple(position, expressions, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Tuple expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(expressions.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           """
  case let .list(position, expressions, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("List expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(expressions.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           """
  case let .literal(position, literal, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Literal expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(literal.description.with(indent: indent))
           """
  case let .name(position, identifier, type, lifetime, _):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Name expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(identifier.with(indent: indent))
           """
  case let .lambda(position, parameter, body, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Lambda expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: parameter, indent: indent + 2))
           \(pretty(ast: body, indent: indent + 2))
           """
  case let .let(position, bindings, expr, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Let expression") \(position.description) \(pretty(type: type, indent: indent + 2))\(bindings.isEmpty ? "" : "\n" + bindings.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           \(pretty(ast: expr, indent: indent + 2))
           """
  case let .case(position, expression, matches, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Case expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: expression, indent: indent + 2))
           \(matches.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           """
  case let .ref(position, expression, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Reference expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: expression, indent: indent + 2))
           """
  case let .deref(position, expression, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Dereference expression") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: expression, indent: indent + 2))
           """
  case _:
    return ""
  }
}

// MARK: - Match
func pretty(ast: AST.Expression.Match, indent: Int) -> String {
  """
  \("Match".with(indent: indent)) \(ast.position.description)
  \(pretty(ast: ast.pattern, indent: indent + 2))
  \(pretty(ast: ast.expression, indent: indent + 2))
  """
}

// MARK: - Binding
func pretty(ast: AST.Binding, indent: Int) -> String {
  switch ast {
  case let .binding(position, _, cases, type, lifetime, borrowLifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\(pretty(lifetime: borrowLifetime, indent: 0))Binding \(position.description) \(pretty(type: type, indent: indent + 2))
           \(cases.map { pretty(ast: $0, indent: indent) }.joined(separator: "\n| ".with(indent: indent + 2)))
           """
  case let .prototype(position, name, type, tcType):
    return """
           \("Binding prototype".with(indent: indent)) \(position.description) \(pretty(type: tcType, indent: indent + 2))
           \(name.with(indent: indent + 2))
           \(pretty(ast: type, indent: indent + 2))
           """
  case let .data(position, name, params, constructors, type):
    return """
           \("Data binding".with(indent: indent)) \(position.description) \(pretty(type: type, indent: 0))
           \(name.with(indent: indent + 2)) \(params.lifetimeParameters.map { $0.with(indent: 0) }.joined(separator: " ")) \(params.typeParameters.map { $0.with(indent: 0) }.joined(separator: " "))
           \(constructors.map { $0.description.with(indent: indent + 4) }.joined(separator: "\n"))
           """
  case _:
    return ""
  }
}

// MARK: - Lifetime
func pretty(lifetime: AST.Lifetime?, indent: Int) -> String {
  lifetime.map { "[\($0)] ".with(indent: indent) } ?? "".with(indent: indent)
}

// MARK: - Case
func pretty(ast: AST.Binding.Case, indent: Int) -> String {
  """
  \(pretty(lifetime: ast.lifetime, indent: indent))\(ast.name) \(ast.position.description) \(pretty(type: ast.type, indent: indent + 2))\(ast.parameters.isEmpty ? "" : "\n" + ast.parameters.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
  \(pretty(ast: ast.body, indent: indent + 2))
  """
}

// MARK: - Pattern
func pretty(ast: AST.Pattern, indent: Int) -> String {
  switch ast {
  case let .identifier(position, identifier, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Identifier pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(identifier.with(indent: indent))
           """
  case let .literal(position, literal, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Literal pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(literal.description.with(indent: indent))
           """
  case let .wildcard(position, type):
    return "\("Wildcard pattern") \(position.description)\(pretty(type: type, indent: indent + 2))".with(indent: indent)
  case let .tuple(position, patterns, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Tuple pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(patterns.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           """
  case let .list(.empty(position, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Empty list pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           """
  case let .list(.cons(position, head, tail, type, lifetime)):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Cons pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: head, indent: indent + 2))
           \(pretty(ast: tail, indent: indent + 2))
           """
  case let .ref(position, pattern, type, lifetime):
    return """
           \(pretty(lifetime: lifetime, indent: indent))\("Reference pattern") \(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: pattern, indent: indent + 2))
           """
  case let .deconstruct(position, constructor, patterns, type, lifetime):
    return """
           \("Deconstruct pattern".with(indent: indent)) \(lifetime.map { pretty(lifetime: $0, indent: 0) + " " } ?? "")\(position.description) \(pretty(type: type, indent: 0))
           \("Constructor: \(constructor)".with(indent: indent + 2))
           \(patterns.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n".with(indent: indent + 2)))
           """
  case _:
    return ""
  }
}

// MARK: - Type
func pretty(ast: AST.AType, indent: Int) -> String {
  switch ast {
  case let .name(position, name, lifetimeSpecifier, type):
    return """
           \("Type name".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: type, indent: 0))
           \(name.with(indent: indent))
           """
  case let .function(position, arg, ret, lifetimeSpecifier, type):
    return """
           \("Function type".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: type, indent: indent + 2))
           \("Arg type:".with(indent: indent + 2))
           \(pretty(ast: arg, indent: indent + 2 * 2))
           \("Return type:".with(indent: indent + 2))
           \(pretty(ast: ret, indent: indent + 2 * 2))
           """
  case let .tuple(position, types, lifetimeSpecifier, type):
    return """
           \("Tuple type".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: type, indent: indent + 2))
           \(types.map { pretty(ast: $0, indent: indent + 2) }.joined(separator: "\n"))
           """
  case let .list(position, element, lifetimeSpecifier, type):
    return """
           \("List type".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: type, indent: indent + 2))
           \(pretty(ast: element, indent: indent + 2))
           """
  case let .typeConstructor(position, name, types, lifetimeSpecifiers, lifetimeSpecifier, type):
    return """
           \("Type constructor".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: type, indent: indent + 2))
           \(name.with(indent: indent + 2)) [\(lifetimeSpecifiers.isEmpty ? "" : (lifetimeSpecifiers.joined(separator: ",")))]
           \(types.map { pretty(ast: $0, indent: indent + 2 * 2) }.joined(separator: "\n"))
           """
  case let .ref(position, type, lifetimeSpecifier, tcType):
    return """
           \("Reference".with(indent: indent)) \(lifetimeSpecifier.map { "[\($0)] " } ?? "")\(position.description) \(pretty(type: tcType, indent: indent + 2))
           \(pretty(ast: type, indent: indent + 2))
           """
  }
}

func pretty(type: TCType?, indent: Int) -> String {
  type.map { "(" + $0.description + ")" } ?? ""
}

extension String {
  func with(indent: Int) -> String {
    [String](repeating: ".", count: indent).joined() + self
  }
}

public extension String.StringInterpolation {
  mutating func appendInterpolation(_ value: AST) {
    appendLiteral(value.prettyDescription)
  }
}

extension Array: PrettyStringConvertible where Element: PrettyStringConvertible {
  public var prettyDescription: String {
    enumerated()
      .map { "\($0.offset):\n" + $0.element.prettyDescription }.joined(separator: "\n")
  }
}
