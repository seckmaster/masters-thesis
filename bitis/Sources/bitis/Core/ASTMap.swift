//
//  ASTMap.swift
//  
//
//  Created by Toni K. Turk on 08/01/2022.
//

import Foundation

public struct ASTMap<V> {
  enum Node {
    case binding(BCAst.Binding)
    case `case`(BCAst.Binding.Case)
    case expression(BCAst.Expression)
    case pattern(BCAst.Pattern)
    case constructor(BCAst.Binding.Constructor)
  }
  
  var scope: [Node: V]
}

extension ASTMap {
  init() {
    scope = [:]
  }
}

extension ASTMap {
  subscript(key: Node) -> V? {
    get { scope[key] }
    set { scope[key] = newValue }
  }
}

extension ASTMap.Node: Hashable, CustomStringConvertible, PrettyStringConvertible {
  var hashValue: Int {
    switch self {
    case .binding(let binding):
      return binding.hashValue
    case .`case`(let `case`):
      return `case`.hashValue
    case .expression(let expression):
      return expression.hashValue
    case .pattern(let pattern):
      return pattern.hashValue
    case .constructor(let constructor):
      return constructor.hashValue
    }
  }
  
  public var description: String {
    switch self {
    case .binding(let binding):
      return binding.description
    case .`case`(let `case`):
      return `case`.description
    case .expression(let expression):
      return expression.description
    case .pattern(let pattern):
      return pattern.description
    case .constructor(let constructor):
      return constructor.description
    }
  }
  
  public var prettyDescription: String {
    switch self {
    case .binding(let binding):
      return binding.prettyDescription
    case .`case`(let `case`):
      return `case`.prettyDescription
    case .expression(let expression):
      return expression.prettyDescription
    case .pattern(let pattern):
      return pattern.prettyDescription
    case .constructor(let constructor):
      return constructor.description // TODO: - 
    }
  }
  
  public var position: Lexer.Position {
    switch self {
    case .binding(let binding):
      return binding.position
    case .`case`(let `case`):
      return `case`.position
    case .expression(let expression):
      return expression.position
    case .pattern(let pattern):
      return pattern.position
    case .constructor(let constructor):
      return constructor.position
    }
  }
}

extension ASTMap.Node: Equatable {
  static func == (lhs: Self, rhs: Self) -> Bool {
    lhs.hashValue == rhs.hashValue
  }
}
