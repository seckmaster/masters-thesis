//
//  Mergable.swift
//  bitis
//
//  Created by Toni K. Turk on 01/12/2020.
//

import Foundation

public protocol Mergable {
  associatedtype E: Error
  func formUnion(_ other: Self) -> Result<Self, E>
}
