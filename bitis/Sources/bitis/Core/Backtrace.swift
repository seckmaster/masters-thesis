//
//  Backtrace.swift
//  
//
//  Created by Toni K. Turk on 22/12/2021.
//

import Foundation

public struct Backtrace: Equatable {
  public var trace: [HasPosition]
  
  public static func == (lhs: Backtrace, rhs: Backtrace) -> Bool {
    lhs.trace.lazy.map { $0.position } == rhs.trace.lazy.map { $0.position }
  }
}

public extension Backtrace {
  init() {
    self.trace = []
  }
  
  init(backtrace: Backtrace) {
    self.trace = backtrace.trace
  }
}

public protocol BacktraceError: Error, HasPosition {
  var fullFilePath: String { get }
}

public struct BacktraceErrorImpl<E> {
  public let backtrace: Backtrace
  public let error: E
  public let fullFilePath: String
  
  internal init(
    backtrace: Backtrace,
    error: E
  ) {
    self.backtrace = backtrace
    self.error = error
    // @FIXME: - An ad-hoc hack for a quick win!
    // @TODO: - In future, implement in a correct manner!
    self.fullFilePath = CURRENT_FILE_BEING_COMPILED
  }
}

extension BacktraceErrorImpl: Error, BacktraceError where E: Error {}
extension BacktraceErrorImpl: Equatable where E: Equatable {}

extension BacktraceErrorImpl: CustomStringConvertible {
  public var description: String {
    "\(backtrace.trace.last?.position ?? .zero): \(String(describing: error))"
  }
}

extension BacktraceErrorImpl: HasPosition {
  public var position: Lexer.Position {
    backtrace.trace.last?.position ?? .zero
  }
}
