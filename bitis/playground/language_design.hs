x :: Lazy<[Int]>
x :: [Int] -- ekvivalentno
x = 1:2:3:4:5:[] -- na kopici alociran "thunk", x je njegov lastnik

-- vsak "kompleksen" objekt, t.j. objekt, ki si lasti druge objekte na kopici, 
-- ima impliciten destruktor, ki v pravem času počisti vire tega objekta

y :: NonLazy<[Int]>
y = 1:2:3:4:5:[] -- na kopici alocirian povezan-seznam!, y je njegov lastnik

-- Lazy<...> ali NonLazy<...> je potencialen design konstrukt, ki 
-- programerju omogoča, da prevajalniku poda "namig", v primeru, da ne želi Lazy semantike.
-- Lazy<...> je impliciten in privzet

-- [Int] je podan funkciji s semantiko premika, t.j., sum_if_true postane lastnik objekta.
-- Po prenehanju izvajanja se dinamično alociran pomnilnik za [Int] samodejno sprosti
sum_if_true :: Bool -> [Int] -> Int -- <-- "enostavne" tipe (Int, Bool, ..) tukaj in v nadaljevanju smatram kot "copy-able". kot ste izpostavili 
sum_if_true b x = case b of         --     je to lahko napačna predpostavka, ampak za namene te predstavitve mislim, da je ok
    True -> (case x of -- lastništvo [Int] se premake v notranji mehanizem case expressiona
        [] -> 0
        (n:ns) -> n + sum_if_true b ns) -- b can still be used here because it is a Copy-able type
        -- (_ -> sum_if_true b x) <-- error: `x` already moved
    False -> 0

-- [Int] je podan funkciji s semantiko reference, t.j., sum_if_true ne postane lastnik objekta, ampak si za čas izvajanja referenco izposodi.
-- Po prenehanju izvajanja se dinamično alociran pomnilnik za [Int] pusti pri miru.
sum_if_true2 :: Bool -> &[Int] -> Int
sum_if_true2 False _ = 0
sum_if_true2 True [] = 0
sum_if_true2 True (x:xs) = x + sumIfTrue True xs

s1 :: NonLazy<Int>
s1 = sum_if_true True x -- 15
-- fmap id x  <-- error: `x` already moved

s2 :: NonLazy<Int>
s2 = sum_if_true2 True &x -- 15
fmap id x -- ok

--------

-- Kompleksni objekti:

-- instance katerih tipov potrebujejo destruktor?

data Bool = True | False -- ne potrebuje

data Maybe a = None | Just a
--                        ⤴
--                        potrebuje, če "a" potrebuje

type Tup a = (Int, a)
--                ⤴
--                potrebuje, če "a" potrebuje

data List a = [] | Cons (a, List a)
--                           ⤴ 
--                           potrebuje, ker je rekurzivna struktura
data List a = [] | Cons (a, Box (List a)) -- ali je potrebno uporabiti `Box`, tako kot v Rustu?

data Lists = Lb (List Bool) | Li (List Int)


-- V splošnem ADT potrebuje destruktor, kadar:
--   1. ga potrebuje katerikoli izmed parametrov ADT-ja (primer Maybe, Tup),
--   2. ga potrebuje katerikoli pod-tip (primer Lists), ali 
--   3. je rekurziven (se nanaša sam nase, primer List)
--   
-- Poleg tega destruktor potrebujejo tudi:
--   1. funkcijske ovojnice, ki si lastijo objekt, ki potrebuje destruktor,
--   2. suspenzi,                              - || -
--   3. pametni kazalci (npr. Box, Rc, Cell, ...), če se izkaže, da so potrebni
--   

--------

-- Funkcijske ovojnice --

fold :: (&a -> &b -> &b) -> b -> &[a] -> b
fold _ acc [] = acc
fold f acc (x:xs) = foldl f (f x acc) xs

drop :: a -> ()
drop _ = ()

g1 :: [Int] -> () -> Int
g1 xs = --    funkcijska ovojnica mora postati lastnik 'xs', kljub temu, da si referenco samo 'izposodi'
    let --                       ⤵
        sum = \() -> fold (+) 0 &xs -- <--znotraj 'sum' je 'xs' lahko večkrat deljen
        --    ~~~~~~~~~~~~~~~~~~~~~ popravek: to je napaka saj referenciranje lokalnih vrednosti ni dovoljeno
        -- xss = xs <-- error: `xs` already moved
    in
        sum
    end

s = g1 [1,2,3,4] -- s je posreden lastnik seznama
s () -- ok
-- dinamično alociran pomnilnik za seznam se sprosti, ko `s` zapusti scope


drop :: a -> ()
drop _ = ()

g :: [Int] -> () -> ()
g xs = let
  -- Should we, for the time being, prevent closures 
  -- from taking ownership of values?
  --
  -- The alternative is to make `FnOnce` implicit and
  -- consider fn. application to move ownership of the function.
  -- However, the ownership should only be moved if the function
  -- has non-weak dependencies (consult the dependency graph).
  f = \\() -> drop xs -- `f` is FnOnce
  a1 = f ()
  a2 = f () -- dangling pointer (dereferencing a null pointer)
in f

g3 :: [Int] -> Once<() -> Int> -- potencialen design
g3 xs =
    let
        sum = \() -> drop xs -- ok, ovojnica se lahko evalvira največ enkrat
    in
        sum
    end

s = g3 [1,2,3,4]
s () -- ok
-- s () <-- not ok, `s` already moved

-- Reference in življenske dobe --

f1 :: &[Int] -> () -> Int -- Rust javi napako za izomorfno definicijo
f2 :: &'STATIC [Int] -> () -> Int -- Rust zahteva statično življensko dobo
f3 ['A, 'B | 'A > 'B] :: &'A [Int] -> 'B () -> Int -- potencialen dizajn, eksplicitno zahtevamo, da seznam živi dlje od ovojnice.

fN xs = --    funkcijska ovojnica si 'izposodi' 'xs';     Zagotoviti je potrebno, da funkcijska ovojnica zagotovo 
    let --                     ⤵                         ne živi dlje od 'xs'. Ali to lahko prevajalnik zagotovi,  
        sum = () -> fold (+) 0 &xs --                     brez eksplicitnih življenskih dob (f3)? 
    in
        sum
    end

s = f2 &x -- ok, 'x' ima statično življensko dobo
s = f3 &x -- ok, 'x' živi dlje od 's'
s () -- ok
-- s = f3 &[1,2,3,4] <-- error: pomnilnik začasne vrednosti je sproščen med tem, ko je le-ta izposojena
--                       error: 's' živi dlje od začasne vrednosti

not_ok :: &[Int] -> () -> Int
-- not_ok xs = f3 xs <-- error: 'xs' morebiti živi dalj od funkcijske ovojnice, ki jo vrne `fN`

ok :: &[Int] -> () -> Int
ok xs = -- to je v redu, saj `xs` zagotovo živi dalj od `s` 
    let
        s = f3 xs
    in
        s ()
    end

-- Podatkovni tipi, ki vsebujejo reference --

-- type String = [Char]

f1 :: String -> [Int] -> (String, [Int])
f1 s xs = (s, xs)

f2 :: &String -> &[Int] -> (String, [Int])
-- f2 s xs = (*s, *xs) <-- error: cannot move out of '&s'
--           ⤴
--       dereference

f3 :: &String -> &[Int] -> (&String, &[Int])
f3 s xs = (s, xs)

data MaybeRef ['A, a] = NoneR | JustR (&'A a)
data StaticMaybeRef [a] = NoneSR | JustSR (&'static a)
-- ali
data StaticMaybeRef = NoneSR | JustSR (&'static a)

data Demo ['A, 'B | 'A < 'B, a, b] = Demo (&'A a) (&'B b)
data EitherRef ['A, 'B, a, b] = L (&'A a) | R (&'B b)

new ['A] :: &a -> MaybeRef<'A> a
new value = JustR value

-- TODO: ...

....


-- težava so cikli

-- sledijo primeri programov, ki so v Haskell-u veljavni

-- 1.
x = [1..3] ++ (drop 1 x)

-- 2. (a)
gen a = let x = a:y
            y = (a+1):x
        in x

-- 2. (b)
a = 1:b
b = 2:a

------------

(++) :: [a] -> [a] -> [a]
(:) :: a -> [a] -> [a]
drop_first :: Int -> &[a] -> &[a]

--

-- 1.
x :: [Int]
-- x = [1..3] ++ (drop_first 1 x) <-- error: expected a reference in (drop_first 1 x)
-- x = [1..3] ++ (drop_first 1 &x) <-- error: cannot move out of reference

-- 2.

a :: [Int]
b :: [Int]
a = 1:b
b = 2:a
-- ⤴
-- this is ok
-- ampak ...
-- a_ = a <-- error: 'a' already moved
-- b_ = b <-- error: 'b' already moved

--      need to define syntax for type constructors with lifetime specifiers 
--                                                    ⤵
data ListRef ['A, a] = EmptyR | ConsR (a, &'A ListRef<'A, a>)

gen ['A] :: Int -> ListRef<'A, a>
gen a = let -- x :: ListRef<'A, Int>
            -- y :: ListRef<'A, Int>
            x = ConsR a &y
            y = ConsR (a + 1) &x
        in x -- <-- error: returns a value referencing data owned by the current function

{ -- inside some scope
    a :: ListRef<Int>
    b :: ListRef<Int>
    a = ConsR 1 &b
    b = ConsR 2 &a
    -- ??? should this compile at all or not
}

{ -- inside some scope
    a :: ListRef<Int>
    b :: ListRef<Int>
    a = ConsR 1 &b
    {
        b = ConsR 2 &a -- <-- error: lifetimes do not match
    }
}

-- static scope
a :: ListRef<'static, Int>
b :: ListRef<'static, Int>
a = ConsR 1 &b
b = ConsR 2 &a

--

-- 1.

drop_first_ref ['A] :: Int -> &'A ListRef<'A, a> -> &'A ListRef<'A, a>
append_ref ['A] :: &'A ListRef<'A, a> -> &'A ListRef<'A, a> -> &'A ListRef<'A, a>

a :: ListRef<'static, Int>
a = [1..3]

b :: ListRef<'static, Int>
b = append_ref &a (drop_first_ref 1 &b)

-----









-- currying tudi predstavlja težave

f :: [Int] -> Bool -> Int
f xs = \_ -> case xs of [] => -1 | (x:xs) => x

g = f [1,2,3]
g True
-- g True <-- error: dinamično alociran pomnilnik za seznam je že sproščen

f_correct :: [Int] -> FnOnce<Bool -> Int>
f_correct xs = \_ -> case xs of [] => -1 | (x:xs) => x

f_correct2 :: [Int] -> Bool -> Int
f_correct2 xs = \_ -> case &xs of [] => -1 | (x:xs) => *x
--                                                     ⤴
--   we can dereference 'x' here because `Int` is Copy-able, 
--   though this does not hold in general

f_correct3 <'A> :: [Int] -> Bool -> &'A Int
f_correct3 xs = \_ -> case &xs of [] => &-1 | (x:xs) => x
--                                      ⤴
--                        would this compile actually ?? (comment: it does on Rust :))
