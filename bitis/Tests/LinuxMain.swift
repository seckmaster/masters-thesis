import XCTest

import bitisTests

var tests = [XCTestCaseEntry]()
tests += bitisTests.allTests()
XCTMain(tests)
