import XCTest

#if !canImport(ObjectiveC)
public func allTests() -> [XCTestCaseEntry] {
  [ testCase(bitisTests.allTests)]
}
#endif
