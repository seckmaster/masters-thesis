//
//  ParserTests.swift
//  bitisTests
//
//  Created by Toni K. Turk on 29/10/2020.
//

@testable import Bitis
import XCTest
import SwiftParsec

class ParserTests: XCTestCase {
  let parser: Parser = .init()
}

// MARK: - Expression
extension ParserTests {
  func testLiteralExpression() {
    test("true", matchWith: AST.Expression.literal(position: .init(), literal: .logical(.true)))
    test("false", matchWith: AST.Expression.literal(position: .init(), literal: .logical(.false)))
    test("10.151", matchWith: AST.Expression.literal(position: .init(), literal: .float(10.151)))
    test("314", matchWith: AST.Expression.literal(position: .init(), literal: .int(314)))
    test("'T'", matchWith: AST.Expression.literal(position: .init(), literal: .char("T")))
    test("\"Today is a nice day!\"", matchWith: AST.Expression.literal(position: .init(), literal: .string("Today is a nice day!")))
  }
  
  func testNameExpression() {
    test("falsed", matchWith: .name(position: .init(), identifier: "falsed"))
    test("letAndMore", matchWith: .name(position: .init(), identifier: "letAndMore"))
    test("_let123", matchWith: .name(position: .init(), identifier: "_let123"))
    test("ifIdent", matchWith: .name(position: .init(), identifier: "ifIdent"))
    test("xyz", matchWith: .name(position: .init(), identifier: "xyz"))
    test("&xyz", matchWith: .ref(
      position: .init(),
      expression: .name(position: .init(), identifier: "xyz")))
  }
  
  func testIfThenElseExpression() {
    test("if true then 1 else 2", matchWith: .binary(.ifThenElse(
      position: .init(),
      condition: .literal(position: .init(), literal: .logical(.true)),
      true: .literal(position: .init(), literal: .int(1)),
      false: .literal(position: .init(), literal: .int(2)))))
    test("if &true then &1 else &2", matchWith: .binary(.ifThenElse(
      position: .init(),
      condition: .ref(
        position: .init(),
        expression: .literal(position: .init(), literal: .logical(.true))),
      true: .ref(
        position: .init(),
        expression: .literal(position: .init(), literal: .int(1))),
      false: .ref(
        position: .init(),
        expression: .literal(position: .init(), literal: .int(2))))))
    test("if true /= false then 1 + 2 else if true then 1 else 3", matchWith: .binary(.ifThenElse(
      position: .init(),
      condition: .binary(.logical(
        position: .init(),
        operator: .notEqual,
        left: .literal(position: .init(), literal: .logical(.true)),
        right: .literal(position: .init(), literal: .logical(.false)))),
      true: .binary(.arithmetic(
        position: .init(),
        operator: .plus,
        left: .literal(position: .init(), literal: .int(1)),
        right: .literal(position: .init(), literal: .int(2)))),
      false: .binary(.ifThenElse(
        position: .init(),
        condition: .literal(position: .init(), literal: .logical(.true)),
        true: .literal(position: .init(), literal: .int(1)),
        false: .literal(position: .init(), literal: .int(3)))))))
    test("if true /= false then 1 * 2 else if true then 1 else 3", matchWith: .binary(.ifThenElse(
      position: .init(),
      condition: .binary(.logical(
        position: .init(),
        operator: .notEqual,
        left: .literal(position: .init(), literal: .logical(.true)),
        right: .literal(position: .init(), literal: .logical(.false)))),
      true: .binary(.arithmetic(
        position: .init(),
        operator: .mul,
        left: .literal(position: .init(), literal: .int(1)),
        right: .literal(position: .init(), literal: .int(2)))),
      false: .binary(.ifThenElse(
        position: .init(),
        condition: .literal(position: .init(), literal: .logical(.true)),
        true: .literal(position: .init(), literal: .int(1)),
        false: .literal(position: .init(), literal: .int(3)))))))
    test("""
      if true
      then 1
      else 2
      """,
         matchWith: .binary(.ifThenElse(
          position: .init(),
          condition: .literal(position: .init(), literal: .logical(.true)),
          true: .literal(position: .init(), literal: .int(1)),
          false: .literal(position: .init(), literal: .int(2)))))
    test("""
      if true
      then
      1
      else
      2
      """,
         matchWith: .binary(.ifThenElse(
          position: .init(),
          condition: .literal(position: .init(), literal: .logical(.true)),
          true: .literal(position: .init(), literal: .int(1)),
          false: .literal(position: .init(), literal: .int(2)))))
  }
  
  func testLambdaExpression() {
    test("\\x -> x", matchWith: .lambda(
      position: .init(),
      parameter: .identifier(position: .init(), identifier: "x"),
      body: .name(position: .init(), identifier: "x")))
    test("\\_ -> x", matchWith: .lambda(
      position: .init(),
      parameter: .wildcard(position: .init()),
      body: .name(position: .init(), identifier: "x")))
    test("λx -> x", matchWith: .lambda(
      position: .init(),
      parameter: .identifier(position: .init(), identifier: "x"),
      body: .name(position: .init(), identifier: "x")))
    test("λ_ -> x", matchWith: .lambda(
      position: .init(),
      parameter: .wildcard(position: .init()),
      body: .name(position: .init(), identifier: "x")))
    test("\\(x:xs) -> x", matchWith: .lambda(
      position: .init(),
      parameter: .list(.cons(
        position: .init(),
        head: .identifier(position: .init(), identifier: "x"),
        tail: .identifier(position: .init(), identifier: "xs"))),
      body: .name(position: .init(), identifier: "x")))
    test("\\x -> \\y -> x + y", matchWith: .lambda(
      position: .init(),
      parameter: .identifier(position: .init(), identifier: "x"),
      body: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "y"),
        body: .binary(.arithmetic(
          position: .init(),
          operator: .plus,
          left: .name(position: .init(), identifier: "x"),
          right: .name(position: .init(), identifier: "y"))))))
    test("""
      \\x ->
        x
      """, matchWith: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x"),
        body: .name(position: .init(), identifier: "x")))
    test("""
      \\x
      -> x
      """, matchWith: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x"),
        body: .name(position: .init(), identifier: "x")))
    test("""
      \\x
      ->
        x
      """, matchWith: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x"),
        body: .name(position: .init(), identifier: "x")))
  }
  
  func testLogicalExpression() {
    test("x < y", matchWith: .binary(.logical(
      position: .init(),
      operator: .lowerThan,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x > y", matchWith: .binary(.logical(
      position: .init(),
      operator: .greaterThan,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x <= y", matchWith: .binary(.logical(
      position: .init(),
      operator: .lowerOrEqualThan,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x >= y", matchWith: .binary(.logical(
      position: .init(),
      operator: .greaterOrEqualThan,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x == y", matchWith: .binary(.logical(
      position: .init(),
      operator: .equal,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x /= y", matchWith: .binary(.logical(
      position: .init(),
      operator: .notEqual,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x && y", matchWith: .binary(.logical(
      position: .init(),
      operator: .and,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x || y", matchWith: .binary(.logical(
      position: .init(),
      operator: .or,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("&(x || y)", matchWith: .ref(
      position: .init(),
      expression: .binary(.logical(
        position: .init(),
        operator: .or,
        left: .name(position: .init(), identifier: "x"),
        right: .name(position: .init(), identifier: "y")))))
  }
  
  func testArithmeticExpression() {
    test("x + y", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .plus,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x - y", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .minus,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x * y", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .mul,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("x / y", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .div,
      left: .name(position: .init(), identifier: "x"),
      right: .name(position: .init(), identifier: "y"))))
    test("&(x + y)", matchWith: .ref(
      position: .init(),
      expression: .binary(.arithmetic(
        position: .init(),
        operator: .plus,
        left: .name(position: .init(), identifier: "x"),
        right: .name(position: .init(), identifier: "y")))))
    test("&(&x + y)", matchWith: .ref(
      position: .init(),
      expression: .binary(.arithmetic(
        position: .init(),
        operator: .plus,
        left: .ref(position: .init(), expression: .name(position: .init(), identifier: "x")),
        right: .name(position: .init(), identifier: "y")))))
    test("*(x + y)", matchWith: .deref(
      position: .init(),
      expression: .binary(.arithmetic(
        position: .init(),
        operator: .plus,
        left: .name(position: .init(), identifier: "x"),
        right: .name(position: .init(), identifier: "y")))))
  }
  
  func testArithmeticExpressionAssociativity() {
    test("x + y * 3", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .plus,
      left: .name(position: .init(), identifier: "x"),
      right: .binary(.arithmetic(
        position: .init(),
        operator: .mul,
        left: .name(position: .init(), identifier: "y"),
        right: .literal(position: .init(), literal: .int(3)))))))
    test("x - y / 3", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .minus,
      left: .name(position: .init(), identifier: "x"),
      right: .binary(.arithmetic(
        position: .init(),
        operator: .div,
        left: .name(position: .init(), identifier: "y"),
        right: .literal(position: .init(), literal: .int(3)))))))
    test("(x + y) * 3", matchWith: .binary(.arithmetic(
      position: .init(),
      operator: .mul,
      left: .binary(.arithmetic(
        position: .init(),
        operator: .plus,
        left: .name(position: .init(), identifier: "x"),
        right: .name(position: .init(), identifier: "y"))),
      right: .literal(position: .init(), literal: .int(3)))))
  }
  
  func testEmptyTupleExpression() {
    test("()", matchWith: .tuple(
      position: .init(),
      expressions: []))
  }
  
  func testTupleExpression() {
    test("(1)", matchWith: AST.Expression.literal(position: .init(), literal: .int(1)))
    test("(1, x, 2)", matchWith: .tuple(
      position: .init(),
      expressions: [
        .literal(position: .init(), literal: .int(1)),
        .name(position: .init(), identifier: "x"),
        .literal(position: .init(), literal: .int(2))
      ]))
    test("&(1, x, 2)", matchWith: .ref(
      position: .init(),
      expression: .tuple(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .int(1)),
          .name(position: .init(), identifier: "x"),
          .literal(position: .init(), literal: .int(2))
        ])))
    test("*(1, x, 2)", matchWith: .deref(
      position: .init(),
      expression: .tuple(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .int(1)),
          .name(position: .init(), identifier: "x"),
          .literal(position: .init(), literal: .int(2))
        ])))
    test("(\\x -> x)", matchWith: .lambda(
      position: .init(),
      parameter: .identifier(position: .init(), identifier: "x"),
      body: .name(position: .init(), identifier: "x")))
    test("(1, x, (true, false, a))", matchWith: .tuple(
      position: .init(),
      expressions: [
        .literal(position: .init(), literal: .int(1)),
        .name(position: .init(), identifier: "x"),
        .tuple(
          position: .init(),
          expressions: [
            .literal(position: .init(), literal: .logical(.true)),
            .literal(position: .init(), literal: .logical(.false)),
            .name(position: .init(), identifier: "a"),
          ])
      ]))
  }
  
  func testEmptyListExpression() {
    test("[]", matchWith: .list(
      position: .init(),
      expressions: []))
  }
  
  func testListExpression() {
    test("[1]", matchWith: .list(
      position: .init(),
      expressions: [
        .literal(position: .init(), literal: .int(1))
      ]))
    test("&[1]", matchWith: .ref(
      position: .init(),
      expression: .list(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .int(1))
        ])))
    test("*[1]", matchWith: .deref(
      position: .init(),
      expression: .list(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .int(1))
        ])))
    test("[1, x, 2]", matchWith: .list(
      position: .init(),
      expressions: [
        .literal(position: .init(), literal: .int(1)),
        .name(position: .init(), identifier: "x"),
        .literal(position: .init(), literal: .int(2))
      ]))
    test("[1, x, [true, false, a]]", matchWith: .list(
      position: .init(),
      expressions: [
        .literal(position: .init(), literal: .int(1)),
        .name(position: .init(), identifier: "x"),
        .list(
          position: .init(),
          expressions: [
            .literal(position: .init(), literal: .logical(.true)),
            .literal(position: .init(), literal: .logical(.false)),
            .name(position: .init(), identifier: "a"),
          ])
      ]))
  }
  
  func testFunctionApplication() {
    test("x y", matchWith: .binary(.application(
      position: .init(),
      fn: .name(position: .init(), identifier: "x"),
      arg: .name(position: .init(), identifier: "y"))))
    test("x y z", matchWith: .binary(.application(
      position: .init(),
      fn: .binary(.application(
        position: .init(),
        fn: .name(position: .init(), identifier: "x"),
        arg: .name(position: .init(), identifier: "y"))),
      arg: .name(position: .init(), identifier: "z"))))
    test("f x + y 10", matchWith: .binary(
      .arithmetic(
        position: .init(),
        operator: .plus,
        left: .binary(.application(
          position: .init(),
          fn: .name(position: .init(), identifier: "f"),
          arg: .name(position: .init(), identifier: "x"))),
        right: .binary(.application(
          position: .init(),
          fn: .name(position: .init(), identifier: "y"),
          arg: .literal(position: .init(), literal: .int(10)))))))
    test("f (&1)", matchWith: .binary(
      .application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f"),
        arg: .ref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(1))))))
    test("f (&[1, 2, 3])", matchWith: .binary(
      .application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f"),
        arg: .ref(
          position: .init(),
          expression: .list(
            position: .init(),
            expressions: [
              .literal(position: .init(), literal: .int(1)),
              .literal(position: .init(), literal: .int(2)),
              .literal(position: .init(), literal: .int(3))
            ])))))
    test("f (*1)", matchWith: .binary(
      .application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f"),
        arg: .deref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(1))))))
    test("(\\x -> x) 10", matchWith: .binary(.application(
      position: .init(),
      fn: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x"),
        body: .name(position: .init(), identifier: "x")),
      arg: .literal(position: .init(), literal: .int(10)))))
  }
  
  func testLetExpression() {
    test("let in 10", matchWith: .let(
      position: .init(),
      bindings: [],
         expression: .literal(position: .init(), literal: .int(10))))
    test("let in &10", matchWith: .let(
      position: .init(),
      bindings: [],
         expression: .ref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(10)))))
    test("let in *10", matchWith: .let(
      position: .init(),
      bindings: [],
         expression: .deref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(10)))))
    test("let x = 10 in 10", matchWith: .let(
      position: .init(),
      bindings: [
        .binding(position: .init(), name: "x", cases: [
          .init(name: "x", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(10)))
        ]),
      ],
         expression: .literal(position: .init(), literal: .int(10))))
    test("let x = &10 in 10", matchWith: .let(
      position: .init(),
      bindings: [
        .binding(position: .init(), name: "x", cases: [
          .init(name: "x", position: .init(), parameters: [], body: .ref(
            position: .init(),
            expression: .literal(position: .init(), literal: .int(10))))
        ]),
      ],
         expression: .literal(position: .init(), literal: .int(10))))
    test("let x = 10, y = \"abc\", z = 10 + 20 in x", matchWith: .let(
      position: .init(),
      bindings: [
        .binding(position: .init(), name: "x", cases: [
          .init(name: "x", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(10)))
        ]),
        .binding(position: .init(), name: "y", cases: [
          .init(name: "y", position: .init(), parameters: [], body: .literal(position: .init(), literal: .string("abc")))
        ]),
        .binding(position: .init(), name: "z", cases: [
          .init(name: "z", position: .init(), parameters: [], body: .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .literal(position: .init(), literal: .int(10)),
            right: .literal(position: .init(), literal: .int(20)))))
        ]),
      ],
         expression: .name(position: .init(), identifier: "x")))
    test("let in if 1 then 2 else 3", matchWith: .let(
      position: .init(),
      bindings: [],
         expression: .binary(.ifThenElse(
          position: .init(),
          condition: .literal(position: .init(), literal: .int(1)),
          true: .literal(position: .init(), literal: .int(2)),
          false: .literal(position: .init(), literal: .int(3))))))
    test("let in let in let in \"abc\"", matchWith: .let(
      position: .init(),
      bindings: [],
         expression: .let(
          position: .init(),
          bindings: [],
         expression: .let(
          position: .init(),
          bindings: [],
         expression: .literal(position: .init(), literal: .string("abc"))))))
    test("""
      let in
      10
      """,
         matchWith: .let(
          position: .init(),
          bindings: [],
         expression: .literal(position: .init(), literal: .int(10))))
    test("""
      let
      in 10
      """,
         matchWith: .let(
          position: .init(),
          bindings: [],
         expression: .literal(position: .init(), literal: .int(10))))
    test("""
      let

      in
      10
      """,
         matchWith: .let(
          position: .init(),
          bindings: [],
         expression: .literal(position: .init(), literal: .int(10))))
    test("""
      let
        a = 10
        b = 20
        c = 30
      in a
      """,
         matchWith: .let(
          position: .init(),
          bindings: [
            .binding(
              position: .init(),
              name: "a",
              cases: [.init(name: "a", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(10)))]),
            .binding(
              position: .init(),
              name: "b",
              cases: [.init(name: "b", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(20)))]),
            .binding(
              position: .init(),
              name: "c",
              cases: [.init(name: "c", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(30)))])
          ],
         expression: .name(position: .init(), identifier: "a")))
  }
  
  func testCaseExpression() {
    test("case true of true -> 1 | false -> 2", matchWith: .case(
      position: .init(),
      expression: .literal(
        position: .init(),
        literal: .logical(.true)),
      matches: [
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.true)),
          expression: .literal(position: .init(), literal: .int(1))),
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.false)),
          expression: .literal(position: .init(), literal: .int(2))),
      ]))
    test("case &x of true -> 1 | false -> 2", matchWith: .case(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .name(
          position: .init(),
          identifier: "x")),
      matches: [
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.true)),
          expression: .literal(position: .init(), literal: .int(1))),
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.false)),
          expression: .literal(position: .init(), literal: .int(2))),
      ]))
    test("case *x of true -> 1 | false -> 2", matchWith: .case(
      position: .init(),
      expression: .deref(
        position: .init(),
        expression: .name(
          position: .init(),
          identifier: "x")),
      matches: [
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.true)),
          expression: .literal(position: .init(), literal: .int(1))),
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.false)),
          expression: .literal(position: .init(), literal: .int(2))),
      ]))
    test("case true of true -> 1 | false -> 2 | (_:xs) -> x", matchWith: .case(
      position: .init(),
      expression: .literal(
        position: .init(),
        literal: .logical(.true)),
      matches: [
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.true)),
          expression: .literal(position: .init(), literal: .int(1))),
        .init(
          position: .init(),
          pattern: .literal(
            position: .init(),
            literal: .logical(.false)),
          expression: .literal(position: .init(), literal: .int(2))),
        .init(
          position: .init(),
          pattern: .list(.cons(
            position: .init(),
            head: .wildcard(position: .init()),
            tail: .identifier(position: .init(), identifier: "xs"))),
          expression: .name(position: .init(), identifier: "x")),
      ]))
    test("""
      case true of
        true -> 1
        | false -> 2
      """,
         matchWith: .case(
          position: .init(),
          expression: .literal(
            position: .init(),
            literal: .logical(.true)),
          matches: [
            .init(
              position: .init(),
              pattern: .literal(
                position: .init(),
                literal: .logical(.true)),
              expression: .literal(position: .init(), literal: .int(1))),
            .init(
              position: .init(),
              pattern: .literal(
                position: .init(),
                literal: .logical(.false)),
              expression: .literal(position: .init(), literal: .int(2))),
          ]))
    test("""
      case
      true
      of
        true -> 1
        | false -> 2
      """,
         matchWith: .case(
          position: .init(),
          expression: .literal(
            position: .init(),
            literal: .logical(.true)),
          matches: [
            .init(
              position: .init(),
              pattern: .literal(
                position: .init(),
                literal: .logical(.true)),
              expression: .literal(position: .init(), literal: .int(1))),
            .init(
              position: .init(),
              pattern: .literal(
                position: .init(),
                literal: .logical(.false)),
              expression: .literal(position: .init(), literal: .int(2))),
          ]))
  }
  
  func testReferenceExpression() {
    test("&1", matchWith: .ref(
      position: .init(),
      expression: .literal(position: .init(), literal: .int(1))))
    test("&&&1", matchWith: .ref(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .ref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(1))))))
    test("&(if true then 1 else 2)", matchWith: .ref(
      position: .init(),
      expression: .binary(.ifThenElse(
        position: .init(),
        condition: .literal(position: .init(), literal: .logical(.true)),
        true: .literal(position: .init(), literal: .int(1)),
        false: .literal(position: .init(), literal: .int(2))))))
    test("&(1, \"abc\")", matchWith: .ref(
      position: .init(),
      expression: .tuple(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .int(1)),
          .literal(position: .init(), literal: .string("abc")),
        ])))
    test("&[true, false]", matchWith: .ref(
      position: .init(),
      expression: .list(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .logical(.true)),
          .literal(position: .init(), literal: .logical(.false)),
        ])))
    test("&(f 10)", matchWith: .ref(
      position: .init(),
      expression: .binary(.application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f"),
        arg: .literal(position: .init(), literal: .int(10))))))
  }
  
  func testDereferenceExpression() {
    test("*1", matchWith: .deref(
      position: .init(),
      expression: .literal(position: .init(), literal: .int(1))))
    test("***1", matchWith: .deref(
      position: .init(),
      expression: .deref(
        position: .init(),
        expression: .deref(
          position: .init(),
          expression: .literal(position: .init(), literal: .int(1))))))
    test("*(if true then 1 else 2)", matchWith: .deref(
      position: .init(),
      expression: .binary(.ifThenElse(
        position: .init(),
        condition: .literal(position: .init(), literal: .logical(.true)),
        true: .literal(position: .init(), literal: .int(1)),
        false: .literal(position: .init(), literal: .int(2))))))
    test("*[true, false]", matchWith: .deref(
      position: .init(),
      expression: .list(
        position: .init(),
        expressions: [
          .literal(position: .init(), literal: .logical(.true)),
          .literal(position: .init(), literal: .logical(.false)),
        ])))
    test("*(f 10)", matchWith: .deref(
      position: .init(),
      expression: .binary(.application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f"),
        arg: .literal(position: .init(), literal: .int(10))))))
    test("*&1", matchWith: .deref(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .literal(
          position: .init(), literal: .int(1)))))
    test("*(&1)", matchWith: .deref(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .literal(
          position: .init(), literal: .int(1)))))
    test("(*&1)", matchWith: .deref(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .literal(
          position: .init(), literal: .int(1)))))
    test("(*(&1))", matchWith: .deref(
      position: .init(),
      expression: .ref(
        position: .init(),
        expression: .literal(
          position: .init(), literal: .int(1)))))
  }
}

// MARK: - Bindings
extension ParserTests {
  func testVariableBinding() {
    test("x = 10", matchWith: .binding(
      position: .init(),
      name: "x",
      cases: [
        .init(name: "x", position: .init(), parameters: [], body: .literal(position: .init(), literal: .int(10)))
      ]))
    test("x = true && false", matchWith: .binding(
      position: .init(),
      name: "x",
      cases: [
        .init(
          name: "x",
          position: .init(),
          parameters: [],
          body: .binary(.logical(
            position: .init(),
            operator: .and,
            left: .literal(position: .init(), literal: .logical(.true)),
            right: .literal(position: .init(), literal: .logical(.false)))))
      ]))
  }
  
  func testPrototype() {
    test("x :: Int", matchWith: .prototype(
      position: .init(),
      name: "x",
      type: .name(position: .init(), name: "Int")))
    test("x :: Int -> Int", matchWith: .prototype(
      position: .init(),
      name: "x",
      type: .function(
        position: .init(),
        arg: .name(position: .init(), name: "Int"),
        ret: .name(position: .init(), name: "Int"))))
    test("x :: Int -> Int -> Int", matchWith: .prototype(
      position: .init(),
      name: "x",
      type: .function(
        position: .init(),
        arg: .name(position: .init(), name: "Int"),
        ret: .function(
          position: .init(),
          arg: .name(position: .init(), name: "Int"),
          ret: .name(position: .init(), name: "Int")))))
    test("x :: (Int -> Int) -> Int", matchWith: .prototype(
      position: .init(),
      name: "x",
      type: .function(
        position: .init(),
        arg: .function(
          position: .init(),
          arg: .name(position: .init(), name: "Int"),
          ret: .name(position: .init(), name: "Int")),
        ret: .name(position: .init(), name: "Int"))))
    test("x :: (Int -> Int) -> (Int -> Int)", matchWith: .prototype(
      position: .init(),
      name: "x",
      type: .function(
        position: .init(),
        arg: .function(
          position: .init(),
          arg: .name(position: .init(), name: "Int"),
          ret: .name(position: .init(), name: "Int")),
        ret: .function(
          position: .init(),
          arg: .name(position: .init(), name: "Int"),
          ret: .name(position: .init(), name: "Int")))))
  }
  
  func testFunctionBinding() {
    test("f x y = x + y", matchWith: .binding(
      position: .init(),
      name: "f",
      cases: [
        .init(
          name: "f",
          position: .init(),
          parameters: [
            .identifier(position: .init(), identifier: "x"),
            .identifier(position: .init(), identifier: "y"),
          ],
          body: .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .name(position: .init(), identifier: "x"),
            right: .name(position: .init(), identifier: "y"))))
      ]))
    test("f x = 10", matchWith: .binding(
      position: .init(),
      name: "f",
      cases: [
        .init(
          name: "f",
          position: .init(),
          parameters: [.identifier(position: .init(), identifier: "x")],
          body: .literal(position: .init(), literal: .int(10)))
      ]))
    test("""
         f 10 y = 10
         f x 20 = 20
         f x y = 30
         """, matchWith: .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .literal(position: .init(), literal: .int(10)),
                .identifier(position: .init(), identifier: "y"),
              ],
              body: .literal(position: .init(), literal: .int(10))),
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .identifier(position: .init(), identifier: "x"),
                .literal(position: .init(), literal: .int(20)),
              ],
              body: .literal(position: .init(), literal: .int(20))),
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .identifier(position: .init(), identifier: "x"),
                .identifier(position: .init(), identifier: "y"),
              ],
              body: .literal(position: .init(), literal: .int(30))),
          ]))
  }
  
  func testDataBinding() {
    test("data Bool = True | False", matchWith: .data(
      position: .init(),
      name: "Bool",
      params: .init(),
      constructors: [
        .init(position: .init(), name: "True", types: []),
        .init(position: .init(), name: "False", types: []),
      ]))
    test("data Bool [] = True | False", matchWith: .data(
      position: .init(),
      name: "Bool",
      params: .init(),
      constructors: [
        .init(position: .init(), name: "True", types: []),
        .init(position: .init(), name: "False", types: []),
      ]))
    test(
      "data Pair = Cons Int String",
      matchWith: .data(
        position: .init(),
        name: "Pair",
        params: .init(),
        constructors: [
          .init(position: .init(), name: "Cons", types: [.name(position: .init(), name: "Int"), .name(position: .init(), name: "String")])
        ]))
    test("data Maybe [a] = Just a | Nothing", matchWith: .data(
      position: .init(),
      name: "Maybe",
      params: .init(lifetimeParameters: [], typeParameters: ["a"]),
      constructors: [
        .init(position: .init(), name: "Just", types: [.name(position: .init(), name: "a")]),
        .init(position: .init(), name: "Nothing", types: []),
      ]))
    test("data Maybe [a, b] = Just a | Nothing b", matchWith: .data(
      position: .init(),
      name: "Maybe",
      params: .init(lifetimeParameters: [], typeParameters: ["a", "b"]),
      constructors: [
        .init(position: .init(), name: "Just", types: [.name(position: .init(), name: "a")]),
        .init(position: .init(), name: "Nothing", types: [.name(position: .init(), name: "b")]),
      ]))
    test("data Maybe [a, b] = Just a (a, b) [a] | Nothing b", matchWith: .data(
      position: .init(),
      name: "Maybe",
      params: .init(lifetimeParameters: [], typeParameters: ["a", "b"]),
      constructors: [
        .init(
          position: .init(),
          name: "Just",
          types: [
            .name(position: .init(), name: "a"),
            .tuple(position: .init(), types: [
              .name(position: .init(), name: "a"),
              .name(position: .init(), name: "b"),
            ]),
            .list(position: .init(), element: .name(position: .init(), name: "a"))
          ]),
        .init(position: .init(), name: "Nothing", types: [.name(position: .init(), name: "b")]),
      ]))
    test("data EitherRef ['A, 'B, a, b] = L (&'A a) | R (&'B b)", matchWith: .data(
      position: .init(),
      name: "EitherRef",
      params: .init(lifetimeParameters: ["'A", "'B"], typeParameters: ["a", "b"]),
      constructors: [
        .init(position: .init(), name: "L", types: [
          .ref(position: .init(), type: .name(position: .init(), name: "a"), lifetimeParameter: "'A"),
        ]),
        .init(position: .init(), name: "R", types: [
          .ref(position: .init(), type: .name(position: .init(), name: "b"), lifetimeParameter: "'B"),
        ]),
      ]))
    test("data EitherRefInt ['A, 'B] = L (&'A Int) | R (&'B Int)", matchWith: .data(
      position: .init(),
      name: "EitherRefInt",
      params: .init(lifetimeParameters: ["'A", "'B"], typeParameters: []),
      constructors: [
        .init(position: .init(), name: "L", types: [
          .ref(position: .init(), type: .name(position: .init(), name: "Int"), lifetimeParameter: "'A"),
        ]),
        .init(position: .init(), name: "R", types: [
          .ref(position: .init(), type: .name(position: .init(), name: "Int"), lifetimeParameter: "'B"),
        ]),
      ]))
    test(
      "data List [a] = Cons a (List a) | Nil",
      matchWith: .data(
        position: .init(),
        name: "List",
        params: .init(lifetimeParameters: [], typeParameters: ["a"]),
        constructors: [
          .init(
            position: .init(),
            name: "Cons",
            types: [
              .name(position: .init(), name: "a"),
              .typeConstructor(position: .init(), name: "List", types: [.name(position: .init(), name: "a")], lifetimeSpecifiers: [])
            ]),
          .init(
            position: .init(),
            name: "Nil",
            types: [])
        ]))
  }
  
  func testGenericParams() {
    /// TODO: - add tests
    /// don't forget to test for invalid ordering ("['A, a, 'B, b]")
  }
  
  func testMultipleBindings() {
    do {
      _ = try parser.parseOrThrow(
        sourceName: "test.bt",
        input: """
        f :: Int -> Int -> Int
        f 10 20 = 10
        g :: Float -> Bool
        g = \\x -> true
        x = 10
        """)
      
      _ = try parser.parseOrThrow(
        sourceName: "test.bt",
        input: """
        x = 10
        --
        """)
      
      _ = try parser.parseOrThrow(
        sourceName: "test.bt", input: """
        
        
        x = 10
        
        
        --
        
        
        """)
      
      _ = try parser.parseOrThrow(
        sourceName: "test.bt", input: """
        f a = 10
        fa :: a -> Int
        fa a = 10
        """)
    } catch {
      print(error)
      XCTFail()
    }
  }
}

// MARK: - Types
extension ParserTests {
  func testTypes() {
    test("Int", matchWith: .name(position: .init(), name: "Int"))
    test("Int -> Int", matchWith: .function(
      position: .init(),
      arg: .name(position: .init(), name: "Int"),
      ret: .name(position: .init(), name: "Int")))
    test("(Int -> Int) -> Int", matchWith: .function(
      position: .init(),
      arg: .function(
        position: .init(),
        arg: .name(position: .init(), name: "Int"),
        ret: .name(position: .init(), name: "Int")),
      ret: .name(position: .init(), name: "Int")))
    test("(Int -> Int, Int, Int)", matchWith: .tuple(
      position: .init(),
      types: [
        .function(
          position: .init(),
          arg: .name(position: .init(), name: "Int"),
          ret: .name(position: .init(), name: "Int")),
        .name(position: .init(), name: "Int"),
        .name(position: .init(), name: "Int"),
      ]))
    test("[Int -> Int]", matchWith: .list(
      position: .init(),
      element: .function(
        position: .init(),
        arg: .name(position: .init(), name: "Int"),
        ret: .name(position: .init(), name: "Int"))
    ))
    test("[Int]", matchWith: .list(
      position: .init(),
      element: .name(position: .init(), name: "Int")))
    test("[[Int]]", matchWith: .list(
      position: .init(),
      element: .list(
        position: .init(),
        element: .name(position: .init(), name: "Int"))))
    test("(F a)", matchWith: .typeConstructor(
      position: .init(),
      name: "F",
      types: [.name(position: .init(), name: "a")],
      lifetimeSpecifiers: []))
    test("(F a b)", matchWith: .typeConstructor(
      position: .init(),
      name: "F",
      types: [.name(position: .init(), name: "a"), .name(position: .init(), name: "b")],
      lifetimeSpecifiers: []))
    test("(F ['A, 'B, a])", matchWith: .typeConstructor(
      position: .init(),
      name: "F",
      types: [.name(position: .init(), name: "a")],
      lifetimeSpecifiers: ["'A", "'B"]))
    test("(F ['A, 'B])", matchWith: .typeConstructor(
      position: .init(),
      name: "F",
      types: [],
      lifetimeSpecifiers: ["'A", "'B"]))
    test("(Maybe a) -> (a -> b) -> (Maybe b)", matchWith: .function(
      position: .init(),
      arg: .typeConstructor(position: .init(), name: "Maybe", types: [.name(position: .init(), name: "a")], lifetimeSpecifiers: []),
      ret: .function(
        position: .init(),
        arg: .function(
          position: .init(),
          arg: .name(position: .init(), name: "a"),
          ret: .name(position: .init(), name: "b")),
        ret: .typeConstructor(position: .init(), name: "Maybe", types: [.name(position: .init(), name: "b")], lifetimeSpecifiers: []))))
    test("&Int", matchWith: .ref(
      position: .init(),
      type: .name(position: .init(), name: "Int"),
      lifetimeParameter: nil))
    test("&[Int]", matchWith: .ref(
      position: .init(),
      type: .list(
        position: .init(),
        element: .name(position: .init(), name: "Int")),
      lifetimeParameter: nil))
    test("&(Int -> Int)", matchWith: .ref(
      position: .init(),
      type: .function(
        position: .init(),
        arg: .name(position: .init(), name: "Int"),
        ret: .name(position: .init(), name: "Int")),
      lifetimeParameter: nil))
    test("&(Int, Bool, [a])", matchWith: .ref(
      position: .init(),
      type: .tuple(
        position: .init(),
        types: [
          .name(position: .init(), name: "Int"),
          .name(position: .init(), name: "Bool"),
          .list(position: .init(), element: .name(position: .init(), name: "a")),
        ]),
      lifetimeParameter: nil))
    test("Int -> &[Bool]", matchWith: .function(
      position: .init(),
      arg: .name(position: .init(), name: "Int"),
      ret: .ref(
        position: .init(),
        type: .list(position: .init(), element: .name(position: .init(), name: "Bool")),
        lifetimeParameter: nil)))
    test("&Int -> Bool", matchWith: .function(
      position: .init(),
      arg: .ref(position: .init(), type: .name(position: .init(), name: "Int"), lifetimeParameter: nil),
      ret: .name(position: .init(), name: "Bool")))
    test("&&Int", matchWith: .ref(
      position: .init(),
      type: .ref(
        position: .init(),
        type: .name(position: .init(), name: "Int"),
        lifetimeParameter: nil),
      lifetimeParameter: nil))
    test("&&&Int", matchWith: .ref(
      position: .init(),
      type: .ref(
        position: .init(),
        type: .ref(
          position: .init(),
          type: .name(position: .init(), name: "Int"),
          lifetimeParameter: nil),
        lifetimeParameter: nil),
      lifetimeParameter: nil))
    test("&'A Int", matchWith: .ref(
      position: .init(),
      type: .name(position: .init(), name: "Int"),
      lifetimeParameter: "'A"))
    test("&'A [Int]", matchWith: .ref(
      position: .init(),
      type: .list(position: .init(), element: .name(position: .init(), name: "Int")),
      lifetimeParameter: "'A"))
    test("'A Bool", matchWith: .name(
      position: .init(),
      name: "Bool",
      lifetimeParameter: "'A"))
    test("&'A Bool -> 'A Bool", matchWith: .function(
      position: .init(),
      arg: .ref(
        position: .init(),
        type: .name(
          position: .init(),
          name: "Bool",
          lifetimeParameter: nil),
        lifetimeParameter: "'A"),
      ret: .name(
        position: .init(),
        name: "Bool",
        lifetimeParameter: "'A"),
      lifetimeParameter: nil))
    // TODO: - Add tests for types with lifetimes (not references)
    test("(Maybe [])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [],
      lifetimeSpecifiers: []))
    test("(Maybe ['A])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [],
      lifetimeSpecifiers: ["'A"]))
    test("(Maybe ['A, 'B, 'C])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [],
      lifetimeSpecifiers: ["'A", "'B", "'C"]))
    test("(Maybe [Int])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [.name(position: .init(), name: "Int")],
      lifetimeSpecifiers: []))
    test("(Maybe ['A, Bool])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [.name(position: .init(), name: "Bool")],
      lifetimeSpecifiers: ["'A"]))
    test("(Maybe ['A, 'B, 'C, Int, Bool, [Int]])", matchWith: .typeConstructor(
      position: .init(),
      name: "Maybe",
      types: [
        .name(position: .init(), name: "Int"),
        .name(position: .init(), name: "Bool"),
        .list(position: .init(), element: .name(position: .init(), name: "Int")),
      ],
      lifetimeSpecifiers: ["'A", "'B", "'C"]))
  }
}

// MARK: - Patterns
extension ParserTests {
  func testPatterns() {
    test("1", matchWith: AST.Pattern.literal(position: .init(), literal: .int(1)))
    test("true", matchWith: AST.Pattern.literal(position: .init(), literal: .logical(.true)))
    test("x", matchWith: .identifier(position: .init(), identifier: "x"))
    test("_", matchWith: .wildcard(position: .init()))
    test("()", matchWith: .tuple(
      position: .init(),
      patterns: []))
    test("(x)", matchWith: .identifier(position: .init(), identifier: "x"))
    test("(x, y)", matchWith: .tuple(
      position: .init(),
      patterns: [
        .identifier(position: .init(), identifier: "x"),
        .identifier(position: .init(), identifier: "y"),
      ]))
    test("(x, (_, y))", matchWith: .tuple(
      position: .init(),
      patterns: [
        .identifier(position: .init(), identifier: "x"),
        .tuple(
          position: .init(),
          patterns: [
            .wildcard(position: .init()),
            .identifier(position: .init(), identifier: "y")
          ])
      ]))
    test("[]", matchWith: .list(.empty(position: .init())))
    test("x:xs", matchWith: .list(.cons(
      position: .init(),
      head: .identifier(position: .init(), identifier: "x"),
      tail: .identifier(position: .init(), identifier: "xs"))))
    test("x:y:xs", matchWith: .list(.cons(
      position: .init(),
      head: .identifier(position: .init(), identifier: "x"),
      tail: .list(.cons(
        position: .init(),
        head: .identifier(position: .init(), identifier: "y"),
        tail: .identifier(position: .init(), identifier: "xs"))))))
    test("x:y:[]", matchWith: .list(.cons(
      position: .init(),
      head: .identifier(position: .init(), identifier: "x"),
      tail: .list(.cons(
        position: .init(),
        head: .identifier(position: .init(), identifier: "y"),
        tail: .list(.empty(position: .init())))))))
    test("x:y:[]", matchWith: .list(.cons(
      position: .init(),
      head: .identifier(position: .init(), identifier: "x"),
      tail: .list(.cons(
        position: .init(),
        head: .identifier(position: .init(), identifier: "y"),
        tail: .list(.empty(position: .init())))))))
    test("10:\"abc\":[]", matchWith: .list(.cons(
      position: .init(),
      head: .literal(position: .init(), literal: .int(10)),
      tail: .list(.cons(
        position: .init(),
        head: .literal(position: .init(), literal: .string("abc")),
        tail: .list(.empty(position: .init())))))))
    test("_:(x,y):[]", matchWith: .list(.cons(
      position: .init(),
      head: .wildcard(position: .init()),
      tail: .list(.cons(
        position: .init(),
        head: .tuple(
          position: .init(),
          patterns: [
            .identifier(position: .init(), identifier: "x"),
            .identifier(position: .init(), identifier: "y"),
          ]),
        tail: .list(.empty(position: .init())))))))
    test("&1", matchWith: .ref(
      position: .init(),
      pattern: .literal(
        position: .init(),
        literal: .int(1))))
    test("&(true:xs)", matchWith: .ref(
      position: .init(),
      pattern: .list(.cons(
        position: .init(),
        head: .literal(
          position: .init(),
          literal: .logical(.true)),
        tail: .identifier(
          position: .init(),
          identifier: "xs")))))
    test("$(Just x y z)", matchWith: .deconstruct(
      position: .init(),
      constructor: "Just",
      patterns: [
        .identifier(position: .init(), identifier: "x"),
        .identifier(position: .init(), identifier: "y"),
        .identifier(position: .init(), identifier: "z"),
      ]))
    test("$(Just $(Just x) (10:xs) $(Nothing))", matchWith: .deconstruct(
      position: .init(),
      constructor: "Just",
      patterns: [
        .deconstruct(
          position: .init(),
          constructor: "Just",
          patterns: [.identifier(position: .init(), identifier: "x")]),
        .list(.cons(
          position: .init(), head: .literal(position: .init(), literal: .int(10)),
          tail: .identifier(position: .init(), identifier: "xs"))),
        .deconstruct(
          position: .init(),
          constructor: "Nothing", patterns: []),
      ]))
    test("$(Just $(Just $(Just (a, b))))", matchWith: .deconstruct(
      position: .init(),
      constructor: "Just",
      patterns: [
        .deconstruct(
          position: .init(),
          constructor: "Just",
          patterns: [
            .deconstruct(
              position: .init(),
              constructor: "Just",
              patterns: [
                .tuple(
                  position: .init(),
                  patterns: [
                    .identifier(position: .init(), identifier: "a"),
                    .identifier(position: .init(), identifier: "b"),
                  ])
              ])
          ])
      ]))
  }
}

//extension ParserTests {
//  func testASTs() {
//    filesInDir(dir: "AST")
//  }
//
//  private func filesInDir(dir: String) -> [String] {
//    let bundle = Bundle(for: type(of: self))
//    let paths = bundle.paths(forResourcesOfType: "ast", inDirectory: nil)
//    return []
//  }
//}

private extension ParserTests {
  func test(_ input: String, matchWith: AST.Expression) {
    do {
      let parsed = try parser.expression.run(sourceName: "test.bt", input: input)
      if matchWith != parsed {
        print("Expected:\n\(matchWith)\n\nGot:\n\(parsed)")
        XCTFail()
      }
    } catch {
      print(input, error)
      XCTFail()
    }
  }
  
  func test(_ input: String, matchWith: AST.Binding) {
    do {
      let parsed = try parser.statement.run(sourceName: "test.bt", input: input)
      if matchWith != parsed {
        print("Expected:\n\(matchWith)\n\nGot:\n\(parsed)")
        XCTFail()
      }
    } catch {
      print(input, error)
      XCTFail()
    }
  }
  
  func test(_ input: String, matchWith: AST.AType) {
    do {
      let parsed = try parser.type.run(sourceName: "test.bt", input: input)
      if matchWith != parsed {
        print("Expected:\n\(matchWith)\n\nGot:\n\(parsed)")
        XCTFail()
      }
    } catch {
      print(input, error)
      XCTFail()
    }
  }
  
  func test(_ input: String, matchWith: AST.Pattern) {
    do {
      let parsed = try parser.pattern.run(sourceName: "test.bt", input: input)
      if matchWith != parsed {
        print("Expected:\n\(matchWith)\n\nGot:\n\(parsed)")
        XCTFail()
      }
    } catch {
      print(input, error)
      XCTFail()
    }
  }
}
