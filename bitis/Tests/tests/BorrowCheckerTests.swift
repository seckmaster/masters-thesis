//
//  BorrowCheckerTests.swift
//  
//
//  Created by Toni K. Turk on 08/12/2021.
//

@testable import Bitis
import XCTest
import SwiftParsec
import Bow

class BorrowCheckerTests: XCTestCase {
}

extension BorrowCheckerTests {
  func testDetectCycle() {
    testBitis(
      code: """
      x :: Int
      y :: Int
      x = 10
      y = x
      """)
    testBitis(
      code: """
      x :: &Int
      y :: &Int
      x = y
      y = x
      """)
    testBitis(
      code: """
      x :: Int
      y :: Int
      x = let y = 10 in y
      y = x
      """)
    testBitis(
      code: """
      f :: &'A a -> Int
      f _ = 10
      x :: Int
      y :: Int
      x = f (&y)
      y = x
      """)
    testBitis(
      code: """
      x :: Int
      y :: Int
      x = (\\y -> y) 10
      y = x
      """)
    testBitisFailing(
      code: """
      x :: Int
      x = x
      """,
      error: .cycleDetected(["x"]))
    testBitisFailing(
      code: """
      x :: Int
      y :: Int
      x = y
      y = x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      x :: Int
      y :: Int
      z = 10
      x = y
      y = if true then z else x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      x = (let
        a :: Int
        b :: Int
        a = b
        b = a
      in 10)
      """,
      error: .cycleDetected(["a", "b"]))
    testBitisFailing(
      code: """
      x :: Int
      y :: Int
      z :: Int
      x = y
      y = z
      z = x
      """,
      error: .cycleDetected(["x", "y", "z"]))
    testBitisFailing(
      code: """
      x :: Int
      y :: Int
      x = (\\y -> y) y
      y = x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      x = let
        a :: Int
        b :: Int
        a = b
        b = (\\i -> a) ()
      in 5
      """,
      error: .cycleDetected(["a", "b"]))
    testBitisFailing(
      code: """
      a = let
        a :: Int
        c :: Int
        a = c
        b = (\\i -> a)
        c = b ()
      in 10
      """,
      error: .cycleDetected(["a", "c"]))
    testBitisFailing(
      code: """
      a :: Int
      c :: Int
      a = c
      b = (\\i -> a)
      c = b ()
      """,
      error: .cycleDetected(["a", "c"]))
    testBitisFailing(
      code: """
      data List [a] = Cons a (List a) | Nil
      x :: (List Int)
      y :: (List Int)
      x = Cons 10 y
      y = Cons 20 x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      data List [a] = Cons a (List a) | Nil
      x :: (List Int)
      y :: (List Int)
      f = Cons 10
      g = Cons 20
      x = f y
      y = f x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      data List [a] = Cons (List a) a | Nil
      x :: (List Int)
      y :: (List Int)
      f = Cons y
      g = Cons x
      x = f 1
      y = g 2
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      data List [a] = Cons (List a) a | Nil
      x :: (List Int)
      f = Cons x
      x = f 1
      """,
      error: .cycleDetected(["x"]))
    testBitisFailing(
      code: """
      f :: a -> a -> a
      g :: a -> a -> a
      f a _ = a
      g _ b = b
      
      x :: Int
      y :: Int
      z :: Int
      x = y
      y = f x z
      z = 10
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      f :: a -> a -> a
      g :: a -> a -> a
      f a _ = a
      g _ b = b
      
      x :: Int
      y :: Int
      z :: Int
      x = y
      y = g x z
      z = 10
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      data List ['A] = Cons (&'A Int) (List ['A]) | Nil
      gen :: &'A Int -> (List ['A])
      gen a = let
        x :: (List [])
        y :: (List [])
        x = Cons a y
        y = Cons a x
      in x
      """,
      error: .cycleDetected(["x", "y"]))
    testBitisFailing(
      code: """
      fst :: (a, b) -> a
      fst (a, b) = a
      a :: Int
      b :: [Int]
      x :: (Int, (Int, [Int]))
      y :: Int
      z :: (Int, [Int])
      a = 10
      b = [1,2,3,y]
      x = (y, z)
      z = (a, b)
      y = fst x
      """,
      error: .cycleDetected(["b", "x", "y", "z"]))
    // TODO: - Not working (too strict)
    testBitis(
      code: """
      x = let
        f x = x * 2
        a = f 10
      in a
      """)
  }
  
  func testAlreadyMoved() {
    testBitisFailing(
      code: """
      x = 10
      y = x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      f x = ()
      x = 10
      z = f x
      e = f x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = 20
      z = (x, y)
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = 20
      z = [x, y]
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = true
      y = if true then x else &x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      z = x * x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = 20
      z = x + y
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = 20
      z = x + y
      a = y
      """,
      error: .alreadyMoved(name: "y"))
    testBitisFailing(
      code: """
      x = true
      y = false
      z = x && y
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = true
      y = false
      z = x && y
      a = y
      """,
      error: .alreadyMoved(name: "y"))
    testBitisFailing(
      code: """
      x = 10
      y = case x of 0 -> true | _ -> false
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = let a = x in 10
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = 20
      z = if true then x else y
      a = (x, y)
      """,
      error: .alreadyMoved(name: "x"))
    // TODO: - Fix (issue with `performInParallel`) ...
    // An example one bellow works, however.
    testBitisFailing(
      code: """
      x = 10
      z = if true then let in if true then x else 0 else 0
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      z = if true then if true then x else 0 else 0
      a = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = \\a -> x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      f _ _ = ()
      x = 10
      y = 20
      a = f x y
      b = (x, y)
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = let a = 10, b = a, c = a in 10
      """,
      error: .alreadyMoved(name: "a"))
    testBitisFailing(
      code: """
      g _ = 10
      f (x:xs) = (g x) + (g x)
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      g (x:xs) = x
      xs = [1, 2, 3]
      x = case xs of (x:ss) -> (g ss) + (g ss)
      """,
      error: .alreadyMoved(name: "ss"))

    testBitisFailing(
      code: """
      g (x:xs) = x
      xs = [1, 2, 3]
      x = case xs of (x:ss) -> (g xs) + (g xs)
      """,
      error: .alreadyMoved(name: "xs"))
    testBitisFailing(
      code: """
      x = true
      y = if &x then x else x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = &x
      z = y
      w = y
      """,
      error: .alreadyMoved(name: "y"))
    testBitisFailing(
      code: """
      x = 10
      y = &x
      z = (y, y)
      """,
      error: .alreadyMoved(name: "y"))
    testBitisFailing(
      code: """
      x = 10
      y = (x, x, x)
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = [x, x, x]
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing( // @FIXME: - 
      code: """
      x = 10
      y = 20
      print case true of true -> x + y | false -> y
      print case false of true -> x + y | false -> y
      """,
      error: .alreadyMoved(name: "x"))
    testBitisFailing(
      code: """
      f x y z = 10
      x = 10
      y = f x x x
      """,
      error: .alreadyMoved(name: "x"))
    testBitis(
      code: """
      x = 10
      y = let x = 10, y = x in y
      z = x
      """)
    testBitis(
      code: """
      x = 10
      y = \\x -> x
      z = x
      """)
    testBitis(
      code: """
      x = true
      y = if true then x else x
      """)
    testBitisFailing(
      code: """
      x = true
      y = if true then x else x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitis(
      code: """
      x = 10
      y = if &x == 1 then x + 1 else x + 2
      """)
    testBitis(
      code: """
      x = 10
      y = if true then x + 1 else x + 2
      """)
    testBitis(
      code: """
      x = 10
      y = if true then x + 1 else x
      """)
    testBitisFailing(
      code: """
      x = 10
      y = if true then x + 1 else x
      z = x
      """,
      error: .alreadyMoved(name: "x"))
    testBitis( // name shadowing
      code: """
      f x = let
        a = x
        g x = let
          b = x
        in 10
      in 20
      """)
    testBitis( // name shadowing
      code: """
      f n = case n of
        n -> case n of
         n -> n
      """)
    testBitis(
      code: """
      a = 10
      x = case 0 of 0 -> a * 5 | m -> a * m
      """)
  }
  
  func testCannotMoveWhileBorrowed() {
    testBitisFailing(
      code: """
      x = 10
      y = &x
      z = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = &x
      f a = a
      z = f x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A a -> &'A a
      f a = a
      x = 10
      y = f (&x)
      z = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      z = &(if true then x else x)
      mv = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A a -> &'A a
      f a = a
      x = 10
      z = f (&(if true then x else x))
      mv = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A [a] -> &'A [a] -> &'A [a]
      f xs _ = xs
      x = [1,2,3]
      y = [4,5,6]
      z = f (&x) (&y)
      
      own _ = ()
      e = own y
      """,
      error: .moveWhileBorrowed(name: "y"))
    testBitisFailing(
      code: """
      x = let
        a = 10
        b = \\i -> &a
        c = a
      in c
      """,
      error: .moveWhileBorrowed(name: "a"))
    testBitisFailing(
      code: """
      move a = ()
      x = let
        a = 10
        b = \\i -> &a
        x = move a
      in 5
      """,
      error: .moveWhileBorrowed(name: "a"))
    testBitisFailing(
      code: """
      a = let
        x = 10
        y = let r = &x in r -- borrow is not released after the scope is popped
        z = x
      in 5
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      b = let
        x = 10
        y = 20
        z = let r = (&x, &y) in r -- borrow is transfered out of scope
        w = x
      in 5
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      b = let
        x = 10
        y = 20
        z = let r = (&x, &y) in r -- borrow is transfered out of scope
        w = y
      in 5
      """,
      error: .moveWhileBorrowed(name: "y"))
    testBitisFailing(
      code: """
      b = let
        x = 10
        y = 20
        z = let r = [&x, &y] in r -- borrow is transfered out of scope
        v = x
      in 5
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      b = let
        x = 10
        y = 20
        z = let r = [&x, &y] in r -- borrow is transfered out of scope
        v = y
      in 5
      """,
      error: .moveWhileBorrowed(name: "y"))
    testBitisFailing(
      code: """
      b = let
        x = 10
        y = 20
        z = (&x, &y)
        v = let r = &z in r -- borrow is transfered out of scope
        w = z
      in 5
      """,
      error: .moveWhileBorrowed(name: "z"))
    testBitisFailing(
      code: """
      f :: &'A a -> &'A b -> (&'A a, &'A b)
      f a b = (a, b)
      
      x = 10
      y = 20
      z = f (&x) (&y)
      w = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A a -> &'B b -> (&'A a, &'B b)
      f a b = (a, b)
      
      x = 10
      y = 20
      z = f (&x) (&y)
      w = y
      """,
      error: .moveWhileBorrowed(name: "y"))
    testBitisFailing(
      code: """
      g :: (&'A a, &'B b) -> &'A a
      g (a, b) = a
      
      x = 10
      y = 20
      z = g (&x, &y)
      w = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      data Ref ['A, a] = R (&'A a)
              
      f :: &'A a -> (Ref ['A, a])
      f a = R a
      
      x = 10
      y = f (&x)
      z = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      data Ref ['A, a] = R (&'A a)
      f :: &'A a -> &'B a -> (Ref ['A, a])
      f a b = R a
      
      x = 10
      y = 20
      r = f (&x) (&y)
      z = y
      w = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      data Ref ['A] = R (&'A (Ref ['A])) | N
      f :: () -> (Ref ['A])
      f () = let
        x :: (Ref [])
        y :: (Ref [])
        y = N ()
        x = R (&y)
      in y
      """,
      error: .moveWhileBorrowed(name: "y"))
    testBitisFailing(
      code: """
      x = let
        b = true
        c = if &b then 1 else 2
        a = b
      in 10
      """,
      error: .moveWhileBorrowed(name: "b"))
    testBitisFailing(
      code: """
      f :: &'A Bool -> 'A Int
      f a = if a then 1 else 2
      
      x = true
      y = f (&x)
      z = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      one = 1
      
      f :: &'A Int -> &'A Int
      f n = &one + n
      
      x = 10
      y = f (&x)
      z = one
      """,
      error: .moveWhileBorrowed(name: "one"))
    testBitis(
      code: """
      x = 10
      y = let y = &x in 10
      z = x
      """)
    testBitis(
      code: """
      f :: &'A a -> ()
      f _ = ()
      x = 10
      y = f (&x)
      z = x
      """)
    testBitis(
      code: """
      f :: &'A [a] -> &'B [a] -> &'A [a]
      f xs _ = xs
      x = [1,2,3]
      y = [4,5,6]
      z = f (&x) (&y)
      
      own _ = ()
      e = own y
      """)
    testBitis(
      code: """
      g :: (&'A a, &'B b) -> &'A a
      g (a, b) = a
      
      x = 10
      y = 20
      z = g (&x, &y)
      w = y
      """)
    
    testBitis(
      code: """
      x = true
      y = if &x then x else x
      """)
    testBitis(
      code: """
      x = true
      y = if true then x else &x
      """)
    testBitis(
      code: """
      x = true
      y = if true then &x else x
      """)
    testBitis(
      code: """
      a = let
        x = true
        z = if true then &x else &x
      in 10
      """)
    testBitisFailing(
      code: """
      a = let
        x = true
        z = if true then &x else &x
        g = x
      in 10
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A [Int] -> 'A Int
      f &[] = 0
      f (x:xs) = x + (f xs)
      
      a = let
        x = [1,2,3]
        y = f (&x)
        z = x
      in 10
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A [Int] -> 'A Int
      f &[] = 0
      f (x:xs) = x + (f xs)
      
      a = let
        x = [1,2,3]
        y = let a = f (&x) in a
        z = x
      in 10
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = &x >= 9
      z = x
      """,
      error: .moveWhileBorrowed(name: "x"))
    testBitis(
      code: """
      x = 10
      y = &x
      z = &x
      """)
    // @TODO: - Not working
    testBitisFailing(
      code: """
      data Maybe ['A, a] = Cons (&'A a)

      f :: &'B (Maybe ['A, a]) -> &'A a
      f $(Cons x) = *x

      x = let
        a = 10
        b = let
          c = Cons (&a)
          d = f (&c)
        in d
        e = a
      in ()
      """,
      error: .moveWhileBorrowed(name: "a"))
  }
  
  func testCannotBorrowFromMovedValue() {
    testBitisFailing(
      code: """
      x = 10
      y = x
      z = &x
      """,
      error: .borrowFromMovedValue(name: "x"))
    testBitisFailing(
      code: """
      f :: &'A a -> &'A a
      x = 10
      y = x
      z = f (&x)
      """,
      error: .borrowFromMovedValue(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = (x, &x)
      """,
      error: .borrowFromMovedValue(name: "x"))
    testBitisFailing(
      code: """
      x = 10
      y = (&x, x)
      """,
      error: .moveWhileBorrowed(name: "x"))
    // self-borrow
    testBitis(
      code: """
      data Ref ['A] = R (&'A (Ref ['A]))
      a = let
        x :: (Ref [])
        x = R (&x)
      in x
      """)
  }
  
  // Move out of borrow is currently disabled ...
  func testCannotMoveOutOfBorrow() {
    testBitisFailing(
      code: """
      f :: &'A Int -> Int
      f x = *x
      """,
      error: .cannotMoveOutOfBorrow)
    testBitisFailing(
      code: """
      f :: &'A Int -> Int
      f x = let a = x in *a
      """,
      error: .cannotMoveOutOfBorrow)
    testBitisFailing(
      code: """
      f :: &'A (Int, String) -> Int
      f (a, b) = *a
      """,
      error: .cannotMoveOutOfBorrow)
    testBitisFailing(
      code: """
      f :: &'A [Int] -> Int
      f (x:xs) = *x
      """,
      error: .cannotMoveOutOfBorrow)
    testBitisFailing(
      code: """
      data Pair = Cons Int String
      f :: &'A (Pair []) -> Int
      f $(Cons a b) = *a
      """,
      error: .cannotMoveOutOfBorrow)
    // TODO: - Fix lifetime analyser first!
    testBitisFailing(
      code: """
      x = let
        a = 10
        b = &a
        c = *b
      in c
      """,
      error: .cannotMoveOutOfBorrow)
  }
  
  func testClique() {
    testBitis(code: """
    data ListRef ['A, a] = Cons (&'A a) (ListRef ['A, a]) | Nil
    data Graph2 ['A] = Node Int (ListRef ['A, (Graph2 ['A])])
    
    fmap :: (a -> (Graph2 ['A])) -> [a] -> [(Graph2 ['A])]
    
    convert :: &'A [a] -> (ListRef ['A, a])
    convert (x:xs) = Cons x (convert xs)
    convert _ = Nil ()
    
    clique :: [Int] -> [(Graph2 ['A])]
    clique xs = let
      c :: [(Graph2 [])]
      c = fmap (\\i -> Node i (convert (&c))) xs -- self-borrow
    in c
    """)
    testBitis(code: """
      data List [c] = Cons c (List c) | Nil ()
      data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef ()
      data Graph ['A] = Node Int (ListRef ['A, (Graph ['A])])
      
      fmap :: (a -> b) -> (List a) -> (List b)
      fmap _ $(Nil ())    = Nil ()
      fmap f $(Cons x xs) = Cons (f x) (fmap f xs)
      
      convert :: &'A (List a) -> (ListRef ['A, a])
      convert $(Cons x xs) = ConsRef x (convert xs)
      convert $(Nil ())    = NilRef ()
      
      clique :: (List Int) -> (List (Graph ['A]))
      clique xs = let
        c :: (List (Graph []))
        c = fmap (\\i -> Node i (convert (&c))) xs
        --               ~~~~~~~~~~~~~~~~~~~~~~ self-borrow
      in c
      """)
  }
  
  func testListRef() {
    testBitis(code: """
    data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef
    
    hd :: &'A (ListRef ['A, a]) -> &'A a
    hd $(ConsRef x _) = *x
    
    tail :: &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
    tail $(ConsRef _ xs) = xs
    
    take :: Int -> &'A (ListRef ['A, a]) -> (ListRef ['A, a])
    take _ $(NilRef _) = NilRef ()
    take 0 _ = NilRef ()
    take n $(ConsRef x xs) = ConsRef (*x) (take (n - 1) xs)
    
    drop :: Int -> &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
    drop n $(ConsRef _ xs) = drop (n - 1) xs
    drop _ $(NilRef _) = &(NilRef ())
    """)
  }
  
  func testRandomPrograms() {
    testBitis(code: """
    data G ['A] = Node (&'A [(G ['A])])
    
    f :: () -> [(G ['A])]
    f () = let
    c :: [(G [])]
    c = [Node (&c)]
    in c
    """)
  }
}

// MARK: - Helpers
extension BorrowCheckerTests {
  func testBitis(
    code: String,
    ast: BCAst,
    env: BCState = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: ast,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBitis(
    code: String,
    env: BCState = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBitisFailing(
    code: String,
    error: BorrowError,
    env: BCState = .empty,
    line: Int = #line
  ) {
    testFailing(
      code: code,
      expected: error,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBinding(
    code: String,
    binding: BCAst.Binding,
    env: BCState = .empty,
    typeEnv: Bitis.Env = .empty
  ) {
    test(
      code: code,
      expected: binding,
      parser: Parser().statement,
      env: env,
      typeEnv: typeEnv)
  }
  
  func testExpression(
    code: String,
    expression: BCAst.Expression,
    env: BCState = .empty
  ) {
    test(
      code: code,
      expected: expression,
      parser: Parser().expression,
      env: env)
  }
  
  func test<T: BorrowCheckable & LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    expected: T,
    parser: Lexer.Parser<T>,
    env: BCState,
    typeEnv: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker().runM((typeEnv, .init(), .empty, .init()))^.mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(LAState.empty)^.mapLeft { $0 as Error } }
        .flatMap { $0.1.borrowChecker().runM(env)^.mapLeft { $0 as Error } }^
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      } else {
        XCTAssertEqual(res.rightValue.1, expected)
        if res.rightValue.1 != expected {
          print("line \(line)")
          print(
            expected.prettyDescription,
            "\n\n---\n\n",
            res.rightValue.1.prettyDescription.trimmingCharacters(in: .whitespaces)
          )
        }
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }

  func test<T: BorrowCheckable & LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    parser: Lexer.Parser<T>,
    env: BCState,
    typeEnv: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker().runM((typeEnv, .init(), .empty, .init()))^.mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(LAState.empty)^.mapLeft { $0 as Error } }
        .flatMap { $0.1.borrowChecker().runM(env)^.mapLeft { $0 as Error } }^
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }

  func testFailing<T: BorrowCheckable & LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    expected: BorrowError,
    parser: Lexer.Parser<T>,
    env: BCState,
    typeEnv: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker().runM((typeEnv, .init(), .empty, .init()))^.mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(LAState.empty)^.mapLeft { $0 as Error } }
        .flatMap { $0.1.borrowChecker().runM(env)^.mapLeft { $0 as Error } }^
      if res.isLeft {
        XCTAssertEqual(expected, (res.leftValue as? BCError)?.error)
        if !(res.leftValue is BCError) || expected != (res.leftValue as? BCError)?.error {
          print(line)
        }
      } else {
        print(line)
        XCTFail("Expected error!")
      }
    } catch {
      XCTFail("")
    }
  }
}
