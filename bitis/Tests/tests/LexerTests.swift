//
// Created by Toni K. Turk on 29/10/2020.
//

@testable import Bitis
import XCTest
import SwiftParsec

class LexerTests: XCTestCase {
  let lexer: Lexer = .init()
}

// MARK: - Identifiers and Keywords
extension LexerTests {
  func testIdentifier() {
    func test(input: String, expected: String) {
      switch lexer.identifier.runTest(input) {
      case .identifier(let identifier, let position)? where identifier == expected:
        XCTAssertEqual(.init(1, 1, 1, 1 + expected.count), position)
      case _:
        XCTFail("Lexer error.")
      }
    }

    func testFail(input: String) {
      switch lexer.identifier.runFailingTest(input) {
      case true:
        XCTFail("Lexer error.")
      case false:
        break
      }
    }

    test(input: "thisIsAValidIdentifier", expected: "thisIsAValidIdentifier")
    test(input: "_123", expected: "_123")
    test(input: "AbC123", expected: "AbC123")
    test(input: "__abcdefg", expected: "__abcdefg")
    test(input: "falsed", expected: "falsed")
    test(input: "falseAndMore", expected: "falseAndMore")
    test(input: "ifThenElse", expected: "ifThenElse")
    testFail(input: "1abc")
    testFail(input: "!abc")
    testFail(input: "{- -}")
    testFail(input: "let")
    testFail(input: "true")
    testFail(input: "false")
  }
  
  func testUppercaseSequence() {
    switch lexer.uppercaseLetterSequence.runTest("UPPERCASE") {
    case .identifier(let identifier, _):
      XCTAssertEqual(identifier, "UPPERCASE")
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLetKeyword() {
    switch lexer.let.runTest("let") {
    case .keyword(.let, let position)?:
      XCTAssertEqual(.init(1, 1, 1, 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testIfKeyword() {
    switch lexer.if.runTest("if") {
    case .keyword(.if, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testThenKeyword() {
    switch lexer.then.runTest("then") {
    case .keyword(.then, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testElseKeyword() {
    switch lexer.else.runTest("else") {
    case .keyword(.else, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testDataKeyword() {
    switch lexer.data.runTest("data") {
    case .keyword(.data, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testTypeKeyword() {
    switch lexer.type.runTest("type") {
    case .keyword(.type, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testCaseKeyword() {
    switch lexer.case.runTest("case") {
    case .keyword(.case, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testInKeyword() {
    switch lexer.in.runTest("in") {
    case .keyword(.in, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testWildcardKeyword() {
    switch lexer.wildcard.runTest("_") {
    case .keyword(._, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testOfKeyword() {
    switch lexer.of.runTest("of") {
    case .keyword(.of, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }
}

// MARK: - Operators
extension LexerTests {
  // MARK: - Arithmetic
  func testPlus() {
    switch lexer.plus.runTest("+") {
    case .operator(.arithmetic(.plus), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testMinus() {
    switch lexer.minus.runTest("-") {
    case .operator(.arithmetic(.minus), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testMul() {
    switch lexer.mul.runTest("*") {
    case .operator(.arithmetic(.mul), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testDiv() {
    switch lexer.div.runTest("/") {
    case .operator(.arithmetic(.div), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  // MARK: - Logical
  func testEqual() {
    switch lexer.equal.runTest("==") {
    case .operator(.logical(.equal), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testNotEqual() {
    switch lexer.notEqual.runTest("/=") {
    case .operator(.logical(.notEqual), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLowerThan() {
    switch lexer.lowerThan.runTest("<") {
    case .operator(.logical(.lowerThan), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLowerOrEqualThan() {
    switch lexer.lowerOrEqualThan.runTest("<=") {
    case .operator(.logical(.lowerOrEqualThan), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testGreaterThan() {
    switch lexer.greaterThan.runTest(">") {
    case .operator(.logical(.greaterThan), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testGreaterOrEqualThan() {
    switch lexer.greaterOrEqualThan.runTest(">=") {
    case .operator(.logical(.greaterOrEqualThan), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testOr() {
    switch lexer.or.runTest("||") {
    case .operator(.logical(.or), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testAnd() {
    switch lexer.and.runTest("&&") {
    case .operator(.logical(.and), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  // MARK: - Brackets
  func testLeftBracket() {
    switch lexer.leftParent.runTest("(") {
    case .operator(.bracket(.leftParent), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testRightBracket() {
    switch lexer.rightParent.runTest(")") {
    case .operator(.bracket(.rightParent), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLeftBrace() {
    switch lexer.leftBrace.runTest("{") {
    case .operator(.bracket(.leftBrace), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testRightBrace() {
    switch lexer.rightBrace.runTest("}") {
    case .operator(.bracket(.rightBrace), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLeftSquareBracket() {
    switch lexer.leftBracket.runTest("[") {
    case .operator(.bracket(.leftBracket), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testRightSquareBracket() {
    switch lexer.rightBracket.runTest("]") {
    case .operator(.bracket(.rightBracket), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  // MARK: - Other
  func testAssign() {
    switch lexer.assign.runTest("=") {
    case .operator(.assign, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testDot() {
    switch lexer.dot.runTest(".") {
    case .operator(.dot, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testLambda() {
    switch lexer.lambda.runTest("\\") {
    case .operator(.lambda, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
    switch lexer.lambda.runTest("λ") {
    case .operator(.lambda, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testEmpty() {
    switch lexer.emptyTuple.runTest("()") {
    case .operator(.emptyTuple, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testEmptyList() {
    switch lexer.emptyList.runTest("[]") {
    case .operator(.emptyList, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testRightArrow() {
    switch lexer.rightArrow.runTest("->") {
    case .operator(.rightArrow, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
  }
  
  func testRef() {
    switch lexer.ref.runTest("&") {
    case .operator(.ref, let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
    case _:
      XCTFail("Lexer error.")
    }
  }
}

// MARK: - Logical
extension LexerTests {
  func testTrue() {
    switch lexer.logicalLiteral.runTest("true") {
    case .literal(.logical(.true), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testFalse() {
    switch lexer.logicalLiteral.runTest("false") {
    case .literal(.logical(.false), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 5), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testTrueOrFalse1() {
    switch lexer.logicalLiteral.runTest("true") {
    case .literal(.logical(.true), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 4), position)
    case _:
      XCTFail("Lexer error.")
    }
  }

  func testTrueOrFalse2() {
    switch lexer.logicalLiteral.runTest("false") {
    case .literal(.logical(.false), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 5), position)
    case _:
      XCTFail("Lexer error.")
    }
  }
}

// MARK: - Numeric
extension LexerTests {
  func testInt() {
    func test(input: String, expected: Int) {
      switch lexer.intLiteral.runTest(input) {
      case .literal(.int(expected), let position):
        XCTAssertEqual(.init(1, 1, 1, 1 + expected.description.count), position)
      case _:
        XCTFail("Lexer error.")
      }
    }

    test(input: "0", expected: 0)
    test(input: "1", expected: 1)
    test(input: "1002", expected: 1002)
    test(input: "8888", expected: 8888)
    test(input: "987654321", expected: 987654321)
    test(input: "-1", expected: -1)
    test(input: "-1002", expected: -1002)
    test(input: "-8888", expected: -8888)
    test(input: "-987654321", expected: -987654321)
  }

  func testFloat() {
    func test(input: String, expected: Double) {
      switch lexer.floatLiteral.runTest(input) {
      case .literal(.float(expected), let position):
        XCTAssertEqual(.init(1, 1, 1, 1 + expected.description.count), position)
      case _:
        XCTFail("Lexer error.")
      }
    }

    test(input: "1.0", expected: 1)
    test(input: "10.02", expected: 10.02)
    test(input: "8888.123", expected: 8888.123)
    test(input: "-0.003", expected: -0.003)
    test(input: "-9.999", expected: -9.999)
    test(input: "-0.123", expected: -0.123)
  }

  func testIntOrFloat() {
    switch lexer.numericLiteral.runTest("10") {
    case .literal(.int(10), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 2), position)
    case _:
      XCTFail("Lexer error.")
    }
    switch lexer.numericLiteral.runTest("10.25") {
    case .literal(.float(10.25), let position):
      XCTAssertEqual(.init(1, 1, 1, 1 + 5), position)
    case _:
      XCTFail("Lexer error.")
    }
  }
}

// MARK: - String
extension LexerTests {
  func testString() {
    func test(input: String, expected: String) {
      switch lexer.stringLiteral.runTest(input) {
      case let .literal(.string(literal), position) where literal == expected:
        XCTAssertEqual(.init(1, 1, 1, 1 + expected.description.count), position)
      case let .literal(.string(literal), _):
        print("Expected", expected, "got", literal)
        XCTFail("Lexer error.")
      case _:
        XCTFail("Lexer error.")
      }
    }

    test(input: "\"\"", expected: "")
    test(input: "\"Lorem ipsum.\"", expected: "Lorem ipsum.")
  }

  func testCharacter() {
    func test(input: String, expected: Character) {
      switch lexer.characterLiteral.runTest(input) {
      case let .literal(.char(char), position) where char == expected:
        XCTAssertEqual(.init(1, 1, 1, 1 + 1), position)
      case let .literal(.char(char), _):
        print("Expected", expected, "got", char)
        XCTFail("Lexer error.")
      case _:
        XCTFail("Lexer error.")
      }
    }

    test(input: "'A'", expected: "A")
    test(input: "'*'", expected: "*")
  }
}

extension LexerTests {
  func testDummyIfExpression() {
    let parser = lexer.if
      .flatMap { _ in
        self.lexer.identifier
      }
      .flatMap { _ in
        self.lexer.then
      }
      .flatMap { _ in
        self.lexer.numericLiteral
      }
      .flatMap { _ in
        self.lexer.else
      }
      .flatMap { _ in
        self.lexer.true
      }
    if parser.runTest("if variable then 10.2 else true") == nil {
      XCTFail("Lexer error.")
    }
    if parser.runTest("if   variable then 10.2  else     true") == nil {
      XCTFail("Lexer error.")
    }
    if parser.runTest("if {- comment -} variable then 10.2 else true -- also comment") == nil {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("if variable then 10.2 true") {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("if variable then 10.2 else false") {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("if let then 10.2 else true") {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("if variable the 10.2 else true") {
      XCTFail("Lexer error.")
    }
  }

  func testDummyFunctionPrototype() {
    let parser = lexer.identifier
      .flatMap { _ in
        self.lexer.hasType
      }
      .flatMap { _ in
        self.lexer.identifier
      }
      .flatMap { _ in
        self.lexer.rightArrow
      }
      .flatMap { _ in
        self.lexer.identifier
      }
    if parser.runTest("map :: Int -> Int") == nil {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("case :: Int -> Int") {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("map : Int -> Int") {
      XCTFail("Lexer error.")
    }
    if parser.runFailingTest("map :: Int ->") {
      XCTFail("Lexer error.")
    }
  }

  func testDummyLambda() {
    let parser = lexer.leftParent
      .flatMap { _ in
        self.lexer.lambda
      }
      .flatMap { _ in
        self.lexer.identifier
      }
      .flatMap { _ in
        self.lexer.rightArrow
      }
      .flatMap { _ in
        self.lexer.identifier
      }
      .flatMap { _ in
        self.lexer.rightParent
      }
    if parser.runTest("(\\x -> x)") == nil {
      XCTFail("Lexer error.")
    }
  }
  
  func testDummyNewLine() {
    let parser = lexer.identifier >>- { i1 in
      self.lexer.newLine >>- {
        self.lexer.identifier >>- { i2 in
          .init(result: (i1.identifier, i2.identifier))
        }
      }
    }
    let res = parser.runTest("""
      ident1
      ident2
      """)
    switch res {
    case let (i1, i2)?:
      XCTAssertEqual(i1, "ident1")
      XCTAssertEqual(i2, "ident2")
    case _:
      XCTFail()
    }
  }
}

// MARK: - Helpers
extension GenericParser where UserState == Lexer.UserState, StreamType == String {
  func runTest(_ input: String) -> Result? {
    do {
      return try run(sourceName: "test.bt", input: input)
    } catch {
      print(input, error)
      return nil
    }
  }

  func runFailingTest(_ input: String) -> Bool {
    do {
      _ = try run(sourceName: "test.bt", input: input)
      return true
    } catch {
      return false
    }
  }
}

extension Lexer.Position {
  init(_ a: Int, _ b: Int, _ c: Int, _ d: Int) {
    self.init(start: .init(line: a, column: b), end: .init(line: c, column: d))
  }

  init(_ a: Int, _ b: Int) {
    self.init(start: .init(line: a, column: b), end: .init(line: a, column: b))
  }
}
