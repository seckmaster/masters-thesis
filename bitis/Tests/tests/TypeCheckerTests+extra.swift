//
//  TypeCheckerTests+extra.swift
//  tests
//
//  Created by Toni K. Turk on 17/11/2020.
//

@testable import Bitis
import XCTest
import SwiftParsec
import Bow

// MARK: - Helpers
fileprivate extension TypeCheckerTests {
  func testExpr(
    expected: TCType,
    ast: AST.Expression,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testFailingExpr(
    expected: TypeError,
    ast: AST.Expression,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testPatt(
    expected: TCType,
    ast: AST.Pattern,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testFailingPatt(
    expected: TypeError,
    ast: AST.Pattern,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testType(
    expected: TCType,
    ast: AST.AType,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testFailingType(
    expected: TypeError,
    ast: AST.AType,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testBinding(
    expected: TCType,
    ast: AST.Binding,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, ast: ast, env: env, line: line)
  }
  
  func testFailingBinding(
    expected: TypeError,
    ast: AST.Binding,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, ast: ast, env: env, line: line)
  }
  
  func test<T: TypeCheckable>(
    expected: TCType,
    ast: T,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
    if res.isLeft {
      print(res.leftValue, "line: \(line)")
      XCTFail()
    } else {
      XCTAssertEqual(res.rightValue.1.type, expected)
      if res.rightValue.1.type != expected {
        print("line \(line)")
      }
    }
  }
  
  func testFailing<T: TypeCheckable>(
    expected: TypeError,
    ast: T,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
    if res.isLeft {
      XCTAssertEqual(res.leftValue.error, expected)
      if res.leftValue != expected {
        print("line \(line)")
      }
    } else {
      print("Expected", expected, "got", res.rightValue.1.type!, "line \(line)")
      XCTFail()
    }
  }
}

// MARK: - Helpers
extension TypeCheckerTests {
  func testExpr(
    expected: TCType,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, code: code, parser: Parser().expression, env: env, line: line)
  }
  
  func testFailingExpr(
    expected: TypeError,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, code: code, parser: Parser().expression, env: env, line: line)
  }
  
  func testPatt(
    expected: TCType,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, code: code, parser: Parser().pattern, env: env, line: line)
  }
  
  func testFailingPatt(
    expected: TypeError,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, code: code, parser: Parser().pattern, env: env, line: line)
  }
  
  func testType(
    expected: TCType,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, code: code, parser: Parser().type, env: env, line: line)
  }
  
  func testFailingType(
    expected: TypeError,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, code: code, parser: Parser().expression, env: env, line: line)
  }
  
  func testBinding(
    expected: TCType,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(expected: expected, code: code, parser: Parser().statement, env: env, line: line)
  }
  
  func testFailingBinding(
    expected: TypeError,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, code: code, parser: Parser().statement, env: env, line: line)
  }
  
  func testBitis(
    expected: [TCType],
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try Parser().bitis.run(sourceName: "test.bt", input: code)
      let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      } else {
        let types = res.rightValue.1.bindings.map { $0.type! }
        XCTAssertEqual(types, expected)
        if types != expected {
          print("line \(line)")
        }
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
  
  func testFailingBitis(
    expected: TypeError,
    code: String,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    testFailing(expected: expected, code: code, parser: Parser().bitis, env: env, line: line)
  }
  
  func test<T: TypeCheckable>(
    expected: TCType,
    code: String,
    parser: Lexer.Parser<T>,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      } else {
        XCTAssertEqual(res.rightValue.1.type, expected)
        if res.rightValue.1.type != expected {
          print("line \(line)")
        }
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
  
  func testFailing<T: TypeCheckable>(
    expected: TypeError,
    code: String,
    parser: Lexer.Parser<T>,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
      if res.isLeft {
        XCTAssertEqual(res.leftValue.error, expected)
        if res.leftValue != expected {
          print("line \(line)")
        }
      } else {
        res.rightValue.1.type.map { print("Expected", expected, "got", $0, "line \(line)") }
        XCTFail()
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
}

extension TypeCheckerTests {
  func testBinding(
    code: String,
    expected: TCAst.Binding,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: expected,
      parser: Parser().statement,
      env: env,
      line: line)
  }
  
  func testExpr(
    code: String,
    expected: TCAst.Expression,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: expected,
      parser: Parser().expression,
      env: env,
      line: line)
  }
  
  func testBitis(
    code: String,
    expected: TCAst,
    env: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: expected,
      parser: Parser().bitis,
      env: env,
      line: line)
  }

  func test<T: TypeCheckable & Equatable & PrettyStringConvertible>(
    code: String,
    expected: T,
    parser: Lexer.Parser<T>,
    env: Bitis.Env = .empty,
    line: Int
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast.typeChecker().runM((env, .init(), .empty, .init()))^
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      } else {
        XCTAssertEqual(res.rightValue.1, expected)
        if res.rightValue.1 != expected {
          print("line \(line)")
          print(
            expected.prettyDescription,
            "\n\n---\n\n",
            res.rightValue.1.prettyDescription.trimmingCharacters(in: .whitespaces)
          )
        }
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
}
