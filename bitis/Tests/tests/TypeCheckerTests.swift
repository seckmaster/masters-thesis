//
// Created by Toni K. Turk on 06/11/2020.
//

@testable import Bitis
import XCTest
import SwiftParsec
import Bow

class TypeCheckerTests: XCTestCase {
}

// MARK: - Bindings
extension TypeCheckerTests {
  func testVariablePrototype() {
    testBinding(
      expected: .int,
      code: "x :: Int")
    testBinding(
      expected: .arrow(arg: .int, ret: .bool),
      code: "x :: Int -> Bool")
    testBinding(
      expected: .list(element: .bool),
      code: "x :: [Bool]")
    testBinding(
      expected: .tuple(
        elements: [
          .bool,
          .arrow(arg: .char, ret: .float)]),
      code: "x :: (Bool, Char -> Float)")
  }
  
  func testPolymorphicPrototype() {
    testBinding(
      expected: .polymorphic(
        typeVariables: ["a", "b", "c"],
        context: .empty,
        type: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .arrow(
            arg: .typeVariable(name: "b"),
            ret: .typeVariable(name: "c")))),
      code: "f :: a -> b -> c")
    testBinding(
      expected: .polymorphic(
        typeVariables: ["a", "b", "c"],
        context: .empty,
        type: .tuple(
          elements: [
            .typeVariable(name: "a"),
            .typeVariable(name: "b"),
            .typeVariable(name: "c")])),
      code: "x :: (a, b, c)")
  }
  
  func testFunctionBindingWithPrototype() {
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .int),
        .arrow(arg: .int, ret: .int)
      ],
      code: """
f :: Int -> Int
f x = 10
""")
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))
      ],
      code: """
f :: Int -> Int -> Int
f = \\x -> \\y -> 10
""")
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
      ],
      code: """
f :: Int -> Int -> Int
f = \\x -> \\y -> x + y
""")
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
      ],
      code: """
f :: Int -> Int -> Int
f 5 = \\x -> 0
""")
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
      ],
      code: """
f :: Int -> Int -> Int
f 5 15 = 30
""")
    testBitis(
      expected: [
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
        .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int)),
      ],
      code: """
f :: Int -> Int -> Int
f _ _ = 0
""")
    testBitis(
      expected: [
        .arrow(arg: .reference(type: .list(element: .int)), ret: .unit),
        .arrow(arg: .reference(type: .list(element: .int)), ret: .unit),
      ],
      code: """
f :: &[Int] -> ()
f (x:xs) = ()
""")
    testBitis(
      expected: [
        .arrow(arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: nil)), lifetimeSpecifier: nil), ret: .unit),
        .arrow(arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: nil)), lifetimeSpecifier: nil), ret: .unit),
      ],
      code: """
f :: &[&Int] -> ()
f (x:xs) = ()
""")
    testBitis(
      expected: [
        .arrow(arg: .reference(type: .tuple(elements: [.int, .bool]), lifetimeSpecifier: nil), ret: .unit),
        .arrow(arg: .reference(type: .tuple(elements: [.int, .bool]), lifetimeSpecifier: nil), ret: .unit),
      ],
      code: """
f :: &(Int, Bool) -> ()
f (a, b) = ()
""")
    testBitis(
      expected: [
        .arrow(arg: .reference(type: .tuple(elements: [.reference(type: .int, lifetimeSpecifier: nil), .reference(type: .bool, lifetimeSpecifier: nil)]), lifetimeSpecifier: nil), ret: .unit),
        .arrow(arg: .reference(type: .tuple(elements: [.reference(type: .int, lifetimeSpecifier: nil), .reference(type: .bool, lifetimeSpecifier: nil)]), lifetimeSpecifier: nil), ret: .unit),
      ],
      code: """
f :: &(&Int, &Bool) -> ()
f (a, b) = ()
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: .arrow(arg: .list(element: .bool), ret: .int),
          rhs: .arrow(arg: .list(element: .bool), ret: "a").asPolymorphic)),
      code: """
f :: [Bool] -> a
f _ = 10
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: .arrow(arg: .list(element: .int), ret: .int),
          rhs: TCType.arrow(
            arg: .list(element: .typeVariable(name: "a")),
            ret: .typeVariable(name: "a")).asPolymorphic)),
      code: """
f :: [a] -> a
f (x:xs) = x
f (1:[]) = 1
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: TCType.int,
          rhs: .string)),
      code: """
f :: Int -> Int
f x = \"abc\"
""")
    testFailingBitis(
      expected: .patternMatching(.cannotMatch(
        pattern: .literal(position: .init(), literal: .logical(.true), tcType: .int),
        type: .int)),
      code: """
f :: Int -> Int
f true = 0
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: TCType.int,
          rhs: .arrow(arg: "a", ret: .int).asPolymorphic)),
      code: """
f :: Int -> Int
f x = \\a -> 10
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: TCType.unit,
          rhs: .int)),
      code: """
f :: Int -> Int
f x y = x + y
""")
    testFailingBitis(
      expected: .typeMismatch(
        .match(
          lhs: .arrow(arg: .list(element: .bool), ret: .bool),
          rhs: TCType.arrow(
            arg: .list(element: .typeVariable(name: "a")),
            ret: .bool).asPolymorphic)),
      code: """
g [] = true
g (x:xs) = x
f :: [a] -> Bool
f _ = true
f x = g x
f = \\x -> g x
""")
    
    // (Int, String) -> [Int] -> Int
    let type = TCType.arrow(
      arg: .tuple(elements: [.int, .string]),
      ret: .arrow(arg: .list(element: .int), ret: .int))
    testFailingBinding(
      expected: .typeMismatch(
        .match(
          lhs: TCType.unit,
          rhs: .int)),
      code: "f x y z = 10",
      env: .withBinding(name: "f", binding: .prototype(type)))
    testFailingBinding(
      expected: .patternMatching(.cannotMatch(
        pattern: .literal(
          position: .init(),
          literal: .logical(.true),
          tcType: .string),
        type: .string)),
      code: "f (10, true) y = 10",
      env: .withBinding(name: "f", binding: .prototype(type)))
    
    // f :: (Int, String) -> [Int] -> Int
    testBinding(
      expected: type,
      code:
        """
f (10, b) y = 10
f (_, b) [] = 1
f x (a:2:as) = 1
f (_, "abc") y = 2
""",
      env: .withBinding(name: "f", binding: .prototype(type)))
  }
  
  func testFunctionBindingWithoutPrototype() {
    testBinding(
      code: """
f a b = 20
f _ c = 0
f _ _ = 0
f _ e = e
f x y = x + y
""",
      expected: .binding(
        position: .init(),
        name: "f",
        cases: [
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .identifier(position: .init(), identifier: "a", tcType: .int),
              .identifier(position: .init(), identifier: "b", tcType: .int),
            ],
            body: .literal(position: .init(), literal: .int(20), tcType: .int),
            type: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .wildcard(position: .init(), tcType: .int),
              .identifier(position: .init(), identifier: "c", tcType: .int),
            ],
            body: .literal(position: .init(), literal: .int(0), tcType: .int),
            type: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .wildcard(position: .init(), tcType: .int),
              .wildcard(position: .init(), tcType: .int),
            ],
            body: .literal(position: .init(), literal: .int(0), tcType: .int),
            type: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .wildcard(position: .init(), tcType: .int),
              .identifier(position: .init(), identifier: "e", tcType: .int),
            ],
            body: .name(position: .init(), identifier: "e", tcType: .int),
            type: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .identifier(position: .init(), identifier: "x", tcType: .int),
              .identifier(position: .init(), identifier: "y", tcType: .int),
            ],
            body: .binary(.arithmetic(
              position: .init(),
              operator: .plus,
              left: .name(position: .init(), identifier: "x", tcType: .int),
              right: .name(position: .init(), identifier: "y", tcType: .int),
              tcType: .int)),
            type: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))),
        ],
        tcType: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))))
    // TODO: - Not yet supported by the compiler !!
    testBinding(
      code: """
f (10, b) y = 10
f (_, b) [] = 1
f x (a:2:as) = 1
f (_, "abc") y = 2
""",
      expected: .binding(
        position: .init(),
        name: "f",
        cases: [
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .tuple(
                position: .init(),
                patterns: [
                  .literal(position: .init(), literal: .int(10), tcType: .int),
                  .identifier(position: .init(), identifier: "b", tcType: .string)
                ],
                tcType: .tuple(elements: [.int, .string])),
              .identifier(position: .init(), identifier: "y", tcType: .list(element: .int))
            ],
            body: .literal(position: .init(), literal: .int(10), tcType: .int),
            type: .arrow(
              arg: .tuple(elements: [.int, .string]),
              ret: .arrow(
                arg: .list(element: .int),
                ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .tuple(
                position: .init(),
                patterns: [
                  .wildcard(position: .init(), tcType: .int),
                  .identifier(position: .init(), identifier: "b", tcType: .string)
                ],
                tcType: .tuple(elements: [.int, .string])),
              .list(.empty(position: .init(), tcType: .list(element: .int)))
            ],
            body: .literal(position: .init(), literal: .int(1), tcType: .int),
            type: .arrow(
              arg: .tuple(elements: [.int, .string]),
              ret: .arrow(
                arg: .list(element: .int),
                ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .identifier(position: .init(), identifier: "x", tcType: .tuple(elements: [.int, .string])),
              .list(.cons(
                position: .init(),
                head: .identifier(position: .init(), identifier: "a", tcType: .int),
                tail: .list(.cons(
                  position: .init(),
                  head: .literal(position: .init(), literal: .int(2), tcType: .int),
                  tail: .identifier(position: .init(), identifier: "as", tcType: .list(element: .int)),
                  tcType: .list(element: .int))),
                tcType: .list(element: .int)))
            ],
            body: .literal(position: .init(), literal: .int(1), tcType: .int),
            type: .arrow(
              arg: .tuple(elements: [.int, .string]),
              ret: .arrow(
                arg: .list(element: .int),
                ret: .int))),
          .init(
            name: "f",
            position: .init(),
            parameters: [
              .tuple(
                position: .init(),
                patterns: [
                  .wildcard(position: .init(), tcType: .int),
                  .literal(position: .init(), literal: .string("abc"), tcType: .string)
                ],
                tcType: .tuple(elements: [.int, .string])),
              .identifier(position: .init(), identifier: "y", tcType: .list(element: .int))
            ],
            body: .literal(position: .init(), literal: .int(2), tcType: .int),
            type: .arrow(
              arg: .tuple(elements: [.int, .string]),
              ret: .arrow(
                arg: .list(element: .int),
                ret: .int)))
        ],
        tcType: .arrow(
          arg: .tuple(elements: [.int, .string]),
          ret: .arrow(
            arg: .list(element: .int),
            ret: .int))))
    testBinding(
      code: """
g [] = true
g (x:xs) = x
""",
      expected: .binding(
        position: .init(),
        name: "g",
        cases: [
          .init(
            name: "g",
            position: .init(),
            parameters: [.list(.empty(position: .init(), tcType: .list(element: .bool)))],
            body: .literal(position: .init(), literal: .logical(.true), tcType: .bool),
            type: .arrow(arg: .list(element: .bool), ret: .bool)),
          .init(
            name: "g",
            position: .init(),
            parameters: [.list(.cons(
              position: .init(),
              head: .identifier(position: .init(), identifier: "x", tcType: .bool),
              tail: .identifier(position: .init(), identifier: "xs", tcType: .list(element: .bool)),
              tcType: .list(element: .bool)))],
            body: .name(position: .init(), identifier: "x", tcType: .bool),
            type: .arrow(arg: .list(element: .bool), ret: .bool)),
        ],
        tcType: .arrow(arg: .list(element: .bool), ret: .bool)))
    testBinding(
      code: "f g = g 0",
      expected: .binding(
        position: .init(),
        name: "f",
        cases: [
          .init(
            name: "f",
            position: .init(),
            parameters: [.identifier(position: .init(), identifier: "g", tcType: .arrow(arg: .int, ret: .typeVariable(name: "c")).asPolymorphic)],
            body: .binary(.application(
              position: .init(),
              fn: .name(position: .init(), identifier: "g", tcType: .arrow(arg: .int, ret: .typeVariable(name: "c")).asPolymorphic),
              arg: .literal(position: .init(), literal: .int(0), tcType: .int),
              tcType: .polymorphic(typeVariables: ["c"], context: .empty, type: .typeVariable(name: "c")))),
            type: .arrow(
              arg: .arrow(arg: .int, ret: .typeVariable(name: "c")),
              ret: .typeVariable(name: "c")).asPolymorphic)
        ],
        tcType: .arrow(
          arg: .arrow(arg: .int, ret: .typeVariable(name: "c")),
          ret: .typeVariable(name: "c")).asPolymorphic))
  }
  
  func testDataBinding() {
    testBinding(
      code: "data Bool = True | False",
      expected: .data(
        position: .init(),
        name: "Bool",
        params: .init(),
        constructors: [
          .init(position: .init(), name: "True", types: [], type: .unit),
          .init(position: .init(), name: "False", types: [], type: .unit)
        ],
        tcType: .data(
          name: "Bool", constructors: [
            .init(
              name: "True",
              type: .arrow(arg: .unit, ret: .typeConstructor(name: "Bool", types: []))),
            .init(
              name: "False",
              type: .arrow(arg: .unit, ret: .typeConstructor(name: "Bool", types: []))),
          ],
          genericParams: .init())))
    testBinding(
      code: "data Bool [a] = True a Int | False a",
      expected: .data(
        position: .init(),
        name: "Bool",
        params: .init(lifetimeParameters: [], typeParameters: ["a"]),
        constructors: [
          .init(
            position: .init(),
            name: "True",
            types: [
              .name(position: .init(), name: "a", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
              .name(position: .init(), name: "Int", tcType: .int),
            ],
            type: .polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(arg: .typeVariable(name: "a"), ret: .int))),
          .init(
            position: .init(),
            name: "False",
            types: [
              .name(position: .init(), name: "a", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
            ],
            type: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))
        ],
        tcType: .data(
          name: "Bool",
          constructors: [
            .init(
              name: "True",
              type: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .typeVariable(name: "a"),
                  ret: .arrow(arg: .int, ret: .typeConstructor(name: "Bool", types: [.typeVariable(name: "a")]))))),
            .init(
              name: "False",
              type: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .typeVariable(name: "a"),
                  ret: .typeConstructor(name: "Bool", types: [.typeVariable(name: "a")])))),
          ],
          genericParams: .init(lifetimeParameters: [], typeParameters: ["a"]))))
    testBinding(
      code: "data Bool ['A, a] = True a (&'A Int) | False a",
      expected: .data(
        position: .init(),
        name: "Bool",
        params: .init(lifetimeParameters: ["'A"], typeParameters: ["a"]),
        constructors: [
          .init(
            position: .init(),
            name: "True",
            types: [
              .name(position: .init(), name: "a", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
              .ref(
                position: .init(),
                type: .name(position: .init(), name: "Int", tcType: .int),
                lifetimeParameter: "'A",
                tcType: .reference(.int, lifetimeSpecifier: "'A")),
            ],
            type: .polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(arg: .typeVariable(name: "a"), ret: .reference(type: .int, lifetimeSpecifier: "'A")))),
          .init(
            position: .init(),
            name: "False",
            types: [
              .name(position: .init(), name: "a", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
            ],
            type: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))
        ],
        tcType: .data(
          name: "Bool",
          constructors: [
            .init(
              name: "True",
              type: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .typeVariable(name: "a"),
                  ret: .arrow(
                    arg: .reference(type: .int, lifetimeSpecifier: "'A"),
                    ret: .typeConstructor(name: "Bool", types: ["a"], lifetimeSpecifiers: ["'A"]))))),
            .init(
              name: "False",
              type: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .typeVariable(name: "a"),
                  ret: .typeConstructor(name: "Bool", types: ["a"], lifetimeSpecifiers: ["'A"])))),
          ],
          genericParams: .init(lifetimeParameters: ["'A"], typeParameters: ["a"]))))
    testBinding(
      code: "data List [a] = Cons (List a) | Nil",
      expected: .data(
        position: .init(),
        name: "List",
        params: .init(lifetimeParameters: [], typeParameters: ["a"]),
        constructors: [
          .init(
            position: .init(),
            name: "Cons",
            types: [
              .typeConstructor(
                position: .init(),
                name: "List",
                types: [.name(position: .init(), name: "a", tcType: .simple("a").asPolymorphic)], lifetimeSpecifiers: [],
                tcType: .simple(.typeConstructor(name: "List", types: ["a"])).asPolymorphic)
            ],
            type: .simple(.typeConstructor(name: "List", types: [.typeVariable(name: "a")])).asPolymorphic),
          .init(
            position: .init(),
            name: "Nil",
            types: [],
            type: .unit)
        ],
        tcType: .data(
          name: "List",
          constructors: [
            .init(
              name: "Cons",
              type: .arrow(
                arg: .typeConstructor(name: "List", types: ["a"]),
                ret: .typeConstructor(name: "List", types: ["a"])).asPolymorphic),
            .init(
              name: "Nil",
              type: .arrow(
                arg: .unit,
                ret: .typeConstructor(name: "List", types: ["a"])).asPolymorphic)
          ],
          genericParams: .init(lifetimeParameters: [], typeParameters: ["a"]))))
    
    testFailingBitis(
      expected: .unknownType(name: "Maybe"),
      code: "data A [a] = Cons (Maybe [a])")
    
    testFailingBitis(
      expected: .unknownTypeVariable("b"),
      code: """
      data List [a] = Cons b
      """)
  }
  
  func testDataBindingPrograms() {
    testBitis(
      code: """
data Maybe [a] = Just a | Nothing
a1 = Just 10
a2 = Nothing () -- <-- in the future '()' should be redundant
""",
      expected: .init(bindings: [
        .data(
          position: .init(),
          name: "Maybe",
          params: .init(lifetimeParameters: [], typeParameters: ["a"]),
          constructors: [
            .init(
              position: .init(),
              name: "Just",
              types: [.name(position: .init(), name: "a", tcType: "a")],
              type: .simple(.typeVariable(name: "a")).asPolymorphic),
            .init(
              position: .init(),
              name: "Nothing",
              types: [],
              type: .unit),
          ],
          tcType: .data(
            name: "Maybe",
            constructors: [
              .init(
                name: "Just",
                type: .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic),
              .init(
                name: "Nothing",
                type: .arrow(arg: .unit, ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic),
            ],
            genericParams: .init(lifetimeParameters: [], typeParameters: ["a"]))),
        .binding(
          position: .init(),
          name: "a1",
          cases: [
            .init(
              name: "a1",
              position: .init(),
              parameters: [],
              body: .binary(
                .application(
                  position: .init(),
                  fn: .name(position: .init(), identifier: "Just", tcType: .arrow(arg: .int, ret: .typeConstructor(name: "Maybe", types: [.int]))),
                  arg: .literal(position: .init(), literal: .int(10), tcType: .int),
                  tcType: .simple(.typeConstructor(name: "Maybe", types: [.int])))),
              type: .simple(.typeConstructor(name: "Maybe", types: [.int])))
          ],
          tcType: .simple(.typeConstructor(name: "Maybe", types: [.int]))),
        .binding(
          position: .init(),
          name: "a2",
          cases: [
            .init(
              name: "a2",
              position: .init(),
              parameters: [],
              body: .binary(
                .application(
                  position: .init(),
                  fn: .name(position: .init(), identifier: "Nothing", tcType: .arrow(arg: .unit, ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic),
                  arg: .tuple(position: .init(), expressions: [], tcType: .unit),
                  tcType: .simple(.typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic)),
              type: .simple(.typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic)
          ],
          tcType: .simple(.typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic),
      ]))
    testBitis(
      code: """
      data Pair = Cons Int String
      
      f :: (Pair []) -> Int
      f $(Cons a b) = a
      
      g :: &'A (Pair []) -> &'A Int
      g $(Cons a b) = a
      """,
      expected: .init(bindings: [
        .data(
          position: .init(),
          name: "Pair",
          params: .init(),
          constructors: [
            .init(
              position: .init(),
              name: "Cons",
              types: [
                .name(position: .init(), name: "Int", tcType: .int),
                .name(position: .init(), name: "String", tcType: .string),
              ],
              type: .arrow(arg: .int, ret: .string))
          ],
          tcType: .data(
            name: "Pair",
            constructors: [
              .init(
                name: "Cons",
                type: .arrow(arg: .int, ret: .arrow(arg: .string, ret: .typeConstructor(name: "Pair", types: []))))
            ],
            genericParams: .init())),
        .prototype(
          position: .init(),
          name: "f",
          type: .function(
            position: .init(),
            arg: .typeConstructor(
              position: .init(),
              name: "Pair",
              types: [],
              lifetimeSpecifiers: [],
              tcType: .simple(.typeConstructor(name: "Pair", types: []))),
            ret: .name(position: .init(), name: "Int", tcType: .int),
            tcType: .arrow(arg: .typeConstructor(name: "Pair", types: []), ret: .int)),
          tcType: .arrow(arg: .typeConstructor(name: "Pair", types: []), ret: .int)),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .deconstruct(
                  position: .init(),
                  constructor: "Cons",
                  patterns: [
                    .identifier(position: .init(), identifier: "a", tcType: .int),
                    .identifier(position: .init(), identifier: "b", tcType: .string),
                  ],
                  tcType: .simple(.typeConstructor(name: "Pair", types: [])))
              ],
              body: .name(position: .init(), identifier: "a", tcType: .int),
              type: .arrow(arg: .typeConstructor(name: "Pair", types: []), ret: .int))
          ],
          tcType: .arrow(arg: .typeConstructor(name: "Pair", types: []), ret: .int)),
        .prototype(
          position: .init(),
          name: "g",
          type: .function(
            position: .init(),
            arg: .ref(
              position: .init(),
              type: .typeConstructor(
                position: .init(),
                name: "Pair",
                types: [],
                lifetimeSpecifiers: [],
                tcType: .simple(.typeConstructor(name: "Pair", types: []))),
              lifetimeParameter: "'A",
              tcType: .reference(
                .typeConstructor(name: "Pair", types: []),
                lifetimeSpecifier: "'A")),
            ret: .ref(
              position: .init(),
              type: .name(position: .init(), name: "Int", tcType: .int),
              lifetimeParameter: "'A",
              tcType: .reference(.int, lifetimeSpecifier: "'A")),
            tcType: .arrow(
              arg: .reference(type: .typeConstructor(name: "Pair", types: []), lifetimeSpecifier: "'A"),
              ret: .reference(type: .int, lifetimeSpecifier: "'A"))),
          tcType: .arrow(
            arg: .reference(type: .typeConstructor(name: "Pair", types: []), lifetimeSpecifier: "'A"),
            ret: .reference(type: .int, lifetimeSpecifier: "'A"))),
        .binding(
          position: .init(),
          name: "g",
          cases: [
            .init(
              name: "g",
              position: .init(),
              parameters: [
                .deconstruct(
                  position: .init(),
                  constructor: "Cons",
                  patterns: [
                    .identifier(position: .init(), identifier: "a", tcType: .reference(.int, lifetimeSpecifier: "'A")),
                    .identifier(position: .init(), identifier: "b", tcType: .reference(.string, lifetimeSpecifier: "'A")),
                  ],
                  tcType: .reference(.typeConstructor(name: "Pair", types: []), lifetimeSpecifier: "'A"))
              ],
              body: .name(position: .init(), identifier: "a", tcType: .reference(.int, lifetimeSpecifier: "'A")),
              type: .arrow(
                arg: .reference(type: .typeConstructor(name: "Pair", types: []), lifetimeSpecifier: "'A"),
                ret: .reference(type: .int, lifetimeSpecifier: "'A")))
          ],
          tcType: .arrow(
            arg: .reference(type: .typeConstructor(name: "Pair", types: []), lifetimeSpecifier: "'A"),
            ret: .reference(type: .int, lifetimeSpecifier: "'A"))),
      ]))
    testBitis(
      code: """
      data List [a] = Cons a (List a) | Nil
      x = Cons 10 (Nil ())
      y = Cons "abc" (Nil ())
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .data(
            position: .init(),
            name: "List",
            params: .init(lifetimeParameters: [], typeParameters: ["a"]),
            constructors: [
              .init(
                position: .init(),
                name: "Cons",
                types: [
                  .name(
                    position: .init(),
                    name: "a",
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeVariable(name: "a"))),
                  .typeConstructor(
                    position: .init(),
                    name: "List",
                    types: [
                      .name(
                        position: .init(),
                        name: "a",
                        lifetimeParameter: nil,
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeVariable(name: "a")))
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil)))
                ],
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .typeVariable(name: "a"),
                    ret: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
              .init(
                position: .init(),
                name: "Nil",
                types: [
                  
                ],
                type: .simple(.unit))
            ],
            tcType: .data(
              name: "List",
              constructors: [
                .init(
                  name: "Cons",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .typeVariable(name: "a"),
                      ret: .arrow(
                        arg: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        ret: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil))),
                .init(
                  name: "Nil",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .unit,
                      ret: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil)))
              ],
              genericParams: .init(lifetimeParameters: [], typeParameters: ["a"]))),
          .binding(
            position: .init(),
            name: "x",
            cases: [
              .init(
                name: "x",
                position: .init(),
                parameters: [],
                body: .binary(
                  .application(
                    position: .init(),
                    fn: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "Cons",
                          tcType: .simple(.arrow(
                            arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            ret: .arrow(
                              arg: .typeConstructor(
                                name: "List",
                                types: [
                                  .atomic(.numeric(.int), lifetimeSpecifier: nil)
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              ret: .typeConstructor(
                                name: "List",
                                types: [
                                  .atomic(.numeric(.int), lifetimeSpecifier: nil)
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .literal(
                          position: .init(),
                          literal: .int(10),
                          tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil),
                        tcType: .simple(.arrow(
                          arg: .typeConstructor(
                            name: "List",
                            types: [
                              .atomic(.numeric(.int), lifetimeSpecifier: nil)
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          ret: .typeConstructor(
                            name: "List",
                            types: [
                              .atomic(.numeric(.int), lifetimeSpecifier: nil)
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    arg: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "Nil",
                          tcType: .simple(.arrow(
                            arg: .unit,
                            ret: .typeConstructor(
                              name: "List",
                              types: [
                                .atomic(.numeric(.int), lifetimeSpecifier: nil)
                              ],
                              lifetimeSpecifiers: [],
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .tuple(position: .init(), expressions: [], tcType: .unit),
                        tcType: .simple(.typeConstructor(
                          name: "List",
                          types: [
                            .atomic(.numeric(.int), lifetimeSpecifier: nil)
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    tcType: .simple(.typeConstructor(
                      name: "List",
                      types: [
                        .atomic(.numeric(.int), lifetimeSpecifier: nil)
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.typeConstructor(
                  name: "List",
                  types: [
                    .atomic(.numeric(.int), lifetimeSpecifier: nil)
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType:
                .simple(.typeConstructor(
                  name: "List",
                  types: [
                    .atomic(.numeric(.int), lifetimeSpecifier: nil)
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "y",
            cases: [
              .init(
                name: "y",
                position: .init(),
                parameters: [],
                body: .binary(
                  .application(
                    position: .init(),
                    fn: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "Cons",
                          tcType: .simple(.arrow(
                            arg: .atomic(.string, lifetimeSpecifier: nil),
                            ret: .arrow(
                              arg: .typeConstructor(
                                name: "List",
                                types: [
                                  .atomic(.string, lifetimeSpecifier: nil)
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              ret: .typeConstructor(
                                name: "List",
                                types: [
                                  .atomic(.string, lifetimeSpecifier: nil)
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .literal(
                          position: .init(),
                          literal: .string("abc"),
                          tcType: .simple(.atomic(.string, lifetimeSpecifier: nil)),
                          lifetime: nil),
                        tcType: .simple(.arrow(
                          arg: .typeConstructor(
                            name: "List",
                            types: [
                              .atomic(.string, lifetimeSpecifier: nil)
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          ret: .typeConstructor(
                            name: "List",
                            types: [
                              .atomic(.string, lifetimeSpecifier: nil)
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    arg: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "Nil",
                          tcType: .simple(.arrow(
                            arg: .unit,
                            ret: .typeConstructor(
                              name: "List",
                              types: [
                                .atomic(.string, lifetimeSpecifier: nil)
                              ],
                              lifetimeSpecifiers: [],
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .tuple(position: .init(), expressions: [], tcType: .unit),
                        tcType: .simple(.typeConstructor(
                          name: "List",
                          types: [
                            .atomic(.string, lifetimeSpecifier: nil)
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    tcType: .simple(.typeConstructor(
                      name: "List",
                      types: [
                        .atomic(.string, lifetimeSpecifier: nil)
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.typeConstructor(
                  name: "List",
                  types: [
                    .atomic(.string, lifetimeSpecifier: nil)
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.typeConstructor(
              name: "List",
              types: [
                .atomic(.string, lifetimeSpecifier: nil)
              ],
              lifetimeSpecifiers: [],
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testBitis(
      code: """
      data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef
      
      append :: (ListRef ['A, a]) -> (ListRef ['A, a]) -> (ListRef ['A, a])
      append $(NilRef _) ys = ys
      append $(ConsRef x xs) ys = ConsRef x (append xs ys)
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .data(
            position: .init(),
            name: "ListRef",
            params: .init(lifetimeParameters: ["'A"], typeParameters: ["a"]),
            constructors: [
              .init(
                position: .init(),
                name: "ConsRef",
                types: [
                  .ref(
                    position: .init(),
                    type: .name(
                      position: .init(),
                      name: "a",
                      lifetimeParameter: nil,
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .typeVariable(name: "a"))),
                    lifetimeParameter: "'A",
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .reference(
                        type: .typeVariable(name: "a"),
                        lifetimeSpecifier: "'A"))),
                  .typeConstructor(
                    position: .init(),
                    name: "ListRef",
                    types: [
                      .name(
                        position: .init(),
                        name: "a",
                        lifetimeParameter: nil,
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeVariable(name: "a")))
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)))
                ],
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .reference(
                      type: .typeVariable(name: "a"),
                      lifetimeSpecifier: "'A"),
                    ret: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
              .init(
                position: .init(),
                name: "NilRef",
                types: [
                  
                ],
                type: .simple(.unit))
            ],
            tcType: .data(
              name: "ListRef",
              constructors: [
                .init(
                  name: "ConsRef",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .reference(
                        type: .typeVariable(name: "a"),
                        lifetimeSpecifier: "'A"),
                      ret: .arrow(
                        arg: .typeConstructor(
                          name: "ListRef",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: ["'A"],
                          lifetimeSpecifier: nil),
                        ret: .typeConstructor(
                          name: "ListRef",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: ["'A"],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil))),
                .init(
                  name: "NilRef",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .unit,
                      ret: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil)))
              ],
              genericParams: .init(lifetimeParameters: ["'A"], typeParameters: ["a"]))),
          .prototype(
            position: .init(),
            name: "append",
            type: .function(
              position: .init(),
              arg: .typeConstructor(
                position: .init(),
                name: "ListRef",
                types: [
                  .name(
                    position: .init(),
                    name: "a",
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeVariable(name: "a")))
                ],
                lifetimeSpecifiers: ["'A"],
                lifetimeParameter: nil,
                tcType: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil))),
              ret: .function(
                position: .init(),
                arg: .typeConstructor(
                  position: .init(),
                  name: "ListRef",
                  types: [
                    .name(
                      position: .init(),
                      name: "a",
                      lifetimeParameter: nil,
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .typeVariable(name: "a")))
                  ],
                  lifetimeSpecifiers: ["'A"],
                  lifetimeParameter: nil,
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil))),
                ret: .typeConstructor(
                  position: .init(),
                  name: "ListRef",
                  types: [
                    .name(
                      position: .init(),
                      name: "a",
                      lifetimeParameter: nil,
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .typeVariable(name: "a")))
                  ],
                  lifetimeSpecifiers: ["'A"],
                  lifetimeParameter: nil,
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil))),
                lifetimeParameter: nil,
                tcType: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    ret: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    ret: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
            tcType: .polymorphic(
              typeVariables: ["a"],
              context: .empty,
              type: .arrow(
                arg: .typeConstructor(
                  name: "ListRef",
                  types: [
                    .typeVariable(name: "a")
                  ],
                  lifetimeSpecifiers: ["'A"],
                  lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil),
                  ret: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "append",
            cases: [
              .init(
                name: "append",
                position: .init(),
                parameters: [
                  .deconstruct(
                    position: .init(),
                    constructor: "NilRef",
                    patterns: [
                      .wildcard(
                        position: .init(),
                        tcType: .simple(.unit))
                    ],
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "ys",
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .name(
                  position: .init(),
                  identifier: "ys",
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil)),
                  lifetime: nil
                ),
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    ret: .arrow(
                      arg: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil),
                      ret: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "append",
                position: .init(),
                parameters: [
                  .deconstruct(
                    position: .init(),
                    constructor: "ConsRef",
                    patterns: [
                      .identifier(
                        position: .init(),
                        identifier: "x",
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .reference(
                            type: .typeVariable(name: "a"),
                            lifetimeSpecifier: "'A")),
                        lifetime: nil),
                      .identifier(
                        position: .init(),
                        identifier: "xs",
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeConstructor(
                            name: "ListRef",
                            types: [
                              .typeVariable(name: "a")
                            ],
                            lifetimeSpecifiers: ["'A"],
                            lifetimeSpecifier: nil)),
                        lifetime: nil)
                    ],
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "ys",
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .application(
                    position: .init(),
                    fn: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "ConsRef",
                          tcType: .polymorphic(
                            typeVariables: ["a"],
                            context: .empty,
                            type: .arrow(
                              arg: .reference(
                                type: .typeVariable(name: "a"),
                                lifetimeSpecifier: "'A"),
                              ret: .arrow(
                                arg: .typeConstructor(
                                  name: "ListRef",
                                  types: [
                                    .typeVariable(name: "a")
                                  ],
                                  lifetimeSpecifiers: ["'A"],
                                  lifetimeSpecifier: nil),
                                ret: .typeConstructor(
                                  name: "ListRef",
                                  types: [
                                    .typeVariable(name: "a")
                                  ],
                                  lifetimeSpecifiers: ["'A"],
                                  lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .name(
                          position: .init(),
                          identifier: "x",
                          tcType: .polymorphic(
                            typeVariables: ["a"],
                            context: .empty,
                            type: .reference(
                              type: .typeVariable(name: "a"),
                              lifetimeSpecifier: "'A")),
                          lifetime: nil
                        ),
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .arrow(
                            arg: .typeConstructor(
                              name: "ListRef",
                              types: [
                                .typeVariable(name: "a")
                              ],
                              lifetimeSpecifiers: ["'A"],
                              lifetimeSpecifier: nil),
                            ret: .typeConstructor(
                              name: "ListRef",
                              types: [
                                .typeVariable(name: "a")
                              ],
                              lifetimeSpecifiers: ["'A"],
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    arg: .binary(
                      .application(
                        position: .init(),
                        fn: .binary(
                          .application(
                            position: .init(),
                            fn: .name(
                              position: .init(),
                              identifier: "append",
                              tcType: .polymorphic(
                                typeVariables: ["a"],
                                context: .empty,
                                type: .arrow(
                                  arg: .typeConstructor(
                                    name: "ListRef",
                                    types: [
                                      .typeVariable(name: "a")
                                    ],
                                    lifetimeSpecifiers: ["'A"],
                                    lifetimeSpecifier: nil),
                                  ret: .arrow(
                                    arg: .typeConstructor(
                                      name: "ListRef",
                                      types: [
                                        .typeVariable(name: "a")
                                      ],
                                      lifetimeSpecifiers: ["'A"],
                                      lifetimeSpecifier: nil),
                                    ret: .typeConstructor(
                                      name: "ListRef",
                                      types: [
                                        .typeVariable(name: "a")
                                      ],
                                      lifetimeSpecifiers: ["'A"],
                                      lifetimeSpecifier: nil),
                                    lifetimeSpecifier: nil),
                                  lifetimeSpecifier: nil)),
                              lifetime: nil
                            ),
                            arg: .name(
                              position: .init(),
                              identifier: "xs",
                              tcType: .polymorphic(
                                typeVariables: ["a"],
                                context: .empty,
                                type: .typeConstructor(
                                  name: "ListRef",
                                  types: [
                                    .typeVariable(name: "a")
                                  ],
                                  lifetimeSpecifiers: ["'A"],
                                  lifetimeSpecifier: nil)),
                              lifetime: nil
                            ),
                            tcType: .polymorphic(
                              typeVariables: ["a"],
                              context: .empty,
                              type: .arrow(
                                arg: .typeConstructor(
                                  name: "ListRef",
                                  types: [
                                    .typeVariable(name: "a")
                                  ],
                                  lifetimeSpecifiers: ["'A"],
                                  lifetimeSpecifier: nil),
                                ret: .typeConstructor(
                                  name: "ListRef",
                                  types: [
                                    .typeVariable(name: "a")
                                  ],
                                  lifetimeSpecifiers: ["'A"],
                                  lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil)),
                            lifetime: nil)),
                        arg: .name(
                          position: .init(),
                          identifier: "ys",
                          tcType: .polymorphic(
                            typeVariables: ["a"],
                            context: .empty,
                            type: .typeConstructor(
                              name: "ListRef",
                              types: [
                                .typeVariable(name: "a")
                              ],
                              lifetimeSpecifiers: ["'A"],
                              lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeConstructor(
                            name: "ListRef",
                            types: [
                              .typeVariable(name: "a")
                            ],
                            lifetimeSpecifiers: ["'A"],
                            lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .typeConstructor(
                      name: "ListRef",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: ["'A"],
                      lifetimeSpecifier: nil),
                    ret: .arrow(
                      arg: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil),
                      ret: .typeConstructor(
                        name: "ListRef",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: ["'A"],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .polymorphic(
              typeVariables: ["a"],
              context: .empty,
              type: .arrow(
                arg: .typeConstructor(
                  name: "ListRef",
                  types: [
                    .typeVariable(name: "a")
                  ],
                  lifetimeSpecifiers: ["'A"],
                  lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil),
                  ret: .typeConstructor(
                    name: "ListRef",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: ["'A"],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    
    // TODO: - not working yet !!
    testBitis(
      code: """
      data Either [a, b] = L a | R b
      -- f :: (Either [Int, Int]) -> Int -- it works with prototype
      f $(L 10) = 10
      f $(R 20) = 20
      """,
      expected: .init(bindings: []))
    
    testFailingBitis(
      expected: .unknownType(name: "Maybe"),
      code: "data List = Cons (Maybe Int)")
    
    testFailingBitis(
      expected: .typeConstructor(.genericTypes(expected: 2, got: 1)),
      code: """
      data Either [a, b] = L a | R b
      f :: (Either [Int]) -> Int
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericTypes(expected: 1, got: 0)),
      code: """
      data List [a] = Cons a (List [])
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericTypes(expected: 1, got: 2)),
      code: """
      data Either [a] = L a | R a
      f :: (Either [Int, Bool]) -> Int
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericTypes(expected: 0, got: 1)),
      code: """
      data Either [] = L Int | R Bool
      f :: (Either [Int]) -> Int
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericLifetimes(expected: 1, got: 0)),
      code: """
      data List ['A] = Ref &'A Int Bool
      f :: (List []) -> Int
      f xs = 10
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericLifetimes(expected: 0, got: 1)),
      code: """
      data List [] = Ref Int Bool
      f :: (List ['A]) -> Int
      f xs = 10
      """)
    
    testFailingBitis(
      expected: .typeConstructor(.genericLifetimes(expected: 0, got: 1)),
      code: """
      data List ['A, a] = Cons &'A a
      x :: (List ['A, Int])
      """)
    
    testFailingBitis(
      expected: .typeMismatch(.match(
        lhs: .simple(.typeConstructor(name: "List", types: [.int])),
        rhs: .simple(.typeConstructor(name: "List", types: [.string])))),
      code: """
      data List [a] = Cons a (List a) | Nil
      x :: (List Int)
      x = Cons "abc" (Nil ())
      """)
  }
  
  func testPrograms() {
    testBitis(
      code: """
fmap _ [] = []
fmap f (x:xs) = [f x]
x = fmap (\\x -> x * 2) []
y = fmap (\\x -> x) [1,2,3]
""",
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .binding(
            position: .init(),
            name: "fmap",
            cases: [
              .init(
                name: "fmap",
                position: .init(),
                parameters: [
                  .wildcard(
                    position: .init(),
                    tcType: .polymorphic(
                      typeVariables: ["b", "c"],
                      context: .empty,
                      type: .arrow(
                        arg: .typeVariable(name: "b"),
                        ret: .typeVariable(name: "c"),
                        lifetimeSpecifier: nil))),
                  .list(.empty(
                    position: .init(),
                    tcType: .polymorphic(
                      typeVariables: ["b"],
                      context: .empty,
                      type: .list(
                        element: .typeVariable(name: "b"),
                        lifetimeSpecifier: nil)),
                    lifetime: nil))
                ],
                body: .list(
                  position: .init(),
                  expressions: [],
                  tcType: .polymorphic(
                    typeVariables: ["c"],
                    context: .empty,
                    type: .list(
                      element: .typeVariable(name: "c"),
                      lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .polymorphic(
                  typeVariables: ["b", "c"],
                  context: .empty,
                  type: .arrow(
                    arg: .arrow(
                      arg: .typeVariable(name: "b"),
                      ret: .typeVariable(name: "c"),
                      lifetimeSpecifier: nil),
                    ret: .arrow(
                      arg: .list(
                        element: .typeVariable(name: "b"),
                        lifetimeSpecifier: nil),
                      ret: .list(
                        element: .typeVariable(name: "c"),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "fmap",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "f",
                    tcType: .polymorphic(
                      typeVariables: ["c", "b"],
                      context: .empty,
                      type: .arrow(
                        arg: .typeVariable(name: "b"),
                        ret: .typeVariable(name: "c"),
                        lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .list(
                    .cons(
                      position: .init(),
                      head: .identifier(
                        position: .init(),
                        identifier: "x",
                        tcType: .polymorphic(
                          typeVariables: ["b"],
                          context: .empty,
                          type: .typeVariable(name: "b")),
                        lifetime: nil),
                      tail: .identifier(
                        position: .init(),
                        identifier: "xs",
                        tcType: .polymorphic(
                          typeVariables: ["b"],
                          context: .empty,
                          type: .list(
                            element: .typeVariable(name: "b"),
                            lifetimeSpecifier: nil)),
                        lifetime: nil),
                      tcType: .polymorphic(
                        typeVariables: ["b"],
                        context: .empty,
                        type: .list(
                          element: .typeVariable(name: "b"),
                          lifetimeSpecifier: nil)),
                      lifetime: nil))
                ],
                body: .list(
                  position: .init(),
                  expressions: [
                    .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "f",
                          tcType: .polymorphic(
                            typeVariables: ["c", "b"],
                            context: .empty,
                            type: .arrow(
                              arg: .typeVariable(name: "b"),
                              ret: .typeVariable(name: "c"),
                              lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .name(
                          position: .init(),
                          identifier: "x",
                          tcType: .polymorphic(
                            typeVariables: ["b"],
                            context: .empty,
                            type: .typeVariable(name: "b")),
                          lifetime: nil
                        ),
                        tcType: .polymorphic(
                          typeVariables: ["c"],
                          context: .empty,
                          type: .typeVariable(name: "c")),
                        lifetime: nil))
                  ],
                  tcType: .polymorphic(
                    typeVariables: ["c"],
                    context: .empty,
                    type: .list(
                      element: .typeVariable(name: "c"),
                      lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .polymorphic(
                  typeVariables: ["c", "b"],
                  context: .empty,
                  type: .arrow(
                    arg: .arrow(
                      arg: .typeVariable(name: "b"),
                      ret: .typeVariable(name: "c"),
                      lifetimeSpecifier: nil),
                    ret: .arrow(
                      arg: .list(
                        element: .typeVariable(name: "b"),
                        lifetimeSpecifier: nil),
                      ret: .list(
                        element: .typeVariable(name: "c"),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .polymorphic(
              typeVariables: ["b", "c"],
              context: .empty,
              type: .arrow(
                arg: .arrow(
                  arg: .typeVariable(name: "b"),
                  ret: .typeVariable(name: "c"),
                  lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .list(
                    element: .typeVariable(name: "b"),
                    lifetimeSpecifier: nil),
                  ret: .list(
                    element: .typeVariable(name: "c"),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "x",
            cases: [
              .init(
                name: "x",
                position: .init(),
                parameters: [],
                body: .binary(
                  .application(
                    position: .init(),
                    fn: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "fmap",
                          tcType: .simple(.arrow(
                            arg: .arrow(
                              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            ret: .arrow(
                              arg: .list(
                                element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              ret: .list(
                                element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .lambda(
                          position: .init(),
                          parameter: .identifier(
                            position: .init(),
                            identifier: "x",
                            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                            lifetime: nil),
                          body: .binary(
                            .arithmetic(
                              position: .init(),
                              operator: .mul,
                              left: .name(
                                position: .init(),
                                identifier: "x",
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                lifetime: nil
                              ),
                              right: .literal(
                                position: .init(),
                                literal: .int(2),
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                lifetime: nil),
                              tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                              lifetime: nil)),
                          tcType: .simple(.arrow(
                            arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil),
                        tcType: .simple(.arrow(
                          arg: .list(
                            element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil),
                          ret: .list(
                            element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    arg: .list(
                      position: .init(),
                      expressions: [
                        
                      ],
                      tcType: .simple(.list(
                        element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil)),
                      lifetime: nil),
                    tcType: .simple(.list(
                      element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType:
                .simple(.list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "y",
            cases: [
              .init(
                name: "y",
                position: .init(),
                parameters: [],
                body: .binary(
                  .application(
                    position: .init(),
                    fn: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "fmap",
                          tcType: .simple(.arrow(
                            arg: .arrow(
                              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            ret: .arrow(
                              arg: .list(
                                element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              ret: .list(
                                element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .lambda(
                          position: .init(),
                          parameter: .identifier(
                            position: .init(),
                            identifier: "x",
                            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                            lifetime: nil),
                          body: .name(
                            position: .init(),
                            identifier: "x",
                            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                            lifetime: nil
                          ),
                          tcType: .simple(.arrow(
                            arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil)),
                          lifetime: nil),
                        tcType: .simple(.arrow(
                          arg: .list(
                            element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil),
                          ret: .list(
                            element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    arg: .list(
                      position: .init(),
                      expressions: [
                        .literal(
                          position: .init(),
                          literal: .int(1),
                          tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil),
                        .literal(
                          position: .init(),
                          literal: .int(2),
                          tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil),
                        .literal(
                          position: .init(),
                          literal: .int(3),
                          tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil)
                      ],
                      tcType: .simple(.list(
                        element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil)),
                      lifetime: nil),
                    tcType: .simple(.list(
                      element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType:
                .simple(.list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    /**
     x = f (\\() -> \"abc\")
     y = f (\\() -> 10)
     ^ this actually works
     */
    // TODO: - Fix (the issue comes from the "specific type" logic
    testBitis(
      code: """
f :: (() -> a) -> a
f g = g ()
x = f (\\_ -> \"abc\")
y = f (\\_ -> 10)
-- z = f (\\() -> \\x -> ())
""",
      expected: .init(bindings: [
        .prototype(
          position: .init(),
          name: "f",
          type: .function(
            position: .init(),
            arg: .function(
              position: .init(),
              arg: .tuple(position: .init(), types: [], tcType: .unit),
              ret: .name(position: .init(), name: "a", tcType: "a"),
              tcType: TCType.arrow(arg: .unit, ret: "a").asPolymorphic),
            ret: .name(position: .init(), name: "a", tcType: "a"),
            tcType: TCType.arrow(arg: .arrow(arg: .unit, ret: "a"), ret: "a").asPolymorphic),
          tcType: TCType.arrow(arg: .arrow(arg: .unit, ret: "a"), ret: "a").asPolymorphic),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [.identifier(position: .init(), identifier: "g", tcType: TCType.arrow(arg: .unit, ret: "a").asPolymorphic)],
              body: .binary(.application(
                position: .init(),
                fn: .name(position: .init(), identifier: "g", tcType: TCType.arrow(arg: .unit, ret: "a").asPolymorphic),
                arg: .tuple(position: .init(), expressions: [], tcType: .unit),
                tcType: TCType.simple("a").asPolymorphic)),
              type: TCType.arrow(arg: .arrow(arg: .unit, ret: "a"), ret: "a").asPolymorphic)
          ],
          tcType: TCType.arrow(arg: .arrow(arg: .unit, ret: "a"), ret: "a").asPolymorphic),
        .binding(
          position: .init(),
          name: "x",
          cases: [
            .init(
              name: "x",
              position: .init(),
              parameters: [],
              body: .binary(.application(
                position: .init(),
                fn: .name(position: .init(), identifier: "f", tcType: .arrow(arg: .arrow(arg: .unit, ret: .string), ret: .string)),
                arg: .lambda(
                  position: .init(),
                  parameter: .wildcard(position: .init(), tcType: .unit),
                  body: .literal(position: .init(), literal: .string("abc"), tcType: .string),
                  tcType: .arrow(arg: .unit, ret: .string)),
                tcType: .string)),
              type: .string)
          ],
          tcType: .string),
        .binding(
          position: .init(),
          name: "y",
          cases: [
            .init(
              name: "y",
              position: .init(),
              parameters: [],
              body: .binary(.application(
                position: .init(),
                fn: .name(position: .init(), identifier: "f", tcType: .arrow(arg: .arrow(arg: .unit, ret: .int), ret: .int)),
                arg: .lambda(
                  position: .init(),
                  parameter: .wildcard(position: .init(), tcType: .unit),
                  body: .literal(position: .init(), literal: .int(10), tcType: .int),
                  tcType: .arrow(arg: .unit, ret: .int)),
                tcType: .int)),
              type: .int)
          ],
          tcType: .int),
        //        .binding(
        //          position: .init(),
        //          name: "z",
        //          cases: [
        //            .init(
        //              position: .init(),
        //              parameters: [],
        //              body: .binary(.application(
        //                              position: .init(),
        //                              fn: .name(
        //                                position: .init(),
        //                                identifier: "f",
        //                                tcType: TCType.arrow(
        //                                  arg: .arrow(arg: .unit, ret: .arrow(arg: "g", ret: .unit)),
        //                                  ret: .arrow(arg: "g", ret: .unit)).asPolymorphic),
        //                              arg: .lambda(
        //                                position: .init(),
        //                                parameter: .tuple(position: .init(), patterns: [], tcType: .unit),
        //                                body: .lambda(
        //                                  position: .init(),
        //                                  parameter: .identifier(position: .init(), identifier: "x", tcType: TCType.simple("g").asPolymorphic),
        //                                  body: .tuple(position: .init(), expressions: [], tcType: .unit),
        //                                  tcType: TCType.arrow(arg: "g", ret: .unit).asPolymorphic),
        //                                tcType: TCType.arrow(arg: .unit, ret: .arrow(arg: "g", ret: .unit)).asPolymorphic),
        //                              tcType: TCType.arrow(arg: "g", ret: .unit).asPolymorphic)),
        //              type: TCType.arrow(arg: "g", ret: .unit).asPolymorphic)
        //          ],
        //          tcType: TCType.arrow(arg: "g", ret: .unit).asPolymorphic),
      ]))
    // TODO: - Fix (typeVariable cannot be matched into a reference)
    testBitis(
      code: """
      h :: &a -> ()
      f (x:xs) = h x
      """,
      expected: .init(bindings: [
        .prototype(
          position: .init(),
          name: "h",
          type: .function(
            position: .init(),
            arg: .ref(
              position: .init(),
              type: .name(position: .init(), name: "a", tcType: .simple("a").asPolymorphic),
              lifetimeParameter: nil,
              tcType: .reference("a").asPolymorphic),
            ret: .tuple(
              position: .init(),
              types: [],
              tcType: .unit),
            tcType: .arrow(arg: .reference(type: "a"), ret: .unit).asPolymorphic),
          tcType: .arrow(arg: .reference(type: "a"), ret: .unit).asPolymorphic),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .list(
                  .cons(
                    position: .init(),
                    head: .identifier(position: .init(), identifier: "x", tcType: .reference("a").asPolymorphic),
                    tail: .identifier(position: .init(), identifier: "xs", tcType: .list(element: .reference(type: "a", lifetimeSpecifier: nil)).asPolymorphic),
                    tcType: .list(element: .reference(type: "a")).asPolymorphic))
              ],
              body: .binary(
                .application(
                  position: .init(),
                  fn: .name(position: .init(), identifier: "h", tcType: .arrow(arg: .reference(type: "a"), ret: .unit).asPolymorphic),
                  arg: .name(
                    position: .init(),
                    identifier: "x",
                    tcType: .reference("a").asPolymorphic),
                  tcType: .unit)),
              type: .arrow(arg: .list(element: .reference(type: "a")), ret: .unit).asPolymorphic)
          ],
          tcType: .arrow(arg: .list(element: .reference(type: "a")), ret: .unit).asPolymorphic)
      ])
    )
    testBitis(
      code: """
      f :: &'A [&'A Int] -> ()
      f (x:xs) = ()
      """,
      expected: .init(bindings: [
        .prototype(
          position: .init(), name: "f", type: .function(
            position: .init(),
            arg: .ref(
              position: .init(),
              type: .list(
                position: .init(),
                element: .ref(
                  position: .init(),
                  type: .name(position: .init(), name: "Int", tcType: .int),
                  lifetimeParameter: "'A",
                  tcType: .reference(.int, lifetimeSpecifier: "'A")),
                tcType: .list(element: .reference(type: .int, lifetimeSpecifier: "'A"))),
              lifetimeParameter: "'A",
              tcType: .reference(.list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A")),
            ret: .tuple(position: .init(), types: [], tcType: .unit),
            tcType: .arrow(
              arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A"),
              ret: .unit)),
          tcType: .arrow(
            arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A"),
            ret: .unit)),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .list(.cons(
                  position: .init(),
                  head: .identifier(
                    position: .init(),
                    identifier: "x",
                    tcType: .reference(.reference(type: .int, lifetimeSpecifier: "'A"), lifetimeSpecifier: "'A")),
                  tail: .identifier(
                    position: .init(),
                    identifier: "xs",
                    tcType: .reference(.list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A")),
                  tcType: .reference(.list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A")))
              ],
              body: .tuple(position: .init(), expressions: [], tcType: .unit),
              type: .arrow(
                arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A"),
                ret: .unit))
          ],
          tcType: .arrow(
            arg: .reference(type: .list(element: .reference(type: .int, lifetimeSpecifier: "'A")), lifetimeSpecifier: "'A"),
            ret: .unit))
      ])
    )
    testBitis(
      code: """
      f :: &'A (&'B String, &'C Int) -> ()
      f (a, b) = ()
      """,
      expected: .init(bindings: [
        .prototype(
          position: .init(),
          name: "f",
          type: .function(
            position: .init(),
            arg: .ref(
              position: .init(),
              type: .tuple(
                position: .init(),
                types: [
                  .ref(
                    position: .init(),
                    type: .name(position: .init(), name: "String", tcType: .string),
                    lifetimeParameter: "'B",
                    tcType: .reference(.string, lifetimeSpecifier: "'B")),
                  .ref(
                    position: .init(),
                    type: .name(position: .init(), name: "Int", tcType: .int),
                    lifetimeParameter: "'C",
                    tcType: .reference(.int, lifetimeSpecifier: "'C"))
                ],
                tcType: .tuple(elements: [
                  .reference(type: .string, lifetimeSpecifier: "'B"),
                  .reference(type: .int, lifetimeSpecifier: "'C")
                ])),
              lifetimeParameter: "'A",
              tcType: .reference(
                .tuple(elements: [
                  .reference(type: .string, lifetimeSpecifier: "'B"),
                  .reference(type: .int, lifetimeSpecifier: "'C")
                ]),
                lifetimeSpecifier: "'A")),
            ret: .tuple(position: .init(), types: [], tcType: .unit),
            tcType: .arrow(
              arg: .reference(
                type: .tuple(elements: [
                  .reference(type: .string, lifetimeSpecifier: "'B"),
                  .reference(type: .int, lifetimeSpecifier: "'C")
                ]),
                lifetimeSpecifier: "'A"),
              ret: .unit)),
          tcType: .arrow(
            arg: .reference(
              type: .tuple(elements: [
                .reference(type: .string, lifetimeSpecifier: "'B"),
                .reference(type: .int, lifetimeSpecifier: "'C")
              ]),
              lifetimeSpecifier: "'A"),
            ret: .unit)),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .tuple(
                  position: .init(),
                  patterns: [
                    .identifier(
                      position: .init(),
                      identifier: "a",
                      tcType: .reference(.reference(type: .string, lifetimeSpecifier: "'B"), lifetimeSpecifier: "'A")),
                    .identifier(
                      position: .init(),
                      identifier: "b",
                      tcType: .reference(.reference(type: .int, lifetimeSpecifier: "'C"), lifetimeSpecifier: "'A"))
                  ],
                  tcType: .reference(
                    .tuple(elements: [
                      .reference(type: .string, lifetimeSpecifier: "'B"),
                      .reference(type: .int, lifetimeSpecifier: "'C")
                    ]),
                    lifetimeSpecifier: "'A"))
              ],
              body: .tuple(position: .init(), expressions: [], tcType: .unit),
              type: .arrow(
                arg: .reference(
                  type: .tuple(elements: [
                    .reference(type: .string, lifetimeSpecifier: "'B"),
                    .reference(type: .int, lifetimeSpecifier: "'C")
                  ]),
                  lifetimeSpecifier: "'A"),
                ret: .unit))
          ],
          tcType: .arrow(
            arg: .reference(
              type: .tuple(elements: [
                .reference(type: .string, lifetimeSpecifier: "'B"),
                .reference(type: .int, lifetimeSpecifier: "'C")
              ]),
              lifetimeSpecifier: "'A"),
            ret: .unit))
      ]))
    testBitis(
      code: """
      f :: &'A a -> &'B a -> &'A a
      f a b = a
      f a b = b
      """,
      expected: .init(bindings: [
        .prototype(
          position: .init(),
          name: "f",
          type: .function(
            position: .init(),
            arg: .ref(
              position: .init(),
              type: .name(position: .init(), name: "a", tcType: .simple("a").asPolymorphic),
              lifetimeParameter: "'A",
              tcType: .reference("a", lifetimeSpecifier: "'A").asPolymorphic),
            ret: .function(
              position: .init(),
              arg: .ref(
                position: .init(),
                type: .name(position: .init(), name: "a", tcType: .simple("a").asPolymorphic),
                lifetimeParameter: "'B",
                tcType: .reference("a", lifetimeSpecifier: "'B").asPolymorphic),
              ret: .ref(
                position: .init(),
                type: .name(position: .init(), name: "a", tcType: .simple("a").asPolymorphic),
                lifetimeParameter: "'A",
                tcType: .reference("a", lifetimeSpecifier: "'A").asPolymorphic),
              tcType: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A")).asPolymorphic),
            tcType: .arrow(
              arg: .reference(type: "a", lifetimeSpecifier: "'A"),
              ret: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A"))).asPolymorphic),
          tcType: .arrow(
            arg: .reference(type: "a", lifetimeSpecifier: "'A"),
            ret: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A"))).asPolymorphic),
        .binding(
          position: .init(),
          name: "f",
          cases: [
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .identifier(position: .init(), identifier: "a", tcType: .reference("a", lifetimeSpecifier: "'A").asPolymorphic),
                .identifier(position: .init(), identifier: "b", tcType: .reference("a", lifetimeSpecifier: "'B").asPolymorphic),
              ],
              body: .name(
                position: .init(),
                identifier: "a",
                tcType: .reference("a", lifetimeSpecifier: "'A").asPolymorphic),
              type: .arrow(
                arg: .reference(type: "a", lifetimeSpecifier: "'A"),
                ret: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A"))).asPolymorphic),
            .init(
              name: "f",
              position: .init(),
              parameters: [
                .identifier(position: .init(), identifier: "a", tcType: .reference("a", lifetimeSpecifier: "'A").asPolymorphic),
                .identifier(position: .init(), identifier: "b", tcType: .reference("a", lifetimeSpecifier: "'B").asPolymorphic),
              ],
              body: .name(
                position: .init(),
                identifier: "b",
                tcType: .reference("a", lifetimeSpecifier: "'B").asPolymorphic),
              type: .arrow(
                arg: .reference(type: "a", lifetimeSpecifier: "'A"),
                ret: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A"))).asPolymorphic)
          ],
          tcType: .arrow(
            arg: .reference(type: "a", lifetimeSpecifier: "'A"),
            ret: .arrow(arg: .reference(type: "a", lifetimeSpecifier: "'B"), ret: .reference(type: "a", lifetimeSpecifier: "'A"))).asPolymorphic)
      ]))
    testBitis(
      code: """
      f :: &'A Bool -> 'A Bool
      f a = if a then true else false
      f _ = true
      f &(true) = true
      f &(false) = false

      g :: 'A Bool -> 'A Bool
      g a = if a then true else false
      g true = true
      g false = false
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .prototype(
            position: .init(),
            name: "f",
            type:
                .function(
                  position: .init(),
                  arg: .ref(
                    position: .init(),
                    type: .name(
                      position: .init(),
                      name: "Bool",
                      lifetimeParameter: nil,
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                    lifetimeParameter: "'A",
                    tcType:
                        .simple(.reference(
                          type: .atomic(.bool, lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A"))),
                  ret: .name(
                    position: .init(),
                    name: "Bool",
                    lifetimeParameter: "'A",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A"))),
                  lifetimeParameter: nil,
                  tcType: .simple(.arrow(
                    arg: .reference(
                      type: .atomic(.bool, lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A"),
                    ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                    lifetimeSpecifier: nil))),
            tcType:
                .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f",
            cases: [
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.reference(
                      type: .atomic(.bool, lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .binary(
                  .ifThenElse(
                    position: .init(),
                    condition: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.reference(
                        type: .atomic(.bool, lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                      lifetime: nil
                    ),
                    true: .literal(
                      position: .init(),
                      literal: .logical(.true),
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                      lifetime: nil),
                    false: .literal(
                      position: .init(),
                      literal: .logical(.false),
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                      lifetime: nil),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .wildcard(
                    position: .init(),
                    tcType: .simple(.reference(
                      type: .atomic(.bool, lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")))
                ],
                body: .literal(
                  position: .init(),
                  literal: .logical(.true),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .ref(
                    position: .init(),
                    pattern: .literal(
                      position: .init(),
                      literal: .logical(.true),
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil),
                    tcType: .simple(.reference(
                      type: .atomic(.bool, lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .literal(
                  position: .init(),
                  literal: .logical(.true),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .ref(
                    position: .init(),
                    pattern: .literal(
                      position: .init(),
                      literal: .logical(.false),
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil),
                    tcType: .simple(.reference(
                      type: .atomic(.bool, lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .literal(
                  position: .init(),
                  literal: .logical(.false),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .reference(
                type: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .atomic(.bool, lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "g",
            type: .function(
              position: .init(),
              arg: .name(
                position: .init(),
                name: "Bool",
                lifetimeParameter: "'A",
                tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A"))),
              ret: .name(
                position: .init(),
                name: "Bool",
                lifetimeParameter: "'A",
                tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A"))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .atomic(.bool, lifetimeSpecifier: "'A"),
                ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: "'A"),
              ret: .atomic(.bool, lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "g",
            cases: [
              .init(
                name: "g",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(.ifThenElse(
                  position: .init(),
                  condition: .name(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil
                  ),
                  true: .literal(
                    position: .init(),
                    literal: .logical(.true),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil),
                  false: .literal(
                    position: .init(),
                    literal: .logical(.false),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                  lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "g",
                position: .init(),
                parameters: [
                  .literal(
                    position: .init(),
                    literal: .logical(.true),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .literal(
                  position: .init(),
                  literal: .logical(.true),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "g",
                position: .init(),
                parameters: [
                  .literal(
                    position: .init(),
                    literal: .logical(.false),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .literal(
                  position: .init(),
                  literal: .logical(.false),
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: "'A"),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: "'A"),
              ret: .atomic(.bool, lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testFailingBitis(
      expected: .variable(.alreadyDefined(name: "x")),
      code: """
        x = 10
        x = 20
        """)
    testFailingBitis(
      expected: .variable(.alreadyDefined(name: "Bool")),
      code: """
        data Bool = True | False
        data Bool = T | F
        """)
    testFailingBitis(
      expected: .typeMismatch(.match(
        lhs: .int,
        rhs: .reference(.int, lifetimeSpecifier: "'A"))),
      code: """
      data L = C Int
      hd_ref :: &'A (L []) -> Int
      hd_ref $(C a) = a
      """)
//    testFailingBitis(
//      expected: .typeMismatch(.match(
//        lhs: .arrow(arg: .reference(type: .bool, lifetimeSpecifier: "'A"), ret: .bool) as TCType,
//        rhs: .arrow(
//          arg: .reference(type: .bool, lifetimeSpecifier: "'A"),
//          ret: .reference(type: .bool, lifetimeSpecifier: "'A")))),
//      code: """
//      f :: &'A Bool -> Bool
//      f a = if a then true else false
//      """)
    testBitis(
      code: """
      f :: &'A [Int] -> 'A Int
      f &[] = 0
      f (x:xs) = x + (f xs)
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .prototype(
            position: .init(),
            name: "f",
            type: .function(
              position: .init(),
              arg: .ref(
                position: .init(),
                type: .list(
                  position: .init(),
                  element: .name(
                    position: .init(),
                    name: "Int",
                    lifetimeParameter: nil,
                    tcType: .int),
                  lifetimeParameter: nil,
                  tcType: .list(element: .int)),
                lifetimeParameter: "'A",
                tcType: .simple(.reference(
                  type: .list(
                    element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"))),
              ret: .name(
                position: .init(),
                name: "Int",
                lifetimeParameter: "'A",
                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'A"))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .reference(
                  type: .list(
                    element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .reference(
                type: .list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f",
            cases: [
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .ref(
                    position: .init(),
                    pattern: .list(.empty(
                      position: .init(),
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .list(
                          element: .typeVariable(name: "a"),
                          lifetimeSpecifier: nil)),
                      lifetime: nil)),
                    tcType: .simple(.reference(
                      type: .list(
                        element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .literal(
                  position: .init(),
                  literal: .int(0),
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .list(
                      element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil),
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .list(
                    .cons(
                      position: .init(),
                      head: .identifier(
                        position: .init(),
                        identifier: "x",
                        tcType: .simple(.reference(
                          type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                        lifetime: nil),
                      tail: .identifier(
                        position: .init(),
                        identifier: "xs",
                        tcType: .simple(.reference(
                          type: .list(
                            element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                        lifetime: nil),
                      tcType: .simple(.reference(
                        type: .list(
                          element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                      lifetime: nil))
                ],
                body: .binary(
                  .arithmetic(
                    position: .init(),
                    operator: .plus,
                    left: .name(
                      position: .init(),
                      identifier: "x",
                      tcType: .simple(.reference(
                        type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                      lifetime: nil
                    ),
                    right: .binary(
                      .application(
                        position: .init(),
                        fn: .name(
                          position: .init(),
                          identifier: "f",
                          tcType: .simple(.arrow(
                            arg: .reference(
                              type: .list(
                                element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A"),
                            ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
                            lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        arg: .name(
                          position: .init(),
                          identifier: "xs",
                          tcType: .simple(.reference(
                            type: .list(
                              element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              lifetimeSpecifier: nil),
                            lifetimeSpecifier: "'A")),
                          lifetime: nil
                        ),
                        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'A")),
                        lifetime: nil)),
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'A")),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .list(
                      element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .reference(
                type: .list(
                  element: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .atomic(.numeric(.int), lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
  }
  
  func testProgramsWithDelayedVariableDefinition() {
    testBitis(
      code: """
      x :: Int
      y :: Int
      x = y
      y = x
      """,
      expected: .init(bindings: [
        .prototype(position: .init(), name: "x", type: .name(position: .init(), name: "Int", tcType: .int), tcType: .int),
        .prototype(position: .init(), name: "y", type: .name(position: .init(), name: "Int", tcType: .int), tcType: .int),
        .binding(position: .init(), name: "x", cases: [.init(name: "x", position: .init(), parameters: [], body: .name(position: .init(), identifier: "y", tcType: .int), type: .int)], tcType: .int),
        .binding(position: .init(), name: "y", cases: [.init(name: "y", position: .init(), parameters: [], body: .name(position: .init(), identifier: "x", tcType: .int), type: .int)], tcType: .int),
      ]))
    /// TODO: - This is not yet supported by the compiler
    testBitis(
      code: """
      x = y
      y = x
      """,
      expected: .init(bindings: [
        .binding(position: .init(), name: "x", cases: [.init(name: "x", position: .init(), parameters: [], body: .name(position: .init(), identifier: "y", tcType: .int), type: .int)], tcType: .int),
        .binding(position: .init(), name: "y", cases: [.init(name: "y", position: .init(), parameters: [], body: .name(position: .init(), identifier: "x", tcType: .int), type: .int)], tcType: .int),
      ]))
  }
}

// MARK: - Expressions
extension TypeCheckerTests {
  func testLiteral() {
    testExpr(expected: .string, code: "\"A\"")
    // TODO: - Fix
    testExpr(expected: .char, code: "'A'")
    testExpr(expected: .bool, code: "true")
    testExpr(expected: .bool, code: "false")
    testExpr(expected: .int, code: "1")
    testExpr(expected: .float, code: "3.141")
  }
  
  func testArithmeticExpression() {
    testExpr(
      expected: .int,
      code: "1 + 2")
    testFailingExpr(
      expected: .typeMismatch(
        .binaryArithmetic(
          op: .plus,
          left: .int,
          right: .bool)),
      code: "1 + true")
    testFailingExpr(
      expected: .typeMismatch(
        .binaryArithmetic(
          op: .plus,
          left: .bool,
          right: .int)),
      code: "true + 1")
    testFailingExpr(
      expected: .typeMismatch(
        .binaryArithmetic(
          op: .plus,
          left: .string,
          right: .bool)),
      code: "\"1\" + true")
    testExpr(
      code: "x + y",
      expected: .binary(
        .arithmetic(
          position: .init(),
          operator: .plus,
          left: .name(position: .init(), identifier: "x", tcType: .int),
          right: .name(position: .init(), identifier: "y", tcType: .int),
          tcType: .int)),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testFailingExpr(
      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .bool, right: .int)),
      code: "x + (if x then 1 else 2)",
      env: .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testFailingExpr(
      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .int, right: .bool)),
      code: "(if x then 1 else 2) + x",
      env: .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testBitis(
      code: """
      a = let
        x = 10
        z1 = &x + 20
        z2 = 30 + &x
      in z1 + z2
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .binding(
            position: .init(),
            name: "a",
            cases: [
              .init(
                name: "a",
                position: .init(),
                parameters: [],
                body: .let(
                  position: .init(),
                  bindings: [
                    .binding(
                      position: .init(),
                      name: "x",
                      cases: [
                        .init(
                          name: "x",
                          position: .init(),
                          parameters: [],
                          body: .literal(
                            position: .init(),
                            literal: .int(10),
                            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                            lifetime: nil),
                          type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "z1",
                      cases: [
                        .init(
                          name: "z1",
                          position: .init(),
                          parameters: [],
                          body: .binary(
                            .arithmetic(
                              position: .init(),
                              operator: .plus,
                              left: .ref(
                                position: .init(),
                                expression: .name(
                                  position: .init(),
                                  identifier: "x",
                                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                  lifetime: nil
                                ),
                                tcType: .simple(.reference(
                                  type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                  lifetimeSpecifier: nil)),
                                lifetime: nil),
                              right: .literal(
                                position: .init(),
                                literal: .int(20),
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                lifetime: nil),
                              tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                              lifetime: nil)),
                          type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "z2",
                      cases: [
                        .init(
                          name: "z2",
                          position: .init(),
                          parameters: [],
                          body: .binary(
                            .arithmetic(
                              position: .init(),
                              operator: .plus,
                              left: .literal(
                                position: .init(),
                                literal: .int(30),
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                lifetime: nil),
                              right: .ref(
                                position: .init(),
                                expression: .name(
                                  position: .init(),
                                  identifier: "x",
                                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                                  lifetime: nil
                                ),
                                tcType: .simple(.reference(
                                  type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                                  lifetimeSpecifier: nil)),
                                lifetime: nil),
                              tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                              lifetime: nil)),
                          type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                      lifetime: nil,
                      borrowLifetime: nil)
                  ],
                  expression: .binary(
                    .arithmetic(
                      position: .init(),
                      operator: .plus,
                      left: .name(
                        position: .init(),
                        identifier: "z1",
                        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                        lifetime: nil
                      ),
                      right: .name(
                        position: .init(),
                        identifier: "z2",
                        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                        lifetime: nil
                      ),
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                      lifetime: nil)),
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                  lifetime: nil),
                type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                lifetime: nil)
            ],
            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
  }
  
  func testLetExpression() {
    testExpr(
      expected: .bool,
      code: "let in true")
    testExpr(
      expected: .int,
      code: "let x :: Int, x = 10 in x")
    testExpr(
      expected: .int,
      code: """
      let
        x :: Int
        x = 10
      in x
      """)
    testExpr(
      expected: .int,
      code: "let x = \"string\" in let x = 10 in x")
    testFailingExpr(
      expected: .variable(.undefined(name: "x")),
      code: "let in x")
    testFailingExpr(
      expected: .variable(.alreadyDefined(name: "x")),
      code: "let x = 10, x = 20 in x")
  }
  
  func testIfExpression() {
    testExpr(
      expected: .string,
      code: "if false then \"A\" else \"B\"")
    testExpr(
      expected: .tuple(elements: [.int, .bool]),
      code: "if 10 < 20 then (1, false) else (10, true)")
    testExpr(
      expected: .bool,
      code: "if true then if 1 < 2 then true else false else false")
    testFailingExpr(
      expected: .typeMismatch(.condition(.string)),
      code: "if \"A\" then \"A\" else \"B\"")
    testFailingExpr(
      expected: .typeMismatch(.ifBranches(left: .int, right: .string)),
      code: "if false then 1 else \"ABC\"")
    testFailingExpr(
      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .bool, right: .int)),
      code: "if y then y else y + 1",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testFailingExpr(
      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .bool, right: .int)),
      code: "if y then y + 1 else y",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testExpr(
      expected: .bool,
      code: "if y then y else y",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testExpr(
      expected: .bool,
      code: "if (if y then true else false) then true else y",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testExpr(
      expected: .int,
      code: "if true then y else hd [0]",
      env: Env
        .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["g"], context: .empty, type: .typeVariable(name: "g"))))
        .withPrototype(name: "hd", type: TCType.arrow(arg: .list(element: "f"), ret: "f").asPolymorphic))
    testFailingExpr(
      expected: .typeMismatch(.ifBranches(left: .int, right: .bool)),
      code: "if (if y then true else false) then 1 else y",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testFailingExpr(
      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .bool, right: .int)),
      code: "if y then (if true then y + 1 else 2) else y",
      env: .withBinding(name: "y", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testExpr(
      code: "if true then (\\x -> x) else (\\a -> a + 1)",
      expected: .binary(.ifThenElse(
        position: .init(),
        condition: .literal(position: .init(), literal: .logical(.true), tcType: .bool),
        true: .lambda(
          position: .init(),
          parameter: .identifier(position: .init(), identifier: "x", tcType: .int),
          body: .name(position: .init(), identifier: "x", tcType: .int),
          tcType: .arrow(arg: .int, ret: .int)),
        false: .lambda(
          position: .init(),
          parameter: .identifier(position: .init(), identifier: "a", tcType: .int),
          body: .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .name(position: .init(), identifier: "a", tcType: .int),
            right: .literal(position: .init(), literal: .int(1), tcType: .int),
            tcType: .int)),
          tcType: .arrow(arg: .int, ret: .int)),
        tcType: .arrow(arg: .int, ret: .int))))
    testBitis(
      code: """
      x = let
        b = true
        c = if &b then 1 else 2
      in c
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .binding(
            position: .init(),
            name: "x",
            cases: [
              .init(
                name: "x",
                position: .init(),
                parameters: [],
                body: .let(
                  position: .init(),
                  bindings: [
                    .binding(
                      position: .init(),
                      name: "b",
                      cases: [
                        .init(
                          name: "b",
                          position: .init(),
                          parameters: [],
                          body: .literal(
                            position: .init(),
                            literal: .logical(.true),
                            tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                            lifetime: nil),
                          type: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                          lifetime: nil)
                      ],
                      tcType:
                          .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "c",
                      cases: [
                        .init(
                          name: "c",
                          position: .init(),
                          parameters: [],
                          body: .binary(
                            .ifThenElse(
                              position: .init(),
                              condition: .ref(
                                position: .init(),
                                expression: .name(
                                  position: .init(),
                                  identifier: "b",
                                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                  lifetime: nil
                                ),
                                tcType: .simple(.reference(
                                  type: .atomic(.bool, lifetimeSpecifier: nil),
                                  lifetimeSpecifier: "'X")),
                                lifetime: nil),
                              true: .literal(
                                position: .init(),
                                literal: .int(1),
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                                lifetime: nil),
                              false: .literal(
                                position: .init(),
                                literal: .int(2),
                                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                                lifetime: nil),
                              tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                              lifetime: nil)),
                          type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                      lifetime: nil,
                      borrowLifetime: nil)
                  ],
                  expression: .name(
                    position: .init(),
                    identifier: "c",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                    lifetime: nil
                  ),
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                  lifetime: nil),
                type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
                lifetime: nil)
            ],
            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: "'X")),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testBitis(
      code: """
      a = let
        x = true
        y = false
        z1 = if true then &x else y
        z2 = if true then x else &y
      in 10
      """,
      expected: .init(  // NOTE: - Generated with XCTestDump
        bindings: [
          .binding(
            position: .init(),
            name: "a",
            cases: [
              .init(
                name: "a",
                position: .init(),
                parameters: [],
                body: .let(
                  position: .init(),
                  bindings: [
                    .binding(
                      position: .init(),
                      name: "x",
                      cases: [
                        .init(
                          name: "x",
                          position: .init(),
                          parameters: [],
                          body: .literal(
                            position: .init(),
                            literal: .logical(.true),
                            tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                            lifetime: nil),
                          type: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "y",
                      cases: [
                        .init(
                          name: "y",
                          position: .init(),
                          parameters: [],
                          body: .literal(
                            position: .init(),
                            literal: .logical(.false),
                            tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                            lifetime: nil),
                          type: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                          lifetime: nil)
                      ],
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "z1",
                      cases: [
                        .init(
                          name: "z1",
                          position: .init(),
                          parameters: [],
                          body: .binary(
                            .ifThenElse(
                              position: .init(),
                              condition: .literal(
                                position: .init(),
                                literal: .logical(.true),
                                tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                lifetime: nil),
                              true: .ref(
                                position: .init(),
                                expression: .name(
                                  position: .init(),
                                  identifier: "x",
                                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                  lifetime: nil
                                ),
                                tcType: .simple(.reference(
                                  type: .atomic(.bool, lifetimeSpecifier: nil),
                                  lifetimeSpecifier: nil)),
                                lifetime: nil),
                              false: .name(
                                position: .init(),
                                identifier: "y",
                                tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                lifetime: nil
                              ),
                              tcType: .simple(.reference(
                                type: .atomic(.bool, lifetimeSpecifier: nil),
                                lifetimeSpecifier: "'X")),
                              lifetime: nil)),
                          type: .simple(.reference(
                            type: .atomic(.bool, lifetimeSpecifier: nil),
                            lifetimeSpecifier: "'X")),
                          lifetime: nil)
                      ],
                      tcType: .simple(.reference(
                        type: .atomic(.bool, lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'X")),
                      lifetime: nil,
                      borrowLifetime: nil),
                    .binding(
                      position: .init(),
                      name: "z2",
                      cases: [
                        .init(
                          name: "z2",
                          position: .init(),
                          parameters: [],
                          body: .binary(
                            .ifThenElse(
                              position: .init(),
                              condition: .literal(
                                position: .init(),
                                literal: .logical(.true),
                                tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                lifetime: nil),
                              true: .name(
                                position: .init(),
                                identifier: "x",
                                tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                lifetime: nil
                              ),
                              false: .ref(
                                position: .init(),
                                expression: .name(
                                  position: .init(),
                                  identifier: "y",
                                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                                  lifetime: nil
                                ),
                                tcType: .simple(.reference(
                                  type: .atomic(.bool, lifetimeSpecifier: nil),
                                  lifetimeSpecifier: nil)),
                                lifetime: nil),
                              tcType: .simple(.reference(
                                type: .atomic(.bool, lifetimeSpecifier: nil),
                                lifetimeSpecifier: "'X")),
                              lifetime: nil)),
                          type: .simple(.reference(
                            type: .atomic(.bool, lifetimeSpecifier: nil),
                            lifetimeSpecifier: "'X")),
                          lifetime: nil)
                      ],
                      tcType: .simple(.reference(
                        type: .atomic(.bool, lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'X")),
                      lifetime: nil,
                      borrowLifetime: nil)
                  ],
                  expression: .literal(
                    position: .init(),
                    literal: .int(10),
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
  }
  
  func testLogicalExpression() {
    testExpr(expected: .bool, code: "10 == 20")
    testExpr(expected: .reference(.bool), code: "&10 == &20")
    testFailingExpr(
      expected: .typeMismatch(.logicalOperation(op: .and, expected: [.bool])),
      code: "10 && 20")
    testFailingExpr(
      expected: .typeMismatch(.binaryLogical(op: .or, left: .int, right: .bool)),
      code: "10 || true")
    testFailingExpr(
      expected: .typeMismatch(.logicalOperation(op: .lowerThan, expected: [.int, .float])),
      code: "true < false")
    testFailingExpr(
      expected: .typeMismatch(.logicalOperation(op: .greaterOrEqualThan, expected: [.int, .float])),
      code: "true >= false")
    testFailingExpr(
      expected: .typeMismatch(.binaryLogical(op: .equal, left: .bool, right: .int)),
      code: "true == 10")
    testBitis(
      code: """
      f1 a b = a < b
      f2 a b = a > b
      f3 a b = a <= b
      f4 a b = a >= b
      f5 a b = a && b
      f6 a b = a || b
      f7 :: Int -> Int -> Bool
      f7 a b = a == b
      f8 :: Char -> Char -> Bool
      f8 a b = a == b
      f9 :: Float -> Float -> Bool
      f9 a b = a == b
      f10 :: Bool -> Bool -> Bool
      f10 a b = a == b
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .binding(
            position: .init(),
            name: "f1",
            cases: [
              .init(
                name: "f1",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .lowerThan,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "f2",
            cases: [
              .init(
                name: "f2",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .greaterThan,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "f3",
            cases: [
              .init(
                name: "f3",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .lowerOrEqualThan,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "f4",
            cases: [
              .init(
                name: "f4",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .greaterOrEqualThan,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "f5",
            cases: [
              .init(
                name: "f5",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .and,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.bool, lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.bool, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .binding(
            position: .init(),
            name: "f6",
            cases: [
              .init(
                name: "f6",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .or,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.bool, lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.bool, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "f7",
            type: .function(
              position: .init(),
              arg: .name(
                position: .init(),
                name: "Int",
                lifetimeParameter: nil,
                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
              ret: .function(
                position: .init(),
                arg: .name(
                  position: .init(),
                  name: "Int",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
                ret: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                lifetimeParameter: nil,
                tcType: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f7",
            cases: [
              .init(
                name: "f7",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .equal,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "f8",
            type: .function(
              position: .init(),
              arg: .name(
                position: .init(),
                name: "Char",
                lifetimeParameter: nil,
                tcType: .simple(.atomic(.char, lifetimeSpecifier: nil))),
              ret: .function(
                position: .init(),
                arg: .name(
                  position: .init(),
                  name: "Char",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.char, lifetimeSpecifier: nil))),
                ret: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                lifetimeParameter: nil,
                tcType: .simple(.arrow(
                  arg: .atomic(.char, lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .atomic(.char, lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .atomic(.char, lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .atomic(.char, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.char, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f8",
            cases: [
              .init(
                name: "f8",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.char, lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.char, lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .equal,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.char, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.char, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.char, lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.char, lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.char, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.char, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "f9",
            type: .function(
              position: .init(),
              arg: .name(
                position: .init(),
                name: "Float",
                lifetimeParameter: nil,
                tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil))),
              ret: .function(
                position: .init(),
                arg: .name(
                  position: .init(),
                  name: "Float",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil))),
                ret: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                lifetimeParameter: nil,
                tcType: .simple(.arrow(
                  arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f9",
            cases: [
              .init(
                name: "f9",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .equal,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.float), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.numeric(.float), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "f10",
            type: .function(
              position: .init(),
              arg: .name(
                position: .init(),
                name: "Bool",
                lifetimeParameter: nil,
                tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
              ret: .function(
                position: .init(),
                arg: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                ret: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil))),
                lifetimeParameter: nil,
                tcType: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .atomic(.bool, lifetimeSpecifier: nil),
                ret: .arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.bool, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f10",
            cases: [
              .init(
                name: "f10",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .equal,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .atomic(.bool, lifetimeSpecifier: nil),
                  ret: .arrow(
                    arg: .atomic(.bool, lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .atomic(.bool, lifetimeSpecifier: nil),
              ret: .arrow(
                arg: .atomic(.bool, lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: nil),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testBitis(
      code: """
      f :: &'A Int -> Int -> 'A Bool
      f a b = a < b
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .prototype(
            position: .init(),
            name: "f",
            type: .function(
              position: .init(),
              arg: .ref(
                position: .init(),
                type: .name(
                  position: .init(),
                  name: "Int",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
                lifetimeParameter: "'A",
                tcType: .simple(.reference(
                  type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"))),
              ret: .function(
                position: .init(),
                arg: .name(
                  position: .init(),
                  name: "Int",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
                ret: .name(
                  position: .init(),
                  name: "Bool",
                  lifetimeParameter: "'A",
                  tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A"))),
                lifetimeParameter: nil,
                tcType: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .reference(
                  type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                ret: .arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .reference(
                type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f",
            cases: [
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "a",
                    tcType: .simple(.reference(
                      type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil),
                  .identifier(
                    position: .init(),
                    identifier: "b",
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .binary(
                  .logical(
                    position: .init(),
                    operator: .lowerThan,
                    left: .name(
                      position: .init(),
                      identifier: "a",
                      tcType: .simple(.reference(
                        type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                      lifetime: nil
                    ),
                    right: .name(
                      position: .init(),
                      identifier: "b",
                      tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                      lifetime: nil
                    ),
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: "'A")),
                    lifetime: nil)),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .reference(
                type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .arrow(
                arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                ret: .atomic(.bool, lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
  }
  
  func testListExpression() {
    testExpr(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .list(element: .typeVariable(name: "a"))),
      code: "[]")
    testExpr(
      expected: .list(element: .int),
      code: "[10, 20, 30]")
    testExpr(
      expected: .list(element: .bool),
      code: "[true, false]")
    testFailingExpr(
      expected: .typeMismatch(.listElementsDontMatch(expected: .int, got: .bool)),
      code: "[10, false]")
    testExpr(
      code: "[x, y]",
      expected: .list(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
          .name(position: .init(), identifier: "y", tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))),
        ],
        tcType: .polymorphic(typeVariables: ["a"], context: .empty, type: .list(element: .typeVariable(name: "a")))),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testExpr(
      code: "[x, y, x + y]",
      expected: .list(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .int),
          .name(position: .init(), identifier: "y", tcType: .int),
          .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .name(position: .init(), identifier: "x", tcType: .int),
            right: .name(position: .init(), identifier: "y", tcType: .int),
            tcType: .int))
        ],
        tcType: .list(element: .int)),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testFailingExpr(
      expected: .typeMismatch(.condition(.int)),
      code: "[x, y, x + y, if x then 1 else 2]",
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    // TODO: - Fix (incorrect error)
//    testFailingExpr(
//      expected: .typeMismatch(.binaryArithmetic(op: .plus, left: .bool, right: .int)),
//      code: "[if x then 1 else 2, x, y, x + y]",
//      env: Env
//        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
//        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testExpr(
      code: "[x, y, [1, 2, 3]]",
      expected: .list(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .list(element: .int)),
          .name(position: .init(), identifier: "y", tcType: .list(element: .int)),
          .list(
            position: .init(),
            expressions: [
              .literal(position: .init(), literal: .int(1), tcType: .int),
              .literal(position: .init(), literal: .int(2), tcType: .int),
              .literal(position: .init(), literal: .int(3), tcType: .int),
            ],
            tcType: .list(element: .int))
        ],
        tcType: .list(element: .list(element: .int))),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testExpr(
      code: "[[1, 2, 3], x, y]",
      expected: .list(
        position: .init(),
        expressions: [
          .list(
            position: .init(),
            expressions: [
              .literal(position: .init(), literal: .int(1), tcType: .int),
              .literal(position: .init(), literal: .int(2), tcType: .int),
              .literal(position: .init(), literal: .int(3), tcType: .int),
            ],
            tcType: .list(element: .int)),
          .name(position: .init(), identifier: "x", tcType: .list(element: .int)),
          .name(position: .init(), identifier: "y", tcType: .list(element: .int)),
        ],
        tcType: .list(element: .list(element: .int))),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testExpr(
      code: "[x, y, (true, \"abc\")]",
      expected: .list(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .tuple(elements: [.bool, .string])),
          .name(position: .init(), identifier: "y", tcType: .tuple(elements: [.bool, .string])),
          .tuple(
            position: .init(),
            expressions: [
              .literal(position: .init(), literal: .logical(.true), tcType: .bool),
              .literal(position: .init(), literal: .string("abc"), tcType: .string),
            ],
            tcType: .tuple(elements: [.bool, .string]))
        ],
        tcType: .list(element: .tuple(elements: [.bool, .string]))),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
  }
  
  func testTupleExpression() {
    testExpr(
      expected: .unit,
      code: "()")
    testExpr(
      expected: .tuple(elements: [.int, .int]),
      code: "(10, 20)")
    testExpr(
      expected: .polymorphic(
        typeVariables: ["a"],
        context: .empty,
        type: .tuple(elements: [
          .int,
          .tuple(elements: [
            .bool,
            .string
          ]),
          .arrow(arg: .typeVariable(name: "a"), ret: .typeVariable(name: "a"))
        ])),
      code: "(10, (true, \"abc\"), \\x -> x)")
    testExpr(
      expected: .bool,
      code: "(false)")
    testExpr(
      code: "(x, y, x + y)",
      expected: .tuple(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .int),
          .name(position: .init(), identifier: "y", tcType: .int),
          .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .name(position: .init(), identifier: "x", tcType: .int),
            right: .name(position: .init(), identifier: "y", tcType: .int),
            tcType: .int))
        ],
        tcType: .tuple(elements: [.int, .int, .int])),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
    testExpr(
      code: "(x, if x then 1 else 2 + y, y)",
      expected: .tuple(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .bool),
          .binary(.ifThenElse(
            position: .init(),
            condition: .name(position: .init(), identifier: "x", tcType: .bool),
            true: .literal(position: .init(), literal: .int(1), tcType: .int),
            false: .binary(.arithmetic(
              position: .init(),
              operator: .plus,
              left: .literal(position: .init(), literal: .int(2), tcType: .int),
              right: .name(position: .init(), identifier: "y", tcType: .int),
              tcType: .int)),
            tcType: .int)),
          .name(position: .init(), identifier: "y", tcType: .int),
        ],
        tcType: .tuple(elements: [.bool, .int, .int])),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b"))))
  }
  
  func testApplication() {
    testExpr(
      expected: .bool,
      code: "f 10",
      env: .withBinding(name: "f", binding: .bind(.arrow(arg: .int, ret: .bool))))
    testExpr(
      expected: .arrow(arg: .bool, ret: .int),
      code: "f 10",
      env: .withBinding(name: "f", binding: .bind(.arrow(arg: .int, ret: .arrow(arg: .bool, ret: .int)))))
    testExpr(
      expected: .int,
      code: "f 10 false",
      env: .withBinding(
        name: "f",
        binding: .bind(.arrow(arg: .int, ret: .arrow(arg: .bool, ret: .int)))))
    testExpr(
      expected: .int,
      code: "(if true then f else f) 10",
      env: .withBinding(
        name: "f",
        binding: .prototype(.polymorphic(
          typeVariables: ["a"],
          context: .empty,
          type: .arrow(arg: .typeVariable(name: "a"), ret: .typeVariable(name: "a"))))))
    testExpr(
      expected: .bool,
      code: "(if true then f else f) true",
      env: .withBinding(name: "f", binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(arg: .typeVariable(name: "a"), ret: .typeVariable(name: "a"))))))
    testExpr(
      expected: .unit,
      code: "f (&[1,2,3])",
      env: .withBinding(name: "f", binding: .bind(.arrow(arg: .reference(type: .list(element: .int)), ret: .unit))))
    testExpr(
      expected: .simple(.typeConstructor(name: "Maybe", types: [.int])),
      code: "Just 10",
      env: .withBinding(name: "Just", binding: .bind(.arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic)))
    testExpr(
      expected: .simple(.typeConstructor(name: "Maybe", types: [.bool])),
      code: "Just true",
      env: .withBinding(name: "Just", binding: .bind(.arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic)))
    testExpr(
      expected: .simple(
        .typeConstructor(
          name: "Maybe",
          types: [
            .typeConstructor(
              name: "Maybe",
              types: [
                .typeConstructor(
                  name: "Maybe",
                  types: [
                    .typeConstructor(
                      name: "Maybe",
                      types: [.int])
                  ])
              ])
          ])),
      code: "Just (Just (Just (Just 10)))",
      env: .withBinding(name: "Just", binding: .bind(.arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic)))
    testFailingExpr(
      expected: .typeMismatch(.application(expected: .int, got: .bool)),
      code: "f false",
      env: .withBinding(name: "f", binding: .bind(.arrow(arg: .int, ret: .bool))))
    // TODO: - Fix (incorrect error)
    testFailingExpr(
      expected: .typeMismatch(.notAFunction(fn: .int)),
      code: "10 10")
    testFailingExpr(
      expected: .typeMismatch(.application(
        expected: .reference(.list(element: .int)),
        got: .list(element: .int))),
      code: "f [1,2,3]",
      env: .withBinding(name: "f", binding: .bind(.arrow(arg: .reference(type: .list(element: .int)), ret: .unit))))
    testFailingBitis(
      expected: .typeMismatch(.application(
        expected: .simple("a").asPolymorphic,
        got: .reference(.int))),
      code: """
      fst :: a -> Int
      fst a = 10

      x = 10
      y = fst (&x)
      """)
    testFailingBitis(
      expected: .typeMismatch(.application(
        expected: .reference(.list(element: "a")).asPolymorphic,
        got: .simple(.typeVariable(name: "a")).asPolymorphic)),
      code: """
      data List [c] = Cons c (List [c]) | Nil ()
      hd :: &'A (List [a]) -> &'A a
      hd $(Cons xss _) = xss
      r = hd (hd (&y))
      """)
  }
  
  func testLambdaExpression() {
    testExpr(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(arg: .typeVariable(name: "a"), ret: .typeVariable(name: "a"))),
      code: "\\x -> x")
    testExpr(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(arg: .typeVariable(name: "a"), ret: .int)),
      code: "\\x -> 1")
    testExpr(
      expected: .arrow(arg: .bool, ret: .string),
      code: "\\true -> \"abc\"")
    testExpr(
      code: "\\x -> x + 1",
      expected: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x", tcType: .int),
        body: .binary(.arithmetic(
          position: .init(),
          operator: .plus,
          left: .name(position: .init(), identifier: "x", tcType: .int),
          right: .literal(position: .init(), literal: .int(1), tcType: .int),
          tcType: .int)),
        tcType: .arrow(arg: .int, ret: .int)))
    testExpr(
      code: "\\x -> \\y -> x + y",
      expected: .lambda(
        position: .init(),
        parameter: .identifier(position: .init(), identifier: "x", tcType: .int),
        body: .lambda(
          position: .init(),
          parameter: .identifier(position: .init(), identifier: "y", tcType: .int),
          body: .binary(.arithmetic(
            position: .init(),
            operator: .plus,
            left: .name(position: .init(), identifier: "x", tcType: .int),
            right: .name(position: .init(), identifier: "y", tcType: .int),
            tcType: .int)),
          tcType: .arrow(arg: .int, ret: .int)),
        tcType: .arrow(arg: .int, ret: .arrow(arg: .int, ret: .int))))
  }
  
  func testExpressionCombinationRigorous() {
    testExpr(
      code: "[x, y, z, if true then x else hd [a, (true, 3.2), (false, 3.0)], x, y, z]",
      expected: .list(
        position: .init(),
        expressions: [
          .name(position: .init(), identifier: "x", tcType: .tuple(elements: [.bool, .float])),
          .name(position: .init(), identifier: "y", tcType: .tuple(elements: [.bool, .float])),
          .name(position: .init(), identifier: "z", tcType: .tuple(elements: [.bool, .float])),
          .binary(.ifThenElse(
            position: .init(),
            condition: .literal(position: .init(), literal: .logical(.true), tcType: .bool),
            true: .name(position: .init(), identifier: "x", tcType: .tuple(elements: [.bool, .float])),
            false: .binary(.application(
              position: .init(),
              fn: .name(
                position: .init(),
                identifier: "hd",
                tcType: .arrow(arg: .list(element: .tuple(elements: [.bool, .float])), ret: .tuple(elements: [.bool, .float]))),
              arg: .list(
                position: .init(),
                expressions: [
                  .name(position: .init(), identifier: "a", tcType: .tuple(elements: [.bool, .float])),
                  .tuple(
                    position: .init(),
                    expressions: [
                      .literal(position: .init(), literal: .logical(.true), tcType: .bool),
                      .literal(position: .init(), literal: .float(3.2), tcType: .float)
                    ],
                    tcType: .tuple(elements: [.bool, .float])),
                  .tuple(
                    position: .init(),
                    expressions: [
                      .literal(position: .init(), literal: .logical(.false), tcType: .bool),
                      .literal(position: .init(), literal: .float(3.0), tcType: .float)
                    ],
                    tcType: .tuple(elements: [.bool, .float]))
                ],
                tcType: .list(element: .tuple(elements: [.bool, .float]))),
              tcType: .tuple(elements: [.bool, .float]))),
            tcType: .tuple(elements: [.bool, .float]))),
          .name(position: .init(), identifier: "x", tcType: .tuple(elements: [.bool, .float])),
          .name(position: .init(), identifier: "y", tcType: .tuple(elements: [.bool, .float])),
          .name(position: .init(), identifier: "z", tcType: .tuple(elements: [.bool, .float])),
        ],
        tcType: .list(element: .tuple(elements: [.bool, .float]))),
      env: Env
        .withBinding(name: "x", binding: .prototype(.polymorphic(typeVariables: ["f"], context: .empty, type: .typeVariable(name: "f"))))
        .withPrototype(name: "y", type: .polymorphic(typeVariables: ["b"], context: .empty, type: .typeVariable(name: "b")))
        .withPrototype(name: "z", type: .polymorphic(typeVariables: ["c"], context: .empty, type: .typeVariable(name: "c")))
        .withPrototype(name: "a", type: .polymorphic(typeVariables: ["d"], context: .empty, type: .typeVariable(name: "d")))
        .withPrototype(name: "hd", type: .polymorphic(typeVariables: ["e"], context: .empty, type: .arrow(arg: .list(element: .typeVariable(name: "e")), ret: .typeVariable(name: "e")))))
  }
  
  func testCaseExpressionRigorous() {
    testExpr(
      code: """
            case (true, 1) of
              (x, y) -> if x then y + 1 else y
              | a -> 0
              | _ -> 100
              | (false, _) -> 2 + 5
            """,
      expected: .case(
        position: .init(),
        expression: .tuple(
          position: .init(),
          expressions: [
            .literal(position: .init(), literal: .logical(.true), tcType: .bool),
            .literal(position: .init(), literal: .int(1), tcType: .int),
          ],
          tcType: .simple(.tuple(elements: [.bool, .int]))),
        matches: [
          .init(
            position: .init(),
            pattern: .tuple(
              position: .init(),
              patterns: [
                .identifier(position: .init(), identifier: "x", tcType: .bool),
                .identifier(position: .init(), identifier: "y", tcType: .int)
              ],
              tcType: .simple(.tuple(elements: [.bool, .int]))),
            expression: .binary(.ifThenElse(
              position: .init(),
              condition: .name(position: .init(), identifier: "x", tcType: .bool),
              true: .binary(.arithmetic(
                position: .init(),
                operator: .plus,
                left: .name(position: .init(), identifier: "y", tcType: .int),
                right: .literal(position: .init(), literal: .int(1), tcType: .int),
                tcType: .int)),
              false: .name(position: .init(), identifier: "y", tcType: .int),
              tcType: .int))),
          .init(
            position: .init(),
            pattern: .identifier(position: .init(), identifier: "a", tcType: .simple(.tuple(elements: [.bool, .int]))),
            expression: .literal(position: .init(), literal: .int(0), tcType: .int)),
          .init(
            position: .init(),
            pattern: .wildcard(position: .init(), tcType: .simple(.tuple(elements: [.bool, .int]))),
            expression: .literal(position: .init(), literal: .int(100), tcType: .int)),
          .init(
            position: .init(),
            pattern: .tuple(
              position: .init(),
              patterns: [
                .literal(position: .init(), literal: .logical(.false), tcType: .bool),
                .wildcard(position: .init(), tcType: .int)
              ],
              tcType: .simple(.tuple(elements: [.bool, .int]))),
            expression: .binary(.arithmetic(
              position: .init(),
              operator: .plus,
              left: .literal(position: .init(), literal: .int(2), tcType: .int),
              right: .literal(position: .init(), literal: .int(5), tcType: .int),
              tcType: .int))),
        ],
        tcType: .int))
  }
  
  func testCaseExpression() {
    testExpr(
      code: "case x of a -> let f :: Int -> Bool, f _ = true in f a",
      expected: .case(
        position: .init(),
        expression: .name(position: .init(), identifier: "x", tcType: .int),
        matches: [.init(
          position: .init(),
          pattern: .identifier(position: .init(), identifier: "a", tcType: .int),
          expression: .let(
            position: .init(),
            bindings: [
              .prototype(
                position: .init(),
                name: "f",
                type: .function(
                  position: .init(),
                  arg: .name(position: .init(), name: "Int", tcType: .int),
                  ret: .name(position: .init(), name: "Bool", tcType: .bool),
                  tcType: .arrow(arg: .int, ret: .bool)),
                tcType: .arrow(arg: .int, ret: .bool)),
              .binding(
                position: .init(),
                name: "f",
                cases: [.init(
                  name: "f",
                  position: .init(),
                  parameters: [.wildcard(position: .init(), tcType: .int)],
                  body: .literal(position: .init(), literal: .logical(.true), tcType: .bool),
                  type: .arrow(arg: .int, ret: .bool))],
                tcType: .arrow(arg: .int, ret: .bool))
            ],
            expression: .binary(.application(
              position: .init(),
              fn: .name(position: .init(), identifier: "f", tcType: .arrow(arg: .int, ret: .bool)),
              arg: .name(position: .init(), identifier: "a", tcType: .int),
              tcType: .bool)),
            tcType: .bool))
                 ],
        tcType: .bool),
      env: .withBinding(
        name: "x",
        binding: .prototype(.polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")))))
    testFailingExpr(
      expected: .patternMatching(.cannotMatch(
        pattern: .literal(
          position: .init(),
          literal: .int(1),
          tcType: .bool),
        type: .bool)),
      code: """
            case true of
              1 -> 0
            """)
    testFailingExpr(
      expected: .typeMismatch(.match(lhs: TCType.int, rhs: .bool)),
      code: """
            case true of
              true -> 0
              | false -> true
            """)
    testFailingExpr(
      expected: .patternMatching(.cannotMatch(
        pattern: .literal(
          position: .init(),
          literal: .int(1),
          tcType: .reference(.int)),
        type: .reference(.int))),
      code: """
            case &1 of
              1 -> 0
              | 2 -> 1
            """)
    // @TODO: -
    //    testFailingExpr(
//      expected: .patternMatching(.cannotMatch(
//        pattern: .ref(
//          position: .init(),
//          pattern: .literal(
//            position: .init(),
//            literal: .int(1),
//            tcType: .int),
//          tcType: .int),
//        type: .int)),
//      code: """
//            case 1 of
//              &1 -> 0
//              | &2 -> 1
//            """)
  }
  
  func testReferenceExpression() {
    testExpr(expected: .reference(.int), code: "&1")
    testExpr(expected: .reference(.reference(type: .int)), code: "&&1")
    testExpr(expected: .reference(.list(element: .int)), code: "&[1, 2, 3]")
    testExpr(expected: .reference(.tuple(elements: [.int, .string])), code: "&(1, \"abc\")")
    testExpr(
      code: "f (&10)",
      expected: .binary(.application(
        position: .init(),
        fn: .name(position: .init(), identifier: "f", tcType: .arrow(arg: .reference(type: .int), ret: .unit)),
        arg: .ref(position: .init(), expression: .literal(position: .init(), literal: .int(10), tcType: .int), tcType: .reference(.int)),
        tcType: .unit)),
      env: .withBinding(name: "f", binding: .prototype(.polymorphic(
        typeVariables: ["a"],
        context: .empty,
        type: .arrow(arg: .reference(type: "a"), ret: .unit)))))
  }
  
  func testDereferenceExpression() {
    testExpr(expected: .int, code: "*&1")
    testExpr(expected: .list(element: .int), code: "*&[1,2,3]")
    testFailingExpr(expected: .cannotDereference(.int), code: "*1")
  }
}

// MARK: - Patterns
extension TypeCheckerTests {
  func testLiteralPattern() {
    testPatt(expected: .int, code: "10")
    testPatt(expected: .float, code: "3.14")
    testPatt(expected: .bool, code: "false")
    testPatt(expected: .bool, code: "true")
    testPatt(expected: .string, code: "\"true\"")
    testPatt(expected: .char, code: "'a'")
  }
  
  func testListPattern() {
    testPatt(expected: .list(element: .int), code: "1:[]")
    testPatt(expected: .list(element: .int), code: "1:_")
    testPatt(
      expected: .polymorphic(
        typeVariables: ["b"],
        context: .empty,
        type: .list(element: .typeVariable(name: "b"))),
      code: "_:_")
    testPatt(
      expected: .list(element: .int),
      code: "x:10:[]")
    testPatt(
      expected: .list(element: .int),
      code: "_:10:[]")
    testPatt(
      expected: .list(element: .int),
      code: "x:1:_")
    testFailingPatt(
      expected: .typeMismatch(.tailNotAList(tail: .int)),
      code: "1:2")
    testFailingPatt(
      expected: .typeMismatch(.headAndTail(head: .int, tail: .list(element: .string))),
      code: "10:\"abc\":[]")
  }
  
  func testListPatternRigorous() {
    func test(code: String, expected: TCAst.Pattern) {
      do {
        let ast = try Parser().pattern.run(sourceName: "test.bt", input: code)
        let result = ast.typeChecker().runM((.empty, .init(), .empty, .init()))^
        if result.isRight {
          XCTAssertEqual(expected, result.rightValue.1)
        } else {
          XCTFail()
        }
      } catch {
        XCTFail()
      }
    }
    test(code: "x:1:_", expected: .list(.cons(
      position: .init(),
      head: .identifier(position: .init(), identifier: "x", tcType: .int),
      tail: .list(.cons(
        position: .init(),
        head: .literal(position: .init(), literal: .int(1), tcType: .int),
        tail: .wildcard(position: .init(), tcType: .list(element: .int)),
        tcType: .simple(.list(element: .int)))),
      tcType: .simple(.list(element: .int)))))
    test(code: "(1, true):_:a:(b, c):[]", expected: .list(.cons(
      position: .init(),
      head: .tuple(
        position: .init(),
        patterns: [
          .literal(position: .init(), literal: .int(1), tcType: .int),
          .literal(position: .init(), literal: .logical(.true), tcType: .bool)
        ],
        tcType: .tuple(elements: [
          .int,
          .bool
        ])),
      tail: .list(.cons(
        position: .init(),
        head: .wildcard(
          position: .init(),
          tcType: .tuple(elements: [
            .int,
            .bool
          ])),
        tail: .list(.cons(
          position: .init(),
          head: .identifier(
            position: .init(),
            identifier: "a",
            tcType: .tuple(elements: [
              .int,
              .bool
            ])),
          tail: .list(.cons(
            position: .init(),
            head: .tuple(
              position: .init(),
              patterns: [
                .identifier(position: .init(), identifier: "b", tcType: .int),
                .identifier(position: .init(), identifier: "c", tcType: .bool)
              ],
              tcType: .tuple(elements: [
                .int,
                .bool
              ])),
            tail: .list(.empty(
              position: .init(),
              tcType: .list(element: .tuple(elements: [
                .int,
                .bool
              ])))),
            tcType: .list(element: .tuple(elements: [
              .int,
              .bool
            ])))),
          tcType: .list(element: .tuple(elements: [
            .int,
            .bool
          ])))),
        tcType: .list(element: .tuple(elements: [
          .int,
          .bool
        ])))),
      tcType: .list(element: .tuple(elements: [
        .int,
        .bool
      ])))))
    test(code: "(b, c):_:a:(1, true):[]", expected: .list(.cons(
      position: .init(),
      head: .tuple(
        position: .init(),
        patterns: [
          .identifier(position: .init(), identifier: "b", tcType: .int),
          .identifier(position: .init(), identifier: "c", tcType: .bool)
        ],
        tcType: .tuple(elements: [
          .int,
          .bool
        ])),
      tail: .list(.cons(
        position: .init(),
        head: .wildcard(
          position: .init(),
          tcType: .tuple(elements: [
            .int,
            .bool
          ])),
        tail: .list(.cons(
          position: .init(),
          head: .identifier(
            position: .init(),
            identifier: "a",
            tcType: .tuple(elements: [
              .int,
              .bool
            ])),
          tail: .list(.cons(
            position: .init(),
            head: .tuple(
              position: .init(),
              patterns: [
                .literal(position: .init(), literal: .int(1), tcType: .int),
                .literal(position: .init(), literal: .logical(.true), tcType: .bool)
              ],
              tcType: .tuple(elements: [
                .int,
                .bool
              ])),
            tail: .list(.empty(
              position: .init(),
              tcType: .list(element: .tuple(elements: [
                .int,
                .bool
              ])))),
            tcType: .list(element: .tuple(elements: [
              .int,
              .bool
            ])))),
          tcType: .list(element: .tuple(elements: [
            .int,
            .bool
          ])))),
        tcType: .list(element: .tuple(elements: [
          .int,
          .bool
        ])))),
      tcType: .list(element: .tuple(elements: [
        .int,
        .bool
      ])))))
  }
  
  func testTuplePattern() {
    testPatt(
      expected: .tuple(elements: [.bool, .string]),
      code: "(true, \"abc\")")
    testPatt(
      expected: .tuple(elements: [.bool, .tuple(elements: [.int, .bool])]),
      code: "(true, (1, false))")
  }
  
  func testNestedPatterns() {
    testPatt(
      expected: .tuple(
        elements: [
          .list(element: .int),
          .string,
          .list(element: .bool)
        ]),
      code: "(1:x:_, \"a\", true:false:xs)")
    testPatt(
      expected: .tuple(
        elements: [
          .list(element: .int),
          .string,
          .list(element: .bool)
        ]),
      code: "(x:1:_, \"a\", true:false:xs)")
    testFailingPatt(
      expected: .typeMismatch(.tailNotAList(tail: .bool)),
      code: "(x:1:true, \"a\", true:false:xs)")
  }
  
  func testRefPatterns() {
    // TODO: -
    //    testPatt(
    //      expected: .reference(.int),
    //      code: "&1")
    //    testPatt(
    //      expected: .reference(.list(element: .int)),
    //      code: "&(1:xs)")
    //    testPatt(
    //      expected: .polymorphic(typeVariables: ["b"], context: .empty, type: .reference(type: .tuple(elements: [
    //        .int, .bool, .list(element: "b")
    //      ]))),
    //      code: "&(1, true, x:xs)")
  }
  
  func testDeconstructPattern() {
    testPatt(
      expected: .simple(.typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic,
      code: "$(Just x)",
      env: .withTypeConstructor(name: "Maybe", constructors: [
        ("Just", .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])))
      ]))
    testPatt(
      expected: .simple(.typeConstructor(name: "Maybe", types: ["a"])).asPolymorphic,
      code: "$(Just _)",
      env: .withTypeConstructor(name: "Maybe", constructors: [
        ("Just", .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])))
      ]))
    testPatt(
      expected: .simple(.typeConstructor(name: "Maybe", types: [.list(element: "b")])).asPolymorphic,
      code: "$(Just (x:xs))",
      env: .withTypeConstructor(name: "Maybe", constructors: [
        ("Just", .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])))
      ]))
    testPatt(
      expected: .simple(.typeConstructor(name: "Maybe", types: [.int])).asPolymorphic,
      code: "$(Just 10)",
      env: .withTypeConstructor(name: "Maybe", constructors: [
        ("Just", .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])))
      ]))
    testPatt(
      expected: .simple(.typeConstructor(name: "Maybe", types: [
        .typeConstructor(name: "Maybe", types: [
          .typeConstructor(name: "Maybe", types: [.tuple(elements: ["a", "b"])])
        ])
      ])).asPolymorphic,
      code: "$(Just $(Just $(Just (a, b))))",
      env: .withTypeConstructor(name: "Maybe", constructors: [
        ("Just", .arrow(arg: "a", ret: .typeConstructor(name: "Maybe", types: ["a"])))
      ]))
    testBitis(
      code: """
      data C = A Int | B Bool

      f :: (C []) -> Int
      f xs = case xs of
        $(A x) -> 0
        | $(B y) -> 1
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .data(
            position: .init(),
            name: "C",
            params: .init(lifetimeParameters: [], typeParameters: []),
            constructors: [
              .init(
                position: .init(),
                name: "A",
                types: [
                  .name(
                    position: .init(),
                    name: "Int",
                    lifetimeParameter: nil,
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)))
                ],
                type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
              .init(
                position: .init(),
                name: "B",
                types: [
                  .name(
                    position: .init(),
                    name: "Bool",
                    lifetimeParameter: nil,
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)))
                ],
                type: .simple(.atomic(.bool, lifetimeSpecifier: nil)))
            ],
            tcType: .data(
              name: "C",
              constructors: [
                .init(
                  name: "A",
                  type: .simple(.arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
                .init(
                  name: "B",
                  type: .simple(.arrow(
                    arg: .atomic(.bool, lifetimeSpecifier: nil),
                    ret: .typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)))
              ],
              genericParams: .init(lifetimeParameters: [], typeParameters: []))),
          .prototype(
            position: .init(),
            name: "f",
            type: .function(
              position: .init(),
              arg: .typeConstructor(
                position: .init(),
                name: "C",
                types: [
                  
                ],
                lifetimeSpecifiers: [],
                lifetimeParameter: nil,
                tcType: .simple(.typeConstructor(
                  name: "C",
                  types: [
                    
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil))),
              ret: .name(
                position: .init(),
                name: "Int",
                lifetimeParameter: nil,
                tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .typeConstructor(
                  name: "C",
                  types: [
                    
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil),
                ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .typeConstructor(
                name: "C",
                types: [
                  
                ],
                lifetimeSpecifiers: [],
                lifetimeSpecifier: nil),
              ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f",
            cases: [
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "xs",
                    tcType: .simple(.typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil)),
                    lifetime: nil)
                ],
                body: .case(
                  position: .init(),
                  expression: .name(
                    position: .init(),
                    identifier: "xs",
                    tcType: .simple(.typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil)),
                    lifetime: nil
                  ),
                  matches: [
                    .init(
                      position: .init(),
                      pattern: .deconstruct(
                        position: .init(),
                        constructor: "A",
                        patterns: [
                          .identifier(
                            position: .init(),
                            identifier: "x",
                            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                            lifetime: nil)
                        ],
                        tcType: .simple(.typeConstructor(
                          name: "C",
                          types: [
                            
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil)),
                        lifetime: nil),
                      expression: .literal(
                        position: .init(),
                        literal: .int(0),
                        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                        lifetime: nil)),
                    .init(
                      position: .init(),
                      pattern: .deconstruct(
                        position: .init(),
                        constructor: "B",
                        patterns: [
                          .identifier(
                            position: .init(),
                            identifier: "y",
                            tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)),
                            lifetime: nil)
                        ],
                        tcType: .simple(.typeConstructor(
                          name: "C",
                          types: [
                            
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil)),
                        lifetime: nil),
                      expression: .literal(
                        position: .init(),
                        literal: .int(1),
                        tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                        lifetime: nil))
                  ],
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .typeConstructor(
                    name: "C",
                    types: [
                      
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .typeConstructor(
                name: "C",
                types: [
                  
                ],
                lifetimeSpecifiers: [],
                lifetimeSpecifier: nil),
              ret: .atomic(.numeric(.int), lifetimeSpecifier: nil),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testBitis(
      code: """
      data C = A Int (C []) | B Bool
      
      one = 1
      f :: &'A (C []) -> &'A Int
      f x = case x of
        $(A a b) -> a
        | $(B _) -> &one
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .data(
            position: .init(),
            name: "C",
            params: .init(lifetimeParameters: [], typeParameters: []),
            constructors: [
              .init(
                position: .init(),
                name: "A",
                types: [
                  .name(
                    position: .init(),
                    name: "Int",
                    lifetimeParameter: nil,
                    tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
                  .typeConstructor(
                    position: .init(),
                    name: "C",
                    types: [
                      
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeParameter: nil,
                    tcType: .simple(.typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil)))
                ],
                type: .simple(.arrow(
                  arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  ret: .typeConstructor(
                    name: "C",
                    types: [
                      
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: nil))),
              .init(
                position: .init(),
                name: "B",
                types: [
                  .name(
                    position: .init(),
                    name: "Bool",
                    lifetimeParameter: nil,
                    tcType: .simple(.atomic(.bool, lifetimeSpecifier: nil)))
                ],
                type: .simple(.atomic(.bool, lifetimeSpecifier: nil)))
            ],
            tcType: .data(
              name: "C",
              constructors: [
                .init(
                  name: "A",
                  type: .simple(.arrow(
                    arg: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    ret: .arrow(
                      arg: .typeConstructor(
                        name: "C",
                        types: [
                          
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      ret: .typeConstructor(
                        name: "C",
                        types: [
                          
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
                .init(
                  name: "B",
                  type: .simple(.arrow(
                    arg: .atomic(.bool, lifetimeSpecifier: nil),
                    ret: .typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)))
              ],
              genericParams: .init(lifetimeParameters: [], typeParameters: []))),
          .binding(
            position: .init(),
            name: "one",
            cases: [
              .init(
                name: "one",
                position: .init(),
                parameters: [],
                body: .literal(
                  position: .init(),
                  literal: .int(1),
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil),
          .prototype(
            position: .init(),
            name: "f",
            type: .function(
              position: .init(),
              arg: .ref(
                position: .init(),
                type: .typeConstructor(
                  position: .init(),
                  name: "C",
                  types: [
                    
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeParameter: nil,
                  tcType: .simple(.typeConstructor(
                    name: "C",
                    types: [
                      
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil))),
                lifetimeParameter: "'A",
                tcType:
                    .simple(.reference(
                      type: .typeConstructor(
                        name: "C",
                        types: [
                          
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A"))),
              ret: .ref(
                position: .init(),
                type: .name(
                  position: .init(),
                  name: "Int",
                  lifetimeParameter: nil,
                  tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil))),
                lifetimeParameter: "'A",
                tcType:
                    .simple(.reference(
                      type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A"))),
              lifetimeParameter: nil,
              tcType: .simple(.arrow(
                arg: .reference(
                  type: .typeConstructor(
                    name: "C",
                    types: [
                      
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                ret: .reference(
                  type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil))),
            tcType: .simple(.arrow(
              arg: .reference(
                type: .typeConstructor(
                  name: "C",
                  types: [
                    
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .reference(
                type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "f",
            cases: [
              .init(
                name: "f",
                position: .init(),
                parameters: [
                  .identifier(
                    position: .init(),
                    identifier: "x",
                    tcType: .simple(.reference(
                      type: .typeConstructor(
                        name: "C",
                        types: [
                          
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil)
                ],
                body: .case(
                  position: .init(),
                  expression: .name(
                    position: .init(),
                    identifier: "x",
                    tcType: .simple(.reference(
                      type: .typeConstructor(
                        name: "C",
                        types: [
                          
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A")),
                    lifetime: nil
                  ),
                  matches: [
                    .init(
                      position: .init(),
                      pattern: .deconstruct(
                        position: .init(),
                        constructor: "A",
                        patterns: [
                          .identifier(
                            position: .init(),
                            identifier: "a",
                            tcType: .simple(.reference(
                              type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A")),
                            lifetime: nil),
                          .identifier(
                            position: .init(),
                            identifier: "b",
                            tcType: .simple(.reference(
                              type: .typeConstructor(
                                name: "C",
                                types: [
                                  
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A")),
                            lifetime: nil)
                        ],
                        tcType: .simple(.reference(
                          type: .typeConstructor(
                            name: "C",
                            types: [
                              
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                        lifetime: nil),
                      expression: .name(
                        position: .init(),
                        identifier: "a",
                        tcType: .simple(.reference(
                          type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                        lifetime: nil
                      )),
                    .init(
                      position: .init(),
                      pattern: .deconstruct(
                        position: .init(),
                        constructor: "B",
                        patterns: [
                          .wildcard(
                            position: .init(),
                            tcType: .simple(.reference(
                              type: .atomic(.bool, lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A")))
                        ],
                        tcType: .simple(.reference(
                          type: .typeConstructor(
                            name: "C",
                            types: [
                              
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                        lifetime: nil),
                      expression: .ref(
                        position: .init(),
                        expression: .name(
                          position: .init(),
                          identifier: "one",
                          tcType: .simple(.atomic(.numeric(.int), lifetimeSpecifier: nil)),
                          lifetime: nil
                        ),
                        tcType: .simple(.reference(
                          type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                          lifetimeSpecifier: nil)),
                        lifetime: nil))
                  ],
                  tcType: .simple(.reference(
                    type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .simple(.arrow(
                  arg: .reference(
                    type: .typeConstructor(
                      name: "C",
                      types: [
                        
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .reference(
                    type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .simple(.arrow(
              arg: .reference(
                type: .typeConstructor(
                  name: "C",
                  types: [
                    
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              ret: .reference(
                type: .atomic(.numeric(.int), lifetimeSpecifier: nil),
                lifetimeSpecifier: "'A"),
              lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]))
    testBitis(
      code: """
      data List [a] = Cons a (List [a]) | Nil ()
      tl :: &'A (List [a]) -> &'A (List [a])
      tl = \\xs -> case xs of
            $(Cons x xss) -> xss
      """,
      expected: .init( // NOTE: - Generated with XCTestDump
        bindings: [
          .data(
            position: .init(),
            name: "List",
            params: .init(lifetimeParameters: [], typeParameters: ["a"]),
            constructors: [
              .init(
                position: .init(),
                name: "Cons",
                types: [
                  .name(
                    position: .init(),
                    name: "a",
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeVariable(name: "a"))),
                  .typeConstructor(
                    position: .init(),
                    name: "List",
                    types: [
                      .name(
                        position: .init(),
                        name: "a",
                        lifetimeParameter: nil,
                        tcType: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeVariable(name: "a")))
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeParameter: nil,
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil)))
                ],
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .typeVariable(name: "a"),
                    ret: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: nil))),
              .init(
                position: .init(),
                name: "Nil",
                types: [
                  .tuple(
                    position: .init(),
                    types: [
                      
                    ],
                    lifetimeParameter: nil,
                    tcType: .simple(.unit))
                ],
                type: .simple(.unit))
            ],
            tcType: .data(
              name: "List",
              constructors: [
                .init(
                  name: "Cons",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .typeVariable(name: "a"),
                      ret: .arrow(
                        arg: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        ret: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil))),
                .init(
                  name: "Nil",
                  type: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .unit,
                      ret: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: nil)))
              ],
              genericParams: .init(lifetimeParameters: [], typeParameters: ["a"]))),
          .prototype(
            position: .init(),
            name: "tl",
            type: .function(
              position: .init(),
              arg: .ref(
                position: .init(),
                type: .typeConstructor(
                  position: .init(),
                  name: "List",
                  types: [
                    .name(
                      position: .init(),
                      name: "a",
                      lifetimeParameter: nil,
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .typeVariable(name: "a")))
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeParameter: nil,
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil))),
                lifetimeParameter: "'A",
                tcType:
                    .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A"))),
              ret: .ref(
                position: .init(),
                type: .typeConstructor(
                  position: .init(),
                  name: "List",
                  types: [
                    .name(
                      position: .init(),
                      name: "a",
                      lifetimeParameter: nil,
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .typeVariable(name: "a")))
                  ],
                  lifetimeSpecifiers: [],
                  lifetimeParameter: nil,
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil))),
                lifetimeParameter: "'A",
                tcType:
                    .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A"))),
              lifetimeParameter: nil,
              tcType: .polymorphic(
                typeVariables: ["a"],
                context: .empty,
                type: .arrow(
                  arg: .reference(
                    type: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  ret: .reference(
                    type: .typeConstructor(
                      name: "List",
                      types: [
                        .typeVariable(name: "a")
                      ],
                      lifetimeSpecifiers: [],
                      lifetimeSpecifier: nil),
                    lifetimeSpecifier: "'A"),
                  lifetimeSpecifier: nil))),
            tcType: .polymorphic(
              typeVariables: ["a"],
              context: .empty,
              type: .arrow(
                arg: .reference(
                  type: .typeConstructor(
                    name: "List",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                ret: .reference(
                  type: .typeConstructor(
                    name: "List",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil))),
          .binding(
            position: .init(),
            name: "tl",
            cases: [
              .init(
                name: "tl",
                position: .init(),
                parameters: [],
                body: .lambda(
                  position: .init(),
                  parameter: .identifier(
                    position: .init(),
                    identifier: "xs",
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                    lifetime: nil),
                  body: .case(
                    position: .init(),
                    expression: .name(
                      position: .init(),
                      identifier: "xs",
                      tcType: .polymorphic(
                        typeVariables: ["a"],
                        context: .empty,
                        type: .reference(
                          type: .typeConstructor(
                            name: "List",
                            types: [
                              .typeVariable(name: "a")
                            ],
                            lifetimeSpecifiers: [],
                            lifetimeSpecifier: nil),
                          lifetimeSpecifier: "'A")),
                      lifetime: nil
                    ),
                    matches: [
                      .init(
                        position: .init(),
                        pattern: .deconstruct(
                          position: .init(),
                          constructor: "Cons",
                          patterns: [
                            .identifier(
                              position: .init(),
                              identifier: "x",
                              tcType: .polymorphic(
                                typeVariables: ["a"],
                                context: .empty,
                                type: .reference(
                                  type: .typeVariable(name: "a"),
                                  lifetimeSpecifier: "'A")),
                              lifetime: nil),
                            .identifier(
                              position: .init(),
                              identifier: "xss",
                              tcType: .polymorphic(
                                typeVariables: ["a"],
                                context: .empty,
                                type: .reference(
                                  type: .typeConstructor(
                                    name: "List",
                                    types: [
                                      .typeVariable(name: "a")
                                    ],
                                    lifetimeSpecifiers: [],
                                    lifetimeSpecifier: nil),
                                  lifetimeSpecifier: "'A")),
                              lifetime: nil)
                          ],
                          tcType: .polymorphic(
                            typeVariables: ["a"],
                            context: .empty,
                            type: .reference(
                              type: .typeConstructor(
                                name: "List",
                                types: [
                                  .typeVariable(name: "a")
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A")),
                          lifetime: nil),
                        expression: .name(
                          position: .init(),
                          identifier: "xss",
                          tcType: .polymorphic(
                            typeVariables: ["a"],
                            context: .empty,
                            type: .reference(
                              type: .typeConstructor(
                                name: "List",
                                types: [
                                  .typeVariable(name: "a")
                                ],
                                lifetimeSpecifiers: [],
                                lifetimeSpecifier: nil),
                              lifetimeSpecifier: "'A")),
                          lifetime: nil
                        ))
                    ],
                    tcType: .polymorphic(
                      typeVariables: ["a"],
                      context: .empty,
                      type: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A")),
                    lifetime: nil),
                  tcType: .polymorphic(
                    typeVariables: ["a"],
                    context: .empty,
                    type: .arrow(
                      arg: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A"),
                      ret: .reference(
                        type: .typeConstructor(
                          name: "List",
                          types: [
                            .typeVariable(name: "a")
                          ],
                          lifetimeSpecifiers: [],
                          lifetimeSpecifier: nil),
                        lifetimeSpecifier: "'A"),
                      lifetimeSpecifier: nil)),
                  lifetime: nil),
                type: .polymorphic(
                  typeVariables: ["a"],
                  context: .empty,
                  type: .arrow(
                    arg: .reference(
                      type: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A"),
                    ret: .reference(
                      type: .typeConstructor(
                        name: "List",
                        types: [
                          .typeVariable(name: "a")
                        ],
                        lifetimeSpecifiers: [],
                        lifetimeSpecifier: nil),
                      lifetimeSpecifier: "'A"),
                    lifetimeSpecifier: nil)),
                lifetime: nil)
            ],
            tcType: .polymorphic(
              typeVariables: ["a"],
              context: .empty,
              type: .arrow(
                arg: .reference(
                  type: .typeConstructor(
                    name: "List",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                ret: .reference(
                  type: .typeConstructor(
                    name: "List",
                    types: [
                      .typeVariable(name: "a")
                    ],
                    lifetimeSpecifiers: [],
                    lifetimeSpecifier: nil),
                  lifetimeSpecifier: "'A"),
                lifetimeSpecifier: nil)),
            lifetime: nil,
            borrowLifetime: nil)
        ]
                     ))
  }
}

fileprivate extension Bitis.Env {
  static func withTypeConstructor(
    name: String,
    constructors: [(String, TCType.SimpleType)]
  ) -> Bitis.Env {
    var env = Bitis.Env.empty
    env.lte = env.lte.bind(name: name, with: .simple(.typeConstructor(name: "Maybe", types: constructors.map { $0.1 })).asPolymorphic)
    for c in constructors {
      env = env.withPrototype(name: c.0, type: .simple(c.1))
    }
    return env
  }
}

// MARK: - Types
extension TypeCheckerTests {
  func testNamedType() {
    testType(expected: .int, code: "Int")
    testType(expected: .float, code: "Float")
    testType(expected: .bool, code: "Bool")
    testType(expected: .char, code: "Char")
    testType(expected: .string, code: "String")
  }
  
  func testUnit() {
    testType(expected: .unit, code: "()")
  }
  
  func testListType() {
    testType(expected: .list(element: .bool), code: "[Bool]")
    testType(expected: .list(element: .list(element: .bool)), code: "[[Bool]]")
  }
  
  func testTupleType() {
    testType(
      expected: .tuple(elements: [.int, .string]),
      code: "(Int, String)")
    testType(
      expected: .tuple(elements: [
        .int,
        .tuple(elements: [
          .string,
          .bool
        ])
      ]),
      code: "(Int, (String, Bool))")
  }
  
  func testFunctionType() {
    testType(
      expected: .arrow(
        arg: .unit,
        ret: .bool),
      code: "() -> Bool")
    testType(
      expected: .arrow(
        arg: .int,
        ret: .bool),
      code: "Int -> Bool")
    testType(
      expected: .arrow(
        arg: .int,
        ret: .arrow(
          arg: .bool,
          ret: .string)),
      code: "Int -> Bool -> String")
    testType(
      expected: .arrow(
        arg: .arrow(
          arg: .bool,
          ret: .string),
        ret: .int),
      code: "(Bool -> String) -> Int")
  }
  
  func testPolymorphicTypes() {
    // ∀a.a
    testType(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .typeVariable(name: "a")),
      code: "a")
    // ∀a.[a]
    testType(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .list(element: .typeVariable(name: "a"))),
      code: "[a]")
    // ∀a.(a, a)
    testType(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .tuple(elements: [
        .typeVariable(name: "a"),
        .typeVariable(name: "a")
      ])),
      code: "(a, a)")
    // ∀abc.(a, b, c)
    testType(
      expected: .polymorphic(typeVariables: ["a", "b", "c"], context: .empty, type: .tuple(elements: [
        .typeVariable(name: "a"),
        .typeVariable(name: "b"),
        .typeVariable(name: "c"),
      ])),
      code: "(a, b, c)")
    // ∀a.a -> a
    testType(
      expected: .polymorphic(typeVariables: ["a"], context: .empty, type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .typeVariable(name: "a"))),
      code: "a -> a")
    // ∀ab.a -> b
    testType(
      expected: .polymorphic(typeVariables: ["a", "b"], context: .empty, type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .typeVariable(name: "b"))),
      code: "a -> b")
    // ∀ab.a -> (a, [b])
    testType(
      expected: .polymorphic(typeVariables: ["a", "b"], context: .empty, type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .tuple(elements: [
          .typeVariable(name: "a"),
          .list(element: .typeVariable(name: "b"))
        ]))),
      code: "a -> (a, [b])")
    // ∀ab.[a -> Bool] -> (a, [b])
    testType(
      expected: .polymorphic(typeVariables: ["a", "b"], context: .empty, type: .arrow(
        arg: .list(element: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .bool)),
        ret: .tuple(elements: [
          .typeVariable(name: "a"),
          .list(element: .typeVariable(name: "b"))
        ]))),
      code: "[a -> Bool] -> (a, [b])")
    testType(
      expected: TCType.arrow(arg: .arrow(
        arg: .unit,
        ret: .typeVariable(name: "a")),
                             ret: .typeVariable(name: "a")).asPolymorphic,
      code: "(() -> a) -> a")
  }
  
  func testNestedTypes() {
    testType(
      expected: .polymorphic(
        typeVariables: ["a"],
        context: .empty,
        type: .arrow(
          arg: .list(
            element: .tuple(
              elements: [
                .arrow(arg: .bool, ret: .string),
                .list(element: .typeVariable(name: "a"))
              ])),
          ret: .tuple(
            elements: [
              .list(element: .arrow(arg: .int, ret: .string)),
              .typeVariable(name: "a")
            ]))),
      code: "[(Bool -> String, [a])] -> ([Int -> String], a)")
  }
}

extension TypeCheckerTests {
  func testMatchTypeWithPattern() {
    let result = TCType.SimpleType.tuple(elements: [
      .list(element: .tuple(elements: [
        .int,
        .bool
      ])),
      .string
    ]).matchWithPattern(.tuple(position: .zero, patterns: [
      .list(.cons(
        position: .zero,
        head: .identifier(position: .zero, identifier: "x"),
        tail: .list(
          .cons(
            position: .zero,
            head: .tuple(position: .zero, patterns: [
              .identifier(position: .zero, identifier: "a"),
              .identifier(position: .zero, identifier: "b"),
            ]),
            tail: .identifier(position: .zero, identifier: "xs"))))),
      .identifier(position: .zero, identifier: "s")
    ]))
    switch result {
    case .success(let names):
      XCTAssertEqual(.tuple(elements: [.int, .bool]), names.first { $0.0 == "x" }?.1)
      XCTAssertEqual(.int, names.first { $0.0 == "a" }?.1)
      XCTAssertEqual(.bool, names.first { $0.0 == "b" }?.1)
      XCTAssertEqual(.list(element: .tuple(elements: [.int, .bool])), names.first { $0.0 == "xs" }?.1)
      XCTAssertEqual(.string, names.first { $0.0 == "s" }?.1)
    case .failure:
      XCTFail()
    }
    XCTAssertIsSuccess(TCType.SimpleType.unit.matchWithPattern(.tuple(position: .init(), patterns: [], tcType: .unit)))
  }
}

extension TypeCheckerTests {
  func testMatchPolymorphicTypesWithoutTypeClasses() {
    // ()
    // ()
    XCTAssertIsSuccess(TCType.unit.matchWith(type: .unit, strict: true))
    // a
    // Int
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .typeVariable(name: "a"))
                        .matchWith(type: .int, strict: true))
    // Int
    // a
    XCTAssertIsFailure(TCType.int
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .typeVariable(name: "a")),
                                   strict: true))
    // a
    // (Int, Bool)
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .typeVariable(name: "a"))
                        .matchWith(type: .tuple(elements: [.int, .bool]),
                                   strict: true))
    // [a]
    // [Int]
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .list(element: .typeVariable(name: "a")))
                        .matchWith(
                          type: .list(element: .int),
                          strict: true),
                       expected: .list(element: .int))
    // (Int, a)
    // (c, String)
    XCTAssertIsSuccess(TCType
                        .tuple(elements: [.int, "a"]).asPolymorphic
                        .matchWith(
                          type: TCType.tuple(elements: ["c", .string]).asPolymorphic,
                          strict: false))
    // (Int, a)
    // (c, String)
    XCTAssertIsFailure(TCType
                        .tuple(elements: [.int, "a"]).asPolymorphic
                        .matchWith(
                          type: TCType.tuple(elements: ["c", .string]).asPolymorphic,
                          strict: true))
    
    ///
    
    // a -> a
    // a -> Int
    XCTAssertIsSuccess(TCType
                        .arrow(arg: .typeVariable(name: "a"), ret: .typeVariable(name: "a")).asPolymorphic
                        .matchWith(
                          type: TCType.arrow(arg: .typeVariable(name: "a"), ret: .int).asPolymorphic,
                          strict: true))
    // (Int, Int) -> Bool
    // (a, Int) -> Bool
    XCTAssertIsFailure(TCType
                        .arrow(arg: .tuple(elements: [.int, .int]), ret: .bool)
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .arrow(arg: .tuple(elements: [.typeVariable(name: "a"), .int]), ret: .bool)),
                                   strict: true))
    // (Int, Int) -> Bool
    // (a, Int) -> Bool
    XCTAssertIsSuccess(TCType
                        .arrow(arg: .tuple(elements: [.int, .int]), ret: .bool)
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .arrow(arg: .tuple(elements: [.typeVariable(name: "a"), .int]), ret: .bool)),
                                   strict: false))
    // a -> b -> Bool
    // e -> f -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .arrow(
          arg: .typeVariable(name: "b"),
          ret: .bool)))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["e", "f"],
                            context: .empty,
                            type: .arrow(
                              arg: .typeVariable(name: "e"),
                              ret: .arrow(
                                arg: .typeVariable(name: "f"),
                                ret: .bool))),
                          strict: true))
    // a -> b -> Bool
    // e -> e -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .arrow(
          arg: .typeVariable(name: "b"),
          ret: .bool)))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["e"],
                            context: .empty,
                            type: .arrow(
                              arg: .typeVariable(name: "e"),
                              ret: .arrow(
                                arg: .typeVariable(name: "e"),
                                ret: .bool))),
                          strict: true))
    // a -> a -> Bool
    // e -> f -> Bool
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .bool)))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["e", "f"],
                            context: .empty,
                            type: .arrow(
                              arg: .typeVariable(name: "e"),
                              ret: .arrow(
                                arg: .typeVariable(name: "f"),
                                ret: .bool))),
                          strict: true))
    // a -> b -> Bool
    // Int -> Int -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .arrow(
          arg: .typeVariable(name: "b"),
          ret: .bool)))
                        .matchWith(
                          type: .arrow(
                            arg: .int,
                            ret: .arrow(
                              arg: .int,
                              ret: .bool)),
                          strict: true))
    // (a -> b) -> a -> b
    // (Bool -> c) -> Bool -> c
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["c"],
                            context: .empty,
                            type: .arrow(
                              arg: .arrow(
                                arg: .bool,
                                ret: .typeVariable(name: "c")),
                              ret: .arrow(
                                arg: .bool,
                                ret: .typeVariable(name: "c")))),
                          strict: true))
    // (a -> b) -> a -> b
    // (Bool -> c) -> Bool -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["c"],
                            context: .empty,
                            type: .arrow(
                              arg: .arrow(
                                arg: .bool,
                                ret: .typeVariable(name: "c")),
                              ret: .arrow(
                                arg: .bool,
                                ret: .bool))),
                          strict: true))
    // (a -> b) -> a -> b
    // (Bool -> c) -> c -> c
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["c"],
                            context: .empty,
                            type: .arrow(
                              arg: .arrow(
                                arg: .bool,
                                ret: .typeVariable(name: "c")),
                              ret: .arrow(
                                arg: .typeVariable(name: "c"),
                                ret: .bool))),
                          strict: true))
    // (a -> b) -> a -> b
    // e -> f -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["e", "f"],
                            context: .empty,
                            type: .arrow(
                              arg: .typeVariable(name: "e"),
                              ret: .arrow(
                                arg: .typeVariable(name: "f"),
                                ret: .bool))),
                          strict: false))
    // e -> f -> Bool
    // (a -> b) -> a -> b
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["e", "f"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "e"),
        ret: .arrow(
          arg: .typeVariable(name: "f"),
          ret: .bool)))
                        .matchWith(
                          type: .polymorphic(
                            typeVariables: ["a", "b"],
                            context: .empty,
                            type: .arrow(
                              arg: .arrow(
                                arg: .typeVariable(name: "a"),
                                ret: .typeVariable(name: "b")),
                              ret: .arrow(
                                arg: .typeVariable(name: "a"),
                                ret: .typeVariable(name: "b")))),
                          strict: true))
    // [a] -> a -> a
    // [e] -> e -> e
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .arrow(
        arg: .list(element: .typeVariable(name: "a")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "a"))))
                        .matchWith(
                          type:
                              .polymorphic(
                                typeVariables: ["e"],
                                context: .empty,
                                type: .arrow(
                                  arg: .list(element: .typeVariable(name: "e")),
                                  ret: .arrow(
                                    arg: .typeVariable(name: "e"),
                                    ret: .typeVariable(name: "e")))),
                          strict: true
                        ))
    // [a] -> a -> a
    // [Bool] -> Bool -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .arrow(
        arg: .list(element: .typeVariable(name: "a")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "a"))))
                        .matchWith(
                          type: .arrow(
                            arg: .list(element: .bool),
                            ret: .arrow(
                              arg: .bool,
                              ret: .bool)),
                          strict: true))
    // [a] -> a -> a
    // [Bool] -> e -> Bool
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .arrow(
        arg: .list(element: .typeVariable(name: "a")),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "a"))))
                        .matchWith(
                          type:
                              .polymorphic(
                                typeVariables: ["e"],
                                context: .empty,
                                type: .arrow(
                                  arg: .list(element: .bool),
                                  ret: .arrow(
                                    arg: .typeVariable(name: "e"),
                                    ret: .bool))),
                          strict: true))
    // [a -> b] -> a -> b
    // [Int -> Bool] -> Int -> Bool
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .list(element: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .arrow(
                            arg: .list(element: .arrow(
                              arg: .int,
                              ret: .bool)),
                            ret: .arrow(
                              arg: .int,
                              ret: .bool)),
                          strict: true))
    // [a -> b] -> a -> b
    // [Int -> Bool] -> String -> Bool
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .list(element: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))),
        ret: .arrow(
          arg: .typeVariable(name: "a"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(
                          type: .arrow(
                            arg: .list(element: .arrow(
                              arg: .int,
                              ret: .bool)),
                            ret: .arrow(
                              arg: .string,
                              ret: .bool)),
                          strict: true))
    // [(a, [b -> c]) -> [(a, b, a -> c)]
    // [(Bool, [e -> String]) -> [(Bool, e, Bool -> String)]
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b", "c"],
      context: .empty,
      type: .arrow(
        arg: .list(
          element: .tuple(elements: [
            .typeVariable(name: "a"),
            .list(element: .arrow(
              arg: .typeVariable(name: "b"),
              ret: .typeVariable(name: "c"))
                 )
          ])
        ),
        ret: .list(
          element: .tuple(elements: [
            .typeVariable(name: "a"),
            .typeVariable(name: "b"),
            .arrow(
              arg: .typeVariable(name: "a"),
              ret: .typeVariable(name: "c"))
          ])
        )))
                        .matchWith(type: .polymorphic(
                          typeVariables: ["e"],
                          context: .empty,
                          type: .arrow(
                            arg: .list(
                              element: .tuple(elements: [
                                .bool,
                                .list(element: .arrow(
                                  arg: .typeVariable(name: "e"),
                                  ret: .string)
                                     )
                              ])
                            ),
                            ret: .list(
                              element: .tuple(elements: [
                                .bool,
                                .typeVariable(name: "e"),
                                .arrow(
                                  arg: .bool,
                                  ret: .string)
                              ])
                            ))),
                                   strict: true))
    // [(a, [b -> c]) -> [(a, b, a -> c)]
    // [(Bool, [e -> String]) -> [(Bool, e, Char -> String)]
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a", "b", "c"],
      context: .empty,
      type: .arrow(
        arg: .list(
          element: .tuple(elements: [
            .typeVariable(name: "a"),
            .list(element: .arrow(
              arg: .typeVariable(name: "b"),
              ret: .typeVariable(name: "c"))
                 )
          ])
        ),
        ret: .list(
          element: .tuple(elements: [
            .typeVariable(name: "a"),
            .typeVariable(name: "b"),
            .arrow(
              arg: .typeVariable(name: "a"),
              ret: .typeVariable(name: "c"))
          ])
        )))
                        .matchWith(type: .polymorphic(
                          typeVariables: ["e"],
                          context: .empty,
                          type: .arrow(
                            arg: .list(
                              element: .tuple(elements: [
                                .bool,
                                .list(element: .arrow(
                                  arg: .typeVariable(name: "e"),
                                  ret: .string)
                                     )
                              ])
                            ),
                            ret: .list(
                              element: .tuple(elements: [
                                .bool,
                                .typeVariable(name: "e"),
                                .arrow(
                                  arg: .char,
                                  ret: .string)
                              ])
                            ))),
                                   strict: true))
    // a -> (b -> b)
    // a -> b
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: .typeVariable(name: "a"),
        ret: .arrow(
          arg: .typeVariable(name: "b"),
          ret: .typeVariable(name: "b"))))
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a", "b"],
                          context: .empty,
                          type: .arrow(
                            arg: .typeVariable(name: "a"),
                            ret: .typeVariable(name: "b"))),
                                   strict: true))
    // a -> b
    // a -> (b -> b)
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .arrow(
        arg: "a",
        ret: "b"))
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a", "b"],
                          context: .empty,
                          type: .arrow(
                            arg: "a",
                            ret: .arrow(
                              arg: "b",
                              ret: "b"))),
                                   strict: true))
    // &a
    // &Int
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .reference(type: "a"))
                        .matchWith(type: .reference(.int), strict: false))
    // &[a]
    // &[Int]
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .reference(type: .list(element: "a")))
                        .matchWith(type: .reference(.list(element: .int)), strict: true))
    // &(a, b)
    // &(Int, Int)
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .reference(type: .tuple(elements: ["a", "b"])))
                        .matchWith(
                          type: .reference(.tuple(elements: [.int, .int])),
                          strict: true))
    // &(a -> b)
    // &(Int -> Int)
    XCTAssertIsSuccess(TCType.polymorphic(
      typeVariables: ["a", "b"],
      context: .empty,
      type: .reference(type: .arrow(arg: "a", ret: "b")))
                        .matchWith(
                          type: .reference(.arrow(arg: .int, ret: .int)),
                          strict: true))
    // Int
    // &a
    XCTAssertIsFailure(TCType.int
                        .matchWith(type: .polymorphic(
                          typeVariables: ["a"],
                          context: .empty,
                          type: .reference(type: "a")), strict: false))
    // &[a]
    // &Int
    XCTAssertIsFailure(TCType.polymorphic(
      typeVariables: ["a"],
      context: .empty,
      type: .list(element: "a"))
                        .matchWith(type: .reference(.int), strict: true))
  }
}

public func XCTAssertIsSuccess<A, E: Error>(
  _ result: Result<A, E>,
  line: Int = #line
) {
  switch result {
  case .success:
    break
  case .failure:
    print(line)
    XCTFail()
  }
}
public func XCTAssertIsSuccess<A: Equatable, E: Error>(
  _ result: Result<A, E>,
  expected: A,
  line: Int = #line
) {
  switch result {
  case .success(let result):
    XCTAssertEqual(expected, result)
  case .failure:
    print(line)
    XCTFail()
  }
}

public func XCTAssertIsFailure<A, E: Error>(
  _ result: Result<A, E>,
  line: Int = #line
) {
  switch result {
  case .success:
    print(line)
    XCTFail()
  case .failure:
    break
  }
}

internal extension TCType {
  static func reference(_ type: SimpleType) -> TCType {
    .reference(type, lifetimeSpecifier: nil)
  }
}

internal extension TCType.SimpleType {
  static func reference(type: TCType.SimpleType) -> Self {
    .reference(type: type, lifetimeSpecifier: nil)
  }
  
  static func typeConstructor(name: String, types: [TCType.SimpleType]) -> Self {
    .typeConstructor(name: name, types: types, lifetimeSpecifiers: [])
  }
}

