//
//  LifetimeAnalyserTests.swift
//  
//
//  Created by Toni K. Turk on 01/08/2021.
//

@testable import Bitis
import XCTest
import Bow

class LifetimeAnalyserTests: XCTestCase {}

extension LifetimeAnalyserTests {
  func testPrototypesAndLifetimes() {
    testBitisFailing(
      code: """
      f :: () -> &Int
      """,
      error: .referenceWithoutLifetimeSpecifier)
    testBitisFailing(
      code: """
      f :: (&a, &b) -> Int
      """,
      error: .referenceWithoutLifetimeSpecifier)
    testBitis(
      code: """
      f :: () -> &'A Int
      """)
    testBitis(
      code: """
      f :: (&'A a, &'B b) -> Int
      """)
    testBitisFailing(
      code: """
      f _ = let
        y = 10
        x :: &'A Int
        x = &y
      in x
      """,
      error: .referenceWithLifetimeSpecifier)
    testBitis(
      code: """
      f :: &'A a -> &'A a
      f a = a
      a = let
        x :: &Int
        y = 10
        x = f (&y)
      in 10
      """)
  }
  
  // TODO: - Fix
//  func testIncompatibleLifetimes() {
//    testBitisFailing(
//      code: """
//      f :: &'STATIC a -> Int
//      f _ = 10
//      g :: &'A a -> Int
//      g x = f x
//      """,
//      error: .incompatibleLifetimes(
//        expected: .static,
//        got: .local("ca")))
//    testBitisFailing(
//      code: """
//      f :: &'A a -> &'A a -> &'A a
//      f a b = a
//
//      x = 10
//      y = let
//      b = 20
//      c = f (&b) (&x)
//      in 10
//      """,
//      error: .incompatibleLifetimes(
//        expected: .local("ba"),
//        got: .static))
//    testBitisFailing(
//      code: """
//      f :: &'A a -> &'A a -> &'A a
//      f a b = a
//
//      x = 10
//      y = let
//      b = 20
//      c = f (&x) (&b)
//      in 10
//      """,
//      error: .incompatibleLifetimes(
//        expected: .static,
//        got: .local("ba")))
//    // TODO: -
//    testBitisFailing(
//      code: """
//      data R ['REF] = C
//
//      f :: (R ['B]) -> &'B a -> Int
//      f _ _ = 10
//
//      a = let
//        y = C ()
//        b = let
//          z = 30
//        in f y (&z)
//      in 20
//      """,
//      error: .incompatibleLifetimes(
//        expected: .local("ba"),
//        got: .local("baba")))
//  }
  
  func testCannotReferenceLiteral() {
    testFailingExpression(code: "&10", expected: .cannotReferenceLiteral)
    testFailingExpression(code: "&true", expected: .cannotReferenceLiteral)
    testFailingExpression(code: "&\"abc\"", expected: .cannotReferenceLiteral)
    testFailingExpression(code: "&()", expected: .cannotReferenceLiteral)
  }
  
  func testCannotReferenceLocalValue() {
    testFailingBinding(
      code: "g x y = &x",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = &(x, y)",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = &(let in x)",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = &(let in &x)",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = &(let in let in (x, y))",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = (10, &(if true then y else y))",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = (let in &x, if true then &y else &y)",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = &[x, y]",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = (&x, &y)",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = [&x, &y]",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = [(10, &(let in \\_ -> x))]",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = \\a -> &x",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g x y = let f a b = &(a, b) in 10",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g _ = let x = 10, ff _ = &x in ff",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g (x:xs) = &xs",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g (a, b) = &a",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g = \\x -> &x",
      expected: .cannotReferenceLocalValue)
    testFailingBinding(
      code: "g = \\x -> \\y -> &(x, y)",
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A a -> &'A a
      f a = a
      h x = f (&x)
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A a -> &'A a
      f a = a
      
      g :: &'A a -> &'A a
      g a = a
      
      add :: &'A a -> &'A a -> &'A a
      add a b = a
      
      h x = add (f (&x)) (g (&x))
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      abc = let
        f :: &'A Int -> &'A Int
        f a = a
        x = 10
        y = f (&x)
      in y
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      x = let
        a = 10
        f () = &a
      in f
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A Bool -> &'A Bool -> &'A Bool -> &'A Bool
      f a b c = if a then b else c
      
      x = let
        a = true
        b = true
        c = true
        d = f (&a) (&b) (&c)
      in d
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A a -> &'B a -> &'B a
      f _ b = b
      
      x = let
        a = 10
      in let
          b = 20
        in f (&a) (&b)
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A a -> &'B a -> &'A a
      f a b = b
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f :: &'A a -> &'B b -> (&'A a, &'A b)
      f a b = (a, b)
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      i :: (&'A a, &'B b) -> &'C a -> (&'A a, &'C a)
      i (a, b) c = (a, b)
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      g :: &'A a -> &'B a -> &'B a
      g a b = b
      
      f :: &'A a -> &'B a -> &'A a
      f a b = g a b
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      a = let
        x = true
        z = if true then x else &x
      in z
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data Ref ['A] = R (&'A Int)
      x = let
        a = 1
        b = R (&a)
      in b
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data Ref ['A, a] = R (&'A a) | N
      f :: () -> (Ref ['A, Int])
      f () = let
        x = 10
        y :: (Ref [Int])
        y = if true then N () else R (&x)
      in y
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data Ref ['A, a] = R (&'A a) | N
      
      g :: &'A a -> 'A (Ref ['A, a])
      g a = R a
      
      f :: () -> 'A (Ref ['A, Int])
      f () = let
        x = 10
        y = g (&x)
      in y
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef
      xs = let
        a = 1
        xs = ConsRef (&a) (NilRef ())
      in xs
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data Tree ['A] = Node (&'A (Tree ['A])) (&'A (Tree ['A])) | Leaf
      a = let
        x :: (Tree [])
        x = Leaf ()
        
        y :: (Tree [])
        y = Node (&x) (&x)
        
        z :: (Tree [])
        z = Node (&y) (&x)
      in z
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      data Mix ['A] = A 'A (Mix ['A]) | B &'A Int
      f () = let
        a :: Int
        a = 10
        x :: (Mix [])
        y :: (Mix [])
        y = B (&a)
        x = A y
      in x
      """,
      expected: .cannotReferenceLocalValue)
    
    ///
    
    testBitis(code: """
    x = 10
    y = &x
    """)
    testBitis(code: """
    y = 10
    x = &(let
      a :: Int
      b :: Int
      a = b
      b = a
    in y)
    """)
    testBitis(code: """
    f :: &'A a -> ()
    f _ = ()
    x = let
      a = 10
    in f (&a)
    """)
    testBitis(code: """
    x = let
      a = 10
      b = &a
    in a
    """)
    testBitis(code: """
    x :: Int
    y :: Int
    x = (\\y -> y) 10
    y = x
    """)
    testBitis(code: """
    y = 1000
    x = let
      a = 10
      b = &a
    in &y
    """)
    testBitis(code: """
    f :: &'A Int -> Int
    f x = *x
    """)
    testBitis(code: """
    f :: (&'A a, &'A b) -> ()
    f _ = ()
    x = let
      a = 10
      b = "abc"
    in f (&a, &b)
    """)
    testBitis(code: """
    f :: &'A a -> ()
    f _ = ()
    x = let
      a = 10
      b = 20
    in f (if true then &a else &b)
    """)
    testBitis(code: """
    f :: &'A a -> &'B a -> &'A a
    f a b = a
    """)
    testBitis(code: """
    f :: &'A a -> &'A a
    f a = a
    """)
    testBitis(code: """
    x = let
      a = 10
      b = 20
      c = 30
    in let
        x = &a
      in true
    """)
    testBitis(code: """
    f :: &'A a -> &'A a
    f a = a
    
    a = 10
    y = let in f (&a)
    """)
    testBitis(code: """
    f :: &'A a -> &'B a -> &'A a
    f a _ = a
    
    a = 10
    y = let b = 20 in f (&a) (&b)
    """)
    testBitis(code: """
    f :: &'A a -> &'A a
    g :: &'A a -> &'A a
    add :: &'A a -> &'A a -> ()
    
    h x = add (f (&x)) (g (&x))
    
    a = [1,2,3]
    b = h a
    """)
    testBitis(code: """
    g :: (&'A a, &'B b) -> &'A a
    g (a, b) = a
    
    x = 10
    y = 20
    z = g (&x, &y)
    w = x
    """)
    testBitis(code: """
    fn :: (&'A a, &'B b, (&'C a, &'D a, (&'E a, &'F a))) -> (&'A a, &'E a)
    fn (a, b, (c, d, (e, f))) = (a, e)
    x = let
      a = true
      b = true
      c = true
      d = true
      e = true
      f = true
      x = fn (&a, &b, (&c, &d, (&e, &f)))
    in 10
    """)
    testBitis(code: """
    x = let
      a = 10
      b = \\i -> &a
      c = a
    in c
    """)
    testBitis(code: """
    b = let
      x = 10
      y = 20
      z = let r = (&x, &y) in r
    in 5
    """)
    testBitis(code: """
    f :: &'A a -> &'A b -> (&'A a, &'A b)
    f a b = (a, b)
    """)
    testBitis(code: """
    f :: &'A a -> &'B b -> (&'A a, &'B b)
    f a b = (a, b)
    """)
    testBitis(code: """
    g :: (&'A a, &'B b) -> &'A a
    g (a, b) = a
    """)
    testBitis(code: """
    h :: (&'A a, &'B b) -> &'B b
    h (a, b) = b
    """)
    testBitis(code: """
    i :: (&'A a, &'B b) -> &'C a -> (&'A a, &'C a)
    i (a, b) c = (a, c)
    """)
    testBitis(code: """
    f :: &'A a -> &'B b -> (&'A a, &'B b)
    f a b = (a, b)
    
    x = 10
    y = 20
    z = f (&x) (&y)
    """)
    testBitis(code: """
    data Ref ['A] = R (&'A (Ref ['A])) | N
    f :: () -> (Ref ['A])
    f () = let
      y :: (Ref [])
      y = N ()
    in y
    """)
    testBitis(code: """
    data Ref ['A] = R (&'A (Ref ['A])) | N
    f :: () -> (Ref ['A])
    f () = N ()
    """)
    testBitis(code: """
    data List ['A] = Cons (&'A Int) (List ['A]) | Nil
    gen :: &'A Int -> (List ['A])
    gen a = let
      x :: (List [])
      y :: (List [])
      x = Cons a y
      y = Cons a x
    in x
    """)
    testBitis(code: """
    data Ref ['A] = R (&'A (Ref ['A])) | N
    f :: (Ref ['A]) -> (Ref ['A])
    f a = let b = a in b
    """)
    testBitis(code: """
    data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef
    
    hd_ref :: &'A (ListRef ['A, a]) -> &'A a
    hd_ref $(ConsRef a _) = *a
    """)
    testBitis(code: """
    data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef
    drop :: Int -> &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
    drop _ $(NilRef _) = &(NilRef ()) -- referencing data type constants is allowed!
    """)
    testBitis(code: """
    data P ['A] = P (&'A Int)
    
    f :: (P ['A]) -> (P ['A])
    f a = a
    """)
    testBitis(code: """
    data ListRef ['A, a] = Cons (&'A a) (ListRef ['A, a]) | Nil
    
    convert :: &'A [a] -> (ListRef ['A, a])
    convert (x:xs) = Cons x (convert xs)
    """)
    testBitis(code: """
    data List ['A] = Cons (&'A Int) (List ['A]) | Nil
    
    gen :: &'AA Int -> (List ['AA])
    gen a = let
      x :: (List [])
      y :: (List [])
      f = Cons a
      x = f y
      y = f x
    in x
    """)
  }
  
  func testCannotReferenceLocalValue2() {
    testBitis(
      code: """
      f :: &'A Bool -> 'A Bool
      f a = if a then true else false
      """)
    testBitis(
      code: """
      x = let
        a = true
        b = let c = if &a then [1,2,3] else [4,5,6] in c
      in 10
      """)
    testFailingBitis(
      code: """
      f :: Bool -> 'A Bool
      f a = if &a then true else false
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      a = let
        b = true
        x = if &b then 1 else 2
      in x
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      f a = if &a then true else false
      """,
      expected: .cannotReferenceLocalValue)
    testFailingBitis(
      code: """
      a = let
        x = true
        z = if true then &x else x
      in z
      """,
      expected: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      a = let
        x = 10
        z1 = &x + 20
        z2 = 30 + &x
      in z1 + z2
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      f :: &'A [Int] -> 'A Int
      f &[] = 0
      f (x:xs) = x + (f xs)

      a = let
        x = [1,2,3]
        y = f (&x)
      in y
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      a = let
        x = 10
        y = 20
        z = &x <= y
      in z
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      a = let
        x = true
        z = &x == false
      in z
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      fmap_ref :: (&'A a -> 'A b) -> &'A [a] -> ['A b]
      fib :: &'A Int -> 'A Int

      a = let
        xs = [1,2,3]
        fibs = fmap_ref fib (&xs)
      in fibs
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      g :: &'A Int -> 'A Bool
      g x = true
      f :: Int -> Bool
      f a = g (&a)
      """,
      error: .cannotReferenceLocalValue)
    testBitis(
      code: """
      add_one :: &'A Int -> 'A Int
      add_one n = n + 1
      """)
    testBitis(
      code: """
      add_one :: &'A Int -> 'A Int
      add_one = \\m -> case m of n -> n + 1
      """)
    testBitisFailing(
      code: """
      g :: &'A Int -> 'A Bool
      g = \\x -> case x of y -> true
      f :: Int -> Bool
      f = \\a -> case a of b -> g (&b)
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      data List [u] = Cons u (List u) | Nil ()

      g :: (List Int) -> (List Int)
      g $(Cons p ps) = *&ps
      """,
      error: .cannotReferenceLocalValue)
    testBitisFailing(
      code: """
      data List [u] = Cons u (List u) | Nil ()
      
      f :: &'A (List Int) -> 'A (List Int)
      f $(Cons a as) = Cons (a + 1) (Nil ())
      
      g :: (List Int) -> (List Int)
      g $(Cons p ps) = f (&ps)
      """,
      error: .cannotReferenceLocalValue)
    // @TODO: -
    testBitisFailing(
      code: """
      data List [u] = Cons u (List u) | Nil ()
      
      f :: &'A (List Int) -> 'A (List Int)
      f $(Cons a as) = Cons (a + 1) (Nil ())
      
      g :: (List Int) -> (List Int)
      g $(Cons p ps) = Cons p (f (&ps))
      """,
      error: .cannotReferenceLocalValue)
    // @TODO: -
    testBitisFailing(
      code: """
      data List [u] = Cons u (List u) | Nil ()
      
      h :: &'A Int -> 'A Int
      h x = x + 0
      
      g :: (List Int) -> (List Int)
      g $(Cons p ps) = Cons (h (&p)) (Nil ())
      """,
      error: .cannotReferenceLocalValue)
    // @TODO: -
    testBitis(
      code: """
      data MaybeRef ['T, T] = JustRef (&'T T) | NothingRef ()
      extract_ref :: &'A (MaybeRef ['A, a]) -> &'A a
      extract_ref $(JustRef x) = *x
      """)
  }
  
  func testCannotReferenceLocalValue3() {
    // @TODO: -
    testBitis(
      code: """
      x = let
        a = 10
        b = &a
      in (a, b, &b) -- this is ok
      """)
  }
  
  func testCannotReferenceLocalValue4() {
    testBitisFailing(
      code: """
      x = let
        hd :: &'A [Int] -> &'A Int
        hd (a:_) = a
      
        f :: &'B [Int] -> () -> &'B Int
        f xs = let
          g = \\() -> hd xs
        in g
        
        xs = [1,2,3]
        fn = f (&xs)
      in fn -- error: the closure outlives "xs"!
      """,
      error: .cannotReferenceLocalValue)
    // @TODO: - ATM, this is rejected by the typechecker!
    testBitis(
      code: """
      x = let
        hd :: &'A [Int] -> &'A Int
        hd (a:_) = a
      
        f :: &'B [Int] -> 'B (() -> &'B Int)
        --                ~~~~~~~~~~~~~~~~~~ the lifetime of the closure must be limited to 'B!
        --                wrapping might not strictly be necessary becaue the compiler could infer automatically
        f xs = let
          g = \\() -> hd xs
        in g
        
        xs = [1,2,3]
        fn = f (&xs)
      in fn
      """)
  }
  
  func testCannotReferenceLocalValue5() {
    // @TODO: -
    testBitisFailing(
      code: """
      f :: &'A Int -> &'A Int -> &'A Int
      f x y = x + y
      
      x = let
        a = 10
        b = let
          c = 20
          -- fixme: this should not compile!
          d = f (&a) (&c)
        in d
        print b
      in 0
      """,
      error: .cannotReferenceLocalValue)
  }
  
  func testSelfBorrow() {
    testBitis(
      code: """
      data G ['A] = Node (&'A (G ['A]))
      
      f :: () -> (G ['A])
      f () = let
        c :: (G [])
        c = Node (&c)
      in c
      """)
    
    testBitis(
      code: """
      data ListRef ['A, a] = Cons (&'A a) (ListRef ['A, a]) | Nil
      data Graph2 ['A] = Node Int (ListRef ['A, (Graph2 ['A])])
      
      fmap :: (a -> (Graph2 ['A])) -> [a] -> [(Graph2 ['A])]
      
      convert :: &'A [a] -> (ListRef ['A, a])
      convert (x:xs) = Cons x (convert xs)
      convert _ = Nil ()
      
      clique :: [Int] -> [(Graph2 ['A])]
      clique xs = let
        c :: [(Graph2 [])]
        c = fmap (\\i -> Node i (convert (&c))) xs -- self-borrow
      in c
      """)
    
    testFailingBitis(
      code: """
      data R ['A, 'B] = A &'A (R ['A, 'B]) (&'A (R ['A, 'B])) | B &'A Int
      x = let
        a = 10
        b = B (&a)
        c :: (R [])
        c = A (&c) (&b) -- self borrow + local borrow
      in c
      """,
      expected: .cannotReferenceLocalValue)
  }
}

// MARK: - Helpers

extension LifetimeAnalyserTests {
  func testBitis(
    code: String,
    ast: LAAst,
    env: LAState = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: ast,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBitis(
    code: String,
    env: LAState = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBitisFailing(
    code: String,
    error: LifetimeError,
    env: LAState = .empty,
    line: Int = #line
  ) {
    testFailing(
      code: code,
      expected: error,
      parser: Parser().bitis,
      env: env,
      line: line)
  }
  
  func testBinding(
    code: String,
    binding: LAAst.Binding,
    env: LAState = .empty,
    typeEnv: Bitis.Env = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: binding,
      parser: Parser().statement,
      env: env,
      line: line)
  }
  
  func testExpression(
    code: String,
    expression: LAAst.Expression,
    env: LAState = .empty,
    line: Int = #line
  ) {
    test(
      code: code,
      expected: expression,
      parser: Parser().expression,
      env: env,
      line: line)
  }
  
  func test<T: LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    expected: T,
    parser: Lexer.Parser<T>,
    env: LAState,
    line: Int
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker().runM((.empty, .init(), .empty, .init()))^.mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(env)^.mapLeft { $0 as Error } }^
      
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      } else {
        XCTAssertEqual(res.rightValue.1, expected)
        if res.rightValue.1 != expected {
          print("line \(line)")
          print(
            expected.prettyDescription,
            "\n\n---\n\n",
            res.rightValue.1.prettyDescription.trimmingCharacters(in: .whitespaces)
          )
        }
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
  
  func test<T: LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    parser: Lexer.Parser<T>,
    env: LAState,
    line: Int
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker().runM((.empty, .init(), .empty, .init()))^.mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(env)^.mapLeft { $0 as Error } }^
      
      if res.isLeft {
        print(res.leftValue, "line: \(line)")
        XCTFail()
      }
    } catch {
      print(error, "line: \(line)")
      XCTFail()
    }
  }
  
  func testFailing<T: LifetimeAnalysable & Equatable & TypeCheckable & PrettyStringConvertible>(
    code: String,
    expected: LifetimeError,
    parser: Lexer.Parser<T>,
    env: LAState = .empty,
    line: Int
  ) {
    do {
      let ast = try parser.run(sourceName: "test.bt", input: code)
      let res = ast
        .typeChecker()
        .runM((.empty, .init(), .empty, .init()))^
        .mapLeft { $0 as Error }
        .flatMap { $0.1.lifetimeAnalyser().runM(env)^.mapLeft { $0 as Error } }^
      if res.isLeft {
        XCTAssertEqual(expected, (res.leftValue as? LAError)?.error)
        if expected != (res.leftValue as? LAError)?.error {
          print(line)
        }
      } else {
        XCTFail("Expected error!")
        print(line)
      }
    } catch {
      XCTFail("")
      print(line)
    }
  }
  
  func testFailingExpression(
    code: String,
    expected: LifetimeError,
    env: LAState = .empty,
    line: Int = #line
  ) {
    testFailing(code: code, expected: expected, parser: Parser().expression, env: env, line: line)
  }
  
  func testFailingBinding(
    code: String,
    expected: LifetimeError,
    env: LAState = .empty,
    line: Int = #line
  ) {
    testFailing(code: code, expected: expected, parser: Parser().statement, env: env, line: line)
  }
  
  func testFailingBitis(
    code: String,
    expected: LifetimeError,
    env: LAState = .empty,
    line: Int = #line
  ) {
    testFailing(code: code, expected: expected, parser: Parser().bitis, env: env, line: line)
  }
}
