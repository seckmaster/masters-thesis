//
//  BitisTests.swift
//  
//
//  Created by Toni K. Turk on 16/01/2022.
//

@testable import Bitis
import XCTest
import SwiftParsec
import Bow

fileprivate var testForMemory           = false
fileprivate var envokeDestructors       = false
fileprivate var allocatedMemory: Byte   = 0
fileprivate var deallocatedMemory: Byte = 0

class BitisTests: XCTestCase {
  override static func setUp() {
    ARE_WARNINGS_ENABLED = false
  }
  
  override static func tearDown() {
    print("  ℹ️ Allocated memory  : \(allocatedMemory.bytes)")
    print("  ℹ️ Deallocated memory: \(deallocatedMemory.bytes)")
    print("  ℹ️ Delta             : \(allocatedMemory - deallocatedMemory)B")

    if testForMemory {
      let delta = allocatedMemory - deallocatedMemory
      let formatter = DateFormatter()
      formatter.dateFormat = "dd.MM.yyyy HH:mm"
      let text = """
          \(formatter.string(from: .init()))
          - Allocated  : \(allocatedMemory)B [\(allocatedMemory.bytes)]
          - Deallocated: \(deallocatedMemory)B [\(deallocatedMemory.bytes)]
          - Delta      : \(delta)B [\(delta.bytes)] \((1.0 - Double(delta) / Double(allocatedMemory)).formatted(.percent))\n\n
          """
      
      let handle = try! FileHandle(forUpdating: .init(fileURLWithPath: "/Users/tony/Faks/masters/Masters/Bitis/Playground/test-memory-cleanup.log"))
      try! handle.seekToEnd()
      try! handle.write(contentsOf: text.data(using: .utf8)!)
      try! handle.close()
    }
  }
  
  func testClosures1() {
    testProgram(
      code: """
      fn x = x + 2
      print (fn 10)
      """,
      expected: """
      12
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fn1 x = if true then x - 9 else 12
      print (fn1 10)
      
      fn2 x = if true then 5 + x + 2 else 12
      print (fn2 10)
      
      fn3 x = if true then x + 2 else x + 5
      print (fn3 10)
      
      fn4 x = if false then x + 2 else x + 5
      print (fn4 10)
      """,
      expected: """
      1
      17
      12
      15
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fn1 x = let in x
      print (fn1 10)
      
      fn2 x = let in let in x
      print (fn2 10)
      """,
      expected: """
      10
      10
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fn1 x = case x of y -> y
      print (fn1 99)
      
      fn2 x = case x of y -> case y of z -> z
      print (fn2 10)
      """,
      expected: """
      99
      10
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fn1 = \\x -> \\y -> x + y
      print (fn1 10 21)
      
      fn2 x y = x + y
      print (fn2 10 22)
      
      fn3 = \\q1 -> \\q2 -> case q1 of x -> case q2 of y -> x + y
      print (fn3 10 23)
      
      fn40 x y = if false then x else y + 5
      print (fn40 5 10)
      
      fn41 x y = if false then x + 1 else y + 2
      print (fn41 5 10)
      
      fn42 x y = if true then x + 1 else y + 2
      print (fn42 5 10)
      """,
      expected: """
      31
      32
      33
      15
      12
      6
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fn1 b x y = if b then x else y
      print fn1 true 10 20
      print fn1 false 10 20
      
      fn2 b x y = if b && true then 5 + x * 3
                      else if y < 3 then 1
                                    else 2
      print fn2 true 10 0
      print fn2 false 0 2
      print fn2 false 0 8
      
      fn3 b x y = if b && true then 5 + x * 3
                          else if y < 3 then 5 + 4 + 3 + 2 + 1 + x
                                        else 10 + 9 + 8 + 7 + 6 + x
      print fn3 true 10 0
      print fn3 false 5 2
      print fn3 false 10 8
      
      id a = a
      
      -- todo: parser incorrectly parses, hence parenthesis around (2*z)
      fn4 x y z = if true && id x then 2 + y + z
                                  else (y * z) - 18
      
      print fn4 true 5 6
      print fn4 false 5 6
      """,
      expected: """
      10
      20
      35
      1
      2
      35
      20
      50
      13
      12
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      one = 1
      f x = x + one
      print f 10
      """,
      expected: """
      11
      """,
      testForMemoryPerTest: true)
  }
  
  func testApplication() {
    testProgram(
      code: """
      f x = x
      g x = x
      
      print f 12
      print (if true then g else g) 13
      print (if true then f else f) 14
      print (if false then \\x -> x else \\x -> x) 15
      h = (if false then \\x -> x else \\x -> x)
      print h 16
      """,
      expected: """
      12
      13
      14
      15
      16
      """,
      testForMemoryPerTest: true)
  }
  
  func testSmallPrograms() {
    testProgram(
      code: """
      x = 10
      print (&x)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 10
      y = x
      print (&y)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 5 / 1
      print (&x)
      print (&x)
      print (&x)
      """,
      expected: """
      5
      5
      5
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 10 + 20
      print (&x)
      """,
      expected: "30",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      id y = y
      f x = id x
      h x = id (id (id (id (id (id (id x))))))

      print (f 10)
      print (h 100)
      """,
      expected: """
      10
      100
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f1 x1 = x1 + 1
      f2 x2 = x2 + 2
      f3 x3 = x3 + 3
      f4 x4 = x4 + 4
      f5 x5 = x5 + 5
      a = 10
      b = f5 (f4 (f3 (f2 (f1 a))))
      print b
      """,
      expected: "25",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x = (1, x)
      a = case f 12 of (e,f) -> f
      print a
      """,
      expected: "12",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x y = (1, 2, 3)
      sum = case f 1 2 of (a, b, c) -> a + b + c
      print sum
      """,
      expected: "6",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x y z = (x, y, z)
      sum = case f 1 2 3 of (a, b, c) -> a + b + c
      print sum
      """,
      expected: "6",
      testForMemoryPerTest: true)
    testProgram( 
      code: """
      a = 10
      b = if true then a else a
      print b
      """,
      expected: "10",
      testForMemoryPerTest: true)
  }
  
  func testIfExpression() {
    testProgram(
      code: """
      f b = if b then 1 else 2
      a = f true
      b = f false
      print (&a)
      print (&b)
      """,
      expected: """
      1
      2
      """,
      testForMemoryPerTest: true)
  }
  
  func testCaseExpression() {
    testProgram(
      code: """
      f x = case x of a -> a
      a = f 10
      print (&a)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x = case x of
        a -> case a of
          b -> b
      a = f 10
      print (&a)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x = case x of
        a -> case a of
          b -> case b of
            c -> c
      a = f 10
      print (&a)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data P = M Int
      c = case M 12 of $(M val) -> val
      print c
      """,
      expected: "12",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data D = B () | C ()
      x = B ()
      y = C ()
      a = case x of
            $(B ()) -> 0
            | $(C _) -> 1
      b = case y of
            $(B ()) -> 0
            | $(C _) -> 1
      print a
      print b
      """,
      expected: """
      0
      1
      """,
      testForMemoryPerTest: true)
  }
  
  func testPartialApplication() {
    testProgram(
      code: """
      f x = x
      g = f
      a = g 123
      print (&a)
      """,
      expected: "123",
      testForMemoryPerTest: true)
  }
  
  func testFunctionWithLocalVariables() {
    testProgram(
      code: """
      f x = let
        a = x
        b = a
        c = b
        d = c
      in d
      a = f 10
      print (&a)
      """,
      expected: "10")
    testProgram(
      code: """
      f x = let
        a = let
          c = 10
          d = 100
        in c * d
        b = 20
      in a + b
      
      x = f ()
      print (&x)
      """,
      expected: "1020")
    testProgram(
      code: """
      x = let
        f n = n * 2
        a = f 10
      in a
      print (&x)
      """,
      expected: "20")
    testProgram(
      code: """
      outer :: Int -> Int
      fn = let
        g = \\a -> a * 2
      in g
      
      x = fn 5
      print (&x)
      """,
      expected: "10")
    testProgram(
      code: """
      id aa = aa
      fun b = let
        a11 = 5
        a1 = let
          a22 = 18
          a2 = let
            a33 = 60
            a3 = id (a11 + a22 + a33 + b)
          in a3
        in a2
      in a1

      x1 = fun 17
      print (&x1)
      """,
      expected: "100")
    testProgram(
      code: """
      x = let
        a = let
          c = 10
          d = 100
        in c * d
        b = 20
      in a + b
      print x
      """,
      expected: "1020")
    testProgram(
      code: """
      x = let
        a = \\x -> x
      in a 10
      print x
      """,
      expected: "10")
  }
  
  func testPatternMatching() {
    testProgram(
      code: """
      f y = case y of
        x -> x
      x = f 10
      print (&x)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f y = case y of
        (true, x) -> x
        | x -> 1
      x = f (true, 5)
      y = f (false, 10)
      print (&x)
      print (&y)
      """,
      expected: """
      5
      1
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = case 0 of
        0 -> 0
        | 1 -> 1
        | 2 -> 2
        | _ -> 500
      
      print (&x)
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      g x = case x of 1 -> 1 | 2 -> 20 | n -> n + 12
      
      f 1 = 1
      f 2 = 20
      f x = x + 12
      
      print g 1
      print g 2
      print g 3
      
      print f 1
      print f 2
      print f 3
      """,
      expected: """
      1
      20
      15
      1
      20
      15
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x = case x of
        0 -> 0
        | 1 -> 1
        | n -> n
      
      a = f 0
      b = f 1
      c = f 2
      d = f 100
      print (&a)
      print (&b)
      print (&c)
      print (&d)
      """,
      expected: """
      0
      1
      2
      100
      """,
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Memory leak
      code: """
      f x = case x of (a,(c,e)) -> a+c+e
      g x = case x of (a,(c,_)) -> a+c
      print (f (3,(2,8)))
      print (g (3,(2,8)))
      """,
      expected: """
      13
      5
      """)
    testProgram(
      code: """
      f x = case x of (a,b) -> case b of (c,e) -> a+c+e
      print (f (3,(2,8)))
      """,
      expected: """
      13
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f y = case y of
        (0, false)  -> 0
        | (0, true) -> 1
        | (1, true) -> 2
        | _         -> 100
      
      x = f (0, false)
      y = f (0, true)
      z = f (1, true)
      w = f (5, true)
      
      print (&x)
      print (&y)
      print (&z)
      print (&w)
      """,
      expected: """
      0
      1
      2
      100
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f y = case y of
        ((false, (true, true)), 1)   -> 1
        | (_, 2)                     -> 2
        | ((true, (true, false)), x) -> 3
        | _                          -> 555
      
      x = f ((false, (true, true)),  1)
      y = f ((true, (false, false)), 2)
      z = f ((true, (true, false)),  3)
      w = f ((true, (false, true)),  0)
      
      print (&x)
      print (&y)
      print (&z)
      print (&w)
      """,
      expected: """
      1
      2
      3
      555
      """,
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Memory leak
      code: """
      f :: (Int, ((Int, Int, Bool), (Bool, Bool)), Int) -> Int
      f (n, ((1, 2, true), (true, false)), 5) = n
      f (12, ((x1, y1, false), _), 5)         = x1 + y1
      
      x  = f (100, ((1,2,true), (true, false)), 5)
      y  = f (12, ((460,540,false), (true, false)), 5)
      print x
      print y
      """,
      expected: """
      100
      1000
      """)
    testProgram(
      code: """
      f :: (Int, Bool) -> Int
      f x = case x of
              (a, true) -> 5 + a * 2
              | (a, b) -> if b then 5 * a else 10 * a + 8
      
      print f (10, true)
      """,
      expected: """
      25
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data C = A Int | B Bool
      
      x = A 5
      y = B true
      
      f :: (C []) -> Int
      f $(A _) = 2
      f $(B _) = 1
      
      print (f x)
      print (f y)
      """,
      expected: """
      2
      1
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data C = A Int (C []) | B Bool
      
      one = 1
      x   = A 5 (A 10 (A 15 (B false)))
      y   = B true
      
      f :: &'A (C []) -> &'A Int
      f $(A a b) = a
      f $(B _)   = &one
      
      print f (&x)
      print f (&y)
      """,
      expected: """
      5
      1
      """,
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Memory leak
      code: """
      data C = A Int (C []) | B Bool
      
      one = 1
      x   = B false
      y   = A 15 x
      z   = A 10 y
      w   = A 5 z
      
      f :: &'A (C []) -> &'A Int
      f $(A a b) = a
      f $(B _)   = &one
      
      print f (&w)
      """,
      expected: """
      5
      """)
    testProgram(
      code: """
      data C = A Int | B Bool
      
      x = A 5
      y = B true
      z = B false
      
      f :: (C []) -> Int
      f $(A q)     = q
      f $(B true)  = 1
      f $(B false) = 0
      
      print (f x)
      print (f y)
      print (f z)
      
      g :: (C []) -> Int
      g $(A q)     = 123
      
      w = A 0
      print (g w)
      """,
      expected: """
      5
      1
      0
      123
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data Y [] = B Int (Bool, (Bool, Int))
      
      f :: (Y []) -> Int
      f $(B x y) = x
      
      print f (B 2 (true, (false, 10)))
      """,
      expected: """
      2
      """,
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Memory leak
      code: """
      data Y [] = B Int (Bool, (Bool, Int))

      f :: (Y []) -> Int
      f $(B x (true, (false, y))) = x + y
      f $(B 8 (a, (false, yy))) = if a then yy else 100

      x = B 2 (true, (false, 10))
      y = B 8 (false, (false, 0))
      z = B 8 (true, (false, 0))

      print (f x)
      print (f y)
      print (f z)
      """,
      expected: """
      12
      100
      8
      """)
  }
  
  func testClosures2() {
    testProgram(
      code: """
      id y = y
      f x y = id x + id y
      
      x = f 10 20
      print (&x)
      """,
      expected: "30")
    // TODO: - Eq. Lifting
//    testProgram(
//      code: """
//      g x = let
//        h () = x
//      in h ()
//      print (g 10)
//      """,
//      expected: "10")
    testProgram(
      code: """
      f x y = x + y
      g = f 10
      b = g 20
      c = g 30
      d = f 1 2
      
      print (&b)
      print (&c)
      print (&d)
      """,
      expected: """
      30
      40
      3
      """)
    testProgram(
      code: """
      f = \\x -> \\y -> x + y
      g = f 10
      b = g 20
      c = g 30
      d = f 1 2
      
      print (&b)
      print (&c)
      print (&d)
      """,
      expected: """
      30
      40
      3
      """)
    testProgram(
      code: """
      f p x = if p then \\y -> x + y
                   else \\z -> x * z

      b = f true 10 20
      c = f false 10 20
      d = f true 1 2
      e = f false 1 2

      print (&b)
      print (&c)
      print (&d)
      print (&e)
      """,
      expected: """
      30
      200
      3
      2
      """)
    testProgram(
      code: """
      f :: (Int -> Int) -> Int -> Int
      f g a = g a

      mult4  = \\n -> 4 * n
      mult10 = \\n -> 10 * n
      
      g1 = f mult4
      g2 = f mult10

      x = g1 10
      y = g2 10

      id x = x
      g = f id
      z = f g 5
      w = f g 50

      print (&x)
      print (&y)
      print (&z)
      print (&w)
      """,
      expected: """
      40
      100
      5
      50
      """)
    testProgram(
      code: """
      f     = \\g -> \\a -> g a
      id    = \\x -> x
      c f g = \\a -> g (f a)
      
      g = f id
      z = f (c id id) 10
      print z
      """,
      expected: "10")
    testProgram(
      code: """
      f x = let
        b = true
        c = true
      in \\y -> if b && c then x else y
      
      x = f 10 20
      
      print (&x)
      """,
      expected: "10")
    testProgram(
      code: """
      f x = let
        a = 1012
        b = true
        c = false
      in \\y -> if b && c then x else y
      
      x = f 10 20
      
      print (&x)
      """,
      expected: "20")
    testProgram(
      code: """
      f x = let
        b = true
        c = let
          a = true
          g = \\y -> if b && a then x else y
        in g
      in c

      x = f 10 20

      print (&x)
      """,
      expected: "10")
    testProgram(
      code: """
      f = \\x -> \\y -> \\z -> x + y + z
      a = f 1 2 3
      print (&a)
      """,
      expected: "6")
    testProgram(
      code: """
      f x y z = x + y + z
      a = f 1 2 3
      print (&a)
      """,
      expected: "6")
  }
  
  func testRecursion() {
    testProgram(
      code: """
      zero = 0
      one  = 1
      
      fact :: &'A Int -> &'A Int
      fact n = if n == &zero then &one
                             else fact (n - (&one)) * n
      
      a = 10
      x = fact (&a)
      print (&x)
      b = 5
      y = fact (&b)
      print (&y)
      """,
      expected: """
      3628800
      120
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      one = 1

      fact :: &'A Int -> &'A Int
      fact n = case n of
        &0 -> &one
        | m -> fact (m - (&one)) * m
        -- | m -> m * fact (m - (&one))
      
      a = 10
      x = fact (&a)
      print (&x)
      """,
      expected: "3628800",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      one = 1
      
      fact :: &'A Int -> &'A Int
      fact &0 = &one
      -- fact  n = (fact (n - (&one))) * n
      fact  n = n * (fact (n - (&one)))
      
      y = 10
      x = fact (&y)
      print (&x)
      """,
      expected: "3628800",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      zero = 0
      one  = 1
      two  = 2
      
      fib :: &'A Int -> &'A Int
      fib n = if n <= &two then &one
                           else (fib (n - (&one))) + (fib (n - (&two)))
      
      a = 5
      x = fib (&a)
      b = 10
      y = fib (&b)
      
      print x
      print y
      """,
      expected: """
      5
      55
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      zero = 0
      one  = 1
      ten  = 10
      
      f :: &'A Int -> &'A Int
      f &0 = &zero
      f n  = n + (f (n - (&one)))
      
      g :: &'A Int -> &'A Int
      g n = case n of
             &0 -> &zero
             | m -> m + (g (m - (&one)))
      
      print f (&ten)
      print g (&ten)
      """,
      expected: """
      55
      55
      """,
      testForMemoryPerTest: true)
  }
  
  func testMutualRecursion() {
    testProgram(
      code: """
      is_odd  :: Int -> Bool
      is_even :: Int -> Bool
      is_odd  0 = false
      is_odd  1 = true
      is_odd  n = is_even (n - 1)
      is_even 0 = true
      is_even 1 = false
      is_even n = is_odd (n - 1)
      
      print (is_even 0)
      print (is_even 1)
      print (is_even 2)
      print (is_even 3)
      print (is_even 4)
      print (is_even 111)
      print (is_even 112)
      
      print (is_odd 0)
      print (is_odd 1)
      print (is_odd 2)
      print (is_odd 3)
      print (is_odd 4)
      print (is_odd 111)
      print (is_odd 112)
      """,
      expected: """
      1
      0
      1
      0
      1
      0
      1
      0
      1
      0
      1
      0
      1
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data List [a] = Cons a (List [a]) | Nil ()
      
      zero  = 0
      
      sum1 :: &'A (List [Int]) -> &'A Int
      sum2 :: &'A (List [Int]) -> &'A Int
      
      sum1 xs = case xs of
        $(Cons x xss) -> x + sum2 xss
        | $(Nil ())   -> &zero
      
      sum2 xs = case xs of
        $(Cons x xss) -> x + sum1 xss
        | $(Nil ())   -> &zero
      
      x :: (List [Int])
      x  = Nil ()
      y  = Cons 30 x
      z  = Cons 20 y
      xs = Cons 10 z
      
      print sum1 (&xs)
      print sum2 (&xs)
      """,
      expected: """
      60
      60
      """,
      testForMemoryPerTest: true)
  }
  
  func testLazyEvaluation() {
    testProgram(
      code: """
      x = 5 / 0
      """,
      expected: "",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 5 / 0
      y = if false then x else 0
      print (&y)
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 5 / 0
      y = if true && false then x else 0
      print (&y)
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 5 / 0
      y = if 5 < 2 then x else 0
      print (&y)
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x = 4321
      x = 5 / 0
      y = f x
      print (&y)
      """,
      expected: "4321",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = (true, 5/0)
      y = case x of
        (true, z) -> 1
        | _ -> 10
      print y
      """,
      expected: "1",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f :: a -> a
      f x = f x -- inf rec
      x = if false then f 5 else 10
      print (&x)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f :: a -> a
      f x = f x -- inf rec
      g x = if true then 0 else x
      x = g (f 5)
      print (&x)
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fst (a, _) = a
      a = (\\x -> x, 5 / 0)
      print ((fst a) 10)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fst (a, _) = a
      f :: a -> a
      f x = f x -- inf rec
      a = (\\x -> x, f 10)
      print ((fst a) 10)
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Does not work because `h` gets cleared by the destructor of let
      code: """
      g x = let h = \\x -> 10 in h ()
      print (g (5/0))
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      f x y z = (x, y, z)
      sum = case f 1 2 (5/0) of (a, b, c) -> a + b
      print sum
      """,
      expected: """
      3
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fst :: (a, b) -> a
      fst (a, b) = a
      print (fst (\\x -> x, 5/0)) 12
      """,
      expected: """
      12
      """,
      testForMemoryPerTest: true)
  }
  
  func testDataBindings() {
    testProgram(
      code: """
      data P = M Int Int
      x = M 12 -55
      a = case &x of
             $(M v1 v2) -> v1
      b = case &x of
             $(M v1 v2) -> v2
      print a
      print b
      """,
      expected: """
      12
      -55
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data List [c] = Cons c (List [c]) | Nil ()
      hd  :: &'A (List [a]) -> &'A a
      tl  :: &'A (List [a]) -> &'A (List [a])
      sum :: &'A (List [Int]) -> &'A Int
      
      zero = 0
      one  = 1
      
      hd $(Cons x xs) = x
      tl $(Cons x xs) = xs
      
      sum xs = case xs of
        $(Cons x xss) -> x + sum xss
        | $(Nil ()) -> &zero
      
      x :: (List [Int])
      x  = Nil ()
      y  = Cons 40 x
      z  = Cons 30 y
      w  = Cons 20 z
      xs = Cons 10 w
      
      print (sum (&xs))
      print sum (tl (&xs))
      print (hd (&xs))
      print (hd (tl (&xs)))
      """,
      expected: """
      100
      90
      10
      20
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data List [c] = Cons c (List [c]) | Nil ()
      sum  :: &'A (List [Int]) -> &'A Int
      fmap :: (a -> b) -> (List [a]) -> (List [b])
      
      zero = 0
      one  = 1
      
      sum xs = case xs of
        $(Cons x xss) -> x + sum xss
        | $(Nil ()) -> &zero
      
      fmap f xs = case xs of
                     $(Nil ()) -> Nil ()
                     | $(Cons x xss) -> Cons (f x) (fmap f xss)
      
      x :: (List [Int])
      x  = Nil ()
      y  = Cons 40 x
      z  = Cons 30 y
      w  = Cons 20 z
      xs = Cons 10 w
      ys = fmap (\\a -> a * 2) xs
      
      print (sum (&ys))
      """,
      expected: """
      200
      """,
      testForMemoryPerTest: true)
  }
  
  func testListRef() {
    testProgram( // @TODO: - Memory leak
      code: """
      data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef ()
      
      sum_ref         :: &'A (ListRef ['A, Int]) -> &'A Int
      drop            :: Int -> &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
      take            :: Int -> &'A (ListRef ['A, a]) -> (ListRef ['A, a])
      create_list_ref :: &'A Int -> (ListRef ['A, a])
      hd_ref          :: &'A (ListRef ['A, a]) -> &'A a
      len_ref         :: (ListRef ['A, a]) -> &'A Int
      tail_ref        :: &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a])
      last_ref        :: &'A (ListRef ['A, a]) -> &'A a
      fmap_ref        :: (&'A h -> &'A j) -> &'A (ListRef ['A, h]) -> 'A (ListRef ['A, j])
      merge_ref       :: &'A (ListRef ['A, a]) -> &'A (ListRef ['A, a]) -> (ListRef ['A, a])
      
      x1 = 10
      x2 = 20
      x3 = 30
      x4 = 40
      
      zero  = 0
      one   = 1
      two   = 2
      three = 3
      nil_ref_empty = NilRef ()

      sum_ref $(NilRef ())    = &zero
      sum_ref $(ConsRef x xs) = *x + sum_ref xs
      
      drop 0 xs              = xs
      drop n $(ConsRef _ xs) = drop (n - 1) xs
      drop _ $(NilRef _)     = &nil_ref_empty
      
      take n xs = case xs of -- todo: also add test for this function implemented as two equations
        $(NilRef ()) -> NilRef ()
        | $(ConsRef x xss) -> case n of
          0 -> NilRef ()
          | m -> ConsRef (*x) (take (m - 1) xss)
      
      len_ref $(NilRef ())     = &zero
      len_ref $(ConsRef _ xss) = &one + len_ref xss
      
      create_list_ref &0 = NilRef ()
      create_list_ref p  = ConsRef p (create_list_ref (p - (&one)))
      
      hd_ref $(ConsRef x _) = *x
      
      tail_ref $(ConsRef _ xs) = xs
      
      last_ref $(ConsRef x1 xss) = case xss of
                                     $(NilRef ()) -> *x1
                                     | _ -> last_ref xss
      last_ref $(ConsRef _ xss)  = last_ref xss
      
      fmap_ref f xs = case xs of
               $(NilRef ()) -> NilRef ()
               | $(ConsRef x xss) -> let
                 b :: (ListRef [b])
                 b = ConsRef (f (*x)) (fmap_ref f xss)
               in b
      
      lambda :: &'A Int -> &'A Int
      lambda = (\\a -> a * (&two))
      
      id :: &'A a -> &'A a
      id a = a
      
      convert_list xs = fmap_ref id xs
      
      merge_ref xs ys = case xs of
        $(NilRef ()) -> convert_list ys
        | $(ConsRef x xss) -> ConsRef (*x) (merge_ref (xss) ys)
      
      xs  = ConsRef (&x1) (ConsRef (&x2) (ConsRef (&x3) (ConsRef (&x4) (NilRef ()))))
      yss = create_list_ref (&three)
      qss = fmap_ref lambda (&xs)
      wss = merge_ref (&qss) (&xs)
      
      print (sum_ref (&xs))
      print (sum_ref (drop 0 (&xs)))
      print (sum_ref (drop 1 (&xs)))
      print (sum_ref (drop 2 (&xs)))
      print (sum_ref (drop 3 (&xs)))
      print (sum_ref (drop 4 (&xs)))
      print (len_ref (take 0 (&xs)))
      print (len_ref (take 1 (&xs)))
      print (len_ref (take 2 (&xs)))
      print (len_ref (take 3 (&xs)))
      print (len_ref (take 4 (&xs)))
      print (len_ref (take 5 (&xs)))
      print (hd_ref (&yss))
      print (hd_ref (tail_ref (&yss)))
      print (hd_ref (tail_ref (tail_ref (&yss))))
      print (last_ref (&xs))
      print (sum_ref (&qss))
      print (sum_ref (&wss))
      """,
      expected: """
      100
      100
      90
      70
      40
      0
      0
      1
      2
      3
      4
      4
      3
      2
      1
      40
      200
      300
      """)
  }
  
  func testMaybe() {
    testProgram(
      code: """
      data Maybe [Wrapped] = Just Wrapped | Nothing ()
      
      is_just :: &'A (Maybe a) -> Bool
      is_just $(Nothing ()) = false
      is_just $(Just _)     = true
      
      is_nothing :: &'A (Maybe a) -> Bool
      is_nothing $(Nothing ()) = true
      is_nothing $(Just _)     = false
      
      fmap :: (a -> b) -> (Maybe a) -> (Maybe b) 
      fmap f m = case m of
        $(Nothing ()) -> Nothing ()
        | $(Just x) -> Just (f x)
      
      flat_map :: (a -> (Maybe b)) -> (Maybe a) -> (Maybe b)
      flat_map f m = case m of
        $(Nothing ()) -> Nothing ()
        | $(Just x) -> f x
      
      extract :: &'A (Maybe a) -> &'A a
      extract $(Just a) = a
      
      neg  true  = false
      neg  false = true
      neg1 true  = false
      neg1 false = true
      
      x = Just 10
      y = Just true
      z = fmap (\\x -> neg (neg1 x)) y
      w = fmap (\\a -> a * 2) (Nothing ())
      q = Just 1
      r = flat_map (\\a -> case a of 0 -> Just false | 1 -> Just true | n -> Nothing ()) q
      
      print (is_just (&x))
      print (is_nothing (&x))
      print (extract (&x))
      print (extract (&z))
      print (is_nothing (&w))
      print (extract (&r))
      """,
      expected: """
      1
      0
      10
      1
      1
      1
      """,
      testForMemoryPerTest: true)
  }
  
  func testMaybeRef() {
    testProgram(
      code: """
      data MaybeRef ['T, T] = JustRef (&'T T) | NothingRef ()
      
      fmap_ref :: (&'A a -> &'A b) -> &'A (MaybeRef ['A, a]) -> (MaybeRef ['A, b])
      fmap_ref f m = case m of
                       $(NothingRef ()) -> NothingRef ()
                       | $(JustRef x) -> JustRef (f (*x))
      
      flat_map_ref :: (&'A a -> (MaybeRef ['A, a])) -> &'A (MaybeRef ['A, a]) -> (MaybeRef ['A, b])
      flat_map_ref f m = case m of
                           $(NothingRef ()) -> NothingRef ()
                           | $(JustRef x) -> f (*x)
      
      extract_ref :: &'A (MaybeRef ['A, a]) -> &'A a
      extract_ref $(JustRef x) = *x
      
      square_ref :: &'A Int -> &'A Int
      square_ref x = x * x
      
      is_nothing_ref :: &'A (MaybeRef ['A, a]) -> Bool
      is_nothing_ref $(NothingRef ()) = true
      is_nothing_ref _                = false
      
      x = 100
      y = 200
      z = true
      
      square_if_less_200 :: &'A Int -> (MaybeRef ['A, Int])
      square_if_less_200 n = if n < &y then JustRef (n * n) else NothingRef ()
      
      a = JustRef (&x)
      b = JustRef (&y)
      c = JustRef (&z)
      d = fmap_ref square_ref (&a)
      e = fmap_ref square_ref (&b)
      f = flat_map_ref square_if_less_200 (&a)
      g = flat_map_ref square_if_less_200 (&b)
      
      print (extract_ref (&a))
      print (extract_ref (&b))
      print (extract_ref (&c))
      print (extract_ref (&d))
      print (extract_ref (&e))
      print (extract_ref (&f))
      print (is_nothing_ref (&g))
      """,
      expected: """
      100
      200
      1
      10000
      40000
      10000
      1
      """,
      testForMemoryPerTest: true)
  }
  
  func testWeakCycles() {
    testProgram(
      code: """
      x :: &Int
      x = x
      print 0
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x :: &Int
      y :: &Int
      x = y
      y = x
      print 0
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x :: &Int
      y :: &Int
      x = if true then y else y
      y = if true then x else x
      print 0
      """,
      expected: "0",
      testForMemoryPerTest: true)
    testProgram( // @FIXME: - Self-borrow on a stack does not work ...
      code: """
      f = \\() -> let
        a :: (&Int)
        a = a
        print a
      in 0
      print f()
      """,
      expected: "0")
  }
  
  func testCyclicList() {
    testProgram(
      code: """
      data CL ['A, u] = Cons ('A u) (CL ['A, u]) | Ref (&'A (CL ['A, u])) | Nil ()
      
      take :: Int -> &'A (CL ['A, a]) -> (CL ['A, a])
      take a as = case a of 0 -> Nil ()
                    | n -> case as of
                             $(Nil ()) -> Nil ()
                             | $(Cons x xs) -> Cons (*x) (take (n - 1) xs)
                             | $(Ref xss) -> take n (*xss)
      
      map :: (&'A a -> 'A b) -> &'A (CL ['A, a]) -> (CL ['A, 'A b])
      -- it does not work with if you uncomment the line bellow
      -- map f $(Ref xs)       = map f (*xs)
      map f $(Cons a as) = Cons (f a) (map f as)
      map f $(Nil ())    = Nil ()
      
      zero = 0
      one  = 1
      
      sum_ref         :: &'A (CL ['A, Int]) -> &'A Int
      sum_ref $(Nil ())    = &zero
      sum_ref $(Cons x xs) = x + sum_ref xs
      sum_ref $(Ref xs)    = sum_ref (*xs)
      
      ones :: (CL Int)
      ones = Cons 1 (Ref (&ones))
      
      five   = take 5 (&ones)
      twenty = take 20 (&ones)
      print sum_ref (&five)
      print sum_ref (&twenty)
      
      -- infinite stream of natural numbers
      
      add_one :: &'A Int -> 'A Int
      add_one n = n + 1
      
      nats :: (CL Int)
      nats = Cons 1 (map add_one (&nats))
      
      ten = take 10 (&nats)
      print sum_ref (&ten)
      
      hundred = take 100 (&nats)
      print sum_ref (&hundred)
      
      -- the fibonacci sequence
      
      add_ref :: &'A Int -> &'A Int -> 'A Int
      add_ref x y = x + y + 0
      
      zip_with :: (&'A a -> &'A b -> 'A c) -> &'A (CL ['A, a]) -> &'A (CL ['A, b]) -> (CL ['A, c])
      zip_with f xs ys = case xs of
                           $(Nil ()) -> Nil ()
                           | $(Ref xss) -> zip_with f (*xss) ys
                           | $(Cons a as) -> (case ys of
                                                $(Nil ()) -> Nil ()
                                                | $(Ref yss) -> zip_with f xs (*yss)
                                                | $(Cons b bs) -> Cons (f a b) (zip_with f as bs))
      
      tail :: &'A (CL ['A, a]) -> (CL ['A, a])
      tail $(Nil ())    = Nil ()
      tail $(Cons x xs) = Ref xs
      tail $(Ref xs)    = tail (*xs)
      
      get :: Int -> &'A (CL ['A, a]) -> &'A Int
      get n xs = case n of
                    0 -> (case xs of
                           $(Ref xss) -> get 0 (*xss)
                           | $(Cons a as) -> a)
                    | m -> (case xs of
                             $(Ref xss) -> get m (*xss)
                             | $(Cons a as) -> get (m - 1) as)
      
      fibs :: (CL Int)
      tail_fibs = tail (&fibs)
      fibs = Cons 1 (Cons 1 (zip_with add_ref (&fibs) (&tail_fibs)))
      
      eight = take 8 (&fibs)
      print sum_ref (&eight)
      
      print get 0 (&fibs)
      print get 1 (&fibs)
      print get 2 (&fibs)
      print get 3 (&fibs)
      print get 4 (&fibs)
      print get 5 (&fibs)
      print get 6 (&fibs)
      
      -- takeWhile
      
      takeWhile :: (&'A a -> 'A Bool) -> (&'A a -> 'A a) -> &'A (CL ['A, a]) -> (CL ['A, a])
      takeWhile f g xs = case xs of
                        $(Ref xss) -> takeWhile f g (*xss)
                        | $(Nil ()) -> Nil ()
                        | $(Cons x ys) -> if f x then Cons (g x) (takeWhile f g ys)
                                                 else Nil ()
      l1 :: &'A Int -> 'A Bool
      l1 x = x < 8
      l2 :: &'A Int -> 'A Int
      l2 x = x + 0
      fibs2 = takeWhile l1 l2 (&fibs)
      
      print get 0 (&fibs2)
      print get 1 (&fibs2)
      print get 2 (&fibs2)
      print get 3 (&fibs2)
      print get 4 (&fibs2)
      print sum_ref (&fibs2)
      """,
      expected: """
      5
      20
      55
      5050
      54
      1
      1
      2
      3
      5
      8
      13
      1
      1
      2
      3
      5
      12
      """,
      testForMemoryPerTest: true)
  }
  
  func testClique() {
    testProgram(
      code: """
      data List [c] = Cons c (List c) | Nil ()
      data ListRef ['A, a] = ConsRef (&'A a) (ListRef ['A, a]) | NilRef ()
      data Graph ['A] = Node Int (ListRef ['A, (Graph ['A])])
      
      fmap :: (a -> b) -> (List a) -> (List b)
      fmap f xs = case xs of
              $(Cons a as) -> Cons (f a) (fmap f as)
              | $(Nil ()) -> Nil ()
      
      convert :: &'A (List a) -> (ListRef ['A, a])
      convert $(Cons x xs) = ConsRef x (convert xs)
      convert $(Nil ())    = NilRef ()
      
      print_list_ref :: &'A (ListRef ['A, c]) -> 'A ()
      print_list_ref $(ConsRef x xs) = let print x in print_list_ref (xs)
      print_list_ref $(NilRef ())    = ()
      
      print_graph :: &'A (Graph ['A]) -> ()
      print_graph $(Node n edges) = let
      print n
      print print_list_ref edges
      in ()
      
      print_graph_list :: &'A (List (Graph ['A])) -> 'A ()
      print_graph_list $(Cons g gs) = let
      print print_graph g
      in print_graph_list gs
      print_graph_list $(NilRef ()) = ()
      
      --
      
      xs :: (List Int)
      xs = Cons 10 (Cons 20 (Cons 30 (Cons 40 (Cons 50 (Nil ())))))
      
      clique :: (List (Graph []))
      clique = fmap (λi -> Node i (convert (&clique))) xs
      
      print print_graph_list (&clique)

      """,
      expected: """
      10
      4640
      8248
      10864
      13480
      16096
      0
      0
      20
      4640
      8248
      10864
      13480
      16096
      0
      0
      30
      4640
      8248
      10864
      13480
      16096
      0
      0
      40
      4640
      8248
      10864
      13480
      16096
      0
      0
      50
      4640
      8248
      10864
      13480
      16096
      0
      0
      0
      """,
      testForMemoryPerTest: true
    )
  }
  
  func testBorrowingAndDestructors() {
    // this tests must pass and memory must be
    // correctly cleaned-up
    testProgram(
      code: """
      a = 2
      b = &a
      c = (b,b)
      d = (&a,&a)
      print case c of (e,f) -> e+f
      print case d of (e,f) -> e+f
      
      x = 10
      y = if true then &x else &x
      z = &y
      print z
      print y
      
      y1 = 20
      z1 = if true then (if true then &y1 else &y1)
             else &y1
      x1 = (z1,z1,123)
      print case x1 of (a,b,c) -> a+b
      
      y2 = 40
      x2 = (if true then &y2 else &y2,if true then &y2 else &y2,5554)
      print case x2 of (e,f,g) -> e+f
      
      y3 = 123
      x3 = let in &y3
      print x3
      
      y4 = 124
      z4 = &y4
      x4 = let in z4
      print x4
      
      y7 = 439
      x7 = case y7 of 0 -> 0 | 439 -> 1
      print x7
      
      y8 = 42
      x8 = case &y8 of &0 -> 0 | &42 -> 1
      print x8
      
      y9 = 55
      x9 = case true of true -> &y9
      z9 = if true then &y9 else &y9
      print x9
      print z9
      
      y10 = 56
      x10 = case true of true -> y10
      print x10
      
      y11 = 90
      z11 = if true then &y11 else &y11
      x11 = case true of true -> z11
      print x11
      
      y12 = 91
      z12 = if true then &y12 else &y12
      x12 = case z12 of _ -> 0
      print x12
      """,
      expected: """
      4
      4
      10
      10
      40
      80
      123
      124
      1
      1
      55
      55
      56
      90
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      fun :: &'A Int -> Int
      fun _ = 10
      
      y5 = 999
      x5 = fun (if true then &y5 else &y5)
      x51 = fun (&y5)
      z5  = if true then &y5 else &y5
      x52 = fun z5
      print x5
      print x51
      print x52
      """,
      expected: """
      10
      10
      10
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      g1 :: Int -> Int
      g1 xx = xx
      
      y6 = 812
      x6 = g1 y6
      print x6
      
      g2 :: &'A Int -> &'A Int
      g2 x = if true then x else x
      
      y13 = 723
      x13 = g2 (&y13)
      print x13
      """,
      expected: """
      812
      723
      """,
      testForMemoryPerTest: true)
  }
  
  func testNonDeterminismAndMemory() {
    testProgram(
      code: """
      x1 = 10
      print if true then x1 else x1
      x2 = 20
      print if false then x2 else x2
      """,
      expected: """
      10
      20
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      o1 = 10
      p1 = 20
      print if true then o1 + p1 else p1
      o2 = 10
      p2 = 20
      print if false then o2 + p2 else p2
      """,
      expected: """
      30
      20
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      o1 = 10
      print if true then o1 else 0
      o2 = 10
      print if false then o2 else 0
      """,
      expected: """
      10
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      o1 = 10
      p1 = 20
      print if true then o1 + p1 else 0
      o2 = 10
      p2 = 20
      print if false then o2 + p2 else 0
      """,
      expected: """
      30
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 10
      print case true of true -> x | false -> x
      """,
      expected: """
      10
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 10
      y = 20
      print case true of true -> x | false -> y
      """,
      expected: """
      10
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      x = 10
      y = 20
      print case false of true -> x + y | false -> y | _ -> 0
      """,
      expected: """
      20
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      o1 = 10
      p1 = 20
      print case true of true -> o1 + p1 | false -> 0
      o2 = 10
      p2 = 20
      print case false of true -> o2 + p2 | false -> 0
      """,
      expected: """
      30
      0
      """,
      testForMemoryPerTest: true)
  }
  
  func testCaseTupleAndMemory() {
    testProgram(
      code: """
      print case (10,20) of (a,b) -> a
      print case (10,20) of (a,b) -> b
      print case (10,20) of (a,b) -> a+b
      print case (10,20) of (a,b) -> 0
      print case if true then (10,20) else (10,20) of (a,b) -> a
      print case if true then (10,20) else (10,20) of (a,b) -> b
      print case if true then (10,20) else (10,20) of (a,b) -> a+b
      print case if true then (10,20) else (10,20) of (a,b) -> 0
      """,
      expected: """
      10
      20
      30
      0
      10
      20
      30
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data Tree = T Int Bool
      print case T 1 true of $(T a b) -> a
      print case T 1 true of $(T a b) -> b
      print case T 1 true of $(T a b) -> if b then a else a
      print case T 1 true of $(T a b) -> 0
      """,
      expected: """
      1
      1
      1
      0
      """,
      testForMemoryPerTest: true)
    testProgram(
      code: """
      data List = Cons ('A Int) (List []) | Nil ()
      print case Cons 1 (Nil ()) of $(Cons a b) -> a
      """,
      expected: """
      1
      """,
      testForMemoryPerTest: true)
  }
  
  func testRecursionAndToRefAndMemory() {
    testProgram(
      code: """
      to_ref :: 'A a -> &'A a
      to_ref a = a
      
      f :: &'A Int -> 'A Int
      f &0 = 1
      f  n = n * f (to_ref (n - 1))
      
      hun = 100
      print f (&hun)
      """,
      expected: "9.33262154439441e+157",
      testForMemoryPerTest: true)
    testProgram( // @TODO: - This does not work because of LA
      code: """
      data List [u] = Cons u (List u) | Nil ()

      f :: &'A Int -> 'A Int
      f &0 = 1
      f  n = n * f (&(n - 1))
      
      ten = 10
      fif = 50
      hun = 100
      
      xs = if f (&hun) > 100 then (Cons (f (&ten)) (Cons (f (&fif)) (Cons (f (&hun)) (Nil ())))) 
                             else Nil ()
      
      print case xs of
        $(Nil ()) -> 0
        | $(Cons 10 ys) -> 10
        | $(Cons 3628800 ys) -> 3628800
        | $(Cons a ys) -> -1
      """,
      expected: "3628800",
      testForMemoryPerTest: true)
  }
  
  func testDeref() {
    testProgram( // @TODO: - Does not work because of LA
      code: """
      deref :: &'A a -> 'A a
      deref a = *a
      
      f :: &'A Int -> 'A Int
      f &0 = 1
      f  n = n * f (&(n - 1))
      
      hun = 10
      a = f (&hun)
      b = deref (&a)
      c = deref (&a)
      print a + b
      """,
      expected: """
      7257600
      """,
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Does not work because of LA
      code: """
      deref :: &'A a -> 'A a
      deref a = *a
      
      f :: &'A Int -> 'A Int
      f &0 = 1
      f  n = n * f (&(n - 1))
      
      hun = 10
      a = &(f (&hun))
      b = deref a
      c = deref a
      print a + b + deref a
      """,
      expected: """
      10886400
      """,
      testForMemoryPerTest: true)
  }
  
  func testMoveOutOfBorrowAndMemory() {
//    testProgram(
//      code: """
//      x = 10
//      y :: Int
//      y = case (&x) of u -> *u
//      print y
//      """,
//      expected: """
//      10
//      """,
//      testForMemoryPerTest: true)
  }
  
  func testDerefRef() {
    testProgram(
      code: """
      x = 10
      print *&x
      """,
      expected: "10",
      testForMemoryPerTest: true)
    testProgram( // @TODO: - Does not work because of LA
      code: """
      -- trick to "move ownership twice"
      f :: 'A Int -> 'A Int
      f a = *&a + *&a
      
      a = let
        x = 10
      in f x
      print a
      """,
      expected: "20",
      testForMemoryPerTest: true)
  }
  
  func testIssuesWithNameShadowing() {
    // @TODO: - Not working
    testProgram(
      code: """
      f p x = if p then \\a -> x + a
                   else \\a -> x * a
      
      b = f true 10 20
      c = f false 10 20
      d = f true 1 2
      e = f false 1 2
      """,
      expected: """
      30
      200
      3
      2
      """)
    testProgram(
      code: """
      f x = case x of x -> x
      print (f 10)
      """,
      expected: "10")
  }
  
  func testMoveOutOfBorrowIssue() {
//    this is okay, but the type of *&a has to be 'AA Int.
//    hence 'cannotReferenceLocalValue' error should be returned.
//    
//    @TODO: - Fix LA
//    testProgram(
//      code: """
//      print let a = 10 in *&a
//      """,
//      expected: "10",
//      testForMemoryPerTest: true)
  }
  
  func testUseAfterFreeIssue() {
    testProgram( // @TODO: - Fix LA
      code: """
      add :: &'A Int -> &'A Int -> &'A Int
      add x y = x + y
      
      a = let
        x = 10
        y = 20
        b = let 
          z = add (&x) (&y)
          print add z (if z < 10 then z * 2 else z + z + z)
        in z
        print b
      in 0
      print a
      """, 
      expected: """
      120
      30
      """)
  }
}

extension BitisTests {
  func testProgram(
    code: String,
    expected: String,
    testForMemoryPerTest: Bool = false,
    line: Int = #line
  ) {
    let stream = Stream()
    let compilerPipeline =
      parse(fileName: "tests.bt", using: .init())
    
      >>- typeChecker
      >>- lifetimeAnalyser
      >>- borrowChecker(discard: true)
    
      >>- liftCases
    
      // @TODO: - Doing it twice
      >>- typeChecker
      >>- lifetimeAnalyser
      >>- borrowChecker()
    
      >>- stackFrames
      >>- generateIR(
        memorySize: 1 << 21, // 2^20B = 1.048.576B = 1MB
        config: .init(
          isLazyEvaluationEnabled: true,
          shouldAutomaticallyEvaluateConstants: true,
          envokeDestructors: envokeDestructors || testForMemoryPerTest,
          isRunningInREPL: false
        ),
        optimizer: .init(optimizations: [
          .constantPropagation,
          .constantFolding,
          .algebraicSimplifications,
        ])
      )
      >>- linearizeCode
      >>- interpret(
        outputStream: stream,
        testForMemoryPerTest: testForMemoryPerTest,
        traceOutputHandle: nil,
        executionMode: .debug // in debug mode to collect runtime data
      )
    let result = compilerPipeline(code)
    switch result.isLeft {
    case true:
      if expected == stream.buffer {
        XCTFail("The test failed because of destructors, otherwise the output is okay: \(result.leftValue)")
      } else {
        XCTFail("In line: \(line), result: \(result.leftValue)")
      }
    case false:
      XCTAssertEqual(expected, stream.buffer)
    }
  }
}

class Stream: TextOutputStream {
  var buffer = ""
  
  func write(_ string: String) {
    if !buffer.isEmpty { buffer += "\n" }
    buffer += string
  }
}

fileprivate func interpret(
  outputStream: TextOutputStream,
  testForMemoryPerTest: Bool,
  traceOutputHandle: FileHandle?,
  executionMode: Interpreter.ExecutionMode
) -> (CodeGenerator.CodeChunk, Interpreter.Memory) -> Bow.Either<Error, ()> {
  { chunk, memory in
    do {
      let interpreter = Interpreter(
        memory: memory,
        stdout: outputStream
      )
      interpreter.trace = traceOutputHandle
      interpreter.executionMode = executionMode
      try interpreter.interpret(chunk: chunk)
      allocatedMemory += interpreter.runtimeInfo.allocatedMemory
      deallocatedMemory += interpreter.runtimeInfo.deallocatedMemory
      if testForMemoryPerTest || testForMemory {
        XCTAssertEqual(
          interpreter.runtimeInfo.allocatedMemory,
          interpreter.runtimeInfo.deallocatedMemory,
          "The test failed because memory is leaking; the output is otherwise okay (\(interpreter.runtimeInfo.description))!"
        )
      }
      return .right(())
    } catch {
      return .left(error)
    }
  }
}
