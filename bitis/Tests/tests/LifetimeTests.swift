//
//  LifetimeTests.swift
//  
//
//  Created by Toni K. Turk on 31/07/2021.
//

@testable import Bitis
import XCTest

class LifetimeTests: XCTestCase {}

extension LifetimeTests {
  func testLifetimes() {
    XCTAssertTrue(DefaultLifetime.static(id: 0).isParent(of: "A"))
    XCTAssertTrue(DefaultLifetime.static(id: 0).isParent(of: "AABBCC"))
    XCTAssertTrue(DefaultLifetime.static(id: 0).isParent(of: .static(id: 0)))
    XCTAssertTrue(DefaultLifetime("A").isParent(of: "AA"))
    XCTAssertTrue(DefaultLifetime("ABA").isParent(of: "ABAEFGH"))
    XCTAssertTrue(DefaultLifetime("BBC").isParent(of: "BBE"))
    XCTAssertTrue(DefaultLifetime("BBC").isParent(of: "BBEA"))
    
    XCTAssertTrue(DefaultLifetime.static(id: 0).isChild(of: .static(id: 0)))
    XCTAssertTrue(DefaultLifetime("A").isChild(of: .static(id: 0)))
    XCTAssertTrue(DefaultLifetime("AAABCC").isChild(of: .static(id: 0)))
    XCTAssertTrue(DefaultLifetime("AAABCC").isChild(of: "A"))
    XCTAssertTrue(DefaultLifetime("AAABCC").isChild(of: "AAABC"))
    
    XCTAssertEqual(DefaultLifetime("A"), "A")
    
    XCTAssertFalse(DefaultLifetime("B").isParent(of: .static(id: 0)))
    XCTAssertFalse(DefaultLifetime("B").isParent(of: "A"))
    XCTAssertFalse(DefaultLifetime("A").isParent(of: "A"))
    XCTAssertFalse(DefaultLifetime("AA").isChild(of: "B"))
    XCTAssertFalse(DefaultLifetime("ABBA").isChild(of: "ABA"))
    XCTAssertFalse(DefaultLifetime("BCAA").isChild(of: "BCC"))
  }
}

extension LifetimeEnv {
  @discardableResult
  func inNewScope(do: (inout LifetimeEnv) -> Void) -> LifetimeEnv {
    var env = self.pushScope()
    `do`(&env)
    return env
  }
}
