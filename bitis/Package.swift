// swift-tools-version:5.6
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
  name: "Bitis",
  platforms: [
    .macOS(.v12),
  ],
  products: [
    // Products define the executables and libraries a package produces, and make them visible to other packages.
    .library(
      name: "Bitis",
      targets: ["Bitis"]),
    .executable(
      name: "Run",
      targets: ["Run"])
  ],
  dependencies: [
    // Dependencies declare other packages that this package depends on.
    // .package(url: /* package url */, from: "1.0.0"),
    .package(
      url: "https://github.com/tonikocjan/SwiftParsec.git",
      branch: "master"),
    .package(
      url: "https://github.com/bow-swift/bow.git",
      from: "0.8.0"),
    .package(
      url: "https://github.com/apple/swift-collections.git",
      .upToNextMajor(from: "1.0.0")),
    .package(
      url: "https://github.com/apple/swift-argument-parser",
      .upToNextMajor(from: "1.0.0")),
    .package(
      url: "https://github.com/onevcat/Rainbow.git",
      from: "4.0.1"),
  ],
  targets: [
    // Targets are the basic building blocks of a package. A target can define a module or a test suite.
    // Targets can depend on other targets in this package, and on products in packages this package depends on.
    .target(
      name: "Bitis",
      dependencies: [
        "SwiftParsec",
        .product(name: "Bow", package: "bow"),
        .product(name: "Collections", package: "swift-collections"),
        "Rainbow"
      ]),
    .executableTarget(
      name: "Run",
      dependencies: [
        "Bitis",
        .product(name: "ArgumentParser", package: "swift-argument-parser"),
        "Rainbow"
      ]),
    .testTarget(
      name: "Tests",
      dependencies: ["Bitis",],
      resources: [
      ]),
  ]
)
