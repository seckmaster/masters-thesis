# Bitis

## Building and running the project

### 1. Clone the repository: 

```bash
$ git clone https://tonikocjan@bitbucket.org/tonikocjan/masters-thesis.git
```

### 2. Make sure Swift 5 is installed:

```bash
$ swift --version
  Apple Swift version 5.1.3 (swiftlang-1100.0.282.1 clang-1100.0.33.15)
  Target: x86_64-apple-darwin19.3.0
```

or

```bash
  Swift version 5.1.5 (swift-5.1.5-RELEASE)
  Target: x86_64-unknown-linux-gnu
```

If you don't have Swift installed follow [Linux installation guidlines](https://itsfoss.com/use-swift-linux/).

### 3. Build and run / test:

```bash
$ cd bitis/
$ swift run
$ swift test
```