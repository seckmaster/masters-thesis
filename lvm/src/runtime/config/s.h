/* operating system settings "gcc-x86_64-apple-darwin19.6.0" */
#define OS_TYPE "unix"
#define OS_UNIX
#define EXE ""
#define DLL ".so"
#define POSIX_SIGNALS
#define CCALL __cdecl
#define STDCALL __stdcall
#define HAS_LABEL_VALUES
#define HAS_DLFCN_H
#define HAS_PTHREAD_H
#define HAS_STDARG_H
#define HAS_TIME_H
#define HAS_SYS_TIMES_H
#define HAS_UNISTD_H
#define HAS_FLOAT_H
#define HAS_STRERROR
#define HAS_MEMMOVE
#define HAS_VSNPRINTF
#define HAS_SNPRINTF
