/* machine architecture settings "gcc-x86_64-apple-darwin19.6.0" */
#define ARCH_COMP	gcc
#define ARCH_HOST "x86_64-apple-darwin19.6.0"
#define ARCH64
#define SIZEOF_INT 4
#define SIZEOF_LONG 8
#define SIZEOF_SHORT 2
#define ARCH_INT64 long
#define ARCH_UINT64 unsigned long
#define ARCH_INT64_SUFFIX L
#define ARCH_INT64_FORMAT "l"
#undef ARCH_BIG_ENDIAN
#undef ARCH_ALIGN_DOUBLE
#define ARCH_ALIGN_INT64
